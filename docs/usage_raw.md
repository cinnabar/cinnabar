## Usage

To use the tool, start by writing C++ code using the special classes provided
in `src/node.hpp`. Most of this work can be done using simple text replacement, but some
constructs such as if-statements need some additional work. The code below shows
a normal C++ function followed by a Cinnabar version which does the same thing.
The `Rc` class is a wrapper around `std::shared_ptr`.

```cpp
// Original function using normal C++ data types
float example(float x, float y) {
    float product = x * y;
    if (x > y) {
        return product + x;
    }
    return product - y;
}
// Cinnabar version of the same function. The template parameters
// specify the min- and max-values of the node. These are used to calculate
// the amount of integer bits required for each value in the computation
Rc<Node> example(Rc<Input<0, 3>> x, Rc<Input<2,5>> y) {
    auto product = x * y;
    return _if((x > y),
        product + x,
        product - y
    )
}
```

The `Cinnabar` node class, and its subclasses represent a computation to be performed
instead of values of the computation. To convert it into verilog, a few steps must be taken

- Call the converted function to get an expression tree
- Optimise the expression tree
- Generate the actual verilog

The code below shows how this is done:

```cpp
Rc<Node> output_node = example(Input<0, 3>::create(), Input<2, 5>::create());
output_node = output_node->optimize().value_or(ouput_node);

Codegen codegen{output_node}

// Chose how to select word length. Currently, only a fixed fractional word
// length is supported. Here we use 12 bits for the fractional part
WordLength word_length{12};
auto verilog_code = codegen.generate_module("example", word_length);
```

The tool can also generate new C++ code which emulates the resulting verilog
using the `codegen::generate_cpp_function` function.
