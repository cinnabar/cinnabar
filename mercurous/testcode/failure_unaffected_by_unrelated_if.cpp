struct Output {
    double a;
    double b;
    bool valid;
};

Output func(double x1, double x2) {
    Output result;

    declare_bounds(x1, 0, 10);
    declare_bounds(x2, 0, 10);
    double v1 = limestone_sqrt(x1);

    if(limestone_abs(v1) < 1e-9) {
        v1 = 0;
    }

    if(x1 > x2) {
        result.valid = false;
        return result;
    }

    result.valid = true;
    result.a = x1;
    result.b = x2;
    return result;
}
