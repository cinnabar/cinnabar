constexpr int nx = 7;
constexpr double x[] = {0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0};
const double y[] = {0., 1., 1.5, 1.75, 2., 3., 2.};
constexpr double xmin = x[0];
constexpr double xmax = x[nx - 1];
constexpr double dx = x[1] - x[0];

double interp1equid(double xp) {
    declare_bounds(xp, -3, 6.2);
    if( xp < xmin || xp >= xmax ) {
        return 0;
    }
    else {
        int i = limestone_trunc((xp - xmin) / dx);
        double ylow = y[i];
        double yhigh = y[i+1];
        double xstep = (xp-x[i])/dx;
        return ylow + (yhigh - ylow) * xstep;
    }
}
