double x(double a) {
    declare_bounds(a, 0, 10);

    return opaque_barrier(a*a);
}
