struct output {
    double a0;
    double a1;
    double a2;
};

output run(double a) {
    declare_bounds(a, 0, 1);
    output o;
    o.a0 = a;
    o.a1 = a;
    o.a2 = a;
    return o;
}
