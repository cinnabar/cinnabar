const int nx = 7;
const double x[] = {
    0,
    0.5,
    1.0,
    1.5,
    2.0,
    2.5,
    3.0
};
const double y[] = {
    0.,
    1.,
    1.5,
    1.75,
    2.,
    3.,
    2.
};

double interp1equid(double xmin, double dx, double xp) {
    declare_bounds(xmin, -5, 5.5);
    declare_bounds(dx, 1, 6.2);
    declare_bounds(xp, -3, 6.2);
    if( (xp < x[0]) || (xp >= x[nx-1]) ) {
        return 0;
    }
    else {
        int ii = limestone_trunc( (xp-xmin)/dx ) + 1;
        double yp = y[ii-1] + (y[ii]-y[ii-1])*(xp - x[ii-1])/dx;
        return yp;
    }
}
