double x(double a, double b) {
    declare_bounds(a, 0, 1048576);
    declare_bounds(b, 0, 1);
    return a + b;
}
