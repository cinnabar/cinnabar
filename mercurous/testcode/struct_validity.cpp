struct Result {
    bool valid;
    double a;
    double b;
};

Result function(double a, double b) {
    declare_bounds(a, 0, 100);
    declare_bounds(b, 50, 100);
    Result result;
    if (a > 0) {
        result.valid = false;
        return result;
    }

    double sum = a+b;
    if (sum < 5) {
        result.valid = false;
        return result;
    }

    result.valid = true;

    result.a = a;
    result.b = b;
    return result;
}
