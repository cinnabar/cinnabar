bool x(double a, double b) {
    declare_bounds(a, 0, 10);
    declare_bounds(b, 0, 10);
    return a < b;
}
