#include <catch2/catch.hpp>

#include <interp1d.hpp>

TEST_CASE("interpolation output is correct") {
    {
        interp1d_input input = {
            .xp = 0.5,
            .dx = 0.5,
            .xmin = 0,
        };
        CHECK(interp1d(input).__0 == 1.);
    }
    // Points between known points works
    {
        interp1d_input input = {
            .xp = 0.75,
            .dx = 0.5,
            .xmin = 0,
        };
        CHECK(interp1d(input).__0 == 1.25);
    }
}
