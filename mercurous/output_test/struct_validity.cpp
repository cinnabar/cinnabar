#include <catch2/catch.hpp>

#include <struct_validity.hpp>

TEST_CASE("interpolation output is correct") {
    {
        struct_validity_input input = {
            .a = 0.5,
            .b = 0.5,
        };
        CHECK(struct_validity(input).__0 == false);
    }
    {
        struct_validity_input input = {
            .a = -0.5,
            .b = 0.5,
        };
        CHECK(struct_validity(input).__0 == false);
    }
    {
        struct_validity_input input = {
            .a = -0.5,
            .b = 100,
        };
        auto result = struct_validity(input);
        CHECK(result.__0 == true);
        CHECK(result.__1 == -0.5);
        CHECK(result.__2 == 100);
    }
}
