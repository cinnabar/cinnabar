#include <catch2/catch.hpp>

#include <interp1d_improved.hpp>

TEST_CASE("interpolation output is correct") {
    {
        interp1d_improved_input input = {
            .xp = 0.5,
        };
        CHECK(interp1d_improved(input).__0 == 1.);
    }
    // Points between known points works
    {
        interp1d_improved_input input = {
            .xp = 0.75,
        };
        CHECK(interp1d_improved(input).__0 == 1.25);
    }
    // Out of bounds interpolation should give 0
    {
        interp1d_improved_input input = {
            .xp = 3.5,
        };
        INFO(interp1d_improved(input).__0.as_float());
        CHECK(interp1d_improved(input).__0 == 0);
    }
}
