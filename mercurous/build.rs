use cxx_build;

use std::path::PathBuf;
use std::process::Command;

fn main() {
    let project_dir = PathBuf::from(
        std::env::var("CARGO_MANIFEST_DIR").expect("CARGO_MANIFEST_DIR not set. Cargo error?"),
    );
    let mut cinnabar_src = project_dir.clone();
    cinnabar_src.push("../src");
    // Cxx does not support relative paths to the ffi header file, so we'll have
    // to copy it here
    let source_link_target = PathBuf::from("cinnabar_src");
    // Check if the target exists. Due to weirdness in the `.exists` function, we check
    // for metadata as seen in https://github.com/rust-lang/rust/issues/83186. Once that
    // feature is stabilized, we should to `.try_exists`
    let link_exists = match std::fs::symlink_metadata(&source_link_target) {
        Ok(_) => Ok(true),
        Err(error) if error.kind() == std::io::ErrorKind::NotFound => Ok(false),
        Err(other) => Err(other),
    }
    .expect("Failed to check if link exists");

    if !link_exists {
        eprintln!("creating symlink");
        println!("{} from path: {}", link_exists, source_link_target.exists());
        std::os::unix::fs::symlink(&cinnabar_src, &source_link_target).expect(&format!(
            "failed to symlink {:?} -> {:?}",
            cinnabar_src, source_link_target
        ));
    }

    // Try making the object file
    eprintln!("Running make");
    // Rebuild the ffi lib
    let mut make_handle = Command::new("make")
        .arg("-C")
        .arg("../")
        .arg("ffi_debug")
        .arg("-j16")
        .spawn()
        .expect("failed to start make");

    if !make_handle
        .wait()
        .expect("failed to wait for make")
        .success()
    {
        panic!("make error")
    }
    eprintln!("Make done");


    // Rerun cxxbridge with the new object file
    cxx_build::bridge("src/ffi.rs") // returns a cc::Build
        .file("cinnabar_src/ffi/ffi.cpp")
        .object("../build/debug/ffi.o")
        .flag_if_supported("-std=c++17")
        .flag("-openmp")
        .flag("-I../affine-arith")
        .flag("-Wno-c99-designator")
        .compiler("clang++")
        .flag("-fdiagnostics-color=always")
        .compile("cinnabar");


    println!("cargo:rerun-if-changed=../src");
    println!("cargo:rerun-if-changed=../src/ffi");
    println!("cargo:rerun-if-changed=src/main.rs");

    // "link_args=-lglpk -lstdc++fs -L../../affine-arith/build -laffine_arith_cpp -laffine_arith_rust"
    // println!("cargo:rustc-link-lib=asan");
    println!("cargo:rustc-link-lib=glpk");
    println!("cargo:rustc-link-lib=stdc++fs");
    println!("cargo:rustc-link-search=../affine-arith/build");
    println!("cargo:rustc-link-lib=affine_arith_cpp");
    println!("cargo:rustc-link-lib=affine_arith_rust");
}
