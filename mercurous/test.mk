# Makefile run after generating all the code using mercurous. This is
# required because makefiles only evaluates things like wildcards once

CXX=clang++
-include overrides.mk

CXXFLAGS := \
	-Wall \
	-Wextra \
	-Wno-c99-designator \
	-g \
	-std=c++17 ${OPT} \
	-MMD \
	-Werror=return-type \
	-fPIC
INCLUDES=-I../yafp/src/ \
		 -I../Catch2/single_include \
		 -I../src/ \
		 -I../affine-arith/

# 

ALL_CPP=$(wildcard generated/**/*.cpp)
ALL_DIRS=$(dir ${ALL_CPP})
# All object files built directly from cpp files
CPP_OBJS=$(patsubst %.cpp, build/%.o, ${ALL_CPP})

# TEST_TARGETS=$(addsuffix test.out, ${ALL_DIRS})
TEST_CPPS=$(wildcard output_test/*.cpp)
TEST_TARGETS=$(patsubst output_test/%.cpp, build/generated/%/test.out.run, ${TEST_CPPS})

TEST_SRCS = $(wildcard output_test/*.cpp) output_test/main/main.cpp
TEST_OBJS = ${OBJS} $(patsubst %.cpp, ${BUILD_DIR}/%.o, $(TEST_SRCS))

all: ${TEST_TARGETS}

%/merged.o: ${CPP_OBJS} .SECONDARY
	@echo -e "[\033[0;34mld\033[0m] merging $@"
	@ld -relocatable $$(ls ${@D}/*.o | grep -v $@) -o $@

build/generated/%/test.out.run: build/generated/%/test.out .PHONY .SECONDARY
	@echo -e "[\033[0;34mtest\033[0m] running $@"
	@./$<

build/generated/%/test.out: build/generated/%/merged.o output_test/%.cpp \
		build/output_test/main/main.o
	@echo -e "[\033[0;34m$(CXX)\033[0m] building test exe for $@"
	@$(CXX) \
		${CXXFLAGS} \
		${INCLUDES} \
		-I$(patsubst build/generated/%, generated/%, $(<D)) \
		$^ \
		-o $@

build/%.o: %.cpp .SECONDARY
	@mkdir -p ${@D}
	@echo -e "[\033[0;34m$(CXX)\033[0m] Building $@"
	@$(CXX) ${CXXFLAGS} ${INCLUDES} -I${<D} $< -c -o $@

.PHONY:
.SECONDARY:
.SUFFIXES:
