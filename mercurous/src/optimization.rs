use cxx::UniquePtr;
use structopt::clap::arg_enum;

use crate::ffi::{self, OptPass as FfiOptPass};

macro_rules! define_passes {
    ( $( ($rust:ident, $ffi:ident) ),* $(,)? ) => {
        // We don't run all opt passes at the moment, prevent warnings about unused variants
        arg_enum! {
            #[derive(Debug)]
            #[allow(dead_code)]
            pub enum OptPass {
                $( $rust ),*
            }
        }

        impl OptPass {
            pub fn ffi_pass(&self) -> UniquePtr<FfiOptPass> {
                match self {
                    $(OptPass::$rust => ffi::$ffi()),*
                }
            }
        }
    }
}

define_passes! {
    (ConstantFold, opt_pass_constant_fold),
    (RemoveRedundantComparisons, opt_pass_remove_redundant_comparisons),
    (RemoveRedundantInterpValidity, opt_pass_remove_redundant_interp_validity),
    (RemoveRedundantBoolComputations, opt_pass_remove_redundant_bool_computations),
    (ConstantDivisionReplacement, opt_pass_constant_division_replacement),
    (MergeBools, opt_pass_merge_bools),
    (SelectElseLast, opt_pass_select_else_last),
    (SelectElseX, opt_pass_select_else_x),
    (RemoveSingleSelect, opt_pass_remove_single_select),
}
