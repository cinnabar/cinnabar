use cxx::UniquePtr;
use limestone::operand::{Identifier, OperandMap};

use crate::ffi;

pub fn pipeline_depth(
    outputs: &[Identifier],
    cinnabar_nodes: &OperandMap<UniquePtr<ffi::RcNode>>,
    word_length: &ffi::WordLength
) -> i32 {
    if outputs.is_empty() {
        panic!("Attempted to get pipeline depth with no outputs");
    }

    // NOTE: +1 here because pipeline_end_depth is the node at which the computation is ready
    // is ready, not the clock cycle. See issue 256
    1 + outputs
        .iter()
        .map(|o| {
            ffi::pipeline_end_depth(cinnabar_nodes.get(&o.clone().into()).unwrap(), &word_length)
        })
        .max()
        .unwrap() // NOTE: Safe unwrap, we have at least one output
}
