use std::path::{Path, PathBuf};

use anyhow::{bail, Context};
use cxx::UniquePtr;
use limestone::{graph::Graph, graphgen::ReturnValue, operand::OperandMap};
use mercverf::config::{Config, CppTask, Input, InputMethod, IoValues, Output, VerilogTask};
use rand::{distributions::Uniform, prelude::*, thread_rng};
use serde::Deserialize;

use crate::{ffi, util};

#[derive(Deserialize)]
struct PartialConfig {
    /// A list of the names of the inputs to the module, ordered as they should be
    /// ordered when values are passed by position
    input_order: Vec<String>,
    outputs: Vec<Output>,
    reference_task: CppTask,
}

impl PartialConfig {
    pub fn example_config() -> Self {
        Self {
            input_order: vec![],
            reference_task: CppTask {
                function: "".to_string(),
                main_file: PathBuf::from(""),
                extra_files: vec![],
                input_method: InputMethod::Positional,
                include_paths: vec![],
                compiler_flags: vec![],
            },
            outputs: vec![],
        }
    }

    // This is needed because of weirdness in the toml crate when tables
    // in tables are present
    pub fn to_toml(&self) -> Result<toml::Value, toml::ser::Error> {
        let Self {
            input_order,
            outputs,
            reference_task,
        } = self;
        let input_order = toml::Value::try_from(input_order)?;
        let outputs = toml::Value::try_from(outputs)?;
        let reference_task = toml::Value::try_from(reference_task)?;

        let mut map = toml::map::Map::new();
        map.insert("input_order".to_string(), input_order);
        map.insert("outputs".to_string(), outputs);
        map.insert("reference_task".to_string(), reference_task);

        Ok(toml::Value::Table(map))
    }
}

pub struct MercverfArgs<'a> {
    pub graph: Graph,
    pub outdir: &'a Path,
    pub input_filename: &'a Path,
    pub output_filename: &'a str,
    pub cinnabar_root: &'a Path,
    pub sample_count: usize,
    pub return_value: &'a ReturnValue,
    pub cinnabar_nodes: &'a OperandMap<UniquePtr<ffi::RcNode>>,
    pub word_length: &'a ffi::WordLength,
    pub lut_cpp_files: &'a [String],
    pub lut_verilog_files: &'a [String],
    pub compiler_flags: &'a [String],
    // If specified: Read input options from files in the specified directory.
    // Each input speicifed in the partial config input order list must have
    // a corresponding file with possible values for that input. From that,
    // `sample_count` input combinations are generated. If unspecified, inputs
    // are picked at random from the bounds on the inputs
    pub read_inputs: Option<PathBuf>,
}

pub fn generate_config(args: &MercverfArgs) -> anyhow::Result<Config> {
    let MercverfArgs {
        outdir: _,
        input_filename,
        output_filename,
        cinnabar_root,
        sample_count,
        return_value,
        graph,
        cinnabar_nodes,
        word_length,
        lut_cpp_files,
        lut_verilog_files,
        compiler_flags,
        read_inputs,
    } = args;

    let outputs = match return_value {
        ReturnValue::Single(value) => vec![value.clone()],
        ReturnValue::Struct {
            struct_node: _,
            values,
        } => values.clone(),
    };

    // We expect a file called `mercverf.partial.toml` to be alongside the cpp file
    // to define the run configuration for the reference
    let partial_config_file = input_filename
        .parent()
        .context("failed to get the parent of the input cpp file")?
        .join("mercverf.partial.toml");

    if !partial_config_file.exists() {
        println!(
            "Example config\n{}",
            toml::to_string_pretty(&PartialConfig::example_config().to_toml().unwrap()).unwrap()
        );
        bail!("Could not find '{}'", partial_config_file.to_string_lossy());
    }
    let partial_config_str = std::fs::read_to_string(&partial_config_file)
        .with_context(|| format!("Failed to read {}", partial_config_file.to_string_lossy()))?;
    let mut partial_config = toml::from_str::<PartialConfig>(&partial_config_str)
        .with_context(|| format!("Failed to parse {}", partial_config_file.to_string_lossy()))?;

    // The reference task c++ filename will be wrong since it needs to be accessible from
    // where we put the new config.
    partial_config.reference_task.main_file = std::env::current_dir()?.join(input_filename);
    partial_config.reference_task.compiler_flags = compiler_flags.iter().cloned().collect();

    let working_dir = std::env::current_dir()?;
    let cinnabar_path = working_dir.join(cinnabar_root);
    let yafp_path = cinnabar_path.join("yafp/src");

    let lut_cpps = lut_cpp_files
        .iter()
        .map(|file| working_dir.join(file))
        .collect();
    let lut_verilogs = lut_verilog_files
        .iter()
        .map(|file| working_dir.join(file))
        .collect();

    let fixedpoint_task = CppTask {
        function: output_filename.to_string(),
        main_file: PathBuf::from(format!("{}/{}.cpp", output_filename, output_filename)),
        extra_files: lut_cpps,
        input_method: InputMethod::ByStruct(format!("{}_input", output_filename)),
        include_paths: vec![
            yafp_path.to_string_lossy().to_string(),
            cinnabar_path.join("src").to_string_lossy().to_string(),
        ],
        compiler_flags: compiler_flags.iter().cloned().collect(),
    };

    let pipeline_depth = util::pipeline_depth(&outputs, cinnabar_nodes, word_length);

    let internal_values = ffi::unique_ancestors(
        cinnabar_nodes
            .get(&return_value.get_root_identifier().into())
            .unwrap(),
    )
    .iter()
    .map(|node| ffi::get_weak_name(node).unwrap().to_string())
    .collect();

    let verilog_task = VerilogTask {
        module: format!("{}", output_filename),
        main_file: PathBuf::from(format!("{}/{}.v", output_filename, output_filename)),
        extra_files: lut_verilogs,
        pipeline_depth: pipeline_depth as u32,
        internal_values,
    };

    let mut inputs = graph
        .nodes
        .iter()
        .filter_map(|(name, node)| match node.kind {
            limestone::graph_node::Kind::Input(min, max) => Some((name, min, max)),
            _ => None,
        })
        .map(|(merc_name, min, max)| {
            let cinnabar_node = cinnabar_nodes.get(&merc_name.into()).unwrap();
            let name = ffi::get_node_name(cinnabar_node);

            let values = if let Some(input_dir) = read_inputs {
                let filename = input_dir.join(&name.to_string());

                let lines = std::fs::read_to_string(&filename)
                    .with_context(|| format!("Failed to read {:?}", filename))?
                    .lines()
                    .map(|s| {
                        s
                            .trim()
                            .parse::<f64>()
                            .with_context(|| format!("failed to parse {} as float", s))
                    })
                    .collect::<anyhow::Result<Vec<_>>>()?;

                let distribution = Uniform::new(0, lines.len());

                distribution
                    .sample_iter(&mut thread_rng())
                    .take(*sample_count)
                    .map(|idx| lines[idx])
                    .collect()
            } else {
                let distribution = Uniform::new_inclusive(min, max);
                distribution
                    .sample_iter(&mut thread_rng())
                    .take(*sample_count)
                    .collect::<Vec<_>>()
            };

            let name = name.to_string();

            let input_position = partial_config.input_order.iter().position(|i| i == &name);

            if input_position.is_none() {
                bail!("Input position for {} not found in input_order", name);
            }

            Ok((
                input_position.unwrap(),
                Input {
                    name,
                    values,
                    int_bits: ffi::integer_bits(&cinnabar_node, &word_length),
                    frac_bits: ffi::fractional_bits(&cinnabar_node, &word_length),
                },
            ))
        })
        .collect::<Result<Vec<_>, _>>()?;

    inputs.sort_by_key(|(pos, _)| *pos);
    let inputs = inputs.into_iter().map(|(_, input)| input).collect();

    let io = IoValues {
        inputs,
        outputs: partial_config.outputs,
    };

    Ok(mercverf::config::Config {
        reference: partial_config.reference_task,
        fixedpoint: fixedpoint_task,
        verilog: verilog_task,
        io,
    })
}

pub fn generate_mercverf<'a>(args: &MercverfArgs<'a>) -> anyhow::Result<()> {
    let config = generate_config(args).context("Failed to generate mercverf config")?;

    let config_path = args.outdir.join("mercverf.json");

    std::fs::write(
        config_path,
        serde_json::to_vec(&config).context("Failed to encode config as json")?,
    )
    .context("Failed to write mercverf config")?;

    Ok(())
}
