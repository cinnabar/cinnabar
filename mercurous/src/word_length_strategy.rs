use cxx::UniquePtr;
use logos::Logos;

use anyhow::{anyhow, Result};

use crate::ffi;

#[derive(Logos, Debug, PartialEq)]
enum Token {
    #[token("_")]
    Underscore,
    #[regex("-?[0-9]+", |lex| lex.slice().parse())]
    Integer(i32),
    #[regex("[a-zA-Z]+", |lex| lex.slice().to_string())]
    Text(String),
    #[regex(r"[ \t\n\f]+", logos::skip)]
    #[error]
    Error,
}


pub fn parse_wls(input: &str) -> Result<UniquePtr<ffi::WordLength>> {
    let mut lex = Token::lexer(input);

    macro_rules! expect_tok {
        ($expected:pat) => {
            match lex.next() {
                Some(a @ $expected) => a,
                Some(_) => return Err(anyhow!("Expected {}", stringify!($expected))),
                None => return Err(anyhow!("Unexpected end of string"))
            }
        }
    }
    let start = expect_tok!(Token::Text(_));
    expect_tok!(Token::Underscore);

    let mut args = vec![];
    loop {
        match lex.next() {
            Some(Token::Integer(val)) => {
                args.push(val);
                match lex.next() {
                    Some(Token::Underscore) => continue,
                    None => break,
                    _ => return Err(anyhow!("Expected , or )"))
                }
            },
            None => break,
            Some(_) => return Err(anyhow!("Expected ) or integer"))
        }
    };

    match start {
        Token::Text(val) if val == "FixedFixpoint" => {
            if args.len() != 1 {
                Err(anyhow!("expected 1 value for FixedFixpoint"))
            }
            else {
                Ok(ffi::fixed_fractional_word_length(args[0]))
            }
        }
        Token::Text(val) if val == "FixedTotalLength" => {
            if args.len() != 1 {
                Err(anyhow!("expected 1 value for FixedTotalLength"))
            }
            else {
                Ok(ffi::fixed_total_word_length(args[0]))
            }
        }
        Token::Text(val) if val == "FullySpecified" => {
            if args.len() != 2 {
                Err(anyhow!("expected 2 values for FullySpecified"))
            }
            else {
                Ok(ffi::fully_specified_word_length(args[0], args[1]))
            }
        }
        Token::Text(other) => Err(anyhow!("Unexpected word length strategy {other}")),
        _ => unreachable!()
    }
}



