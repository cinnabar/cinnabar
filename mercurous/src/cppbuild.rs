use std::ffi::OsString;
use std::path::{Path, PathBuf};
use std::process::{Command, ExitStatus};

use log::trace;
use thiserror::Error;

use which::which;

use crate::opt_level::OptLevel;

#[derive(Error, Debug)]
pub enum Error {
    /// Compilation failed with status code if present, otherwise was terminated
    /// by signal
    #[error("Compilation failed with status code {0:?}")]
    CompilationError(Option<i32>),
    // TODO: Make documented
    #[error("Undocumented IO error")]
    IoError(std::io::Error),
    /// The specified path has no file component, only dirs
    #[error("Expected {0} to be a file, not a directory")]
    NotAFile(PathBuf),
    /// Attempted to convert the specified string, found in a filename
    /// to unicode.
    #[error("Filename {0:?} could not be converted to unicode")]
    FilenameUnicodeError(OsString),
    /// Error while creating a directory
    #[error("Failed to create {0} with error {1}")]
    DirCreationError(PathBuf, std::io::Error),
    /// Error occured while running clang
    #[error("Error while calling clang {0}")]
    ClangError(Box<Error>),
    /// Error occurred while running opt
    #[error("Error while calling `opt`: {0}")]
    OptError(Box<Error>),
    /// Error occurred while running llvm-as
    #[error("Error while calling `llvm-as`: {0}")]
    AsemblerError(Box<Error>),
    #[error("None of the following commands were found: {0:?}")]
    NoSuchCommands(Vec<String>),
    #[error("Clang not found at {0}")]
    ClangNotFound(String),
    #[error("Error looking for command: {0}")]
    WhichError(which::Error),
}
pub type Result<T> = std::result::Result<T, Error>;

/// Creates a `Command` using the first of the specified commands that exists. If none are found,
/// an error is returned.
fn checked_commands(commands: &[&str]) -> Result<Command> {
    for c in commands {
        match which(c) {
            Ok(_) => return Ok(Command::new(c)),
            Err(which::Error::CannotFindBinaryPath) => {}
            Err(e) => return Err(Error::WhichError(e)),
        }
    }
    Err(Error::NoSuchCommands(
        commands.iter().map(|x| x.to_string()).collect(),
    ))
}

/// Run clang from a compile-time set custom location specified in a file called .llvm_dir
fn custom_llvm_command(command: &str) -> Result<Command> {
    let dir = include_str!("../.llvm_dir").trim();
    let path = format!("{}/{}", dir, command);

    if PathBuf::from(&path).exists() {
        Ok(Command::new(path))
    } else {
        Err(Error::ClangNotFound(path))
    }
}

/// Gets the output in the specified dir file with the specified extension corresponding to the
/// input file.
/// This is a unique mapping to avoid accidental overwrites
fn get_corresponding_file(infile: &Path, outdir: &Path, extension: &str) -> PathBuf {
    let with_extension = infile.with_extension(extension);
    let outfile = outdir.join(with_extension);
    outfile
}

/// Takes a file on the form <stem>(.<extension>)? and modifies it to be
/// <stem><postfix>(.<extension>)?
fn add_filename_postfix(infile: &Path, postfix: &str) -> Result<PathBuf> {
    modify_file_stem(infile, |stem| format!("{}{}", stem, postfix).into())
}

/// Replaces the file stem part with the result of applying the specified function
/// to it
fn modify_file_stem(input: &Path, f: impl Fn(&str) -> String) -> Result<PathBuf> {
    let path = input.parent().unwrap_or(Path::new("_"));

    let file_stem_osstr = input
        .file_stem()
        .ok_or_else(|| Error::NotAFile(input.into()))?;
    let file_stem = file_stem_osstr
        .to_str()
        .ok_or_else(|| Error::NotAFile(file_stem_osstr.into()))?;
    // A file extension can: be present, not be present, be non-unicode. The first
    // two are fine, the last case should cause an error
    let file_extension = input
        .extension()
        .map(|s| {
            s.to_str()
                .ok_or_else(|| Error::FilenameUnicodeError(s.into()))
                .map(|s| format!(".{}", s))
        })
        .unwrap_or(Ok("".to_string()))?;

    let filename: PathBuf = format!("{}{}", f(file_stem), file_extension).into();
    Ok(path.join(filename))
}

pub fn handle_status(status: std::io::Result<ExitStatus>) -> Result<()> {
    let status = status.map_err(Error::IoError)?;
    if status.success() {
        Ok(())
    } else {
        Err(Error::CompilationError(status.code()))
    }
}

/// Calls clang to build a cpp file to an ll file with no optimisation. The output filename is
/// returned
pub fn build_cpp_ll(
    infile: &Path,
    limestone_header: &Path,
    outdir: &Path,
    extra_args: &[&str],
) -> Result<PathBuf> {
    let outfile = add_filename_postfix(
        &get_corresponding_file(infile, outdir, "unopt.ll"),
        ".unopt",
    )?;
    let mut full_outdir = outfile.to_path_buf();
    full_outdir.pop();
    println!("{:?}", full_outdir);
    std::fs::create_dir_all(full_outdir)
        .map_err(|e| Error::DirCreationError(outdir.to_path_buf(), e))?;

    let status = custom_llvm_command("clang")?
        // These arguments don't take &str, and therefore are not compatible
        // with the .args list, so we'll pass them separately
        .arg(infile)
        .arg("-o")
        .arg(&outfile)
        .args(&[
            "-Wall",
            "-emit-llvm",
            "-S",
            "-c",
            "-g",
            "-O1",
            "-std=c++20",
            "-Xclang",
            "-disable-llvm-passes",
            "-fno-discard-value-names",
            "-D__IS_CINNABAR__",
        ])
        .args(extra_args)
        .arg("-include")
        .arg(limestone_header)
        .status();

    handle_status(status)?;
    Ok(outfile)
}

pub fn build_rust_ll(
    infile: &Path,
    _limestone_header: &Path,
    outdir: &Path,
    extra_args: &[&str],
) -> Result<PathBuf> {
    let outfile = add_filename_postfix(
        &get_corresponding_file(infile, outdir, "unopt.ll"),
        ".unopt",
    )?;
    let mut full_outdir = outfile.to_path_buf();
    full_outdir.pop();
    println!("{:?}", full_outdir);
    std::fs::create_dir_all(full_outdir)
        .map_err(|e| Error::DirCreationError(outdir.to_path_buf(), e))?;

    let status = checked_commands(&["rustc"])?
        // These arguments don't take &str, and therefore are not compatible
        // with the .args list, so we'll pass them separately
        .arg(infile)
        .arg("-o")
        .arg(&outfile)
        .args(&[
            "--emit",
            "llvm-ir",
            "-g",
            "-O",
            "--crate-type",
            "lib"
            // "-Xclang",
            // "-disable-llvm-passes",
            //"-fno-discard-value-names",
        ])
        .args(extra_args)
        .status();

    handle_status(status)?;
    Ok(outfile)
}

/// Calls the llvm opt tool to run essential passes required for limestone to
/// not freak out. Both input and output files are
pub fn do_essential_opts(infile: &Path, more_flags: &[&str]) -> Result<PathBuf> {
    // If the file has a .unopt postfix, remove it.
    let outfile = add_filename_postfix(
        &modify_file_stem(infile, |s| s.replace(".unopt", ""))?,
        ".opt",
    )?;

    let status = custom_llvm_command("opt")?
        .arg(infile)
        .arg("-o")
        .arg(&outfile)
        // Output new llvm asm instead of bitcode
        .arg("-S")
        .args(&[
            "--mem2reg",
            "-sccp", // Constant propagation
            // "-dce",  // Dead code elimination
            "-loop-unroll",
            "--lowerswitch",
        ])
        .args(more_flags)
        .status();

    handle_status(status)?;
    Ok(outfile)
}

/// Calls the LLVM opt tool using the specified pre-defined optimisation level
pub fn do_opt_level(infile: &Path, level: OptLevel, extra_flags: Vec<&str>) -> Result<PathBuf> {
    let pass_str: String = level.into();
    let outfile = add_filename_postfix(infile, &format!(".{}", pass_str))?;

    let status = custom_llvm_command("opt")?
        .arg(infile)
        .arg("-o")
        .arg(&outfile)
        .arg("-S")
        .args(extra_flags)
        .arg(format!("-{}", pass_str))
        .status();

    handle_status(status)?;
    Ok(outfile)
}

pub fn compile_bitcode(infile: &Path) -> Result<PathBuf> {
    let outfile = infile.with_extension(".bc");

    let status = custom_llvm_command("llvm-as")?
        .arg(infile)
        .arg("-o")
        .arg(&outfile)
        .status();
    handle_status(status)?;
    Ok(outfile)
}

/// Performs all steps to build a cpp file into a .bc file with essential optimisations applied
/// followed by running opt with the specified opt level if specified
pub fn build_source_file(
    infile: &Path,
    limestone_header: &Path,
    essential_opt_extra_flags: &[&str],
    opt_level: Option<OptLevel>,
    compiler_extra_flags: Vec<&str>,
    opt_extra_flags: Vec<&str>,
    outdir: &Path,
) -> Result<PathBuf> {
    let builder = if infile.extension() == Some(&OsString::from("rs")) {
        build_rust_ll
    } else {
        build_cpp_ll
    };

    trace!("Building {:?}", infile);
    let result = builder(infile, limestone_header, outdir, &compiler_extra_flags)
        .map_err(|e| Error::ClangError(Box::new(e)))
        .and_then(|result_file| {
            trace!("Performing essential opts on {:?}", result_file);
            do_essential_opts(&result_file, essential_opt_extra_flags)
                .map_err(|e| Error::OptError(Box::new(e)))
        })
        .and_then(|result_file| {
            if let Some(level) = opt_level {
                trace!("Performing {:?} opts on {:?}", level, result_file);
                do_opt_level(&result_file, level, opt_extra_flags)
                    .map_err(|e| Error::OptError(Box::new(e)))
            } else {
                Ok(result_file)
            }
        })
        .and_then(|result_file| {
            trace!("Building bitcode for {:?}", result_file);
            compile_bitcode(&result_file).map_err(|e| Error::AsemblerError(Box::new(e)))
        });

    result
}

#[cfg(test)]
mod cppbuild_tests {
    use super::*;

    use assert_matches::assert_matches;

    #[test]
    fn get_corresponding_file_works() {
        let input = PathBuf::from("src/test.cpp");
        let expected = PathBuf::from("build/src/test.ll");

        let result = get_corresponding_file(&input, &PathBuf::from("build"), "ll");

        assert_eq!(result, expected);
    }

    #[test]
    fn add_filename_postfix_works() {
        {
            let result = add_filename_postfix(&PathBuf::from("build/test.ll"), ".unopt");
            let expected = PathBuf::from("build/test.unopt.ll");
            assert_matches!(result, Ok(_));
            assert_eq!(result.unwrap(), expected);
        }
        {
            let result = add_filename_postfix(&PathBuf::from("test.ll"), ".unopt");
            let expected = PathBuf::from("test.unopt.ll");
            assert_matches!(result, Ok(_));
            assert_eq!(result.unwrap(), expected);
        }
        {
            let result = add_filename_postfix(&PathBuf::from("a/b/test.ll"), ".unopt");
            let expected = PathBuf::from("a/b/test.unopt.ll");
            assert_matches!(result, Ok(_));
            assert_eq!(result.unwrap(), expected);
        }
    }
}
