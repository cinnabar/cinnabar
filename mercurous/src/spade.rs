use std::path::Path;

use anyhow::Context;
use cxx::UniquePtr;
use limestone::{
    graph::Graph,
    graphgen::ReturnValue,
    operand::{Operand, OperandMap},
};

use crate::{ffi::ffi, util};

pub fn build_spade_stub(
    outdir: &Path,
    module_name: &str,
    graph: &Graph,
    return_value: &ReturnValue,
    nodes: &OperandMap<UniquePtr<ffi::RcNode>>,
    word_length: &ffi::WordLength,
) -> anyhow::Result<()> {
    let outputs = match return_value {
        ReturnValue::Single(value) => vec![value.clone()],
        ReturnValue::Struct {
            struct_node: _,
            values,
        } => values.clone(),
    };

    let inputs = graph
        .nodes
        .iter()
        .filter_map(|(name, node)| match node.kind {
            limestone::graph_node::Kind::Input(_, _) => {
                let size = ffi::register_size(
                    &nodes.get(&Operand::Ident(name.clone())).unwrap(),
                    word_length,
                );
                let name = match name {
                    limestone::operand::Identifier::Str(n) => n,
                    limestone::operand::Identifier::Id(_) => {
                        panic!("anonymous names are unsupported as spade stub inputs")
                    }
                };
                Some((name, size))
            }
            _ => None,
        })
        .collect::<Vec<_>>();

    let spade_inputs = inputs
        .iter()
        .map(|(name, size)| format!("{}: int<{}>", name, size))
        .collect::<Vec<_>>()
        .join(", ");

    let pipeline_depth = util::pipeline_depth(&outputs, nodes, word_length);

    let output_sizes = outputs
        .iter()
        .map(|ident| {
            ffi::register_size(
                &nodes.get(&Operand::Ident(ident.clone())).unwrap(),
                word_length,
            )
        })
        .collect::<Vec<_>>();

    let output_type = if output_sizes.len() == 1 {
        format!("int<{}>", output_sizes[0])
    } else {
        format!(
            "({})",
            output_sizes
                .iter()
                .map(|s| format!("int<{}>", s))
                .collect::<Vec<_>>()
                .join(",")
        )
    };

    let spade_code = format!(
        "pipeline({}) __spade__{}(clk: clk, {}) -> {} __builtin__",
        pipeline_depth, module_name, spade_inputs, output_type
    );

    std::fs::write(outdir.join(&format!("{}.spade", module_name)), spade_code)
        .context("when writing spade stub")?;

    // We also need a shim module to act as glue between the cinnabar verilog module and spade
    // calling convention
    let (verilog_inputs, verilog_input_maps): (Vec<_>, Vec<_>) = inputs
        .iter()
        .map(|(name, size)| {
            (
                format!("input[{}:0] _i_{}", size - 1, name),
                format!(".{}_non_reg(_i_{})", name, name),
            )
        })
        .unzip();

    let verilog_stub_output = format!(
        "output[{}:0] __output",
        output_sizes.iter().sum::<usize>() - 1
    );

    if output_sizes.len() != 1 {
        panic!("Spade generation of module with more than one output is unsupported")
    }
    let verilog_body = format!(
        "{} _inst(.clk(_i_clk), .rst(0), {}, .__0(__output))",
        module_name,
        verilog_input_maps.join(", "),
    );

    let verilog_code = format!(
        r#"module __spade__{}(input _i_clk, {}, {});
            {};
        endmodule"#,
        module_name,
        verilog_inputs.join(", "),
        verilog_stub_output,
        verilog_body
    );

    std::fs::write(
        outdir.join(&format!("_spade_{}.v", module_name)),
        verilog_code,
    )
    .context("when writing spade glue")?;

    Ok(())
}
