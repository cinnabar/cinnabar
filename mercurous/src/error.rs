use cxx::Exception;
use limestone::graph_node::Kind;
use limestone::operand::{Identifier, Operand};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Exception in C++ code")]
    Exception(#[from] Exception),
    #[error("Error converting node {0:?} with kind {1:?}")]
    ConversionError(Identifier, Kind, #[source] Box<Error>),
    #[error("Limestone error")]
    LimestoneError(#[from] limestone::error::Error),
    #[error("Attempted to get node for operand {0:?}")]
    NodeNotYetConverted(Operand),
}

pub type Result<T> = std::result::Result<T, Error>;
