use std::collections::HashSet;
use std::fmt::Write as FmtWrite;
use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use std::{collections::HashMap, ffi::OsStr};

use anyhow::{anyhow, Context};
use limestone::debuginfo::DebugInfo;
use limestone::graphgen::PhiConversionOptions;
use limestone::graphgen::PhiMaskOptions;
use log::trace;
use simplelog::{ConfigBuilder, LevelFilter, TermLogger, TerminalMode};

use structopt::StructOpt;

use limestone::{
    debuginfo::{IdentDebugInfo, SpecificInfo},
    graphgen::{PhiStrategy, ReturnValue},
    graphviz, load_function_from_module, lower_function, Function,
};
use structopt::clap::arg_enum;

use crate::verification::MercverfArgs;

mod conversion;
mod cppbuild;
mod error;
pub mod ffi;
mod opt_level;
mod optimization;
mod spade;
mod util;
mod verification;
mod word_length_strategy;

arg_enum! {
    #[derive(Debug)]
    enum CppDataType {
        Fp,
        Ac,
        Ap
    }
}

#[derive(Debug, StructOpt)]
#[structopt(name = "cinnabar", about = "LLVM frontend for cinnabar")]
struct Opt {
    /// Input filename
    input: PathBuf,
    #[structopt(short = "o", default_value = "build")]
    /// Output directory
    outdir: PathBuf,
    #[structopt(short = "c")]
    /// The name of the output verilog, cpp and hpp files. (Without extension)
    ///
    /// If this is unspecified, no output will be produced
    output_filename: Option<String>,

    #[structopt(long)]
    /// Output a stub spade signature for the pipeline
    spade_stub: bool,

    /// The root directory of the cinnabar installation
    #[structopt(long = "cinnabar-root")]
    cinnabar_root: PathBuf,

    #[structopt(short, long)]
    word_length_strategy: String,

    /// Data type to use in the generated c++ code
    #[structopt(long, default_value = "fp")]
    cpp_data_type: CppDataType,

    /// If this option is set, the output module will only have a single input. The actual inputs
    /// in the module get set by this value one at a time. This is usually undesired but can be
    /// used if benchmarking a module with more inputs than what fits on the output pins without
    /// having to think about passing inputs to the module externally
    #[structopt(long = "compress-inputs")]
    compress_inputs: bool,

    #[structopt(long = "limestone-graph")]
    /// Path to put a graphviz visualisation of the limestone graph. (Relative to outdir)
    limestone_graph: Option<String>,
    #[structopt(long = "unlowered-graph")]
    /// Path to put a graphviz visualisation of the limestone graph before lowering. (Relative to outdir)
    unlowered_graph: Option<String>,
    #[structopt(long = "cinnabar-graph")]
    /// Path to put a graphviz visualisation of the cinnabar graph (Relative to outdir)
    cinnabar_graph: Option<String>,
    #[structopt(long = "merged-cinnabar-graph")]
    /// Path to put a graphviz visualisation of the cinnabar graph with merged bool nodes(Relative to outdir)
    merged_cinnabar_graph: Option<String>,

    #[structopt(long = "block-graph")]
    /// Dump the llvm basic block graph to the specified filename (Relative to outdir)
    block_graph: Option<String>,

    #[structopt(long = "ident-map")]
    /// Path to output a mapping between generated identifiers and original code (relative to
    /// outdir)
    ident_map: Option<String>,

    #[structopt(
        long = "limestone-header",
        short = "h",
        default_value = "limestone/include/liblimestone.hpp"
    )]
    limestone_header: PathBuf,

    /// Arguments passed directly to the frontend compiler. Ignored if the input file is a LL file
    #[structopt(short = "C", long = "clang-arg")]
    compiler_args: Vec<String>,

    /// Set this option to use masked phi nodes instead of using find_significant_paths
    #[structopt(long = "phi-masks")]
    phi_masks: bool,
    /// Set this option to use naive path generation rather than trying to filter out
    /// paths which have no effect. Has no effect if phi-masks is set
    #[structopt(long = "naive-paths")]
    naive_paths: bool,
    /// Set this option to cache the nodes created by convert_phi_node.
    /// Has no effect if phi-masks is set
    #[structopt(long = "no-sig-path-cache")]
    no_phi_cache: bool,
    /// If a phi node has branches containing `undef` values, those are ignored with this option.
    /// The behaviour of incomplete conditions in phi nodes is controlled by the SelectElse...
    /// optimisation passes
    #[structopt(long = "filter-undef")]
    filter_undef: bool,

    /// Do not perform retiming on the cinnabar graph
    #[structopt(long = "no-retiming")]
    no_retiming: bool,

    /// Recompute not values on demand instead of pipelining them
    #[structopt(long)]
    recompute_not: bool,

    /// Perform the specified opt pass
    #[structopt(short = "p")]
    opt_passes: Vec<crate::optimization::OptPass>,

    /// Optimisation level to use.
    #[structopt(long = "opt")]
    opt_level: Option<opt_level::OptLevel>,
    /// Enable LLVM opts that allow opts that do not give an exact floating point behaviour
    #[structopt(long = "unsafe-fp")]
    unsafe_fp: bool,
    /// Disable slp vectorisation in llvm
    #[structopt(long = "no-slp-vectorization")]
    no_slp_vectorization: bool,
    /// Explicitly run dead code elimination
    #[structopt(long = "dce")]
    dce: bool,
    /// Run dead code elimination in the initial opt pass
    #[structopt(long = "edce")]
    early_dce: bool,

    /// Generate a mercverf config with this amount of randomly selected samples.
    /// Input selection is controlled by the verification_inputs argument
    /// For use with the `mercverf` tool.
    #[structopt(long = "verify-samples")]
    verify_samples: Option<usize>,
    /// If set, reads allowed values for each input from files in the specified directory. Each
    /// input must have a corresponding file with the same filename containing one float value per
    /// line. If unspecified, random samples are chosen from within the input range
    #[structopt(long = "verification-inputs")]
    verification_inputs: Option<PathBuf>,
}

/// Builds bitcode into the outdir from the supplied opts. Returns the path to the resulting
/// bitcode. If the file is already in bitcode format, returns that path directly
fn build_bitcode(opts: &Opt, outdir: &Path) -> anyhow::Result<PathBuf> {
    let mut essential_opt_extra_flags = vec![];
    if opts.early_dce {
        essential_opt_extra_flags.push("-dce")
    }

    let mut extra_opts = vec![];
    if opts.unsafe_fp {
        extra_opts.append(&mut vec![
            "--enable-no-infs-fp-math",
            "--enable-no-nans-fp-math",
            "--enable-no-signed-zeros-fp-math",
            "--enable-unsafe-fp-math",
        ])
    };
    if opts.no_slp_vectorization {
        extra_opts.push("-vectorize-slp=false");
    }
    if opts.dce {
        extra_opts.push("-dce")
    }

    let extensions = vec!["cpp", "cc", "rs"]
        .into_iter()
        .map(OsStr::new)
        .map(Some)
        .collect::<Vec<_>>();
    let ext = opts.input.extension();

    let input_bc = if extensions.contains(&ext) {
        cppbuild::build_source_file(
            &opts.input,
            &opts.limestone_header,
            &essential_opt_extra_flags,
            opts.opt_level,
            opts.compiler_args.iter().map(|s| s.as_str()).collect(),
            extra_opts,
            &outdir,
        )
        .context("cpp build failed")?
    } else if ext == Some(OsStr::new("bc")) {
        opts.input.clone()
    } else {
        Err(anyhow!("Input file is neither C++, rust, or llvm bitcode"))?
    };

    Ok(input_bc)
}

fn phi_strategy_from_opts(opts: &Opt) -> PhiStrategy {
    if opts.phi_masks {
        PhiStrategy::PredicationMasks(PhiMaskOptions {
            filter_undef: opts.filter_undef,
        })
    } else {
        PhiStrategy::SignificantPaths(PhiConversionOptions {
            cache: !opts.no_phi_cache,
            filter_undef: opts.filter_undef,
            naive_paths: opts.naive_paths,
        })
    }
}

fn try_if_set<T>(
    condition: &Option<T>,
    error_msg: &str,
    f: impl Fn(&T) -> anyhow::Result<()>,
) -> anyhow::Result<()> {
    condition
        .as_ref()
        .map(f)
        .map(|r| r.with_context(|| error_msg.to_string()))
        .unwrap_or(Ok(()))
}

fn dump_condition_counts(debug: &DebugInfo, outdir: &Path) -> anyhow::Result<()> {
    // Gather some statistics on the amount of nodes used for phi nodes
    let mut mask_count = 0;
    let mut phi_count = HashMap::new();
    let mut condition_count = HashMap::new();
    for (_, info) in &debug.ident_map {
        for data in info {
            match data {
                IdentDebugInfo::Specific(SpecificInfo::PhiCondition(tgt, via)) => {
                    *phi_count
                        .entry(tgt)
                        .or_insert(HashMap::new())
                        .entry(via)
                        .or_insert(0) += 1;
                }
                IdentDebugInfo::Specific(SpecificInfo::ExitCondition(for_node)) => {
                    *condition_count.entry(for_node).or_insert(0) += 1;
                }
                IdentDebugInfo::Specific(SpecificInfo::EdgeMask(_, _)) => {
                    mask_count += 1;
                }
                IdentDebugInfo::Specific(SpecificInfo::EntryMask(_)) => {
                    mask_count += 1;
                }
                _ => {}
            }
        }
    }

    // Dump condition counts
    {
        let target_file = outdir.join("cond_counts.txt");

        let mut file_content = String::new();

        let mut total_phi_cond_count = 0;
        for (node, value) in phi_count {
            writeln!(&mut file_content, "// Phi count for {:?}", node).unwrap();
            for (via, count) in value {
                writeln!(
                    &mut file_content,
                    "\t{:?} has {} condition nodes",
                    via, count
                )
                .unwrap();
                total_phi_cond_count += count;
            }
        }
        for (cond, value) in condition_count {
            writeln!(
                &mut file_content,
                "Exit conditions for {:?} = {}",
                cond, value
            )
            .unwrap();
        }
        writeln!(&mut file_content, "Phi count: {}", total_phi_cond_count).unwrap();
        writeln!(&mut file_content, "Mask count: {}", mask_count).unwrap();

        std::fs::write(target_file, &file_content)?;
    }
    Ok(())
}

// fn build_spade_stub()

fn main() -> anyhow::Result<()> {
    let mut log_config = ConfigBuilder::new();
    log_config
        .add_filter_ignore(format!("llvm_ir"))
        .set_time_level(LevelFilter::Off);
    TermLogger::init(LevelFilter::Trace, log_config.build(), TerminalMode::Mixed)
        .expect("Failed to initialise logger");

    let opts = Opt::from_args();
    let outdir = opts.outdir.clone();

    let cinnabar_root_str = opts.cinnabar_root.to_string_lossy();

    let wl = word_length_strategy::parse_wls(&opts.word_length_strategy).with_context(|| {
        format!(
            "Failed to parse {} as word length strategy",
            opts.word_length_strategy
        )
    })?;
    let config = ffi::new_config(&wl, true);
    ffi::set_recompute_not(opts.recompute_not);

    let input_bc = build_bitcode(&opts, &outdir).context("Failed to build bitcode")?;

    trace!("Loading function from bitcode");
    let unlowered = load_function_from_module(input_bc, phi_strategy_from_opts(&opts))
        .context("Failed to load bitcode function")?;
    trace!("Done");

    opts.block_graph
        .map(|path| -> anyhow::Result<_> {
            let full_path = outdir.join(path);
            std::fs::write(&full_path, unlowered.block_graph.dot_graph())
                .with_context(|| format!("Failed to write to {:?}", full_path))?;
            Ok(())
        })
        .unwrap_or(Ok(()))
        .context("Failed to write block graph")?;

    // If an unlowered graph should be dumpted, do so. Uses a map and closure
    // to add context to the error message
    opts.unlowered_graph
        .map(|path| -> anyhow::Result<_> {
            let full_path = outdir.join(path);
            let mut file = File::create(full_path)?;
            let content = graphviz::visualise_node_graph(&unlowered.graph);
            file.write_all(&content.as_bytes())?;
            Ok(())
        })
        .map(|r| r.context("When generating limestone_graph"))
        .unwrap_or(Ok(()))?;

    let Function {
        return_value,
        graph,
        mut debug,
        next_id,
        lowering_diffs,
        ..
    } = lower_function(unlowered).context("Failed to lower function")?;

    let (root_node, non_dummy) = match &return_value {
        ReturnValue::Single(id) => (id.clone(), vec![id.clone()]),
        ReturnValue::Struct {
            struct_node,
            values,
        } => (struct_node.clone(), values.clone()),
    };
    let root_operand = limestone::operand::ident(root_node);

    try_if_set(
        &opts.limestone_graph,
        "When generating limestone graph",
        |path| {
            let full_path = outdir.join(path);
            let mut file = File::create(full_path).context("failed to create limestone graph")?;
            let content = graphviz::visualise_node_graph_diff(&graph, &lowering_diffs);
            file.write_all(&content.as_bytes())
                .context("Failed to write limestone graph")?;
            Ok(())
        },
    )?;

    let debuginforef = &mut debug;
    let mut nodes = graph
        .cached_traversal(
            &root_operand,
            &mut |name, node, cache| {
                conversion::to_cinnabar(name, node, cache, &graph, debuginforef, &config)
            },
            &conversion::const_to_cinnabar,
        )
        .context("Failed to perform cached traversal")?;

    try_if_set(&opts.ident_map, "When writing debug mapping", |path| {
        let full_path = outdir.join(path);
        let mut file = File::create(full_path)?;

        let content = serde_json::to_string_pretty(&debug)?;
        file.write_all(&content.as_bytes())?;
        Ok(())
    })
    .context("when generating debug mapping")?;

    // We are done adding nodes, so we can add the debug info that was created in the
    // identifier generation
    next_id.push_motivations(&mut debug);
    let root_node = nodes
        .get_mut(&root_operand)
        .context("No node created for the root")?;

    dump_condition_counts(&debug, &outdir).context("Failed to dump condition counts")?;

    if !opts.no_retiming {
        ffi::do_retiming(root_node.pin_mut(), &wl);
    }

    ffi::report_problems(root_node);

    let mut return_values = ffi::new_return_values();
    for node in non_dummy {
        let corresponding = nodes
            .get(&limestone::operand::ident(node))
            .context(format!("No node created for return value"))?;
        ffi::push_return_value(return_values.as_mut().unwrap(), corresponding)
    }

    opts.cinnabar_graph
        .map(|path| -> anyhow::Result<_> {
            let full_path = outdir.join(path);
            let node = ffi::ret_values_as_dummy_node(&return_values)?;
            ffi::dump_graph(&full_path.to_string_lossy(), &node, &wl, true);
            Ok(())
        })
        .map(|r| r.context("When generating limestone_graph"))
        .unwrap_or(Ok(()))?;

    for opt_pass in opts.opt_passes {
        return_values = ffi::opt_return_values(&return_values, &opt_pass.ffi_pass());

        log::info!("Reporting problems after {}", opt_pass);
        let dummy = ffi::ret_values_as_dummy_node(&return_values)
            .context("Failed to get return values as dummy node")?;
        ffi::report_problems(&dummy);
    }

    let mut codegen = ffi::start_codegen(&return_values, &wl).context("Failed to start codegen")?;

    // For the following code, we can't have `nodes` be mutably borrowed, so we have
    // to shadow root_node
    let root_node = nodes
        .get(&root_operand)
        .context("No node created for the root")?;

    let cpp_data_type = match opts.cpp_data_type {
        CppDataType::Fp => ffi::cpp_data_type_fp(),
        CppDataType::Ac => ffi::cpp_data_type_ac(),
        CppDataType::Ap => ffi::cpp_data_type_ap(),
    };

    if let Some(name) = &opts.output_filename {
        let target_dir = outdir.join(name);
        let target_dir_str = target_dir
            .to_str()
            .context("Target dir could not be converted to OsStr")?;
        std::fs::create_dir_all(&target_dir).context("Failed to create cpp output dir")?;

        let cpp = ffi::codegen_get_cpp(name, codegen.pin_mut(), &wl, &cpp_data_type);
        let hpp = ffi::codegen_get_hpp(name, codegen.pin_mut(), &wl, &cpp_data_type);
        let verilog = ffi::codegen_get_verilog(name, codegen.pin_mut(), &wl, opts.compress_inputs);

        let mut cpp_file = File::create(target_dir.join(name).with_extension("cpp"))
            .context("Failed to write output cpp")?;
        cpp_file.write_all(cpp.as_bytes()).unwrap();
        let mut hpp_file = File::create(target_dir.join(name).with_extension("hpp"))
            .context("Failed to write output hpp")?;
        hpp_file.write_all(hpp.as_bytes()).unwrap();

        let mut verilog_file = File::create(target_dir.join(name).with_extension("v"))
            .context("Failed to write output verilog")?;
        verilog_file.write_all(verilog.as_bytes()).unwrap();

        // Copy verilog files to output dir
        for entry in std::fs::read_dir(opts.cinnabar_root.join("verilog"))
            .context("Failed to read verilog dir")?
        {
            let path = entry.context("Failed to get path in cinnabar_root")?.path();
            if !path.is_dir() {
                let filename = &path.file_name().unwrap();
                std::fs::copy(&path, target_dir.join(filename))
                    .with_context(|| format!("Failed to copy {:?} to output dir", path))?;
            }
        }

        ffi::build_lookup_tables(target_dir_str, &cpp_data_type);

        // TODO: Make this path configurable
        ffi::run_verilog_extra_commands(&codegen, &wl, &cinnabar_root_str, target_dir_str);

        if opts.spade_stub {
            spade::build_spade_stub(&outdir, name, &graph, &return_value, &nodes, &wl)?;
        }

        if let Some(count) = opts.verify_samples {
            verification::generate_mercverf(&MercverfArgs {
                graph,
                outdir: &outdir,
                input_filename: &opts.input,
                output_filename: &name,
                sample_count: count,
                return_value: &return_value,
                cinnabar_nodes: &nodes,
                cinnabar_root: &PathBuf::from(opts.cinnabar_root),
                word_length: &wl,
                compiler_flags: &opts.compiler_args,
                lut_cpp_files: &ffi::get_extra_cpps(root_node, &wl)
                    .iter()
                    .map(|s| format!("{}/{}", target_dir.to_string_lossy(), s))
                    .collect::<HashSet<_>>()
                    .into_iter()
                    .collect::<Vec<_>>(),
                lut_verilog_files: &ffi::get_extra_verilogs(root_node, &wl)
                    .iter()
                    .map(|s| format!("{}/{}", target_dir.to_string_lossy(), s))
                    .collect::<HashSet<_>>()
                    .into_iter()
                    .collect::<Vec<_>>(),
                read_inputs: opts.verification_inputs,
            })
            .context("Failed run verification")?;
        }
    } else {
        println!("Warning: No C++ output filename specified, not generating any c++");
        println!("\t(missing -c flag)")
    }

    // we have to do this at the end since we do destructive optimisation, and optimisation
    // modifies the tree until the C++ side gets a correct clone implementation
    opts.merged_cinnabar_graph
        .map(|path| -> anyhow::Result<_> {
            let full_path = outdir.join(path);
            let node = ffi::opt_optimize(
                ffi::ret_values_as_dummy_node(&return_values)
                    .context("Failed to get ret values as dummy node")?
                    .as_ref()
                    .unwrap(),
                &optimization::OptPass::MergeBools.ffi_pass(),
            );
            ffi::dump_graph(&full_path.to_string_lossy(), &node, &wl, true);
            Ok(())
        })
        .map(|r| r.context("When generating limestone_graph"))
        .unwrap_or(Ok(()))?;

    Ok(())
}
