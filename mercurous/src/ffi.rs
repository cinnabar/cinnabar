#[cxx::bridge(namespace = cinnabar)]
pub mod ffi {
    // This is kind of ugly, but we can't pass C++ types through rust vectors
    // without first wrapping them in a struct
    struct PartialSelectWrapper {
        inner: UniquePtr<PartialSelect>,
    }

    // Types not declared in the `cinnabar` ffi namespace, but in the global
    // namespace
    #[namespace = ""]
    unsafe extern "C++" {
        type Config;
        type OptPass;
        type Codegen;
        type WordLength;
        type CppDataType;
    }

    unsafe extern "C++" {
        include!("cinnabar/cinnabar_src/ffi/ffi.hpp");

        type RcNode;
        type WeakNode;

        fn wrap_rc(to_wrap: &RcNode) -> UniquePtr<RcNode>;

        // Node constructor wrappers
        fn new_config(word_length: &WordLength, single_node_interp: bool) -> UniquePtr<Config>;

        fn frac_const(val: f64) -> UniquePtr<RcNode>;
        fn bool_const(val: bool) -> UniquePtr<RcNode>;
        fn new_input(name: &str, min: f64, max: f64, val: f64) -> UniquePtr<RcNode>;
        fn new_non_referenced(
            name: &str,
            description: &str,
            parents: &[UniquePtr<RcNode>],
        ) -> Result<UniquePtr<RcNode>>;

        // TODO: Do this as a lowering pass instead
        fn bypass_nop(input: &RcNode) -> UniquePtr<RcNode>;

        fn new_add(lhs: &RcNode, rhs: &RcNode) -> Result<UniquePtr<RcNode>>;
        fn new_sub(lhs: &RcNode, rhs: &RcNode) -> Result<UniquePtr<RcNode>>;
        fn new_mul(lhs: &RcNode, rhs: &RcNode) -> Result<UniquePtr<RcNode>>;
        fn new_div(lhs: &RcNode, rhs: &RcNode) -> Result<UniquePtr<RcNode>>;

        fn new_gt(lhs: &RcNode, rhs: &RcNode) -> Result<UniquePtr<RcNode>>;
        fn new_gt_or_eq(lhs: &RcNode, rhs: &RcNode) -> Result<UniquePtr<RcNode>>;
        fn new_lt(lhs: &RcNode, rhs: &RcNode) -> Result<UniquePtr<RcNode>>;
        fn new_lt_or_eq(lhs: &RcNode, rhs: &RcNode) -> Result<UniquePtr<RcNode>>;
        fn new_eq(lhs: &RcNode, rhs: &RcNode) -> Result<UniquePtr<RcNode>>;
        fn new_neq(lhs: &RcNode, rhs: &RcNode) -> Result<UniquePtr<RcNode>>;

        fn new_not(op: &RcNode) -> Result<UniquePtr<RcNode>>;
        fn new_and(lhs: &RcNode, rhs: &RcNode) -> Result<UniquePtr<RcNode>>;
        fn new_or(lhs: &RcNode, rhs: &RcNode) -> Result<UniquePtr<RcNode>>;

        fn new_usub(op: &RcNode) -> Result<UniquePtr<RcNode>>;
        fn new_min(lhs: &RcNode, rhs: &RcNode) -> Result<UniquePtr<RcNode>>;
        fn new_max(lhs: &RcNode, rhs: &RcNode) -> Result<UniquePtr<RcNode>>;
        fn new_truncate(op: &RcNode) -> Result<UniquePtr<RcNode>>;
        fn new_sqrt(op: &RcNode) -> Result<UniquePtr<RcNode>>;
        fn new_abs(op: &RcNode) -> Result<UniquePtr<RcNode>>;

        fn new_lut(idx: &RcNode, name: &str, values: &Vec<f64>) -> Result<UniquePtr<RcNode>>;

        // Interpolation functions
        fn new_interp1(
            name: &str,
            x: &Vec<f64>,
            y: &Vec<f64>,
            idx: &RcNode,
            config: &Config,
        ) -> Result<UniquePtr<RcNode>>;
        fn new_interp1_valid(
            name: &str,
            x: &Vec<f64>,
            y: &Vec<f64>,
            idx: &RcNode,
            config: &Config,
        ) -> Result<UniquePtr<RcNode>>;
        fn new_interp2(
            name: &str,
            x: &Vec<f64>,
            y: &Vec<f64>,
            z: &Vec<f64>,
            x_point: &RcNode,
            y_point: &RcNode,
            config: &Config,
        ) -> Result<UniquePtr<RcNode>>;
        fn new_interp2_valid(
            name: &str,
            x: &Vec<f64>,
            y: &Vec<f64>,
            z: &Vec<f64>,
            x_point: &RcNode,
            y_point: &RcNode,
            config: &Config,
        ) -> Result<UniquePtr<RcNode>>;

        fn new_if_bool(
            cond: &RcNode,
            on_true: &RcNode,
            on_false: &RcNode,
        ) -> Result<UniquePtr<RcNode>>;
        fn new_if_fractional(
            cond: &RcNode,
            on_true: &RcNode,
            on_false: &RcNode,
        ) -> Result<UniquePtr<RcNode>>;

        fn new_opaque_barrier(input: &RcNode) -> Result<UniquePtr<RcNode>>;

        // Since select nodes take a bunch of vectors of nodes, and we can't
        // build vectors of UniquePtr or &RcNode, we'll build the relevan
        // structures on the C++ side
        type PartialSelect;
        fn new_select_option(value: &RcNode) -> Result<UniquePtr<PartialSelect>>;
        fn push_select_condition(target: Pin<&mut PartialSelect>, condition: &RcNode);
        fn new_select_frac(input: Vec<PartialSelectWrapper>) -> Result<UniquePtr<RcNode>>;
        fn new_select_bool(input: Vec<PartialSelectWrapper>) -> Result<UniquePtr<RcNode>>;

        //////////////////////////////////////////////////////////////////////////////
        // Node information functions
        fn unique_ancestors(node: &RcNode) -> UniquePtr<CxxVector<WeakNode>>;

        fn print_name(node: &RcNode);
        fn override_name(node: &RcNode, name: &str);
        fn calculate_value(node: &RcNode) -> Result<f32>;

        fn get_node_name(node: &RcNode) -> UniquePtr<CxxString>;
        fn get_weak_name(node: &WeakNode) -> Result<UniquePtr<CxxString>>;
        fn fractional_bits(node: &RcNode, wl: &WordLength) -> usize;
        fn integer_bits(node: &RcNode, wl: &WordLength) -> usize;
        fn register_size(node: &RcNode, wl: &WordLength) -> usize;
        fn pipeline_end_depth(node: &RcNode, wl: &WordLength) -> i32;

        fn dump_graph(filename: &str, root: &RcNode, wl: &WordLength, include_delay: bool);

        fn report_problems(graph_root: &RcNode);
        //////////////////////////////////////////////////////////////////////////////

        // Codegen related things
        fn ret_values_as_dummy_node(values: &ReturnValues) -> Result<UniquePtr<RcNode>>;
        fn start_codegen(root: &ReturnValues, wl: &WordLength) -> Result<UniquePtr<Codegen>>;
        fn codegen_get_cpp(
            fn_name: &str,
            codegen: Pin<&mut Codegen>,
            wl: &WordLength,
            t: &CppDataType,
        ) -> UniquePtr<CxxString>;
        fn codegen_get_hpp(
            fn_name: &str,
            codegen: Pin<&mut Codegen>,
            wl: &WordLength,
            t: &CppDataType,
        ) -> UniquePtr<CxxString>;
        fn codegen_get_verilog(
            module_name: &str,
            codegen: Pin<&mut Codegen>,
            wl: &WordLength,
            compress_inputs: bool,
        ) -> UniquePtr<CxxString>;
        fn build_lookup_tables(storage_location: &str, t: &CppDataType);
        fn run_verilog_extra_commands(
            codegen: &Codegen,
            wl: &WordLength,
            cinnabar_root: &str,
            output_dir: &str,
        );
        fn get_extra_cpps(root: &RcNode, wl: &WordLength) -> UniquePtr<CxxVector<CxxString>>;
        fn get_extra_verilogs(root: &RcNode, wl: &WordLength) -> UniquePtr<CxxVector<CxxString>>;

        /// Word length strategies
        fn fixed_total_word_length(amount: i32) -> UniquePtr<WordLength>;
        fn fixed_fractional_word_length(amount: i32) -> UniquePtr<WordLength>;
        /// Total length will be 1 + integer + fractional
        fn fully_specified_word_length(integer: i32, fractional: i32) -> UniquePtr<WordLength>;

        /// CPP Data type constructors
        fn cpp_data_type_fp() -> UniquePtr<CppDataType>;
        fn cpp_data_type_ac() -> UniquePtr<CppDataType>;
        fn cpp_data_type_ap() -> UniquePtr<CppDataType>;

        fn node_info(node: &RcNode) -> UniquePtr<CxxString>;

        type ReturnValues;
        fn new_return_values() -> UniquePtr<ReturnValues>;
        fn push_return_value(target: Pin<&mut ReturnValues>, value: &RcNode);
        fn opt_return_values(target: &ReturnValues, pass: &OptPass) -> UniquePtr<ReturnValues>;

        fn do_retiming(root: Pin<&mut RcNode>, wl: &WordLength);

        fn set_recompute_not(val: bool);

        ////////////////////////////////////////////////////////////////////
        // Main optimisation function
        fn opt_optimize(root: &RcNode, pass: &OptPass) -> UniquePtr<RcNode>;

        // Opt passes
        fn opt_pass_constant_fold() -> UniquePtr<OptPass>;
        fn opt_pass_remove_redundant_comparisons() -> UniquePtr<OptPass>;
        fn opt_pass_remove_redundant_interp_validity() -> UniquePtr<OptPass>;
        fn opt_pass_remove_redundant_bool_computations() -> UniquePtr<OptPass>;
        fn opt_pass_constant_division_replacement() -> UniquePtr<OptPass>;
        fn opt_pass_merge_bools() -> UniquePtr<OptPass>;
        fn opt_pass_select_else_last() -> UniquePtr<OptPass>;
        fn opt_pass_select_else_x() -> UniquePtr<OptPass>;
        fn opt_pass_remove_single_select() -> UniquePtr<OptPass>;
    }

    extern "Rust" {}
}

pub use ffi::*;
