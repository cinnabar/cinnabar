use cxx::{Exception, UniquePtr};

use limestone::consts::Const;
use limestone::debuginfo::DebugInfo;
use limestone::graph::Graph;
use limestone::graph_node::Kind;
use limestone::name_util::StringifyableName;
use limestone::operand::{ident, Identifier, OperandMap};
use limestone::typecheck::{get_type, Type};

use crate::error::{Error, Result};
use crate::ffi::*;

/// Convert nodes in the graph representation into cinnabar nodes.
/// Assumes that cache contains the operands of the node at the time
/// they are processed, which is guaranteed by `Graph::cached_traversal`
pub fn to_cinnabar(
    name: &Identifier,
    kind: &Kind,
    cache: &OperandMap<UniquePtr<RcNode>>,
    graph: &Graph,
    debug: &mut DebugInfo,
    config: &Config,
) -> Result<UniquePtr<RcNode>> {
    let assume_lowered = || -> ! {
        panic!(
            "Node {:?} with kind {:?} should have been lowered",
            name, kind
        );
    };

    // Convenience closure to look up nodes in the cache
    let c = |op| {
        cache
            .get(op)
            .ok_or_else(|| Error::NodeNotYetConverted(op.clone()))
            .map_err(|e| Error::ConversionError(name.clone(), kind.clone(), Box::new(e)))
    };

    // We want to add diagnostics if an error is thrown, so we'll make this a closure
    let node = match kind {
        Kind::Nop(operand) => Ok(bypass_nop(c(operand)?)),
        Kind::Input(min, max) => Ok(new_input(&name.stringify(), *min, *max, *min)),
        Kind::GreaterThan(lhs, rhs) => new_gt(c(lhs)?, c(rhs)?),
        Kind::GreaterThanOrEq(lhs, rhs) => new_gt_or_eq(c(lhs)?, c(rhs)?),
        Kind::LessThan(lhs, rhs) => new_lt(c(lhs)?, c(rhs)?),
        Kind::LessThanOrEq(lhs, rhs) => new_lt_or_eq(c(lhs)?, c(rhs)?),
        Kind::Equal(lhs, rhs) => new_eq(c(lhs)?, c(rhs)?),
        Kind::NotEqual(lhs, rhs) => new_neq(c(lhs)?, c(rhs)?),
        Kind::Add(lhs, rhs) => new_add(c(lhs)?, c(rhs)?),
        Kind::Sub(lhs, rhs) => new_sub(c(lhs)?, c(rhs)?),
        Kind::Div(lhs, rhs) => new_div(c(lhs)?, c(rhs)?),
        Kind::Mul(lhs, rhs) => new_mul(c(lhs)?, c(rhs)?),
        Kind::Min(lhs, rhs) => new_min(c(lhs)?, c(rhs)?),
        Kind::Max(lhs, rhs) => new_max(c(lhs)?, c(rhs)?),
        Kind::USub(op) => new_usub(c(op)?),
        Kind::Truncate(op) => new_truncate(c(op)?),
        Kind::Sqrt(op) => new_sqrt(c(op)?),
        Kind::Abs(op) => new_abs(c(op)?),
        Kind::Not(op) => new_not(c(op)?),
        Kind::And(lhs, rhs) => new_and(c(lhs)?, c(rhs)?),
        Kind::Or(lhs, rhs) => new_or(c(lhs)?, c(rhs)?),
        Kind::Select(inner) => {
            let partial_select = inner
                .iter()
                .map(|(conditions, value)| {
                    let mut partial = new_select_option(c(&value).unwrap())?;
                    for condition in &conditions.inner {
                        push_select_condition(partial.pin_mut(), c(&condition).unwrap());
                    }
                    Ok(PartialSelectWrapper { inner: partial })
                })
                .collect::<std::result::Result<Vec<_>, Exception>>()
                .map_err(Error::from)
                .map_err(|e| Error::ConversionError(name.clone(), kind.clone(), Box::new(e)))?;

            match get_type(&ident(name.clone()), graph)? {
                Type::Bool => new_select_bool(partial_select),
                Type::Frac => new_select_frac(partial_select),
                Type::NonSynth => panic!("synthesising non synthable type"),
                Type::Int => unimplemented!(),
                Type::Struct => unimplemented!(),
            }
        }
        Kind::IfElse(cond, on_true, on_false) => match get_type(&ident(name.clone()), graph)? {
            Type::Bool => new_if_bool(c(cond)?, c(on_true)?, c(on_false)?),
            Type::Frac => new_if_fractional(c(cond)?, c(on_true)?, c(on_false)?),
            Type::NonSynth => panic!("synthesising non synthable type"),
            Type::Int => unimplemented!(),
            Type::Struct => unimplemented!(),
        },
        Kind::Lookup { name, data, idx } => new_lut(c(idx)?, &name, &data),

        Kind::Interp1 { x, y, x_point } => {
            new_interp1(&name.stringify(), x, y, c(x_point)?, config)
        }
        Kind::Interp1Valid { x, y, x_point } => {
            new_interp1_valid(&name.stringify(), x, y, c(x_point)?, config)
        }
        Kind::Interp2 {
            x,
            y,
            z,
            x_point,
            y_point,
        } => new_interp2(&name.stringify(), x, y, z, c(x_point)?, c(y_point)?, config),
        Kind::Interp2Valid {
            x,
            y,
            z,
            x_point,
            y_point,
        } => new_interp2_valid(&name.stringify(), x, y, z, c(x_point)?, c(y_point)?, config),

        Kind::StructWithMembers(name, operands) => new_non_referenced(
            &format!("{}_member_definition", name),
            &format!("Struct member definition"),
            &operands
                .iter()
                .map(|op| Ok(ffi::wrap_rc(c(op)?)))
                .collect::<Result<Vec<_>>>()?,
        ),

        Kind::OpaqueBarrier(input) => new_opaque_barrier(c(input)?),
        // Unsynthesisable
        Kind::GetElementPtr { .. } => assume_lowered(),
        Kind::Load(_) => assume_lowered(),
        Kind::InputBound(_, _, _) => assume_lowered(),
        Kind::UnboundedInput => assume_lowered(),
        Kind::StructAllocation(_) => assume_lowered(),
        Kind::Store { .. } => assume_lowered(),
        Kind::InsertValue { .. } => assume_lowered(),
        Kind::UnloweredInterp1(_) => assume_lowered(),
        Kind::UnloweredInterp1Valid(_) => assume_lowered(),
        Kind::UnloweredInterp2(_) => assume_lowered(),
        Kind::UnloweredInterp2Valid(_) => assume_lowered(),
    }
    .map_err(Error::from)
    .map_err(|e| Error::ConversionError(name.clone(), kind.clone(), Box::new(e)))?;

    if let Identifier::Str(_) = name {
        override_name(&node, &name.stringify());
    }

    let new_identifier = Identifier::Str(get_node_name(&node).to_string());
    if &new_identifier != name {
        debug.add_alias(&new_identifier, &name)
    }

    debug.add_note(&name, &ffi::node_info(&node).to_string_lossy());
    Ok(node)
}

pub fn const_to_cinnabar(
    val: &Const,
    _: &OperandMap<UniquePtr<RcNode>>,
) -> Result<UniquePtr<RcNode>> {
    Ok(match val {
        Const::IntConst(val) => frac_const(*val as f64),
        Const::DoubleConst(val) => frac_const(*val),
        Const::BoolConst(val) => bool_const(*val),
        Const::ArrayReference(_) => panic!("array reference should already be lowered"),
        Const::GetElementPtr { .. } => panic!("GetElementPtr should already be lowered"),
    })
}
