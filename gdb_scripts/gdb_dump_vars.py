import gdb
import os

sys.path.append(os.path.join(os.path.dirname(__file__), "../yafp/gdb_scripts"))

import pretty_fp

def main():
    gdb.execute(f'break __gdb_trampoline', to_string=True)
    gdb.execute(f'run', to_string=True)

    # We shouldn't continue from here 
    if not gdb.selected_inferior().threads()[0].is_exited():
        gdb.execute(f'fin')
    else:
        color = "\033[94m"
        print(f"{color}Tried running until __gdb_trampoline but it was not hit")
        return

    if not gdb.selected_inferior().threads()[0].is_exited():
        variables = []

        # https://stackoverflow.com/questions/30013252/get-all-global-variables-local-variables-in-gdbs-python-interface
        frame = gdb.selected_frame()
        block = frame.block()
        names = set()
        while block:
            if(block.is_global):
                print()
                print('global vars')
            for symbol in block:
                if (symbol.is_argument or symbol.is_variable or symbol.is_constant):
                    name = symbol.name
                    if not name in names:
                        pretty_printer = pretty_fp.pretty_fp(symbol.value(frame))
                        if pretty_printer is not None:
                            variables.append((name, pretty_printer.to_string()))
                        else:
                            variables.append((name, symbol.value(frame)))
                        names.add(name)
            block = block.superblock

        with open('vardump.txt', 'w') as f:
            for (name, val) in variables:
                f.write(f"{name} = {val}\n")

        gdb.execute('quit')
    else:
        return

main()
