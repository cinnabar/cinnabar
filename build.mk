MAKEFILE=build.mk

CC =NOTACOMMAND
CXX := clang++
-include overrides.mk

# Global flags
LINK_FLAGS := \
	-lstdc++fs \
	-lglpk \
	-ldl \
	-Laffine-arith/build \
	-laffine_arith_cpp \
	-laffine_arith_rust \
	-pthread

CXXFLAGS := \
	-Wall \
	-Wextra \
	-Wno-c99-designator \
	-g \
	-std=c++17 ${OPT} \
	-MMD \
	-ICatch2/single_include/ \
	-Iaffine-arith/ \
	-Werror=return-type \
	-include src/test_config.hpp \
	-fPIC \
	-fdiagnostics-color=always
	# -fsanitize=address


# Set flags per build type
ifeq ($(COMPILETYPE), test)
	CXXFLAGS += -D_GLIBCXX_DEBUG
else ifeq ($(COMPILETYPE), debug)
	CXXFLAGS += -DRANDOM_NAME_SUFFIX# -D_GLIBCXX_DEBUG
else ifeq ($(COMPILETYPE), opt)
	CXXFLAGS += -DRANDOM_NAME_SUFFIX -O3
else
$(error "Compile type '${COMPILETYPE}' not recognised")
endif

# Build to different dirs for different compile types
BUILD_DIR=build/${COMPILETYPE}



# Find all relevant sources
SRCS = $(wildcard src/*.cpp) $(wildcard src/node/*.cpp)
TEST_SRCS = $(wildcard test/*.cpp) test/main/main.cpp

# Locate  the dependency files
ALL_SRCS = $(SRCS) $(TEST_SRCS) $(BIN_SRCS)
DEPENDS = $(patsubst %.cpp, ${BUILD_DIR}/%.d, $(ALL_SRCS))

# Various groups of object files
OBJS = $(patsubst %.cpp, ${BUILD_DIR}/%.o, $(SRCS))
TEST_OBJS = ${OBJS} $(patsubst %.cpp, ${BUILD_DIR}/%.o, $(TEST_SRCS))

FFI_SRCS=$(wildcard src/ffi/*.cpp)
FFI_OBJS=$(patsubst %.cpp, ${BUILD_DIR}/%.o, $(SRCS))

AFFINE_ARITH_LIB=affine-arith/libaffine_arith.a

# Final binaries
${BUILD_DIR}/testbin: ${TEST_OBJS} ${MAKEFILE} | ${AFFINE_ARITH_LIB}
	@echo -e "[\033[0;34m$(CXX)\033[0m] Building test binary"
	$(CXX) $(CXXFLAGS) ${TEST_OBJS} ${LINK_FLAGS} -o $@

${BUILD_DIR}/ffi.o: ${OBJS} ${FFI_OBJS} | ${AFFINE_ARITH_LIB}
	@echo ${FFI_OBJS}
	@echo -e "[\033[0;34m$(CXX)\033[0m] Building ffi with ${CXXFLAGS}"
	ld -relocatable ${OBJS} -o $@

# General object build rule
${BUILD_DIR}/%.o: %.cpp ${MAKEFILE} | ${AFFINE_ARITH_LIB} 
	@echo -e "[\033[0;34m$(CXX)\033[0m] Building $@"
	@mkdir -p ${@D}
	@$(CXX) $(CXXFLAGS) -MMD -c -o $@ $<


affine_arith_lib: ${AFFINE_ARITH_LIB} .PHONY

${AFFINE_ARITH_LIB}: .PHONY
	make -C affine-arith

-include ${DEPENDS}

.PHONY:

