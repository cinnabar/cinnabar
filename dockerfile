FROM debian
RUN set -eux;\
    apt-get update && apt-get install -y gnupg2 wget; \
    echo "deb http://apt.llvm.org/buster/ llvm-toolchain-buster-11 main" > /etc/apt/sources.list.d/llvm.list; \
    wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -; \
    apt-get update; \
    apt-get -t llvm-toolchain-buster-11 install -y llvm-11 clang-11 clang++-11 llvm-11; \
    apt-get update -qq && apt-get install -y -qq make clang lld libglpk-dev haskell-stack curl git cmake ninja-build; \
    git clone https://gitlab.com/cinnabar/llvm-project.git --depth 1 \
        && cd llvm-project \
        && mkdir build \
        && cd build \
        && cmake -G Ninja \
            -DLLVM_ENABLE_PROJECTS=clang \
            -DCMAKE_BUILD_TYPE=MinSizeRel  \
            -DLLVM_USE_LINKER=lld \
            ../llvm \
        && ninja clang -j4 \
        && ninja opt -j4 \
        && ninja llvm-as -j4 \
        && ninja install clang \
        && ninja install opt \
        && ninja install llvm-as \
        && cd ../.. \
        && rm llvm-project -rf

