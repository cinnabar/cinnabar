#include <pybind11/embed.h>
#include <pybind11/functional.h>
#include <pybind11/numpy.h>

#include <functional>

namespace py = pybind11;

struct FnWrapper {
    std::function<double(py::array_t<double>)> fn;
};

PYBIND11_EMBEDDED_MODULE(example, m) {
    py::class_<FnWrapper>(m, "FnWrapper")
        .def(py::init<>())
        .def("__call__",
            [](const FnWrapper &a, py::array_t<double> in) {
                return a.fn(in);
            }
        );
    m.attr("a") = 1;
}

int main() {
    py::scoped_interpreter guard{};

    auto locals = py::dict();
    auto globals = py::globals();

    double bounds = 0.5;

    globals["example"] = py::module::import("example");
    globals["np"] = py::module::import("numpy");
    globals["fn"] = FnWrapper{[bounds](py::array_t<double> a){
        auto buf = a.request();
        auto ptr = (double*) buf.ptr;
        return bounds - (
            255 * pow(2, -ptr[0]) +
            255 * pow(2, -ptr[1]) +
            255 * pow(2, -ptr[2]) +
            pow(2, -ptr[3]) +
            pow(2, -ptr[4]) +
            pow(2, -ptr[5])
        );
    }};
    py::exec(R"(
        import scipy.optimize

        def fun(x):
            return np.sum(x)

        bounds = [(0, 32) for _ in range(8)]

        x0 = list([3 for x in range(8)])

        print(fn(x0))

        output_constraint = lambda x: fn(x)
        print(output_constraint(x0))

        constraints = [
            {'type': 'ineq', 'fun': fn},
            {'type': 'eq', 'fun': lambda x: x[5] - x[6]},
            {'type': 'eq', 'fun': lambda x: x[6] - x[7]}
        ]

        result = scipy.optimize.minimize(fun, x0, bounds=bounds, constraints=constraints)

        print(result)
    )", py::globals(), locals);

}
