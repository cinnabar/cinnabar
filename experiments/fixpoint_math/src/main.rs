#![allow(dead_code)]
const FIXPOINT: usize = 12;
const FIXPOINT_MUL: f64 = 4096.;

fn main() {
    println!("Hello, world!");
}

fn fixpoint_mul(a: i32, b:i32) -> i32 {
    (a * b >> FIXPOINT)
}

fn fixpoint_div(a: i32, b: i32) -> i32 {
    ((a << FIXPOINT) / b)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn multiplication() {
        let a = 1.5 * FIXPOINT_MUL;
        let b = 3.3 * FIXPOINT_MUL;

        let diff = (fixpoint_mul(a as i32, b as i32) as f64/FIXPOINT_MUL - 1.5*3.3).abs();
        assert!(dbg!(diff) < 1./FIXPOINT_MUL*2.);
    }

    #[test]
    fn division() {
        let a = 1.5 * FIXPOINT_MUL;
        let b = 3.3 * FIXPOINT_MUL;

        let diff = (fixpoint_div(a as i32, b as i32) as f64/FIXPOINT_MUL - 1.5/3.3).abs();
        assert!(dbg!(diff) < 1./FIXPOINT_MUL*2.);
    }
}
