{-# language OverloadedStrings #-}
{-# language QuasiQuotes #-}
{-# language BinaryLiterals #-}

module Verilog where

import qualified Data.Text as T
import qualified Data.Map as Map
import Data.Map ((!?))

import Data.String.Interpolate (i)

indent :: [T.Text] -> [T.Text]
indent = fmap (T.append "    ")


variable :: T.Text -> Int-> T.Text -> T.Text -> T.Text
variable kind size name assignment =
    let
        size' :: T.Text
        size' =
            case size of
                  1 -> ""
                  val -> [i|[#{val-1}:0]|]
    in
    [i|#{kind}#{size'} #{name}#{assignment};|]


input :: Int -> T.Text -> T.Text
input size name =
    variable "input" size name ""

output :: Int -> T.Text -> T.Text
output size name =
    variable "output" size name ""


reg :: Int -> T.Text -> T.Text
reg size name =
    variable "reg" size name ""

wire :: Int -> T.Text -> Maybe T.Text -> T.Text
wire size name value =
    let
        value' =
            case value of
                Just val -> [i| = #{val}|]
                Nothing -> ""
    in
    variable "wire" size name value'



block :: T.Text -> [T.Text] -> [T.Text]
block kind content =
    [ kind `T.append` " begin"]
    ++ indent content
    ++ ["end"]

clockedBlock :: [T.Text] -> [T.Text]
clockedBlock content =
    block "always @(posedge clk)" content

initialBlock :: [T.Text] -> [T.Text]
initialBlock content =
    block "initial" content


ifStatement :: T.Text -> [T.Text] -> [T.Text] -> [T.Text]
ifStatement condition onTrue onFalse =
    [ [i|if (#{condition}) begin|] ]
    ++ indent onTrue
    ++ ["end else begin"]
    ++ indent onFalse
    ++ ["end"]


(<<=) :: T.Text -> T.Text -> T.Text
(<<=) var val = [i|#{var} <= #{val};|]

(=:) :: T.Text -> T.Text -> T.Text
(=:) var val = [i|#{var} = #{val};|]


assign :: T.Text -> T.Text -> T.Text
assign var val =
    [i|assign #{var} = #{val};|]



delaySignal :: Int -> T.Text -> Int -> T.Text -> [T.Text]
delaySignal size source delay delayedName =
    -- Build registers
    ( fmap (\num -> reg size [i|#{delayedName}#{num}|])
        $ take (delay)
        $ iterate (1+) (0 :: Int)
    ) ++
    ( clockedBlock $
        ( fmap
            (\num -> [i|#{delayedName}#{num}|] <<= [i|#{delayedName}#{num-1}|])
            $ take (delay-1)
            $ iterate (1+) (1 :: Int)
        ) ++
        [ [i|#{delayedName}0|] <<= source
        ]
    )


moduleInstance :: T.Text -> T.Text -> Maybe [(T.Text, T.Text)] -> [(T.Text, T.Text)] -> T.Text
moduleInstance name instanceName params vars =
    let
        paramsStr = case params of
            Just p ->
                "#(" `T.append`
                    ( T.intercalate ", "
                         $ fmap
                             (\(var, binding) -> [i|.#{var}(#{binding})|])
                             p
                    )
                    `T.append` ")"
            Nothing -> ""
    in
    [i|#{name}#{paramsStr} #{instanceName} (|]
    `T.append`
        ( T.intercalate ", "
             $ fmap
                 (\(var, binding) -> [i|.#{var}(#{binding})|])
                 vars
         )
    `T.append` [i|);|]


expect :: String -> Maybe a -> a
expect _ (Just a) = a
expect message Nothing = error message

getArg :: Read a => Map.Map String a -> String -> a
getArg args arg =
    expect [i|Missing arg #{arg}|]
        $ args !? arg
