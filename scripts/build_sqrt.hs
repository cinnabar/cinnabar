#!/usr/bin/env stack
{- stack script
 --resolver lts-14.18
 --package "string-interpolate"
 --package "text"
 --package "containers"
 --ghc-options -Wall
-}

{-
Generates fixed point square root verilog code.

3 parameters have to be specified:
input_size: Total amount of bits in the input
input_fixedpoint: Number of fixed point bits in the input
output_fixedpoint: Number of fixed point bits to produce

The number of integer bits in the output is the number of integer bits in the input / 2

Assumes a positive input and produces an unsigned output
-}

{-# language OverloadedStrings #-}
{-# language QuasiQuotes #-}
{-# language BinaryLiterals #-}
{-# language NamedFieldPuns #-}

import Data.String.Interpolate (i)
import qualified Data.Text as T
import qualified Data.Text.IO as IO
import Data.Bits
import System.Environment (getArgs)
import System.Exit (die)
import Verilog

data BitSpec = BitSpec
    { inputOriginalSize :: Int
    , inputFixedPoint :: Int
    , outputFixedPoint :: Int
    }

outputSize :: BitSpec -> Int
outputSize spec =
    let
        BitSpec {inputOriginalSize, inputFixedPoint, outputFixedPoint} = spec

        inputIntBits = inputOriginalSize - inputFixedPoint
        outputIntBits = inputIntBits `div` 2
    in
    outputIntBits + outputFixedPoint


-- To get n output fractional bits, we need 2n input fractional bits, so we
-- have to pad the input with 2n - inputFixedPoint bits. TODO: Handle 2n <
-- inputFixedPointBit
inputPadding :: BitSpec -> Int
inputPadding bitSpec =
    2 * (outputFixedPoint bitSpec) - (inputFixedPoint bitSpec)

inputSize :: BitSpec -> Int
inputSize bitSpec =
    let
        extraBits = inputPadding bitSpec
    in
    inputOriginalSize bitSpec + extraBits

buildFirstStage :: BitSpec -> [T.Text]
buildFirstStage bitSpec =
    -- Avoid problems if the amount of fractional bits in the input is even
    -- [ wire (inputSize bitSpec) "s0Input"  (Just [i|{i[0] ? i : i << 1, #{inputPadding bitSpec}'d0}|])
    [ wire (inputSize bitSpec) "s0Input" (Just [i|{i, #{inputPadding bitSpec}'d0}|])
    , wire (inputSize bitSpec) "s0Result" (Just "0")
    ]


buildStage :: BitSpec -> Int -> [T.Text]
buildStage bitSpec stage =
    let
        bitIndex = (((inputSize bitSpec) - (stage * 2)) `div` 2 ) * 2

        currBit :: Int
        currBit = 1 `shiftL` (bitIndex)

        currBitStr :: T.Text
        currBitStr = [i|(1 << #{bitIndex})|]

        prevResult :: T.Text
        prevResult = [i|s#{stage-1}Result|]
        stageInput = [i|s#{stage}Input|]
        stageResult = [i|s#{stage}Result|]
        stageSub = [i|s#{stage}Sub|]
    in
    [ reg (inputSize bitSpec) stageInput
    , reg (inputSize bitSpec) stageResult
    , wire (inputSize bitSpec) stageSub
        $ Just [i|s#{stage-1}Input - (s#{stage-1}Result + #{currBitStr})|]
    ]
    ++ (clockedBlock $
            ifStatement [i|$signed(#{stageSub}) >= 0|]
                [ stageInput <<= stageSub
                , stageResult <<= [i|(#{prevResult} >> 1) + #{currBitStr}|]
                ]
                [ stageInput <<= [i|s#{stage-1}Input|]
                , stageResult <<= [i|(#{prevResult} >> 1)|]
                ]
       )




buildModule :: BitSpec -> [T.Text]
buildModule bitSpec =
    let
        BitSpec {inputOriginalSize, inputFixedPoint, outputFixedPoint} = bitSpec
    in
    [ [i|module sqrt_#{inputOriginalSize}_#{inputFixedPoint}_#{outputFixedPoint} (clk, i, o);|]
    ]
    ++
    indent
        [ input 1 "clk"
        , input inputOriginalSize "i"
        , output (outputSize bitSpec) "o"
        ]
    ++
    (indent $ buildFirstStage bitSpec)
    ++
    (foldl (++) [] 
        $ fmap (indent . buildStage bitSpec)
        $ take (outputSize bitSpec)
        $ iterate (1+) 1)
    ++
    (indent [assign "o" [i|s#{outputSize bitSpec}Result|]])
    ++
    [ [i|endmodule|]
    ]



buildTestModule :: BitSpec -> [(Int, Int)] -> [T.Text]
buildTestModule bitSpec cases =
    let
        BitSpec { inputOriginalSize, inputFixedPoint, outputFixedPoint } = bitSpec
        testCase :: (Int, Int) -> [T.Text]
        testCase (inputVal, o) =
            [ "i" =: [i|#{inputVal}|]
            , [i|repeat (#{outputSize bitSpec}) `WAIT_NEGEDGE(clk);|]
            , [i|`ASSERT_EQ(o, #{o})|]
            ]

    in
    [ "`include \"vatch/main.v\""
    , [i|module sqrt_tb();|]
    ]
    ++ ( indent
            [ reg 1 "clk"
            , reg (inputSize bitSpec) "i"
            , wire (outputSize bitSpec) "o" Nothing
            , "`SETUP_TEST"
            ]
       )
    ++ indent (initialBlock
            [ "clk" =: "0"
            , "`ifdef __ICARUS__"
            , "$dumpfile(\"build/sqrt_tb.vcd\");"
            , "$dumpvars(0, uut);"
            , "`endif"
            , "forever begin"
            , "#1"
            , "clk" =: "~clk"
            , "end"
            ]
        )
    ++ indent
        ( initialBlock $ ((foldl (++) [] $ fmap testCase cases) ++ ["`END_TEST"])
        )
    ++ indent
            [ [i|sqrt_#{inputOriginalSize}_#{inputFixedPoint}_#{outputFixedPoint} uut(.clk(clk), .i(i), .o(o));|]
            ]
    ++
    [ [i|endmodule|] ]


doWork :: String -> BitSpec -> Bool -> IO()
doWork outfile bitSpec doTest =
    let
        BitSpec { inputOriginalSize, inputFixedPoint, outputFixedPoint } = bitSpec
        testCases = fmap
            (\(i, o) -> (i `shiftL` inputFixedPoint, o `shiftL` outputFixedPoint))
            [(4, 2), (25, 5)]
        test = if doTest then (buildTestModule bitSpec testCases) else []
    in
    IO.writeFile outfile $ T.unlines $ (buildModule bitSpec) ++ test


parseArgs :: [String] -> IO ()
parseArgs (outfile:sInputSize:sInputFixedPoint:sOutputFixedPoint:sTest:[]) =
    doWork
        outfile
        (BitSpec (read sInputSize) (read sInputFixedPoint) (read sOutputFixedPoint))
        (read sTest)
parseArgs (outfile:sInputSize:sInputFixedPoint:sOutputFixedPoint:[]) =
    -- Default to not including tests if unspecified
    doWork
        outfile
            (BitSpec (read sInputSize) (read sInputFixedPoint) (read sOutputFixedPoint))
            False
parseArgs _ = do
    die "Usage: <outfile> <inputSize> <inputFixedPoint> <outputFixedPoint> [include tests]"

main :: IO ()
main = do
    args <- getArgs
    parseArgs args
