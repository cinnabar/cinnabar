#!/usr/bin/env stack
{- stack script
 --resolver lts-14.18
 --package "string-interpolate"
 --package "text"
 --package "containers"
 --ghc-options -Wall
-}

{-# language OverloadedStrings #-}
{-# language QuasiQuotes #-}
{-# language BinaryLiterals #-}

import Data.String.Interpolate (i)
import qualified Data.Text as T
import qualified Data.Text.IO as IO
import qualified Data.Map as Map
import System.Environment (getArgs)
import System.Exit (die)
import Verilog


data Params = Params
    { moduleName_ :: T.Text
    , zLut_ :: T.Text
    , xPreMult_ :: Int
    , yPreMult_ :: Int
    , xSize_ :: Int
    , ySize_ :: Int
    , xLutLsb_ :: Int
    , yLutLsb_ :: Int
    , outputSize_ :: Int
    , ny_ :: Int
    , mul_dx_dy_ :: Int
    , dx_ :: Int
    , dy_ :: Int
    }


buildModule :: Params -> [T.Text]
buildModule p  =
    let
        zLut = zLut_ p
        xPreMult = xPreMult_ p
        yPreMult = yPreMult_ p
        xSize = xSize_ p
        ySize = ySize_ p
        xLutLsb = xLutLsb_ p
        yLutLsb = yLutLsb_ p
        outputSize = outputSize_ p
        ny = ny_ p
        mul_dx_dy = mul_dx_dy_ p
        dx = dx_ p
        dy = dy_ p
        moduleName = moduleName_ p
    in
    [ [i|module #{moduleName} (clk, x, y, z);|]
    ]
    ++
    ( indent
        [ input 1 "clk"
        , input xSize "x"
        , input ySize "y"
        , output outputSize "z"
        , reg (xSize-xLutLsb) "xIdx"
        , reg (ySize-yLutLsb) "yIdx"
        , wire (xSize) "xMul" (Just [i|(x * #{xPreMult})|])
        , wire (ySize) "yMul" (Just [i|(y * #{yPreMult})|])
        ] ++
        -- Why has no-one come up with a better way to handle delaying
        -- signals yet?
        ( delaySignal xLutLsb [i|xMul[#{xLutLsb-1}:0]|] 5 "xStep"
        ) ++
        ( delaySignal xLutLsb [i|yMul[#{yLutLsb-1}:0]|] 6 "yStep"
        ) ++
        -- Align to make finding the look up index simple
        ( clockedBlock
            [ "xIdx" <<= [i|xMul[#{xSize-1}:#{xLutLsb}]|]
            , "yIdx" <<= [i|yMul[#{ySize-1}:#{yLutLsb}]|]
            ]
        ) ++
        -- Compute the index of the next x and y indices
        [ reg (xSize-xLutLsb) "xIdxUpper"
        , reg (xSize-xLutLsb) "yIdxUpper"
        -- Delay for pipelining
        , reg (xSize-xLutLsb) "xIdx1"
        , reg (xSize-xLutLsb) "yIdx1"
        ] ++
        ( clockedBlock
            [ "xIdxUpper" <<= "xIdx + 1"
            , "yIdxUpper" <<= "yIdx + 1"
            , "xIdx1" <<= "xIdx"
            , "yIdx1" <<= "yIdx"
            ]
        ) ++
        [ reg (xSize + ySize) "q11Idx_pre_add"
        , reg (xSize + ySize) "q12Idx_pre_add"
        , reg (xSize + ySize) "q21Idx_pre_add"
        , reg (xSize + ySize) "q22Idx_pre_add"
        ] ++
        ( clockedBlock
            [ "q11Idx_pre_add" <<= [i|(xIdx1 * #{ny})|]
            , "q12Idx_pre_add" <<= [i|(xIdx1 * #{ny})|]
            , "q21Idx_pre_add" <<= [i|(xIdxUpper * #{ny})|]
            , "q22Idx_pre_add" <<= [i|(xIdxUpper * #{ny})|]
            ]
        ) ++
        [ reg (xSize + ySize) "q11Idx"
        , reg (xSize + ySize) "q12Idx"
        , reg (xSize + ySize) "q21Idx"
        , reg (xSize + ySize) "q22Idx"
        ] ++
        ( clockedBlock
            [ "q11Idx" <<= [i|q11Idx_pre_add + yIdx1|]
            , "q12Idx" <<= [i|q12Idx_pre_add + yIdxUpper|]
            , "q21Idx" <<= [i|q21Idx_pre_add + yIdx1|]
            , "q22Idx" <<= [i|q22Idx_pre_add + yIdxUpper|]
            ]
        ) ++
        -- LUTs
        [ wire (outputSize) "q11" Nothing
        , wire (outputSize) "q12" Nothing
        , wire (outputSize) "q21" Nothing
        , wire (outputSize) "q22" Nothing
        , ( moduleInstance
            zLut
            "q11_lut"
            (Just [("output_size", [i|#{outputSize}|])])
            [("clk", "clk"), ("addr", "q11Idx"), ("result", "q11")]
          )
        , ( moduleInstance
            zLut
            "q12_lut"
            (Just [("output_size", [i|#{outputSize}|])])
            [("clk", "clk"), ("addr", "q12Idx"), ("result", "q12")]
          )
        , ( moduleInstance
            zLut
            "q21_lut"
            (Just [("output_size", [i|#{outputSize}|])])
            [("clk", "clk"), ("addr", "q21Idx"), ("result", "q21")]
          )
        , ( moduleInstance
            zLut
            "q22_lut"
            (Just [("output_size", [i|#{outputSize}|])])
            [("clk", "clk"), ("addr", "q22Idx"), ("result", "q22")]
          )
        , reg (xLutLsb) "xStepUpper"
        ] ++
        ( clockedBlock
            [ "xStepUpper" <<= [i|xStep3 + #{dx}|]
            ]
        ) ++
        -- Mult by x and y steps
        [ reg (outputSize) "q11_xstep"
        , reg (outputSize) "q12_xstep"
        , reg (outputSize) "q21_xstep"
        , reg (outputSize) "q22_xstep"
        , reg (yLutLsb) "yStepUpper"
        ] ++
        ( clockedBlock
            [ "q11_xstep" <<= "q11 * xStepUpper"
            , "q12_xstep" <<= "q12 * xStep4"
            , "q21_xstep" <<= "q21 * xStepUpper"
            , "q22_xstep" <<= "q22 * xStep4"
            , "yStepUpper" <<= [i|yStep4 + #{dy}|]
            ]
        ) ++
        [ reg (outputSize) "q11_xstep_ystep"
        , reg (outputSize) "q12_xstep_ystep"
        , reg (outputSize) "q21_xstep_ystep"
        , reg (outputSize) "q22_xstep_ystep"
        ] ++
        ( clockedBlock
            [ "q11_xstep_ystep" <<= "q11_xstep * yStepUpper"
            , "q12_xstep_ystep" <<= "q12_xstep * yStepUpper"
            , "q21_xstep_ystep" <<= "q21_xstep * yStep5"
            , "q22_xstep_ystep" <<= "q22_xstep * yStep5"
            ]
        ) ++
        -- Sum multiplied q-values
        [ reg (outputSize) "q1x_sum"
        , reg (outputSize) "q2x_sum"
        ] ++
        ( clockedBlock
            [ "q1x_sum" <<= "q11_xstep_ystep + q12_xstep_ystep"
            , "q2x_sum" <<= "q21_xstep_ystep + q22_xstep_ystep"
            ]
        ) ++
        [ reg (outputSize) "qxx_sum"
        ] ++
        ( clockedBlock
            [ "qxx_sum" <<= "q1x_sum + q2x_sum"
            ]
        ) ++
        [ reg (outputSize) "result"
        ] ++
        ( clockedBlock
            [ "result" <<= [i|qxx_sum * #{mul_dx_dy}|]]
        ) ++
        [ assign "z" "result"
        ]
    )
    ++
    [ [i|endmodule|]
    ]




parseArgs :: [String] -> IO ()
parseArgs (moduleName:zLut:args) =
    let
        argTuples :: [(String, Int)]
        argTuples = fmap read args

        argMap = Map.fromList argTuples

        params = Params
            { moduleName_ = T.pack moduleName
            , zLut_ = T.pack zLut
            , xPreMult_ = argMap `getArg` "xPreMult"
            , yPreMult_ = argMap `getArg` "yPreMult"
            , xSize_ = argMap `getArg` "xSize"
            , ySize_ = argMap `getArg` "ySize"
            , xLutLsb_ = argMap `getArg` "xLutLsb"
            , yLutLsb_ = argMap `getArg` "yLutLsb"
            , outputSize_ = argMap `getArg` "outputSize"
            , ny_ = argMap `getArg` "ny"
            , mul_dx_dy_ = argMap `getArg` "mul_dx_dy"
            , dx_ = argMap `getArg` "dx"
            , dy_ = argMap `getArg` "dy"
            }

        result =
            buildModule
                params


        outfile = moduleName ++ ".v"
    in
    IO.writeFile outfile $ T.unlines $ result
parseArgs _ = die "Not enoguh arguments specified"

main :: IO ()
main = do
    args <- getArgs
    parseArgs args
