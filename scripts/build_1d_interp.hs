#!/usr/bin/env stack
{- stack script
 --resolver lts-14.18
 --package "string-interpolate"
 --package "text"
 --package "containers"
 --package "ghc"
 --package "containers"
 --ghc-options -Wall
-}

{-# language OverloadedStrings #-}
{-# language QuasiQuotes #-}
{-# language BinaryLiterals #-}

import Data.String.Interpolate (i)
import qualified Data.Text as T
import qualified Data.Text.IO as IO
import qualified Data.Map as Map
import System.Environment (getArgs)
import System.Exit (die)
import Verilog


buildModule :: T.Text -> T.Text -> T.Text -> Int -> Int -> Int -> Int -> Int -> [T.Text]
buildModule moduleName fLut derivLut preMultiplyer inputSize outputSize derivativeSize lutLsb =
    [ [i|module #{moduleName} (clk, i, o);|]
    ]
    ++
    indent
        [ input 1 "clk"
        , input inputSize "i"
        , output outputSize "o"
        , reg (inputSize - lutLsb) "lutIndex"
        , reg inputSize "i1"
        , wire inputSize "multipliedX" (Just [i|(i * #{preMultiplyer})|])
        ] ++
        ( clockedBlock
            [ "lutIndex" <<= [i|multipliedX[#{inputSize}-1:#{lutLsb}]|]
            , "i1" <<= "i"
            ]
        ) ++
        [ wire outputSize "fLutResult" Nothing
        , wire derivativeSize "fDerivResult" Nothing
        , reg inputSize "i2"
        , moduleInstance
            fLut
            "fLutMod"
            ( Just
                [ ("output_size", T.pack $ show outputSize)
                ]
            )
            [ ("clk","clk")
            , ("addr", "lutIndex")
            , ("result", "fLutResult")
            ]
        , moduleInstance
            derivLut
            "fDerivMod"
            ( Just
                [ ("output_size", T.pack $ show outputSize)
                ]
            )
            [ ("clk","clk")
            , ("addr", "lutIndex")
            , ("result", "fDerivResult")
            ]
        ] ++
        ( clockedBlock
            [ "i2" <<= "i1"
            ]
        ) ++
        [ reg outputSize "yStep"
        , reg outputSize "fLutResult1"
        ] ++
        ( clockedBlock
            [ "yStep" <<= "yStep * fDerivResult"
            , "fLutResult1" <<= "fLutResult"
            ]
        ) ++
        [ reg outputSize "result"
        ] ++
        ( clockedBlock
            [ "result" <<= "fLutResult1 + yStep"]
        ) ++
        [ assign "o" "result"
        ]
    ++
    [ [i|endmodule|]
    ]


parseArgs :: [String] -> IO ()
parseArgs (moduleName:fLut:derivLut:args) =
    let
        argTuples :: [(String, Int)]
        argTuples = fmap read args

        argMap = Map.fromList argTuples

        result =
            buildModule
                (T.pack moduleName)
                (T.pack fLut)
                (T.pack derivLut)
                (argMap `getArg` "preMultiplyer")
                (argMap `getArg` "inputSize")
                (argMap `getArg` "outputSize")
                (argMap `getArg` "derivativeSize")
                (argMap `getArg` "lutLsb")

        outfile = moduleName ++ ".v"
    in
    IO.writeFile outfile $ T.unlines $ result
parseArgs _ = die "Not enoguh arguments specified"

main :: IO ()
main = do
    args <- getArgs
    parseArgs args
