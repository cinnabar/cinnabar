# Cinnabar

A tool for generating Verilog code from a subset of C, C++, and other LLVM
supported languages. Unlike most similar tools tools, Cinnabar takes the liberty of
changing datatypes, order of computations and other operations used in the
program in order to build efficient hardware. This of course means that the
output is no longer identical to the input, a small error is introduced but
comes with the benefit of greatly reduced hardware usage.

## Usage

Cinnabar has two modes of operations. The primary one is as a standalone
program that reads llvm bytecode to produce its output. Its usage is described
below. There is also a semi-obsolete version which uses custom classes defined
by cinnabar to describe the computation structure. For details on its usage see
[docs/usage_raw.md](docs/usage_raw.md)

### Installing dependencies

Building cinnabar requires some external tools, primarily llvm-10, clang and
the rust compiler.

Currently, we only have instructions for building on debian or arch linux. For
other linux versions, the process should be similar. Windows and mac support
might also work with some effort

#### Docker

A docker image with a pre-set up environment is available as `thezoq2/cinnabar-ci`

#### Debian

Unfortunately the required versions of LLVM are not in the official debian
repositories. To fix it, we need to add the llvm repository ourselves.  Start
off by installing gnupg2 and wget

```
apt-get update && apt-get install -y gnupg2 wget;
```

Then add the llvm repo to your sources and add their gpg key. This assumes
you're using debian buster. If this is not the case, change to the
corresponding url found here https://apt.llvm.org/

```
echo "deb http://apt.llvm.org/buster/ llvm-toolchain-buster-10 main" > /etc/apt/sources.list.d/llvm.list
wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -
apt-get update
```

Now you should be able to install llvm and clang
```
apt-get -t llvm-toolchain-buster-10 install llvm-10 clang-10 clang++-10
```

You also need a few more tools from the normal repos

```
apt install make clang libglpk-dev haskell-stack curl graphviz
```

Next we need to install the rust compiler

Start by installing `rustup`, a tool to manage rust compiler installations by
following the instructions here: https://rustup.rs/.

While rustup says that it configures your path for you, this seems to be
incorrect. To be able to use the rust tools, you need to add `source
$HOME/.cargo/env` to your `.bashrc` or equivalent and restart the shell

Once rustup is installed, install the latest stable version of the compiler by running

```
rustup toolchain install stable
```

Finally, you need to install a rust binary called cxxbridge-cmd. To do so, use

```
cargo install cxxbridge-cmd --version "^0.4"
```

You should now be ready to build cinnabar

#### Arch Linux
For arch linux, all required software is in the official repos and running the following should be sufficient
```
pacman -S clang llvm rustup
rustup toolchain install stable
cargo install cxxbridge-cmd --version "^0.4"
```

```
rustup toolchain install stable
```

Finally, you need to install a rust binary called cxxbridge-cmd. To do so, use

```
cargo install cxxbridge-cmd --version "^0.4"
```


### Cloning Cinnabar

To start using cinnabar as a standalone program, you first need to clone the git repository and its
submodules

```
git clone https://gitlab.com/cinnabar/cinnabar.git
cd cinnabar
git submodule init
git submodule update
```

Some properties of cinnabar are configured using a header file. A default
configuration is provided, to use it execute

```
cp default_config.hpp src/config.hpp
```

If everything is set up correctly you should now be able to call `make` and have
the project build successfully and then run some tests.

```
...
===============================================================================
All tests passed (397 assertions in 162 test cases)
```


### Actually building some code

Now that all the boring build stuff is out of the way, we can finally build
some actual code.

At present, the tool assumes a few files to be in hardcoded places, so while
you could run the tool as a standalone project, we'll just do everything in the
`mercurous` directory.

To run the cinnabar cli, call `cargo run`. You can give it additional arguments, which
are described by the `--help` flag. Note the `--` before it, otherwise you will pass arguments to `cargo`.

```
$ cargo run -- --help
cinnabar 0.1.0
LLVM frontend for cinnabar

USAGE:
    cinnabar [OPTIONS] <input>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
        --ident-map <ident-map>                Where to output a mapping between generated identifiers and original
                                               code. (relative to outdir)
        --limestone-graph <limestone-graph>    Path to put graphviz graph visualisation of limestone structure if
                                               desired (relative to outdir)
    -o <outdir>                                 [default: build]
    -c <output-cpp>                            Name of the output cpp and hpp files. (Without extension)
        --unlowered-graph <unlowered-graph>    Path to put graphviz graph visualisation of limestone structure before
                                               lowering if desired (relative to outdir)

ARGS:
    <input>
```

Let's look at a usage example. Start by creating a new file called `code.cpp` with the following
function which performs one dimensional interpolation.

```cpp
constexpr int nx = 7;
constexpr double x[] = { 0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0 };
const double y[] = { 0., 1., 1.5, 1.75, 2., 3., 2.  };
constexpr double xmin = x[0];
constexpr double xmax = x[nx-1];
constexpr double dx = x[1] - x[0];

double interp1equid(double xp) {
    declare_bounds(xp, -1, 4);
    if( xp < xmin || xp >= xmax ) {
        return 0;
    }
    else {
        int i = limestone_trunc((xp - xmin) / dx);
        double ylow = y[i];
        double yhigh = y[i+1];
        double xstep = (xp-x[i])/dx;
        return ylow + (yhigh - ylow) * xstep;
    }
}

```

Then build it using cinnabar (in the mercurous directory, execute):

```
cargo run -- code.cpp -c code -w 12 --cinnabar-root ..
```

The `-c` flag specifies the name of the genrated hpp and cpp and verilog files,
along with the name of the resulting function. The `w` flag specifies the
amount of bits to use for fractional values, in this case 12.

This should give you a `build` directory which has a few `.bc` and `.ll` files along with
a directory called `code`. The `code` is the most interesting as it contains the generated
C++ code which can be used to simulate the FPGA model on a CPU.

Inside, you will find `build/code/code.v` which along with the lookup
table verilog is the primary output of the tool and can be simulated using a
verilog simulator of choice.

You will also find some C++ files which can be used for testing and simulation
of the resulting hardware. The main files are `build/code/code.cpp` and
`build/code/code.hpp`. There are also a few additional files which are used to
store look up table information, in this case the `x` and `y` arrays.

Reading `build/code/code.hpp` we see 2 structs and a function.

```cpp
#pragma once
#include <fixedpoint.hpp>
#include <functions.hpp>
#include <interpolation.hpp>
#include "luts.hpp"
struct code_output {
	Fp<35> __0;
};
struct code_input {
	Fp<35> xp;
};
code_output code (code_input __input);
```

If we want to check the behaviour of the code, we can do something like this:

```cpp
#include <iostream>

#include <code.hpp>

int main() {
    // Check the interpolation result at 20 points
    for(std::size_t i = 0; i < 20; i++) {
        // The interpolation map is defined for values between 0 and 3, let's test
        // a few out of bounds values as well, so -1 to 4
        float x = (i / 20.) * 5 - 1;

        code_input input {.xp = x};

        auto result = code(input).__0;

        std::cout << x << " -> " << result.as_float() << std::endl;
    }
}
```

Everything can be compiled together with this quite long command (this is going to be improved at some point)
```bash
clang++ build/code/*.cpp main.cpp -Ibuild/code -I../yafp/src/ -I../src -I../affine-arith/ --std=c++17
```
and the result can be executed with
```bash
./a.out
```
which should give an output like this:
```
-1 -> 0
-0.75 -> 0
-0.5 -> 0
-0.25 -> 0
0 -> 0
0.25 -> 0.5
0.5 -> 1
0.75 -> 1.25
1 -> 1.5
1.25 -> 1.625
1.5 -> 1.75
1.75 -> 1.875
2 -> 2
2.25 -> 2.5
2.5 -> 3
2.75 -> 2.5
3 -> 0
3.25 -> 0
3.5 -> 0
3.75 -> 0
```

(NOTE: the values after 3.0 should be 0, the cause of this is being investigated)






## Publications

Developed at the Department of Electrical Engineering at Linköping University.

- [High Level Synthesis for Optimising Hybrid Electric Vehicle Fuel Consumption Using FPGAs and Dynamic Programming](http://liu.diva-portal.org/smash/record.jsf?pid=diva2%3A1327672&dswid=-8427)


## CI setup

To improve build times in CI, a custom docker image (`thezoq2/cinnabar-ci`) is used. The corresponding dockerfile is in the root of the repo.

To update it:

```
docker build --tag cinnabar:cinnabar-ci --file dockerfile .
```

Tag it

```
docker tag <ID> accountname/cinnabar-ci:latest
```

where `<ID>` can be found using `docker images`

Finally, pubhlish it:

docker push `accountname/cinnabar-ci`


