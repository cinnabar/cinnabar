test: .PHONY
	@COMPILETYPE=test $(MAKE) -s -f build.mk
	@echo -e "[\033[0;34mtest\033[0m] Running tests"
	@$(DEBUGGER) ./build/test/testbin "$(TEST)"

ffi_debug:
	@COMPILETYPE=debug $(MAKE) -s -f build.mk build/debug/ffi.o

ffi_release:
	@COMPILETYPE=opt $(MAKE) -s -f build.mk build/opt/ffi.o

lib: .PHONY


IVERILOG_FLAGS=-g2012 -Wall -gstrict-expr-width

check_sqrt_module: .PHONY
	@./scripts/build_sqrt.hs build/sqrt.v 8 2 2 True
	@echo -e "[\033[0;34mverilator\033[0m] Linting"
	@vcheck build/sqrt.v
	@echo -e "[\033[0;34miverilog\033[0m] Building"
	@iverilog \
		-o build/sqrt_tb \
		-s sqrt_tb \
		${IVERILOG_FLAGS} \
		-DVCD_OUTPUT=\"output/div.vcd\" \
		build/sqrt.v
	@vvp build/sqrt_tb

RUST_TBS=$(patsubst %.rs, build/%_rs, $(wildcard verilog/tb/*.rs))

check_verilog_modules: ${RUST_TBS} .PHONY

build/verilog/tb/%_rs: verilog/tb/%.rs .PHONY
	@echo -e "[\033[0;34mrust-script\033[0m] building tb runner"
	$< -o -- $@ $(patsubst %, -A%, ${IVERILOG_FLAGS})

clean:
	rm -rf build/* ${VERILOG_DIR}
	make -C affine-arith clean

.PHONY:
