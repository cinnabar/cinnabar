#ifndef H_CODEGEN
#define H_CODEGEN

#include <vector>
#include <map>
#include <filesystem>

#include "node.hpp"
#include "exception_util.hpp"

namespace codegen {
    // Thrown when the code generator encounters multiple nodes sharing the same name
    // in the graph
    class DuplicateNodeError : std::exception {
        public:
            using ValueType = std::vector<std::pair<std::string, std::vector<std::string>>>;

            DuplicateNodeError(ValueType collisions) : collisions{collisions} {
                message.clear();
                message += "Duplicate nodes detected\n";
                message += "Duplicate nodes, grouped by which output first revealed them\n";
                for (auto [cause, node_list] : collisions) {
                    message += " - " + cause + "\n";
                    for(auto n : node_list) {
                        message += "   - " + n + "\n";
                    }
                }
            }

            const char* what() const noexcept override {
                return message.c_str();
            }

        private:
            // The name collisions that occurred, grouped by at which node insertion
            // they were discovered.
            ValueType collisions;

            std::string message;
    };
}

std::vector<std::string> validation_delay(
    std::string in_validity_name,
    std::string out_validity_name,
    int depth
);

struct CppCode {
    std::vector<std::string> hpp;
    std::vector<std::string> cpp;
};

class Codegen {
    public:
        Codegen(std::map<std::string, Rc<Node>> nodes, WordLength wl);

        std::vector<std::string> generate_code(WordLength word_lengths);
        std::vector<std::string> generate_module(
            std::string name,
            WordLength word_lengths,
            bool compress_inputs
        );
        /*
          Runs the commands returned by the extra_verilog_commands function in
          each node. No internal order between the command lists is guaranteed,
          and duplicate commands are only run once.

          The working directory of the commands is the current working directory,
          and cinnabar_root is appended to the command paths
        */
        void run_verilog_extra_commands(
            WordLength wl,
            std::string cinnabar_root,
            std::filesystem::path output_dir
        ) const;

        std::vector<std::string> list_variables();

        std::vector<std::string> variable_length_defines();

        std::vector<std::string> generate_cpp_body(WordLength word_lengths, CppDataType);
        CppCode generate_cpp_function(std::string name, WordLength word_lengths, CppDataType t);

    private:
        // Generate code for input in the argument list
        std::vector<std::string> input_arguments();
        // Generates the definitions for the input variables
        std::vector<std::string> input_definitions(WordLength wl);

        // Generates the argument list for the input iterator
        std::vector<std::string> input_iterator_arguments();
        // Generates code for the input iterator for input compression
        std::vector<std::string> input_iterator_code(WordLength wl);

        int output_depth(WordLength wl);
        std::set<Weak<Node>> nodes;

        void add_node(Weak<Node> node);

        std::map<std::string, Rc<Node>> outputs;

        std::vector<Node*> sort_nodes();

        WordLength const wl;
};


#endif
