#include "types.hpp"

struct TestConfig {
    static bool constexpr SINGLE_1D_NODE_INTERP = false;
    static bool constexpr SINGLE_2D_NODE_INTERP = false;
};
