#include "log.hpp"

std::mutex _log_mutex;

std::string log_string(const LogLevel level) {
    std::string inner;
    switch (level) {
        case LogLevel::DEBUG:
            inner = "\033[0;37mdebug";
            break;
        case LogLevel::INFO:
            inner = "\033[0;35minfo";
            break;
        case LogLevel::WARN:
            inner = "\033[0;93mwarn";
            break;
        case LogLevel::ERR:
            inner = "\033[0;91merr";
            break;
    }
    return "[" + inner + "\033[0m]";
}

void _log() {}
