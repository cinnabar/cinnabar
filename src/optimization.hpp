#pragma once

#include "rc.hpp"
#include "macro_util.hpp"

#include <optional>

#include <functional>
#include <map>

// #define CASE_STRING(x) case OptPass::x: return #x;
#define PASS_NAME(x) [(int)OptPass::x] = #x

// Defines an enum class with the specified members, along with
// a function for getting a string representing the name of the opt pass.
#define OptPasses(...) \
    enum class OptPass { \
        __VA_ARGS__\
    }; \
    constexpr char const* opt_pass_name_map[] = { \
        MAP_LIST(PASS_NAME, __VA_ARGS__) \
    }; \
    constexpr char const* opt_pass_name(const OptPass pass) { \
        return opt_pass_name_map[(int) pass]; \
    } \

OptPasses(
    // Constant folding
    ConstantFold,
    // Remove comparisons where it is statically known that
    // they will not be true or false
    RemoveRedundantComparisons,
    // Removes interpolation validity nodes where it is known beforehand
    // that the node is always valid
    RemoveRedundantInterpValidity,
    // Removes redundant bool operations. I.e. `a && true` is simplified to `a`,
    // a || true to `true` etc.
    RemoveRedundantBoolComputations,
    // Replaces x/a with x * 1/a when a is constant
    ConstantDivisionReplacement,
    // Merge boolean operations with only boolean inptus into a MergedBool node
    // Primarily used as a debug thing as MergedBool has no codegen
    MergeBools,
    // Replace the else if of final conditions in Select statements with an else
    SelectElseLast,
    // Add a final else branch to Select nodes where the output is `X`
    SelectElseX,
    // Remove select nodes which only have one branch, and replace them with the value
    // of that branch
    RemoveSingleSelect
)

#undef OptPasses
#undef PASS_NAME


class Node;
class FracNode;

template<typename T>
void maybe_replace(Rc<T>* parent, std::optional<Rc<Node>> replacement) {
    if (replacement.has_value()) {
        *parent = replacement.value().pointer_cast<T>().value();
    }
}


template<typename T>
void optimize_parents(OptPass pass, Rc<T>* parent) {
    maybe_replace(parent, (*parent)->optimize(pass));
}

template<typename T, typename... Args>
void optimize_parents(OptPass pass, T first, Args... rest) {
    optimize_parents(pass, first);
    optimize_parents(pass, rest...);
}


struct OptLogger {
    static std::map<OptPass, int> counter;
};
