#include "codegen.hpp"

#include "util.hpp"
#include "rc.hpp"
#include "log.hpp"
#include "lookup_table.hpp"

#include <algorithm>
#include <utility>
#include <cstdlib>

using util::indent
    , util::verilog_variable
    , util::indent_lines
    , util::sign_extend
    , util::reset_or_value
    , util::merge_code
    , util::register_code
    , util::clocked_block;

/*
  Sets the specified nodes as outputs.

  During construction, a check is made for duplicate nodes as these indicate
  nasty internal bugs. If this checks fail, a codegen::DuplicateNodeError is thrown

  This computes the required delay slots and adds all nodes to the codegen for
  generation
*/
Codegen::Codegen(std::map<std::string, Rc<Node>> nodes, WordLength wl) 
{
    bool found_duplicates = false;
    std::vector<std::pair<std::string, std::vector<std::string>>> duplicates;

    std::vector<Rc<Node>> only_nodes;

    for(auto n : nodes) {
        only_nodes.push_back(n.second);
        this->nodes.merge(n.second->unique_ancestors());
        this->nodes.insert(n.second.weak());

        // info("Adding ", n.second->base_name());
        std::set<std::string> unique_nodes;
        duplicates.push_back({n.second->base_name(), {}});
        for(auto n : this->nodes) {
            auto name = n.lock()->base_name();
            if(unique_nodes.count(name) > 0) {
                found_duplicates = true;
                duplicates.back().second.push_back(name);
            }
            unique_nodes.insert(name);
        }
    }

    if(found_duplicates) {
        throw codegen::DuplicateNodeError{duplicates};
    }

    this->outputs = nodes;
    node::assign_delay_slots(only_nodes, this->output_depth(wl) + 1, wl);
}

/*
  Generates the verilog code for each node
*/
std::vector<std::string> Codegen::generate_code(WordLength wl) {
    // Since verilog is such a nice and friendly language, it declares variables
    // passed as inputs or outputs to modules as they appear. Then it gets upset
    // when the actual variable is declared later -.-
    // This means that it generally doesn't care about variable order, except for when
    // it does
    // To remedy that, we have to sort the variables
    auto sorted_nodes = this->sort_nodes();

    std::vector<std::string> result;
    for(auto node : sorted_nodes) {
        // Get the code for the node and insert it into the result
        auto code = node->get_code(wl);
        result.insert(result.end(), code.begin(), code.end());
        auto pipeline = node->generate_pipeline_code(wl);
        result.insert(result.end(), pipeline.begin(), pipeline.end());
    }

    return result;
}


std::vector<std::string> Codegen::input_arguments() {
    std::vector<std::string> result;
    for(auto node_ : nodes) {
        auto node = node_.lock();
        if(node->is_input()) {
            result.push_back(indent(2, ", " + node->base_name() + "_non_reg"));
        }
    }
    return result;
}

std::vector<std::string> Codegen::input_definitions(WordLength wl) {
    std::vector<std::string> result;
    for(auto node_ : nodes) {
        auto input = std::dynamic_pointer_cast<Input>(node_.lock());
        if(input != nullptr) {
            result.push_back(indent(1, input->variable_definition(wl)));
        }
    }
    return result;
}

std::vector<std::string> Codegen::input_iterator_arguments() {
    return {indent(2, ", input_value")};
}

std::vector<std::string> Codegen::input_iterator_code(WordLength wl) {
    std::vector<std::string> result;

    int input_var_size = 0;
    std::size_t input_amount = 0;
    // Define all inputs as local variables
    for(auto node_ : nodes) {
        auto node = node_.lock();
        if(node->is_input()) {
            input_var_size = std::max(input_var_size, node->register_size(wl));
            result.push_back(indent(1, verilog_variable(
                VarType::REG,
                node->base_name() + "_non_reg",
                node->register_size(wl)
            )));
            // result.push_back(indent(2, "input" + node->base_name() + "_non_reg"));
            input_amount++;
        }
    }
    // Define the true input variable
    result.push_back(indent(1, verilog_variable(VarType::INPUT, "input_value", input_var_size)));
    result.push_back(indent(
        1,
        verilog_variable(VarType::REG, "input_iterator", ceil(log2(input_var_size)))
    ));
    // Add a state machine to set inputs from a single input node
    int i = 0;
    std::vector<std::string> input_case;
    input_case.push_back("case (input_iterator)");
    for(auto node_ : nodes) {
        auto node = node_.lock();
        if(node->is_input()) {
            auto var_name = node->base_name() + "_non_reg";
            input_case.push_back(std::to_string(i) + ": " + var_name + " <= input_value;");
            i++;
        }
    }
    // input_case.push_back(indent(1, "default:"));
    input_case.push_back("endcase");

    std::vector<std::string> input_it = {
        "if(rst) begin",
        indent(1, "input_iterator <= 0;"),
        "end else if(input_iterator == " + std::to_string(input_amount) + ") begin",
        indent(1, "input_iterator <= 0;"),
        "end else begin",
        indent(1, "input_iterator <= input_iterator + 1;"),
        "end"
    };

    auto input_generator = indent_lines(clocked_block(merge_code({input_case, input_it})));

    return merge_code({result, input_generator});
}

/*
  Generates a module for the current nodes. Input type nodes will be added
  as inputs to the model and the specified outputs will be outputs.
*/
std::vector<std::string> Codegen::generate_module(
    std::string name,
    WordLength wl,
    bool compress_inputs
) {
    // Generate the module keyword and name
    std::vector<std::string> result = {
        "`include \"divider.sv\"",
        "`include \"interp1d.v\"",
        "module " + name
    };

    // Add clock input
    result.push_back(indent(2, "( clk"));
    result.push_back(indent(2, ", rst"));
    result.push_back(indent(2, ", input_valid"));
    result.push_back(indent(2, ", output_valid"));
    // Add other inputs
    if(!compress_inputs) {
        result = merge_code({result, input_arguments()});
    }
    else {
        result = merge_code({result, input_iterator_arguments()});
    }

    // Add outputs
    for(auto output : outputs) {
        result.push_back(indent(2, ", " + output.first));
    }
    // Add end of parameter declaration
    result.push_back(indent(2, ");"));

    // Declare types of outputs
    for(auto [name, node] : outputs) {
        // Add the output keyword and name
        result.push_back(
            indent(1, verilog_variable(VarType::OUTPUT, name, node->register_size(wl)))
        );
        // Addign the variable containing the output value to the actual output
        auto var_name = node->pipeline_buffer_name(
            output_depth(wl) - node->end_depth(wl)
        );
        result.push_back(indent(
            1,
            "assign " + name + " = " + var_name + ";"
        ));
    }


    // Add output validity output
    result.push_back(indent(1, "output output_valid;"));
    // Declare the clock and reset lines as input
    result.push_back(indent(1, "input clk;"));
    result.push_back(indent(1, "input rst;"));
    // Add input validity input
    result.push_back(indent(1, "input input_valid;"));

    if(!compress_inputs) {
        result = merge_code({result, input_definitions(wl)});
    }
    else {
        result = merge_code({result, input_iterator_code(wl)});
    }





    // Generate the code for all the nodes
    auto code = indent_lines(this->generate_code(wl));
    result.insert(result.end(), code.begin(), code.end());

    auto pl_validity_code = indent_lines(
        validation_delay("input_valid", "output_valid", output_depth(wl))
    );
    result.insert(result.end(), pl_validity_code.begin(), pl_validity_code.end());
    // Add endmodule
    result.push_back("endmodule");

    result.push_back("`define " + name + "_DEPTH " + std::to_string(output_depth(wl)));

    return result;
}

void Codegen::run_verilog_extra_commands(
    WordLength wl,
    std::string cinnabar_root,
    std::filesystem::path output_dir
) const {
    std::set<std::vector<std::string>> command_sequences = {};
    for(auto&& node_ : nodes) {
        if(auto node = node_.lock()) {
            auto commands = node->verilog_extra_commands(wl);
            command_sequences.insert(commands);
        }
        else {
            err("Attempted to find verilog_extra_commands from \
                    an expired Node weak pointer");
        }
    }

    auto prev_wd = std::filesystem::current_path();
    std::filesystem::create_directory(output_dir);
    std::filesystem::current_path(output_dir);

    std::vector<std::vector<std::string>> command_seq_vec;
    std::copy(
        command_sequences.begin(),
        command_sequences.end(),
        std::back_inserter(command_seq_vec)
    );
    // TODO: Re-enable once omp works with rust linking
    // #pragma omp parallel for
    for(std::size_t i = 0; i < command_seq_vec.size(); i++) {
        auto&& seq = command_seq_vec[i];
        for(auto&& command : seq) {
            auto full_command = cinnabar_root + "/" + command;
            locked_info("Executing ", full_command.substr(0, 70));
            // NOTE: Exit statis is implementation defined. This might fail
            // on non-linux/unix systems, maybe.
            int exit_status = std::system((prev_wd / full_command).c_str());
            if(exit_status) {
                locked_err("Subcommand failed with status ", exit_status);
                exit(-1);
            }
        }
    }
    std::filesystem::current_path(prev_wd);
}

int Codegen::output_depth(WordLength wl) {
    int depth = outputs.begin()->second->end_depth(wl);
    for(auto n : outputs) {
        depth = std::max(depth, n.second->end_depth(wl));
    }
    return depth;
}

std::vector<std::string> Codegen::list_variables() {
    std::vector<std::string> result;
    for(auto node : nodes) {
        result.push_back(node.lock()->base_name());
    }
    return result;
}


// C++ Codegen

std::vector<std::string> Codegen::generate_cpp_body(WordLength wl, CppDataType t) {
    auto sorted_nodes = this->sort_nodes();

    std::vector<std::string> result;
    for(auto node : sorted_nodes) {
        auto code = node->get_cpp(wl, t);
        result.insert(result.end(), code.begin(), code.end());
    }

    return result;
}

CppCode Codegen::generate_cpp_function(
    std::string name,
    WordLength wl,
    CppDataType t
) {
    // Preprocessor directives
    std::vector<std::string> hpp_result = {
        "#pragma once",
    };

    if(LookupTable::lut_count() != 0) {
        hpp_result = merge_code({hpp_result, {"#include \"luts.hpp\""}});
    }

    switch (t) {
        case CppDataType::Fp:
            hpp_result = merge_code({hpp_result, {
                "#include <fixedpoint.hpp>",
                "#include <functions.hpp>",
                "#include <interpolation_impl.hpp>",
            }
            });
            break;
        case CppDataType::Ap: {
            hpp_result = merge_code({hpp_result, {
                "#include <ap_fixed.h>",
                "#include <hls_math.h>",
                "",
                // Note: The file being included here in the benchmarks is not
                // the file from this repo, but from the benchmarks/hev_model
                "#include \"interpolation_impl.hpp\""
            }});
            break;
          }
        case CppDataType::Ac:
            hpp_result = merge_code({hpp_result, {
                "#include <ac_fixed.h>"
            }});
            break;
        case CppDataType::Float:
             hpp_result = {};
            break;
    }

    // Define the output struct
    auto output_struct_name = name + "_output";
    hpp_result.push_back("struct " + output_struct_name + " {");
    for(auto [name, node] : outputs) {
        hpp_result.push_back(indent(1, node->cpp_datatype(wl, t) + " " + name + ";"));
    }
    hpp_result.push_back("};");


    // Find inputs
    std::vector<std::shared_ptr<FracNode>> inputs;
    for(auto node_ : nodes) {
        auto node = node_.lock();
        if(node->is_input()) {
            auto casted = std::dynamic_pointer_cast<FracNode>(node);
            inputs.push_back(casted);
        }
    }

    // Define the input struct
    auto input_struct_name = name + "_input";
    hpp_result.push_back("struct " + input_struct_name + " {");
    for (auto input : inputs) {
        auto datatype = cppcode::fixed_point_type(input->integer_bits(wl), input->fractional_bits(wl), t);
        hpp_result.push_back(indent(1, datatype + " " + input->base_name() + ";"));
    }
    hpp_result.push_back("};");

    // Include the header file
    std::vector<std::string> cpp_result = {
        "#include \"" + name + ".hpp\""
    };


    std::vector<std::string> function_head = {};

    std::string input_instance_name = "__input";

    function_head.push_back(
        output_struct_name + " " + name + " (" + input_struct_name + " " + input_instance_name + ")"
    );

    // GDB does not allow running until a function returns, only until after it has returned
    // To set a breakpoint at the end of the function using a script, we'll add a dummy function
    // at the end of the real function. Here we can set a breakpoint, then run until 'finish'
    // to end up at the end of the function
    std::vector<std::string> trampoline_header = {
        "__attribute__((optnone)) void __gdb_trampoline() {}"
    };
    cpp_result.insert(cpp_result.end(), trampoline_header.begin(), trampoline_header.end());

    // Insert the function head to both files
    hpp_result.insert(hpp_result.end(), function_head.begin(), function_head.end());
    cpp_result.insert(cpp_result.end(), function_head.begin(), function_head.end());
    // Add the closing brace and ; to the header and { to the cpp
    cpp_result.back() += " {";
    hpp_result.back() += ";";

    // Turn on pipelining in vivado
    cpp_result.push_back("#pragma HLS pipeline II=1");

    for(auto input : inputs) {
        auto name = input->base_name();
        auto datatype = cppcode::fixed_point_type(input->integer_bits(wl), input->fractional_bits(wl), t);
        cpp_result.push_back(indent(
            1,
            datatype + " " + name + " = " + input_instance_name + "." + name + ";"
        ));
    }

    // Generate the code for all the nodes
    auto code = indent_lines(this->generate_cpp_body(wl, t));
    cpp_result.insert(cpp_result.end(), code.begin(), code.end());

    cpp_result.push_back(indent(1, "__gdb_trampoline();"));
    cpp_result.push_back(indent(1, "return " + output_struct_name + " {"));
    for(auto output : outputs) {
        auto [name, node] = output;
        cpp_result.push_back(indent(2, "." + name + " = " + node->base_name() + ","));
    }
    cpp_result.push_back(indent(1, "};"));
    cpp_result.push_back("}");

    CppCode result;
    result.cpp = cpp_result;
    result.hpp = hpp_result;
    return result;
}


// Private functions
/*
  Sort the nodes in computation order
*/
std::vector<Node*> Codegen::sort_nodes() {
    std::vector<Node*> result;
    for(auto [_name, output] : outputs) {
        output->visit_with_cache<bool>([&result] (auto node) {
            if(std::find(result.begin(), result.end(), node) == result.end()) {
                result.push_back(node);
            }
            return true;
        });
    }
    return result;
    // std::vector<Weak<Node>> result;
    // for(auto node : nodes) {
    //     result.push_back(node);
    // }
    // std::sort(result.begin(), result.end(), [](auto a, auto b){
    //     return a.lock()->start_depth() < b.lock()->start_depth();
    // });
    // return result;
}

// Free functions
std::vector<std::string> validation_delay(
    std::string in_validity_name,
    std::string out_validity_name,
    int depth
) {
    if(depth == 1) {
        return merge_code({
            {register_code("pl_validity_buffer", depth)},
            clocked_block(reset_or_value("pl_validity_buffer", "0", in_validity_name)),
            {"assign " + out_validity_name + " = pl_validity_buffer;"}
        });
    }
    else {

    }

    std::string value =
                "{pl_validity_buffer["
                + std::to_string(depth-2)
                + ":0], "
                + in_validity_name
                + "}";

    return merge_code({
        {register_code("pl_validity_buffer", depth)},
        clocked_block(reset_or_value("pl_validity_buffer", "0", value)),
        { "assign " + out_validity_name + " = "
        + "pl_validity_buffer[" + std::to_string(depth - 1) + "];"
        }
    });
}
