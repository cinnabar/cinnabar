#ifndef H_GRAPH
#define H_GRAPH

#include "node.hpp"

namespace graph {
    using ValueFn = std::function<std::string(Node*)>;
    using ColorFn = std::function<std::optional<std::string>(Node*)>;

    void dump_graph(
        std::string dir,
        Rc<Node> root,
        WordLength,
        bool include_delay
    );
    void dump_graph(
        std::string dir,
        std::string filename,
        Rc<Node> root,
        graph::ValueFn fn,
        WordLength,
        bool include_delay
    );
    void dump_graph(
        std::string dir,
        std::string filename,
        Rc<Node> root,
        ValueFn fn,
        ColorFn color,
        WordLength,
        bool include_delay
    );
    void dump_bounds(std::string dir, Rc<Node> root, WordLength);
    std::vector<std::string> generate_graph(
        Rc<Node> root,
        ValueFn fn,
        WordLength,
        bool include_delay
    );
    std::vector<std::string> generate_graph(
        Rc<Node> root,
        ValueFn fn,
        ColorFn color_fn,
        WordLength,
        bool include_delay
    );

    ColorFn value_color(std::function<std::optional<double>(Node*)> value);
};



#endif
