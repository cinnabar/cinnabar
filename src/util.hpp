#ifndef H_UTIL
#define H_UTIL

#include <vector>
#include <string>
#include <cmath>
#include <optional>
#include <cxxabi.h>
#include <fstream>
#include <streambuf>

#include "config.hpp"
#include "runtime_config.hpp"
#include "exception_util.hpp"


#define MAYBE_RETURN(...) \
    { \
        auto __RESULT__ = __VA_ARGS__; \
        if(__RESULT__.has_value()) { \
            return __RESULT__; \
        } \
    }

// Different types of variables
enum class VarType {
    REG,
    INPUT,
    OUTPUT,
    LOCALPARAM,
    WIRE,
};

// Cpp codegen util
namespace cppcode {
    std::string fixed_point_type(int int_bits, int fractional_bits, CppDataType t);
    std::string cast_value(std::string value, int int_bits, int fractional_bits, CppDataType t);
}


namespace util{

    struct __unimplemented : std::exception {
        __unimplemented(std::string msg) : std::exception{}, msg{msg} {}
        std::string msg;
        WITH_MESSAGE(msg)
    };

    struct file_write_error : std::exception {
        file_write_error(std::string file, std::string desc)
            : std::exception{}, file{file}, desc{desc}
        {}
        std::string file, desc;
        WITH_MESSAGE("Failed to write to ", file, ". ", desc)
    };


    // Returns the keyword used to delare a variable with the specified type
    std::string var_type_name(VarType type);

    // Adds `amount` indentation levels to the specified line
    std::string indent(std::size_t amount, std::string line);
    // Adds one level of indentation to all the specified lines

    std::vector<std::string> indent_lines(std::vector<std::string> lines);
    // Merge a vector of lines to a string with newlines

    std::string join(std::vector<std::string> lines);
    // Returns a verilog "variable" with the specified type, name and amount of bits
    std::string verilog_variable(VarType type, std::string name, std::size_t size);

    // Generates verilog code for a register with the specified name and size.
    std::string register_code(std::string name, std::size_t size);

    // Surrounds the specified block of code in `always @(posedge clk)`
    std::vector<std::string> clocked_block(std::vector<std::string> content);

    // Merges several vectors of lines into a single vector of lines
    std::vector<std::string> merge_code(const std::vector<std::vector<std::string>> code_segments);

    // Generates code for a verilog if statement
    std::vector<std::string> if_statement(
        const std::string condition,
        const std::vector<std::string> true_branch,
        const std::vector<std::string> false_branch
    );

    // Generate code to reset reg_name if rst is high and set it to value otherwise
    std::vector<std::string> reset_or_value(
        std::string reg_name,
        std::string reset_value,
        std::string value
    );

    // TODO: Move more functions into this namespace
    namespace verilog {
        // TODO: Use this function in more places
        std::string sized_constant(int bits, int64_t value);

        std::string int_parameter(std::string const& name, int64_t const value);

        // A verilog module parameter. I.e. `.<name>(std::to_string(<value>))`
        // Requires `std::to_string(T)` to be valid
        template<typename T>
        std::string parameter(std::string const& name, T const value) {
            return parameter(name, value);
            return "." + name + "(" + std::to_string(value) + ")";
        }
        // We need to specialise these templates because aparently, the C++ standards
        // committee in their infinite wisdom decided that std::to_string should not be
        // be implemented for std::string or char*.
        //
        // Inline is required to not violate the ODR rule
        template<>
        inline std::string parameter(std::string const& name, std::string const value) {
            return "." + name + "(" + value + ")";
        }
        template<>
        inline std::string parameter(std::string const& name, const char* const value) {
            return "." + name + "(" + value + ")";
        }
    }

    // Sign extend the content of a register if the original size is smaller than the extended.
    // Otherwise the original register is returned unmodified
    std::string sign_extend(std::string reg, int original_size, int extended_size);


    ExecutionType approximate_log2(ExecutionType input);
    ExecutionType approximate_pow2(ExecutionType input);


    // Writes the specified contents to the specified file if the file does not
    // already contain that content. Returns true if the content was changed,
    // false if not
    bool write_if_changed(std::string filename, std::string content);


    template<typename T>
    [[nodiscard]] std::optional<std::string> compare_types(const T* x, const T* y) {
        auto& self_type = typeid(*x); \
        auto& other_type = typeid(*y); \
        int _status; \
        auto demangled_self = __cxxabiv1::__cxa_demangle(self_type.name(), 0, 0, &_status); \
        auto demangled_other = __cxxabiv1::__cxa_demangle(other_type.name(), 0, 0, &_status); \
        if(self_type != other_type) { \
            return std::string("Incorrect type, expected: ") + demangled_self + ", got: " + demangled_other; \
        } \
        return std::nullopt;
    }


    template<typename T>
    std::string tuple_string(std::string const name, T const& t) {
        return "'(\"" + name + "\"," + std::to_string(t) + ")'";
    }

    void log_config_flags(Config const& cfg);


    template<typename T, typename F>
    class copyable {
        public:
            copyable() = delete;

            copyable(const T& inner)
                : inner{F::copy(inner)}
            {}

            copyable(const copyable& other)
                : inner{F::copy(other.inner)}
            {}

            const copyable& operator=(const copyable& other) {
                this->inner = F::copy(other.inner);
                return *this;
            }

            T& operator*() {
                return inner;
            }

            T inner;
    };

}

#define UNIMPLEMENTED \
    if(1) {\
        std::string __message; \
        __message += __FILE__; \
        __message += ":"; \
        __message += std::to_string(__LINE__); \
        __message += ": reached unimplemented code"; \
        throw util::__unimplemented{ __message }; \
    }

#define TODO(...) \
    if(1) {\
        std::string __message; \
        __message += __FILE__; \
        __message += ":"; \
        __message += std::to_string(__LINE__); \
        __message += ": reached todo '" __VA_ARGS__ "'"; \
        throw util::__unimplemented{ __message }; \
    }


#define UNIMPLEMENTED_VIRTUAL \
    std::string __message; \
    __message += __PRETTY_FUNCTION__; \
    __message += " is unimplemented"; \
    __message += "\n\tin subclass "; \
    auto& self_type = typeid(*this); \
    int _status; \
    auto demangled_self = __cxxabiv1::__cxa_demangle(self_type.name(), 0, 0, &_status); \
    __message += demangled_self; \
    throw __message;

#endif
