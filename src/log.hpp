#pragma once
#include <iostream>
#include <mutex>

extern std::mutex _log_mutex;

enum class LogLevel {
    DEBUG,
    INFO,
    WARN,
    ERR,
};

std::string log_string(const LogLevel level);

void _log();
template<typename T, typename... TS>
void _log(T t, TS... ts) {
    std::cout << t;
    _log(ts...);
}

#define ADD_LOG_LEVEL(NAME, LEVEL) \
    template<typename... TS> \
    void NAME(TS... ts) { \
        std::cout << log_string(LogLevel::LEVEL) << " "; \
        _log(ts...); \
        std::cout << std::endl; \
    } \
    template<typename... TS> \
    void locked_##NAME(TS... ts) { \
        const std::lock_guard<std::mutex> lock(_log_mutex); \
        NAME(ts...); \
    }


ADD_LOG_LEVEL(debug, DEBUG)
ADD_LOG_LEVEL(info, INFO)
ADD_LOG_LEVEL(warn, WARN)
ADD_LOG_LEVEL(err, ERR)

