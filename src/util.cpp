#include "util.hpp"

#include "log.hpp"

#include <cassert>

using namespace util;

std::vector<std::string> util::indent_lines(std::vector<std::string> lines) {
    std::vector<std::string> result;
    for(auto line: lines) {
        result.push_back(indent(1, line));
    }
    return result;
}

std::string util::join(std::vector<std::string> lines) {
    std::string result;
    for(auto str : lines) {
        result += str + "\n";
    }
    return result;
}


std::string util::var_type_name(VarType type) {
    switch (type) {
        case VarType::INPUT: return "input";
        case VarType::LOCALPARAM: return "localparam";
        case VarType::OUTPUT: return "output";
        case VarType::REG: return "reg";
        case VarType::WIRE: return "wire";
    }
    // This can never be reached because the switch above covers all
    // cases. Sadly, g++ is too stupid to understand this
    throw "g++ is stupid exception";
}

std::string util::indent(std::size_t amount, std::string line) {
    std::string result = "";
    for(std::size_t i = 0; i < amount; ++i) {
        result += "\t";
    }
    return result + line;
}

std::string util::verilog_variable(VarType type, std::string name, std::size_t size) {
    // The [n-1:0] part of the declaration
    auto length_specifier = size != 0 ? "[" + std::to_string(size-1) + ":0]" : "";
    if(size == 1) {
        length_specifier = "";
    }
    return var_type_name(type) + length_specifier + " " + name + ";";
}

std::string util::register_code(std::string name, std::size_t size) {
    return verilog_variable(VarType::REG, name, size);
}

std::vector<std::string> util::clocked_block(std::vector<std::string> content) {
    auto indented = indent_lines(content);
    // Create the always section
    std::vector<std::string> result = {"always @(posedge clk) begin"};
    // Add the content
    result.insert(result.end(), indented.begin(), indented.end());
    // Add end keyword
    result.push_back("end");
    return result;
}

/*
  Concatenates two vectors of strings
*/
std::vector<std::string> util::merge_code(
    const std::vector<std::vector<std::string>> code_segments
) {
    std::vector<std::string> result;
    for(auto segment : code_segments) {
        result.insert(result.end(), segment.begin(), segment.end());
    }
    return result;
}

std::vector<std::string> util::reset_or_value(
    std::string reg_name,
    std::string reset_value,
    std::string value
) {
    return if_statement(
        "rst == 1",
        {reg_name + " <= " + reset_value + ";"},
        {reg_name + " <= " + value + ";"}
    );
}


std::vector<std::string> util::if_statement(
    const std::string condition,
    const std::vector<std::string> true_branch,
    const std::vector<std::string> false_branch
) {
    return merge_code({
        {"if(" + condition + ") begin"},
        indent_lines(true_branch),
        { "end"
        , "else begin"
        },
        indent_lines(false_branch),
        {"end"}
    });
}


std::string util::sign_extend(std::string reg, int original_size, int extended_size) {
    if(original_size < extended_size) {
        return "{{" + std::to_string(extended_size - original_size)
            + "{" + reg + "[" + std::to_string(original_size - 1) + "]}}, " + reg + "}";
    }
    else {
        return reg;
    }
}



bool util::write_if_changed(std::string filename, std::string content) {
    {
        // read the file to get the previous content
        std::ifstream current_file{filename};
        if(current_file.is_open()) {
            std::string current_content;
            current_content.assign(
                std::istreambuf_iterator<char>(current_file),
                std::istreambuf_iterator<char>()
            );
            if(current_content == content) {
                return false;
            }
        }
    }
    try {
        std::ofstream output_file{filename};
        output_file.exceptions(std::ios::failbit | std::ios::badbit);
        output_file << content;
    }
    catch (std::ios::failure const& e) {
        throw file_write_error(filename, e.what());
    }
    return true;
}

/*
ExecutionType approximate_log2(ExecutionType input) {
#ifdef USE_FIXPOINT
    const int fixpoint_mul = pow(2, FIXPOINT);

    float as_float = input.ToFloat();

    int largest_1 = floor(log2(as_float));

    int x = (input * fixpoint_mul).ToInt<int>();
    int shifted;
    if(largest_1 > 0) {
        shifted = x >> largest_1;
    }
    else {
        shifted = x << (-largest_1);
    }

    float lutout = log2((float) shifted / fixpoint_mul);

    return (ExecutionType) (largest_1 + lutout);
#else
    return log2(input);
#endif
}

ExecutionType approximate_pow2(ExecutionType input) {
#ifdef USE_FIXPOINT
    auto integer_part = input.ToInt<int>();

    auto fractional_part = input - integer_part;

    auto lutout = pow(2, fractional_part.ToDouble());

    return lutout * pow(2, integer_part);
#else
    return pow(2, input);
#endif
}
*/

std::string util::verilog::sized_constant(int bits, int64_t value) {
    std::string sign = value < 0 ? "-" : "";
    return sign + std::to_string(bits) + "'d" + std::to_string(std::abs(value));
}

std::string util::verilog::int_parameter(std::string const& name, int64_t const value) {
    // extra +1 for sign bit
    // int64_t value_size = value != 0 ? floor(log2(std::abs(value))) + 1 + 1 : 1;
    int64_t value_size = 64;
    return "."
        + name
        + "(" + sized_constant(value_size, value) + ")";
}



namespace cppcode {
    std::string fixed_point_type(int int_bits, int fractional_bits, CppDataType t) {
        switch (t) {
            case CppDataType::Fp:
                return "Fp<" + std::to_string(fractional_bits) + ">";
            case CppDataType::Ac:
                return "ac_fixed<" + std::to_string(int_bits + 1 + fractional_bits) + ", " + std::to_string(int_bits + 1) + ">";
            case CppDataType::Ap:
                return "ap_fixed<" + std::to_string(int_bits + 1 + fractional_bits) + ", " + std::to_string(int_bits + 1) + ">";
            case CppDataType::Float:
                return "float";
        }
    }

    std::string cast_value(std::string value, int, int fractional_bits, CppDataType t) {
        switch (t) {
            case CppDataType::Fp:
                return value
                    + ".cast<"
                    + std::to_string(fractional_bits)
                    + ">()";
            case CppDataType::Ac:
                return value;
            case CppDataType::Ap:
                return value;
            case CppDataType::Float:
                return value;
        }
    }
}


void util::log_config_flags(Config const& cfg) {
    debug("==================================================================");
#ifdef CHECK_RESULT
    debug("CHECK_RESULT: enabled");

    #ifdef CHECK_FAILED
        debug("CHECK_FAILED: enabled");
    #else
        debug("Not checking for incorrect failures");
        debug("    Define CHECK_FAILED to enable");
    #endif
#else
    debug("Not checking results");
    debug("    Define CHECK_RESULT to enable");
#endif

    if (cfg.single_node_1d_interp) {
        debug("SINGLE_1D_NODE_INTERP: enabled");
    }
    else {
        debug("Using cinnabar classes for 1d interpolation functions.");
        debug("    Set Config::SINGLE_1D_NODE_INTERP to use a single node");
    }
    if (cfg.single_node_2d_interp) {
        debug("SINGLE_2D_NODE_INTERP: enabled");
    }
    else {
        debug("Using cinnabar classes for 2d interpolation functions.");
        debug("    Set Config::SINGLE_2D_NODE_INTERP to use a single node");
    }
    debug("==================================================================");
}
