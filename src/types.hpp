#pragma once

#include <variant>

struct FixedFixpoint {
    int position;
};
struct FixedTotalLength {
    int length;
};
struct FixedType {
    int int_bits;
    int frac_bits;
};

using WordLength = std::variant<FixedFixpoint, FixedTotalLength, FixedType>;


enum class CppDataType {
    Ap,
    Ac,
    Fp,
    Float,
};
