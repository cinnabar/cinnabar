#include "node.hpp"

#include "graph.hpp"
#include "log.hpp"
#include "util.hpp"

#include <iostream>
#include <sstream>


using namespace NodeError;

using node::do_constant_fold;

using util::merge_code
    , util::clocked_block
    , util::register_code
    ;


//////////////////////////////////////////////////////////////////////
// Standalone functions
//////////////////////////////////////////////////////////////////////


#define OVERLOADED_IF(TYPE, IF_TYPE) \
Rc<IF_TYPE> _if(Rc<BoolNode> condition, Rc<TYPE> on_true, Rc<TYPE> on_false) { \
    return IF_TYPE::create(condition, on_true, on_false); \
}
OVERLOADED_IF(FracNode, IfFractional);
OVERLOADED_IF(BoolNode, IfBoolean);

Rc<IfFractional> min(Rc<FracNode> lhs, Rc<FracNode> rhs) {
    auto result = _if(lhs < rhs, lhs, rhs);
    result->override_name("min_" + lhs->base_name() + "_" + rhs->base_name());
    result->_override_min(std::min(lhs->min(), rhs->min()));
    result->_override_max(std::min(lhs->max(), rhs->max()));
    return result;
}
Rc<IfFractional> max(Rc<FracNode> lhs, Rc<FracNode> rhs) {
    auto result = _if(lhs > rhs, lhs, rhs);
    result->override_name("max_" + lhs->base_name() + "_" + rhs->base_name());
    result->_override_min(std::max(lhs->min(), rhs->min()));
    result->_override_max(std::max(lhs->max(), rhs->max()));
    return result;
}


// This is a free standing function to allow [[nodiscard]], though that
// requires passing self
[[nodiscard]] std::optional<Rc<Node>> node::do_constant_fold(
    Node& self,
    const Rc<FracNode>* const node_,
    std::function<float(float)> op
) {
    if(auto node = std::dynamic_pointer_cast<Constant>(node_->get_inner())) {
        auto result = Constant::create(self.base_name(), op(node->value));
        result->copy_properties(self);
        return result;
    }
    return std::nullopt;
}

[[nodiscard]] std::optional<Rc<Node>> node::do_constant_fold(
    Node& self,
    const Rc<FracNode>* const lhs_,
    const Rc<FracNode>* const rhs_,
    std::function<float(float, float)> op
) {
    auto lhs = std::dynamic_pointer_cast<Constant>(lhs_->get_inner());
    auto rhs = std::dynamic_pointer_cast<Constant>(rhs_->get_inner());

    if(lhs != nullptr && rhs != nullptr) {
        auto result = Constant::create(
            self.base_name(),
            op(lhs->value, rhs->value)
        );
        result->copy_properties(self);
        return result;
    }

    return std::nullopt;
}
[[nodiscard]] std::optional<Rc<Node>> node::do_bool_constant_fold(
    Node&,
    const Rc<FracNode>* const lhs_,
    const Rc<FracNode>* const rhs_,
    std::function<bool(float, float)> op
) {
    auto lhs = std::dynamic_pointer_cast<Constant>(lhs_->get_inner());
    auto rhs = std::dynamic_pointer_cast<Constant>(rhs_->get_inner());

    if(lhs != nullptr && rhs != nullptr) {
        if(op(lhs->value, rhs->value)) {
            return BoolConst::True;
        }
        else {
            return BoolConst::False;
        }
    }
    return std::nullopt;
}
[[nodiscard]] std::optional<Rc<Node>> node::do_bool_constant_fold(
    Node&,
    const Rc<FracNode>* const lhs_,
    std::function<bool(float)> op
) {
    auto lhs = std::dynamic_pointer_cast<Constant>(lhs_->get_inner());

    if(lhs != nullptr) {
        if(op(lhs->value)) {
            return BoolConst::True;
        }
        else {
            return BoolConst::False;
        }
    }
    return std::nullopt;
}
