#pragma once

#include "macro_util.hpp"

#define __APPEND_TO_MESSAGE(str) message += str;
#define WITH_MESSAGE(...) \
    mutable std::string message; \
    const char* what() const noexcept override { \
        MAP(__APPEND_TO_MESSAGE, __VA_ARGS__) \
        return message.c_str(); \
    }

