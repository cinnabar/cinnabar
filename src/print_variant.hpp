#pragma once

#include <ostream>
#include <variant>

#include <cxxabi.h>


template<class T>
struct streamer {
    const T& val;
};
template<class T> streamer(T) -> streamer<T>;

template<class T>
std::ostream& operator<<(std::ostream& os, streamer<T> s) {
    int _status; \
    auto demangled_self = __cxxabiv1::__cxa_demangle(typeid(T).name(), 0, 0, &_status);
    os << demangled_self << "(" << s.val << ")";
    return os;
}

template<class... Ts>
std::ostream& operator<<(std::ostream& os, streamer<std::variant<Ts...>> sv) {
   std::visit([&os](const auto& v) { os << streamer{v}; }, sv.val);
   return os;
}
