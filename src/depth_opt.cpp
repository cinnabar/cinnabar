#include "depth_opt.hpp"

#include "util.hpp"

#include <fstream>
#include <filesystem>
#include <unistd.h>

using depth_opt::Glpk;

using util::merge_code;

// Model generation

std::string depth_name(Node* node) {
    return "d_" + node->base_name();
}
std::string latency_name(Node* node) {
    return "l_" + node->base_name();
}
std::string path_name(Node* start, Node* end) {
    return "y_" + start->base_name() + "_" + end->base_name();
}
std::string register_count_name(Node* node) {
    return "regs_" + node->base_name();
}

std::vector<std::string> depth_opt::generate_gmpl_variables(Node* root, WordLength wl) {
    auto variable_visitor = [wl](auto node) -> std::vector<std::string> {
        if(!node->is_constant()) {
            auto result = std::vector{
                "var " + depth_name(&*node) + " integer;",
                "var "
                    + latency_name(&*node)
                    + " integer = "
                    + std::to_string(node->computation_time(wl))
                    + ";",
                "var " + register_count_name(&*node) + " integer;",
            };
            for(auto parent : node->get_unique_parents()) {
                if(!parent->is_constant()) {
                    result.push_back(
                        "var " + path_name(&*parent, &*node) + " integer;"
                    );
                }
            }
            return result;
        }
        return {};
    };

    std::vector<std::vector<std::string>> variables =
        root->visit_with_cache<std::vector<std::string>>(variable_visitor);

    std::vector<std::string> result;
    for(auto vars : variables) {
        std::copy(vars.begin(), vars.end(), std::back_inserter(result));
    }
    return result;
}


std::vector<std::string> get_parent_constraints(
    Node* node,
    Rc<Node>& parent,
    std::optional<Rc<Node>> const& prev_parent,
    int const curr
) {
    std::vector<std::string> result;

    // Computation can't start before a parent is ready
    result.push_back(
        "s.t. depth_"
            + node->base_name()
            + "_"
            + std::to_string(curr)
            + ": "
            + depth_name(&*node)
            + " = "
            + depth_name(&*parent)
            + " + "
            + path_name(&*parent, &*node)
            + " + "
            + latency_name(&*node)
            + ";"
    );

    result.push_back(
        "s.t. regs_"
            + parent->base_name()
            + "_max_"
            + node->base_name()
            + ": "
            + register_count_name(&*parent)
            + " >= "
            + path_name(&*parent, &*node)
            + ";"
    );

    // Negative delay isn't allowed
    result.push_back(
        "s.t. positive_"
            + parent->base_name()
            + "_"
            + node->base_name()
            + ": "
            + path_name(&*parent, &*node)
            + " >= 0;"
    );

    // Inputs must arrive at the same clock cycle
    if(auto prev = prev_parent) {
        result.push_back(
            "s.t. "
                + node->base_name()
                + "_inputs_same_cycle_"
                + std::to_string(curr)
                + ": "
                + depth_name(&*parent)
                + " + "
                + path_name(&*parent, &*node)
                + " = "
                + depth_name(&**prev)
                + " + "
                + path_name(&**prev, &*node)
                + ";"
        );
    }

    return result;
}

std::vector<std::string> depth_opt::generate_constraints(Node* root) {
    auto visitor = [](auto node) -> std::vector<std::string> {
        if(!node->is_constant()) {
            auto result = std::vector<std::string>{};

            if(node->get_parents().size() == 0) {
                result.push_back(
                    "s.t. depth_"
                        + node->base_name()
                        + "_0: "
                        + depth_name(&*node)
                        + " = "
                        + latency_name(&*node)
                        + ";"
                );
            }

            result.push_back(
                "s.t. regs_"
                    + node->base_name()
                    + "_max_0: "
                    + register_count_name(&*node)
                    + " >= "
                    + std::to_string(0)
                    + ";"
            );

            int curr = 0;
            std::optional<Rc<Node>> prev_parent = std::nullopt;
            for(auto parent : node->get_unique_parents()) {
                if(!parent->is_constant()) {
                    auto c = get_parent_constraints(
                        node,
                        parent,
                        prev_parent,
                        curr
                    );

                    std::copy(c.begin(), c.end(), std::back_inserter(result));

                    prev_parent = parent;
                    curr += 1;
                }
            }
            return result;
        }
        else {
            return {};
        }
    };

    std::vector<std::vector<std::string>> variables =
        root->visit_with_cache<std::vector<std::string>>(visitor);

    std::vector<std::string> result;
    for(auto vars : variables) {
        std::copy(vars.begin(), vars.end(), std::back_inserter(result));
    }
    return result;
}



std::vector<std::string> depth_opt::generate_gmpl(Node* root, WordLength wl) {
    auto vars = depth_opt::generate_gmpl_variables(root, wl);
    auto constraints = depth_opt::generate_constraints(root);

    auto visitor = [wl](auto node) -> std::optional<std::pair<std::string, std::string>> {
        if(!node->is_constant()) {
            auto start_depth_expr =
                "\"start_"
                    + node->base_name()
                    + "\", ("
                    + depth_name(node)
                    + " - "
                    + latency_name(node)
                    + ")";

            auto reg_size = std::to_string(node->register_size(wl));
            return std::make_pair(
                register_count_name(&*node) + " * " + reg_size,
                start_depth_expr
            );
        }
        else {
            return {};
        }
    };

    std::vector<std::optional<std::pair<std::string, std::string>>> nodes =
        root->visit_with_cache<std::optional<std::pair<std::string, std::string>>>(visitor);

    std::vector<std::string> objective = {
        "minimize obj:",
        "0",
    };
    std::vector<std::string> display = {
        "display",
        "\"result\""
    };
    for(auto node : nodes) {
        if(!node.has_value()) continue;
        auto [reg, start_expr] = node.value();

        objective.push_back("+ " + reg);
        display.push_back(", " + start_expr);
    }
    objective.push_back(";");
    display.push_back(";");

    return merge_code({
        vars,
        constraints,
        objective,
        {"solve;"},
        display,
        {"end;"},
    });
}

void depth_opt::use_new_depths(std::vector<Weak<Node>> nodes, Glpk& opt_result) {
    for(auto _node : nodes) {
        auto node = _node.lock();
        if(!node->is_constant()) {
            // If all the parents of a node are constant, there will
            // be no depth of this node in the model. This should
            // never happen if constant folding is enabled and working
            bool has_non_const_parent = false;
            for(auto parent : node->get_unique_parents()) {
                has_non_const_parent |= !parent->is_constant();
            }
            if(has_non_const_parent) {
                node->adjust_start_depth(
                    opt_result.get_value(depth_name(&*node)) -
                    opt_result.get_value(latency_name(&*node))
                );
            }
        }
    }
}



// Model execution

// See mpl source code/examples/mplsamp2.c for more details
Glpk::Glpk(std::string model)
    : tran{glp_mpl_alloc_wksp()}
    , mip{glp_create_prob()}
{
    glp_term_out(GLP_OFF);
    // Write the model to a file
    std::string filename = "glpk_"
        + std::to_string(getpid())
        + ".mod";

    {
        std::ofstream f(filename);
        f << model;
    }

    int ret = glp_mpl_read_model(tran, filename.c_str(), 1);
    if (ret != 0) {
        err("Failed to translate gmpl model. Error code: ", ret);
        exit(255);
    }
    std::filesystem::remove(filename);
    ret = glp_mpl_generate(tran, nullptr);
    if (ret != 0) {
        err("Failed to generate gmpl model. Error code: ", ret);
        exit(255);
    }

    glp_mpl_build_prob(tran, mip);
    glp_simplex(mip, nullptr);
    glp_intopt(mip, nullptr);

    ret = glp_mpl_postsolve(tran, mip, GLP_MIP);
    if (ret != 0) {
        err("Failed to postsolve gmpl model. Error code: ", ret);
        exit(255);
    }

    // Gather the results
    for(int col = 1; col <= glp_get_num_cols(mip); col++) {
        this->variables.insert({
            glp_get_col_name(mip, col),
            glp_get_col_prim(mip, col)
        });
    }
    info("GLPK opt done");
}

Glpk::~Glpk() {
    glp_mpl_free_wksp(tran);
    glp_delete_prob(mip);
}

float Glpk::get_value(std::string name) const {
    return this->variables.at(name);
}

std::vector<std::pair<std::string, float>> Glpk::all_variables() const {
    std::vector<std::pair<std::string, float>> result;
    std::copy(variables.begin(), variables.end(), std::back_inserter(result));
    return result;
}
