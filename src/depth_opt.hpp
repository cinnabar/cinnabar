#ifndef H_DEPTH
#define H_DEPTH

#include "node.hpp"

#include <map>
#include <glpk.h>

namespace depth_opt {
    // RAII friendly wrapper around glpk. The constructor loads and solves
    // the model and results can be accessed using the member functions
    class Glpk {
        public:
            Glpk(std::string model);
            ~Glpk();

            float get_value(std::string name) const;
            std::vector<std::pair<std::string, float>> all_variables() const;
        private:
            Glpk(Glpk const& other) = delete;
            Glpk& operator=(Glpk const& other) = delete;

            glp_tran *tran;
            glp_prob *mip;

            std::map<std::string, float> variables;
    };

    // Generates the variable defintions for the depth optimisation
    std::vector<std::string> generate_gmpl_variables(Node* root, WordLength);

    std::vector<std::string> generate_constraints(Node* root);

    std::vector<std::string> generate_gmpl(Node* root, WordLength wl);

    void use_new_depths(std::vector<Weak<Node>> nodes, Glpk& depths);
}

#endif
