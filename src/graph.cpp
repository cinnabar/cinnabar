#include "graph.hpp"

#include <set>
#include <fstream>
#include <chrono>
#include <algorithm>
#include <map>

#include <sys/stat.h>

using namespace graph;

using util::merge_code, util::join;

std::optional<std::string> default_color(Node*) {return std::nullopt;}

std::vector<std::string> graph::generate_graph(
    Rc<Node> root,
    ValueFn fn,
    ColorFn color_fn,
    WordLength wl,
    bool include_delay
) {
    auto unique = root->unique_ancestors();

    // Gather the graph code for each node
    std::vector<std::string> code;
    std::map<int, std::vector<std::string>> depths;
    for(auto _node : unique) {
        auto node = _node.lock();
        code = merge_code({code, node->generate_graph(fn, color_fn, wl, include_delay)});

        // Also collect the depth and name to set rank=same. Constant nodes are
        // better suited to be rendered where they are used as they do not
        // have any pipeline registers
        if(! std::dynamic_pointer_cast<Constant>(node)) {
            depths[node->start_depth(wl)].push_back(node->graph_identifier());
        }
    }
    code = merge_code({code, root->generate_graph(fn, color_fn, wl, include_delay)});

    for(auto [depth, nodes] : depths) {
        // Set the rank of nodes to be same for same depths
        std::string this_line = "{rank=same; ";
        for (auto&& node : nodes) {
            this_line += node + " ";
        }
        this_line += "}";
        code = merge_code({code, {this_line}});

        // Create a subgraph around them
        std::string subgraph = "subgraph cluster_d" + std::to_string(depth) + " {";
        for (auto&& node : nodes) {
            subgraph += node + ";";
        }
        subgraph += "graph[style=dotted];}";
        code = merge_code({code, {subgraph}});
    }


    return code;
}
std::vector<std::string> graph::generate_graph(
    Rc<Node> root,
    ValueFn fn,
    WordLength wl,
    bool include_delay
) {
    auto unique = root->unique_ancestors();
    auto color_fn = [](Node*){return std::nullopt;};

    std::vector<std::string> code;
    for(auto node : unique) {
        code = merge_code({code, node.lock()->generate_graph(fn, color_fn, wl, include_delay)});
    }
    code = merge_code({code, root->generate_graph(fn, color_fn, wl, include_delay)});

    return code;
}

void graph::dump_graph(
    std::string directory,
    std::string filename,
    Rc<Node> root,
    ValueFn fn,
    WordLength wl,
    bool include_delay
) {
    dump_graph(directory, filename, root, fn, default_color, wl, include_delay);
}
void graph::dump_graph(std::string directory, Rc<Node> root, WordLength wl, bool include_delay) {
    dump_graph(
        directory,
        "graph.gv",
        root,
        [](Node* node){return node->display_value();},
        wl,
        include_delay
    );
}
void graph::dump_graph(
    std::string directory,
    std::string filename,
    Rc<Node> root,
    ValueFn fn,
    ColorFn color_fn,
    WordLength wl,
    bool include_delay
) {
    if (directory != "") {
        filename = directory + "/" + filename;
    }
    mkdir(directory.c_str(), 0777);
    std::ofstream output(filename);

    if(!output) {
        err("Failed to open", filename, "to write graph");
    }
    else {
        output << "digraph G {";
        output << join(generate_graph(root, fn, color_fn, wl, include_delay));
        output << "}";
    }
}

void graph::dump_bounds(std::string directory, Rc<Node> root, WordLength wl) {
    auto fn = [](Node* node) {
        auto frac = dynamic_cast<FracNode*>(node);
        if(frac != nullptr) {
            return std::to_string(frac->min()) + " | " + std::to_string(frac->max());
        }
        else {
            return std::string("-");
        }
    };
    dump_graph(directory, "bounds.gv", root, fn, wl, false);
}

ColorFn graph::value_color(std::function<std::optional<double>(Node*)> value_fn) {
    return [value_fn](Node* node) -> std::optional<std::string> {
        auto value = value_fn(node);
        if(value.has_value()) {
            auto _value = value.value();
            int const color_amount = 8;
            int const color_min = 1;
            std::string const scheme_name = "bugn9";

            auto capped_value = std::max(0., std::min(_value, 1.));
            return "\"/" + scheme_name + "/"
                + std::to_string((int)(color_amount * capped_value + color_min))
                + "\"";
        }
        return std::nullopt;
    };
}
