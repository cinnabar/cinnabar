#pragma once

#include "bool.hpp"

class BoolConst : public BoolNode {
    public:
        static Rc<BoolConst> create(bool value);
        const static Rc<BoolConst> True;
        const static Rc<BoolConst> False;

        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<std::string> get_cpp(WordLength word_length, CppDataType) override;

        // Not tested because it's too trivial
        std::vector<Rc<Node>> get_parents() const override {return {};}

        std::string pipeline_buffer_name(int) override { return base_name(); }
        std::vector<std::string> generate_pipeline_code(WordLength) override {
            return {};
        }

        std::string graph_label_name() override;
        std::string graph_node_color() const override {return "dodgerblue2";}

        void optimize_parents(OptPass pass) override;
        std::optional<Rc<Node>> constant_fold() override;

        const bool value;

        std::optional<std::string> compare_local(Weak<Node> other) const override;

        bool is_constant() const override {return true;};

        IMPL_DEFAULT_CLONE(BoolConst);
        NO_EXTRA_FILES
    protected:
        bool recalculate_value() override {return value;}
        BoolConst(bool value);

        std::string variable_name() const override;
};

