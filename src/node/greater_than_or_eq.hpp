#pragma once

#include "frac.hpp"
#include "boolean_binary_operator.hpp"

class GreaterThanOrEq : public BooleanBinaryOperator<FracNode> {
    BOOLBINOP_COMMON(GreaterThanOrEq, FracNode);
    HAS_REDUNDANT_COMPARISON_REMOVAL
};
