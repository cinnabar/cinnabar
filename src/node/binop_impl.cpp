#include "boolean_binary_operator.hpp"
#include "binary_operator.hpp"
#include "less_than.hpp"
#include "greater_than.hpp"
#include "less_than_or_eq.hpp"
#include "greater_than_or_eq.hpp"
#include "equal.hpp"
#include "not_equal.hpp"
#include "and.hpp"
#include "or.hpp"

#include "constant.hpp"
#include "bool_const.hpp"
#include <string>

using util::merge_code
    , util::clocked_block
    , util::register_code
    , util::verilog_variable;

using node::do_constant_fold;

BinaryOperator::BinaryOperator(
    Rc<FracNode> lhs,
    Rc<FracNode> rhs
)
    : FracNode({lhs.weak(), rhs.weak()}), lhs(lhs), rhs(rhs)
{}
std::string BinaryOperator::variable_name() const {
    return operator_name() + "_" + lhs->base_name() + "_" + rhs->base_name();
}
std::string BinaryOperator::graph_label_name() {return operator_symbol();}

DERIVE_PARENT_FNS(BinaryOperator, lhs, rhs);



#define BINOP_BOILERPLATE(CLASS, SYMBOL, NAME, OP, CALC_OP) \
    CLASS::CLASS(Rc<FracNode> lhs, Rc<FracNode> rhs) \
        : BinaryOperator(lhs, rhs) \
    {} \
    std::string CLASS::operator_symbol() const {return SYMBOL;} \
    std::string CLASS::operator_name() const {return NAME;} \
    Rc<CLASS> OP(Rc<FracNode> lhs, Rc<FracNode> rhs) { \
        return Rc<CLASS>(new CLASS(lhs, rhs)); \
    } \
    ExecutionType CLASS::recalculate_value() { \
        return this->lhs->calculate_value() CALC_OP this->rhs->calculate_value(); \
    } \
    std::vector<std::string> CLASS::get_cpp(WordLength wl, CppDataType t) { \
        /* TODO: handle casting for other cpp data types */ \
        auto value = "(" + lhs->base_name() + " " + SYMBOL + " " + rhs->base_name() + ")"; \
        return { \
            this->cpp_datatype(wl, t) \
                + " " + base_name() \
                + " = " + cppcode::cast_value(value, this->integer_bits(wl), this->fractional_bits(wl), t) + ";" \
        }; \
    } \
    std::optional<Rc<Node>> CLASS::constant_fold() { \
        return do_constant_fold(*this, &lhs, &rhs, [](auto x, auto y){ \
                return x CALC_OP y; \
        }); \
    }

#define BINOP_DEFAULT_CODE(CLASS) \
    std::vector<std::string> CLASS::get_code(WordLength wl) { \
        auto input_int_bits = std::max(lhs->integer_bits(wl), rhs->integer_bits(wl)); \
        auto input_frac_bits = this->fractional_bits(wl); \
        bool truncate = this->has_forced_bound(); \
        auto lhs_val = lhs->as_parent_register(*this, wl, input_int_bits, input_frac_bits, truncate); \
        auto rhs_val = rhs->as_parent_register(*this, wl, input_int_bits, input_frac_bits, truncate); \
        return merge_code({ \
            { \
                register_code(base_name(), register_size(wl)), \
            }, \
            clocked_block( \
                { base_name() + " <= $signed(" + lhs_val + ")" + operator_symbol() + "$signed(" + rhs_val + ");"} \
            ) \
        }); \
    } \

double Add::_min() const {return rhs->min() + lhs->min();}
double Add::_max() const {return rhs->max() + lhs->max();}
BINOP_BOILERPLATE(Add, "+", "add", operator+, +);
BINOP_DEFAULT_CODE(Add);

double Sub::_min() const {return lhs->min() - rhs->max();}
double Sub::_max() const {return lhs->max() - rhs->min();}
BINOP_BOILERPLATE(Sub, "-", "sub", operator-, -);
BINOP_DEFAULT_CODE(Sub);

double Mul::_min() const {
    // Cache these calculations since they are recursive and traverse the
    // whole tree
    auto lhsmin = lhs->min();
    auto lhsmax = lhs->max();
    auto rhsmin = rhs->min();
    auto rhsmax = rhs->max();
    return std::min(
        std::min(lhsmin*rhsmin, lhsmin*rhsmax),
        std::min(lhsmax*rhsmin, lhsmax*rhsmax)
    );
}
double Mul::_max() const {
    // Cache these calculations since they are recursive and traverse the
    // whole tree
    auto lhsmin = lhs->min();
    auto lhsmax = lhs->max();
    auto rhsmin = rhs->min();
    auto rhsmax = rhs->max();
    return std::max(
        std::max(lhsmin*rhsmin, lhsmin*rhsmax),
        std::max(lhsmax*rhsmin, lhsmax*rhsmax)
    );
}
BINOP_BOILERPLATE(Mul, "*", "mul", operator*, *);
std::vector<std::string> Mul::get_code(WordLength wl) {
    auto lhs_val = lhs->as_parent_register_unchanged(*this, wl);
    auto rhs_val = rhs->as_parent_register_unchanged(*this, wl);

    auto buf_name = base_name() + "_buf";
    auto post1_name = base_name() + "_post1";
    auto post2_name = base_name() + "_post2";
    auto buffer_size = lhs->register_size(wl) + rhs->register_size(wl);
    auto register_size = this->register_size(wl);

    auto buffer_fixedpoint = lhs->fractional_bits(wl) + rhs->fractional_bits(wl);

    auto fractional_bits = this->fractional_bits(wl);

    auto output_start = buffer_fixedpoint - fractional_bits;
    auto output_end = std::to_string(output_start + register_size - 1);

    auto lhs_pre_reg = base_name() + "_lhs_pre";
    auto rhs_pre_reg = base_name() + "_rhs_pre";

    return merge_code({
        { register_code(lhs_pre_reg, lhs->register_size(wl))
        , register_code(rhs_pre_reg, rhs->register_size(wl))
        , register_code(post1_name, buffer_size)
        , register_code(post2_name, buffer_size)
        },
        clocked_block({
            lhs_pre_reg + " <= " + lhs_val + ";",
            rhs_pre_reg + " <= " + rhs_val + ";"
        }),
        { register_code(base_name(), register_size)
        , verilog_variable(VarType::WIRE, buf_name, buffer_size)
        , "assign " + buf_name + " = $signed(" + lhs_pre_reg + ")*$signed(" + rhs_pre_reg + ");"
        },
        { clocked_block({
            post1_name + " <= " + buf_name + ";",
            post2_name + " <= " + post1_name + ";",
            base_name()
                + " <= "
                + post2_name
                + "["
                + output_end
                + ":"
                + std::to_string(output_start)
                + "]"
                + ";"
        })
        }
    });
}
int Mul::computation_time(WordLength) const {
    return 4;
}

// TODO: Test
double Div::_min() const {
    auto lhsmin = lhs->min();
    auto lhsmax = lhs->max();
    auto rhsmin = rhs->min();
    auto rhsmax = rhs->max();
    return std::min(
        std::min(lhsmin/rhsmin, lhsmin/rhsmax),
        std::min(lhsmax/rhsmin, lhsmax/rhsmax)
    );
}
double Div::_max() const {
    auto lhsmin = lhs->min();
    auto lhsmax = lhs->max();
    auto rhsmin = rhs->min();
    auto rhsmax = rhs->max();
    return std::max(
        std::max(lhsmin/rhsmin, lhsmin/rhsmax),
        std::max(lhsmax/rhsmin, lhsmax/rhsmax)
    );
}
std::vector<std::string> Div::check_for_problems() const {
    auto rhsmin = rhs->min();
    auto rhsmax = rhs->max();
    if(rhsmin <= 0 && rhsmax >= 0) {
        return {"Division by value that can be 0 causes incorrect min/max value"};
    }
    return {};
}
BINOP_BOILERPLATE(Div, "/", "div", operator/, /);
std::vector<std::string> Div::get_code(WordLength wl) {
    bool truncate = this->has_forced_bound();
    auto lhs_val = lhs->as_parent_register(*this, wl, lhs->integer_bits(wl), lhs->fractional_bits(wl), truncate);
    auto rhs_val = rhs->as_parent_register(*this, wl, rhs->integer_bits(wl), rhs->fractional_bits(wl), truncate);

    int register_size = this->register_size(wl);

    return {
        verilog_variable(VarType::WIRE, this->base_name(), register_size),
        std::string("divider")
            + "#(.LHS_INT_BITS(" + std::to_string(lhs->integer_bits(wl)) + ")"
            + ", .LHS_FRAC_BITS(" + std::to_string(lhs->fractional_bits(wl)) + ")"
            + ", .RHS_INT_BITS(" + std::to_string(rhs->integer_bits(wl)) + ")"
            + ", .RHS_FRAC_BITS(" + std::to_string(rhs->fractional_bits(wl)) + ")"
            + ", .OUT_INT_BITS(" + std::to_string(this->integer_bits(wl)) + ")"
            + ", .OUT_FRAC_BITS(" + std::to_string(this->fractional_bits(wl)) + ")"
            + ") "
            + this->base_name() + "_mod"
            + "(.clk(clk)"
            + ", .lhs_signed(" + lhs_val + ")"
            + ", .rhs_signed(" + rhs_val + ")"
            + ", .out_signed(" + this->base_name() + ")"
            + ");"
    };
}
int Div::computation_time(WordLength wl) const {
    auto shift_amount = this->fractional_bits(wl) + rhs->fractional_bits(wl) - lhs->fractional_bits(wl);
    return 2
        + this->lhs->integer_bits(wl)
        + this->lhs->fractional_bits(wl)
        + shift_amount;
}
std::optional<Rc<Node>> Div::replace_division_by_constant() {
    if(auto divisor = this->rhs.pointer_cast<Constant>()) {
        return (lhs * Constant::create(
            this->base_name() + "_divisor",
            1./(*divisor)->value
        ))->with_name(this->base_name());
    }
    return std::nullopt;
}



#define BOOL_BINOP_BOILERPLATE(CLASS, INPUT_CLASS, SYMBOL, CPP_SYMBOL, NAME, OP, CALC_OP, RUNTIME_TYPE) \
    CLASS::CLASS(Rc<INPUT_CLASS> lhs, Rc<INPUT_CLASS> rhs) \
        : BooleanBinaryOperator<INPUT_CLASS>(lhs, rhs) \
    {} \
    std::string CLASS::operator_symbol() const {return SYMBOL;} \
    std::string CLASS::operator_name() const {return NAME;} \
    Rc<CLASS> OP(Rc<INPUT_CLASS> lhs, Rc<INPUT_CLASS> rhs) { \
        return Rc<CLASS>(new CLASS(lhs, rhs)); \
    } \
    bool CLASS::recalculate_value() { \
        return this->lhs->calculate_value() CALC_OP this->rhs->calculate_value(); \
    } \
    std::vector<std::string> CLASS::get_cpp(WordLength, CppDataType) { \
        return { \
            "bool " + this->base_name() + " = " \
                + lhs->base_name() + " " + CPP_SYMBOL + " " + rhs->base_name() + ";" \
        }; \
    }

#define BOOL_BINOP_SIGNED_COMPARISON(CLASS) \
    std::vector<std::string> CLASS::get_code(WordLength wl) {\
        auto int_bits = std::max(lhs->integer_bits(wl), rhs->integer_bits(wl)); \
        auto frac_bits = std::max(lhs->fractional_bits(wl), rhs->fractional_bits(wl)); \
        auto lhs_val = lhs->as_parent_register(*this, wl, int_bits, frac_bits, false); \
        auto rhs_val = rhs->as_parent_register(*this, wl, int_bits, frac_bits, false); \
        return merge_code({ \
            { \
                register_code(base_name(), 1), \
            }, \
            clocked_block( \
                { base_name() + " <= $signed(" + lhs_val + ")" + operator_symbol() \
                    + "$signed(" + rhs_val + ");"} \
            ) \
        }); \
    }

BOOL_BINOP_BOILERPLATE(LessThan, FracNode, "<", "<", "lt", operator<, <, ExecutionType);
BOOL_BINOP_SIGNED_COMPARISON(LessThan);
BOOL_BINOP_BOILERPLATE(GreaterThan, FracNode, ">", ">", "gt", operator>, >, ExecutionType);
BOOL_BINOP_SIGNED_COMPARISON(GreaterThan);
BOOL_BINOP_BOILERPLATE(LessThanOrEq, FracNode, "<=", "<=", "leq", operator<=, <=, ExecutionType);
BOOL_BINOP_SIGNED_COMPARISON(LessThanOrEq);
BOOL_BINOP_BOILERPLATE(GreaterThanOrEq, FracNode, ">=", ">=", "geq", operator>=, >=, ExecutionType);
BOOL_BINOP_SIGNED_COMPARISON(GreaterThanOrEq);

BOOL_BINOP_BOILERPLATE(Equal, FracNode, "==", "==", "eq", operator==, ==, ExecutionType);
BOOL_BINOP_SIGNED_COMPARISON(Equal);

BOOL_BINOP_BOILERPLATE(NotEqual, FracNode, "!=", "!=", "neq", operator!=, !=, ExecutionType);
BOOL_BINOP_SIGNED_COMPARISON(NotEqual);

BOOL_BINOP_BOILERPLATE(Or, BoolNode, "|", "||", "or", operator||, ||, bool);
std::vector<std::string> Or::get_code(WordLength wl) {
    auto lhs_name = lhs->as_parent_register(*this, wl);
    auto rhs_name = rhs->as_parent_register(*this, wl);
    return merge_code({
        {"wire " + base_name() + " = " + lhs_name + " | " + rhs_name + ";"}
    });
}
int Or::computation_time(WordLength) const {return 0;}

BOOL_BINOP_BOILERPLATE(And, BoolNode, "&", "&&", "and", operator&&, &, bool);
std::vector<std::string> And::get_code(WordLength wl) {
    auto lhs_name = lhs->as_parent_register(*this, wl);
    auto rhs_name = rhs->as_parent_register(*this, wl);
    return merge_code({
        {"wire " + base_name() + " = " + lhs_name + " & " + rhs_name + ";"}
    });
}
int And::computation_time(WordLength) const {return 0;}


std::optional<Rc<Node>> LessThan::remove_redundant_comparisons() {
    if(lhs->max() < rhs->min()) {
        return BoolConst::True->with_name(this->base_name());
    }
    if(lhs->min() > rhs->max()) {
        return BoolConst::False->with_name(this->base_name());
    }
    return std::nullopt;
}
std::optional<Rc<Node>> LessThanOrEq::remove_redundant_comparisons() {
    if(lhs->max() <= rhs->min()) {
        return BoolConst::True->with_name(this->base_name());
    }
    if(lhs->min() >= rhs->max()) {
        return BoolConst::False->with_name(this->base_name());
    }
    return std::nullopt;
}
std::optional<Rc<Node>> GreaterThan::remove_redundant_comparisons() {
    if(rhs->min() > lhs->max()) {
        return BoolConst::False->with_name(this->base_name());
    }
    if(lhs->min() > rhs->max()) {
        return BoolConst::True->with_name(this->base_name());
    }
    return std::nullopt;
}
std::optional<Rc<Node>> GreaterThanOrEq::remove_redundant_comparisons() {
    if(lhs->min() >= rhs->max()) {
        return BoolConst::True->with_name(this->base_name());
    }
    if(lhs->max() < rhs->min()) {
        return BoolConst::False->with_name(this->base_name());
    }
    return std::nullopt;
}
std::optional<Rc<Node>> Equal::remove_redundant_comparisons() {
    return std::nullopt;
}
std::optional<Rc<Node>> NotEqual::remove_redundant_comparisons() {
    return std::nullopt;
}

#define IMPL_BOOL_BINOP_CONSTANT_FOLD(NODE) \
    std::optional<Rc<Node>> NODE::constant_fold() { \
        if(lhs->is_constant() && rhs->is_constant()) { \
            return BoolConst::create(this->calculate_value()) \
                ->with_name(this->base_name()); \
        } \
        return std::nullopt; \
    }
IMPL_BOOL_BINOP_CONSTANT_FOLD(LessThan);
IMPL_BOOL_BINOP_CONSTANT_FOLD(GreaterThan);
IMPL_BOOL_BINOP_CONSTANT_FOLD(GreaterThanOrEq);
IMPL_BOOL_BINOP_CONSTANT_FOLD(LessThanOrEq);
IMPL_BOOL_BINOP_CONSTANT_FOLD(Equal);
IMPL_BOOL_BINOP_CONSTANT_FOLD(NotEqual);
IMPL_BOOL_BINOP_CONSTANT_FOLD(Or);
IMPL_BOOL_BINOP_CONSTANT_FOLD(And);

std::optional<Rc<Node>> Or::remove_redundant_bool_computations() {
    auto lhs_const = this->lhs.pointer_cast<BoolConst>();
    auto rhs_const = this->rhs.pointer_cast<BoolConst>();

    if (lhs_const.has_value() && lhs_const.value().is_same(BoolConst::True)) {
        return BoolConst::True;
    }
    if (rhs_const.has_value() && rhs_const.value().is_same(BoolConst::True)) {
        return BoolConst::True;
    }
    if (lhs_const.has_value() && lhs_const.value().is_same(BoolConst::False)) {
        return rhs;
    }
    if (rhs_const.has_value() && rhs_const.value().is_same(BoolConst::False)) {
        return lhs;
    }
    return std::nullopt;
}

std::optional<Rc<Node>> And::remove_redundant_bool_computations() {
    auto lhs_const = this->lhs.pointer_cast<BoolConst>();
    auto rhs_const = this->rhs.pointer_cast<BoolConst>();

    if (lhs_const.has_value() && lhs_const.value().is_same(BoolConst::True)) {
        return rhs;
    }
    if (rhs_const.has_value() && rhs_const.value().is_same(BoolConst::True)) {
        return lhs;
    }
    if (lhs_const.has_value() && lhs_const.value().is_same(BoolConst::False)) {
        return BoolConst::False;
    }
    if (rhs_const.has_value() && rhs_const.value().is_same(BoolConst::False)) {
        return BoolConst::False;
    }
    return std::nullopt;
}
