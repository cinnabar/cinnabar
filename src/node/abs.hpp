#pragma once

#include "frac.hpp"

class Abs : public FracNode {
    public:
        static Rc<Abs> create(Rc<FracNode> input);

        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<std::string> get_cpp(WordLength word_length, CppDataType) override;
        double _min() const override;
        double _max() const override;

        std::string graph_label_name() override;

        void optimize_parents(OptPass pass) override;
        std::optional<Rc<Node>> constant_fold() override;

        IMPL_DEFAULT_COMPARISON;
        IMPL_DEFAULT_CLONE(Abs);
        NO_EXTRA_FILES
    protected:
        std::string variable_name() const override;
        std::vector<Rc<Node>> get_parents() const override;
        ExecutionType recalculate_value() override;
    private:
        Abs(Rc<FracNode> other);

        Rc<FracNode> input;
};

