#pragma once

#include "base.hpp"

// Dummy node with no behaviour but which can be used to collect a list of nodes,
// for example to generate a graph
class DummyNode : public Node {
    public:
        DummyNode(std::vector<Rc<Node>> parents);
        static Rc<DummyNode> create(std::vector<Rc<Node>> parents);
        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<std::string> get_cpp(WordLength word_length, CppDataType) override;
        std::vector<Rc<Node>> get_parents() const override;
        void mark_dirty() override;
        std::string display_value() override;
        std::string graph_label_name() override;
        int register_size(WordLength word_length) const override;
        // Recalculate the value if it is dirty
        void update_value() override;
        std::string cpp_datatype(WordLength word_length, CppDataType kind) const override;
        std::optional<std::string> compare_local(Weak<Node> other) const override;
        IMPL_DEFAULT_CLONE(DummyNode);
        NO_EXTRA_FILES;
        NO_EXTRA_OPT_CACHE(Node);
    protected:
        std::string variable_name() const override;

        void optimize_parents(OptPass pass) override;
        std::optional<Rc<Node>> constant_fold() override;
    private:
        std::vector<Rc<Node>> parents;
};

