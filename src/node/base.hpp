#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <optional>
#include <functional>
#include <memory>
#include <set>
#include <map>

#include <affine_arith.hpp>

#include "../config.hpp"
#include "../util.hpp"
#include "../rc.hpp"
#include "../optimization.hpp"
#include "../types.hpp"
#include "../macro_util.hpp"
#include "../exception_util.hpp"
#include "../runtime_config.hpp"


namespace NodeError {
    struct UnsupportedNodeOp : std::exception {
        UnsupportedNodeOp(std::string node_name, std::string description)
            : node_name{node_name}
            , description{description}
        {};
        std::string node_name;
        std::string description;
        WITH_MESSAGE("Unsupported operation on node ", node_name, "\n\t", description);
    };
}


// Implements a default comparison function for nodes. This is useful for nodes where
// no extra details, like values, have to be checked.
#define IMPL_DEFAULT_COMPARISON \
    std::optional<std::string> compare_local(Weak<Node> other_) const override { \
        auto other = other_.lock().get(); \
        MAYBE_RETURN(util::compare_types<Node>(this, other)); \
        return std::nullopt; \
    }

#define IMPL_DEFAULT_COPY(ClassName) \
    protected: \
        ClassName(ClassName const&) = default;

#define IMPL_CLONE(ClassName) \
    public: \
        Rc<Node> clone() const override { \
            return Rc(new ClassName(*this)); \
        }

#define IMPL_DEFAULT_CLONE(ClassName) \
    IMPL_DEFAULT_COPY(ClassName) \
    IMPL_CLONE(ClassName) \


// Optimization related functions



class Node {
    public:
        Node(std::set<Weak<Node>> parents);
        virtual ~Node() = default;

        // Non-virtual functions
        // Depth at which the computation of the node value can start
        int start_depth(WordLength);
        // Depth at which the node value is available
        int end_depth(WordLength);
        void adjust_start_depth(int adjusted_start_depth);
        void request_delay_slots(int amount);
        // Used by assign_delay_slots to relay the request to another node if that
        // acts as the actual pipeline register for this node. Used by the `Not` node
        // to set the number of pipeline registers for its parent
        virtual Node* pipeline_proxy();
        void traverse_depth_first(
            Weak<Node> self,
            std::function<void(Weak<Node>)> fn
        );
        void override_name(std::string forced_name);
        Rc<Node> with_name(std::string name);
        // TODO: This seems unused, remove it if true
        std::optional<int> get_delay_slots() {return this->delay_slots;}
        std::string base_name();
        std::set<Weak<Node>> unique_ancestors();
        std::optional<std::string> compare(Weak<Node> other) const;
        std::optional<Rc<Node>> optimize(OptPass pass);
        // Returns the parents of this node without returning the same node
        // twice if it is a parent twice. I.e. (a+a).get_unique_parents() returns
        // {a}
        std::vector<Rc<Node>> get_unique_parents() const;

        std::vector<std::string> generate_graph(
            std::function<std::string(Node*)> fn,
            std::function<std::optional<std::string>(Node*)> color_function,
            WordLength,
            bool include_delay
        );
        std::string graph_identifier();
        // Returns a list of extra nodes in the graph related to this node.
        // Each node is at depth `n + this->start_depth()` where `n` is
        // the location in the vector. If this is not overridden, the default
        // behaviour is to add one node for each extra delay contributed
        // by this node
        std::vector<std::string> graph_extra_nodes(WordLength, bool show_delay);
        // The name of the node in the graph where the output of this node
        // appears
        std::string graph_end_name(WordLength, bool include_delay);

        // Overrideable virtual functions
        virtual std::string pipeline_buffer_name(int n);
        virtual bool is_input() const {return false;}
        virtual std::vector<std::string> generate_pipeline_code(WordLength word_length);
        virtual void copy_properties(const Node& other);
        // Amount of cycles required to compute the node value after the
        // inputs are available
        virtual int computation_time(WordLength) const;
        virtual std::string graph_node_color() const {return "white";}
        // Returns a list of additional commands needed in order to generate
        // the full output verilog. This is primarily used for generating
        // modules from "template" modules.
        virtual std::vector<std::string> verilog_extra_commands(WordLength) const {
            return {};
        }
        // Returns true if the value is a constant. This can be used by things
        // like constant folding to see if constant folding is possible
        virtual bool is_constant() const {return false;};
        // Return any potential problems with the node. This could be things like
        // min/max values which would cause incorrect 
        virtual std::vector<std::string> check_for_problems() const {return {};}

        // Pure virtual functions
        // List of files which need to be included for this node to work. File paths
        // are relative to the main file produced by Codegen (or absolute).
        virtual std::vector<std::string> extra_cpp_files(WordLength) = 0;
        virtual std::vector<std::string> extra_verilog_files(WordLength) = 0;

        virtual std::vector<std::string> get_code(WordLength word_length) = 0;
        virtual std::vector<std::string> get_cpp(WordLength word_length, CppDataType t) = 0;
        virtual std::string cpp_datatype(WordLength word_length, CppDataType kind) const = 0;
        virtual std::vector<Rc<Node>> get_parents() const = 0;
        virtual void mark_dirty() = 0;
        virtual std::string display_value() = 0;
        virtual std::string graph_label_name() = 0;
        virtual int register_size(WordLength word_length) const = 0;
        // Recalculate the value if it is dirty
        virtual void update_value() = 0;
        // Compares this node to the specified node. If there is a difference,
        // a string describing that difference is returned
        virtual std::optional<std::string> compare_local(Weak<Node> other) const = 0;
        // Return a cloned version of the node. Should use the copy constructor
        // to clone the data and can be derived using IMPL_CLONE.
        // If the copy constructor is default, both can be derived using
        // IMPL_DEFAULT_CLONE
        virtual Rc<Node> clone() const = 0;
        // Clear all cached values which are invalidated by optimisation.
        // Subclasses must call this method on its base class. A class
        // with nothing to cache can use the NO_EXTRA_OPT_CACHE macro which simply
        // calls the base class implementation.
        // If the node has extra cache, the EXTRA_OPT_CACHE macro clears those
        // cache items and calls the base class method
        virtual void clear_local_opt_cache() = 0;

        // Visits each node in this tree exactly once using `fn`. The order
        // of the results is not guaranteed
        template<typename T, typename F>
        std::vector<T> visit_with_cache(F fn);
    protected:
        virtual std::string variable_name() const = 0;

        std::optional<int> delay_slots = std::nullopt;

        std::set<Weak<Node>> ancestors;

        std::optional<std::string> forced_name;

        ///////////////////////////////////////////////////////////////////////
        //                          Optimisations
        ///////////////////////////////////////////////////////////////////////
        // Apply optimizations on the parents of the node, replacing them
        // if they changed
        virtual void optimize_parents(OptPass pass) = 0;

        // Return a constant folded version of self if possible
        // Most nodes are expected to be constant folding, so it is pure virtual
        virtual std::optional<Rc<Node>> constant_fold() = 0;

#define OPT_PASS_MEMBER(NAME) virtual std::optional<Rc<Node>> NAME() {return std::nullopt;}
        // Fold chained boolean computations into a single node that performs all
        // computations
        OPT_PASS_MEMBER(boolean_merge);
        OPT_PASS_MEMBER(remove_redundant_comparisons);
        OPT_PASS_MEMBER(remove_redundant_interp_validity);
        OPT_PASS_MEMBER(remove_redundant_bool_computations);
        OPT_PASS_MEMBER(replace_division_by_constant);
        OPT_PASS_MEMBER(select_else_last);
        OPT_PASS_MEMBER(select_else_dont_care);
        OPT_PASS_MEMBER(remove_single_select);
#undef OPT_PASS_MEMBER
    private:
        // Performs optimisations on the node. If the type of the node
        // is changed by the optimisations, a new node is returned. Otherwise
        // the node is modified in place
        std::optional<Rc<Node>> uncached_optimize(const OptPass pass);

        // The start depth of this node if it has been adjusted, for example
        // by retiming
        std::optional<int> adjusted_start_depth = std::nullopt;

        // The cached depth at which the node can start computing its value
        std::optional<int> cached_start_depth;
        std::optional<std::string> cached_name;

        // Possibly holds a possibly optimized version of this node. The outer
        // optional has a value if optimization has been performed. The inner node
        // has one if this node was found to be suboptimal. The result is returned
        // by optimize.
        std::optional<std::pair<OptPass, std::optional<Rc<Node>>>> optimized;

        template<typename T, typename F>
        void _visit_with_cache(F fn, std::unordered_map<std::string, T>& cache);
};

template<typename T, typename F>
std::vector<T> Node::visit_with_cache(
    F fn
) {
    std::unordered_map<std::string, T> cache{};
    _visit_with_cache(fn, cache);

    std::vector<T> result;
    for(auto [_, value] : cache) {
        result.push_back(value);
    }
    return result;
}

template<typename T, typename F>
void Node::_visit_with_cache(
    F fn,
    std::unordered_map<std::string, T>& cache
) {
    if(cache.find(this->base_name()) != std::end(cache)) {
        return;
    }
    for(auto&& parent : get_parents()) {
        parent->_visit_with_cache(fn, cache);
    }
    cache[this->base_name()] = fn(this);
}

struct MissmatchedTypeException : public std::exception {};


// Free functions
namespace node {
    [[nodiscard]] std::optional<std::string> compare_types(Node* x, Node* y);

    void assign_delay_slots(std::vector<Rc<Node>> nodes, int depth, WordLength);

    // Convenienve function for calling traverse depth first without having
    // to write the node first
    void traverse_depth_first(Weak<Node> root, std::function<void(Weak<Node>)> fn);
    void traverse_depth_first(Rc<Node> root, std::function<void(Weak<Node>)> fn);

    [[nodiscard]] std::optional<Rc<Node>>do_constant_fold(
        Node& self,
        const Rc<FracNode>* const node_,
        std::function<float(float)> op
    );
    [[nodiscard]] std::optional<Rc<Node>>do_constant_fold(
        Node& self,
        const Rc<FracNode>* const lhs,
        const Rc<FracNode>* const rhs,
        std::function<float(float, float)> op
    );
    [[nodiscard]] std::optional<Rc<Node>>do_bool_constant_fold(
        Node& self,
        const Rc<FracNode>* const lhs,
        const Rc<FracNode>* const rhs,
        std::function<bool(float, float)> op
    );
    [[nodiscard]] std::optional<Rc<Node>>do_bool_constant_fold(
        Node& self,
        const Rc<FracNode>* const lhs,
        std::function<bool(float)> op
    );
    // Compares self to other under the assumptioin that other can be cast to Weak<T>.
    // If this is not the case, an exception is thrown
    template<typename T>
    [[nodiscard]]std::optional<std::string> do_compare_local(
        T const& self,
        Weak<Node> other,
        std::function<std::optional<std::string>(T const&, T const&)> const comparator
    ) {
        // Attempt to cast the node to the right type. Callers of do_compare_local
        // should ensure that this cast is possible
        if(auto casted = std::dynamic_pointer_cast<T>(other.lock())) {
            return comparator(self, *casted);
        }
        else {
            Node* other_deref = other.lock().get();
            err(
                "Missmatched comparison type. Expected: ",
                typeid(T).name(), " got ", typeid(*other_deref).name()
            );
            throw MissmatchedTypeException{};
        }
    }


    // Converts a vector of nodes to a set of weak nodes.
    std::set<Weak<Node>> weak_pointers(std::vector<Rc<Node>> in);
}

#define DERIVE_GET_PARENTS_BODY(...) \
        return {__VA_ARGS__};

#define DERIVE_OPTIMIZE_PARENTS_BODY(...) \
    ::optimize_parents(pass, MAP_LIST(MAKE_POINTER, __VA_ARGS__)); \


#define DERIVE_PARENT_FNS(NODE, ...) \
    std::vector<Rc<Node>> NODE::get_parents() const { \
        DERIVE_GET_PARENTS_BODY(__VA_ARGS__) \
    } \
    void NODE::optimize_parents(OptPass pass) { \
        DERIVE_OPTIMIZE_PARENTS_BODY(__VA_ARGS__) \
    }

#define DERIVE_PARENT_FNS_INLINE(...) \
    std::vector<Rc<Node>> get_parents() const override { \
        DERIVE_GET_PARENTS_BODY(__VA_ARGS__) \
    } \
    void optimize_parents(OptPass pass) override { \
        DERIVE_OPTIMIZE_PARENTS_BODY(__VA_ARGS__) \
    }

#define NO_EXTRA_VERILOGS \
    std::vector<std::string> extra_verilog_files(WordLength) override {return {};}

#define NO_EXTRA_CPPS \
    std::vector<std::string> extra_cpp_files(WordLength) override {return {};}

#define NO_EXTRA_FILES \
    NO_EXTRA_VERILOGS \
    NO_EXTRA_CPPS


// Cache invalidation macros
#define NO_EXTRA_OPT_CACHE(BASE_CLASS) \
    void clear_local_opt_cache() override { \
        BASE_CLASS::clear_local_opt_cache(); \
    }

#define __CLEAR_CACHE__(name) name = std::nullopt;
#define EXTRA_OPT_CACHE(BASE_CLASS, ...) \
    void clear_local_opt_cache() override { \
        BASE_CLASS::clear_local_opt_cache(); \
        MAP(__CLEAR_CACHE__, __VA_ARGS__) \
    }

