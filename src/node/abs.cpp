#include "abs.hpp"

using util::register_code
    , util::merge_code
    , util::if_statement
    , util::clocked_block;
using node::do_constant_fold;

Abs::Abs(Rc<FracNode> input)
    : FracNode({input.weak()}), input(input)
{}

Rc<Abs> Abs::create(Rc<FracNode> input) {return Rc<Abs>(new Abs(input));}

std::vector<std::string> Abs::get_code(WordLength wl) {
    return merge_code({
        {register_code(base_name(), register_size(wl))},
        clocked_block(if_statement(
            {"$signed(" + input->as_parent_register_unchanged(*this, wl) + ") < 0"},
            {base_name() + " <= -" + input->as_parent_register_unchanged(*this, wl) + ";"},
            {base_name() + " <= " + input->as_parent_register_unchanged(*this, wl) + ";"}
        ))
    });
}
std::vector<std::string> Abs::get_cpp(WordLength word_length, CppDataType t) {
    switch (t) {
        case CppDataType::Fp:
            return {
                this->cpp_datatype(word_length, t) + " " + base_name()
                    + " = " + input->base_name() + ".abs();"
            };
        case CppDataType::Ap:
            return {
                this->cpp_datatype(word_length, t) + " " + base_name()
                    + " = hls::abs(" + input->base_name() + ");"
            };
        case CppDataType::Ac:
            UNIMPLEMENTED;
        case CppDataType::Float:
            return {
                "float " + base_name() + " = abs(" + input->base_name() + ")"
            };
    }
}


std::string Abs::variable_name() const {return "abs_" + input->base_name();}

// TODO: Update bounds for cases where input bounds are on the same side of 0
// I.e. -100 < x < -50 -> 50 < y < 100, not 0 < y
double Abs::_min() const {return 0;}
double Abs::_max() const {return std::max(std::abs(input->min()), std::abs(input->max()));}

ExecutionType Abs::recalculate_value() {
    // return abs(this->input->calculate_value());
    // Fixpoint lib does not support abs
    auto input = this->input->calculate_value();
    if(input < (ExecutionType) 0) {
        return -input;
    }
    return input;
}

std::string Abs::graph_label_name() {return "abs";}


std::optional<Rc<Node>> Abs::constant_fold() {
    return do_constant_fold(*this, &input, [](auto x){return std::abs(x);});
}

DERIVE_PARENT_FNS(Abs, input);

