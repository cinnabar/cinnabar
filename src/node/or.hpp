#pragma once

#include "boolean_binary_operator.hpp"

class Or : public BooleanBinaryOperator<BoolNode> {
    BOOLBINOP_COMMON(Or, BoolNode);
    std::optional<Rc<Node>> remove_redundant_bool_computations() override;
    public:
        int computation_time(WordLength) const override;
};
