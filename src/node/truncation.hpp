#pragma once

#include "frac.hpp"

class Truncate : public FracNode {
    public:
        static Rc<Truncate> create(Rc<FracNode> input);

        double _min() const override;
        double _max() const override;
        // TODO: Handle bit lengths < 0

        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<std::string> get_cpp(WordLength word_length, CppDataType) override;
        std::vector<Rc<Node>> get_parents() const override;

        std::string graph_label_name() override;

        void optimize_parents(OptPass pass) override;
        std::optional<Rc<Node>> constant_fold() override;

        IMPL_DEFAULT_COMPARISON;
        IMPL_DEFAULT_CLONE(Truncate);
        NO_EXTRA_FILES
    protected:
        ExecutionType recalculate_value() override;
        std::string variable_name() const override;

        Truncate(Rc<FracNode> input);
        Rc<FracNode> input;
};
