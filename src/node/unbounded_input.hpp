#pragma once

#include "frac.hpp"

class UnboundedInput : public FracNode {
    public:
        static Rc<UnboundedInput> create(std::string name);

        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<std::string> get_cpp(WordLength word_length, CppDataType) override;
        double _min() const override;
        double _max() const override;
        bool is_input() const override;
        std::string graph_label_name() override;
        void optimize_parents(OptPass pass) override;
        std::optional<Rc<Node>> constant_fold() override;

        IMPL_DEFAULT_COMPARISON;
        IMPL_DEFAULT_CLONE(UnboundedInput);
        NO_EXTRA_FILES
    protected:
        ExecutionType recalculate_value() override;

        std::string variable_name() const override;

        std::vector<Rc<Node>> get_parents() const override;
    private:
        UnboundedInput(std::string name);

        const std::string name;
};

