#pragma once

#include "frac.hpp"

class Input : public FracNode {
    public:
        static Rc<Input> create(
            std::string name,
            float min,
            float max,
            ExecutionType value
        );

        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<std::string> get_cpp(WordLength, CppDataType) override;
        double _min() const override;
        double _max() const override;
        bool is_input() const override;
        std::string graph_label_name() override;
        void optimize_parents(OptPass pass) override;
        std::optional<Rc<Node>> constant_fold() override;
        void set_value(ExecutionType value);

        std::string variable_definition(WordLength wl);

        IMPL_DEFAULT_COMPARISON;
        IMPL_DEFAULT_CLONE(Input);
        NO_EXTRA_FILES
    protected:
        ExecutionType recalculate_value() override;

        std::string variable_name() const override;

        std::vector<Rc<Node>> get_parents() const override;
    private:
        Input(std::string name, float min, float max, ExecutionType value);

        const std::string name;
        const float min_val;
        const float max_val;
        ExecutionType value;
};
