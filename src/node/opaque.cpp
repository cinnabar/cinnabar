#include "opaque.hpp"

using NodeError::UnsupportedNodeOp;

using util::clocked_block
    , util::merge_code;


OpaqueNode::OpaqueNode(Rc<FracNode> parent) : FracNode({}), parent{parent} {}

Rc<OpaqueNode> OpaqueNode::create(Rc<FracNode> parent) {
    return Rc<OpaqueNode>(new OpaqueNode(parent));
}

std::vector<std::string> OpaqueNode::get_code(WordLength wl) {
    auto register_end = std::to_string(register_size(wl) - 1);
    return merge_code({
        {"reg[" + register_end + ":0] " + base_name() + ";"},
        // {"input[" + register_end + ":0] " + base_name() + "_non_reg;"},
        clocked_block({base_name() + " <= " + base_name() + "_non_reg;"})
    });
}

std::vector<std::string> OpaqueNode::get_cpp(WordLength, CppDataType) {
    return {};
}

double OpaqueNode::_min() const {
    return parent->min();
}
double OpaqueNode::_max() const {
    return parent->max();
}

std::string OpaqueNode::graph_label_name() {
    return "Opaque";
}

bool OpaqueNode::is_input() const {
    return true;
}


// We don't run any optimisations on opaque nodes or their parents since their parents
// will be invisible to the outside world
void OpaqueNode::optimize_parents(OptPass) {}
std::optional<Rc<Node>> OpaqueNode::constant_fold() { return std::nullopt; }
std::vector<Rc<Node>> OpaqueNode::get_parents() const {
    return {};
}

ExecutionType OpaqueNode::recalculate_value() {
    throw UnsupportedNodeOp{this->base_name(), "Opaque nodes can not be used in computation"};
}

std::string OpaqueNode::variable_name() const {
    return "opaque_" + parent->base_name();
}

