#pragma once

#include "frac.hpp"
#include "bool.hpp"
#include "bool_const.hpp"
#include "../exception_util.hpp"

namespace select_util {
    // Determines how non-inclusive Select values are handled. Only applies to
    // verilog codegen, not c++.
    enum class ElseMethod {
        // Add no else clause at the end, instead let the previous value be
        // kept around
        Previous,
        // Set the last branch as the else branch, essentially making it a
        // "when others" branch
        ElseLast,
        // Explicitly add a new else branch which sets the output value to verilog `X`
        ElseUnknown,
    };

    struct SingleBranchSelect : std::exception {
        WITH_MESSAGE("Found a select node with only a single branch. Run the RemoveSingleSelect opt pass")
    };
};

// Selects one of a list of values using an if-else statement. The conditoins
// may be overlapping, if this is the case, the first value with a true condition
// is selected.
//
// If no conditions hold, behaviour is undefined. The C++ implementation uses
// an uninitialised value while the verilog impl uses the previous value.
template<typename T>
class Select : public T {
    public:
        using InnerType = std::vector<std::pair<Rc<T>, std::vector<Rc<BoolNode>>>>;

        std::vector<std::string> get_code(WordLength word_length) override {
            using select_util::ElseMethod;

            std::vector<std::string> if_statement;

            if (vals.size() <= 1) {
                throw select_util::SingleBranchSelect();
            }

            for(auto it = vals.begin(); it != vals.end(); it++) {
                bool is_last_iteration = it == vals.end() - 1;
                auto [val, conds] = *it;
                bool replace_elseif = conds.empty()
                        || (is_last_iteration && else_method == ElseMethod::ElseLast);
                std::string cond_string;
                if (it == vals.begin()) {
                    cond_string = "if";
                }
                else if (replace_elseif) {
                    cond_string = "else ";
                }
                else {
                    cond_string = "else if";
                }

                // TODO: The previous code allowed this. In the future we can
                // get rid of this check, and the conds.empty() check, and just
                // assume that it would be the last element. This seems like
                // the right behaviour, but I'll put this assertion here until
                // we aren't sure this causes issues
                // 2021-06-16
                if(conds.empty() && !is_last_iteration) {
                    err("Found an empty condition in a select, but this was not the last element");
                }

                if (!replace_elseif) {
                    cond_string += "(";
                    bool first_condition = true;
                    for(auto cond : conds) {
                        cond_string += first_condition ? "" : " && ";
                        cond_string += cond->as_parent_register(*this, word_length);
                        first_condition = false;
                    }
                    cond_string += ") ";
                }

                cond_string += "begin";
                if_statement.push_back(cond_string);
                if_statement.push_back(
                    "\t" + this->base_name() + " <= " + this->value_register(*val, word_length) + ";"
                );
                if_statement.push_back("end");
            }

            if(else_method == select_util::ElseMethod::ElseUnknown) {
                if_statement.push_back("else begin");
                if_statement.push_back(
                    // TODO: Use proper word length here
                    "\t" + this->base_name() + " <= 128'bx;"
                );
                if_statement.push_back("end");
            }

            // If we need to sign extend the true branch
            return util::merge_code({
                {util::register_code(this->base_name(), this->register_size(word_length))},
                util::clocked_block(if_statement)
            });
        }
        std::vector<std::string> get_cpp(WordLength wl, CppDataType t) override {
            std::vector<std::string> if_statement;
            bool first_iteration = true;

            for(auto [val, conds] : vals) {
                std::string cond_string;
                if (first_iteration) {
                    cond_string = "if";;
                }
                else if (!conds.empty()) {
                    cond_string = "else if";
                }
                else {
                    cond_string = "else";
                }

                if (!conds.empty()) {
                    cond_string += "(";
                    bool first_condition = true;
                    for(auto cond : conds) {
                        cond_string += first_condition ? "" : " && ";
                        cond_string += cond->base_name();
                        first_condition = false;
                    }
                    cond_string += ")";
                }
                cond_string += " {";
                if_statement.push_back(cond_string);
                if_statement.push_back(
                    "\t" + this->base_name() + " = " + this->optional_cast_method(val->base_name(), wl, t) + ";"
                );
                if_statement .push_back("}");
                first_iteration = false;
            }
            // If we need to sign extend the true branch
            return util::merge_code({
                {this->cpp_datatype(wl, t) + " " + this->base_name() + ";"},
                if_statement
            });
        }

        std::string graph_label_name() override {
            return "select";
        }

        void optimize_parents(OptPass pass) override {
            for (auto& [val, conds] : vals) {
                for(Rc<BoolNode>& cond : conds) {
                    ::optimize_parents(pass, &cond);
                }
                ::optimize_parents(pass, &val);
            }
        }
        std::optional<Rc<Node>> constant_fold() override {
            // Because this might be a fairly tricky thign to constant fold, we'll hold
            // back on actually doing it, but we'll report an error if we are in a situation
            // where we can do constant fold
            bool has_non_const_condition = false;
            for (auto& [val, conds] : vals) {
                for(Rc<BoolNode>& cond : conds) {
                    if(!cond.pointer_cast<BoolConst>().has_value()) {
                        has_non_const_condition = true;
                        break;
                    }
                }
            }

            if(!has_non_const_condition) {
                UNIMPLEMENTED
            }
            return std::nullopt;
        }



        std::vector<Rc<Node>> get_parents() const override {
            std::vector<Rc<Node>> result;
            for(auto [val, conds] : vals) {
                result.push_back(val);
                std::copy(conds.begin(), conds.end(), std::back_inserter(result));
            }
            return result;
        }

        NO_EXTRA_FILES
    protected:
        std::string variable_name() const override {
            std::string result = "select";
            for(auto [val, conds] : this->vals) {
                for(auto cond : conds) {
                    result += "_";
                    result += cond->base_name();
                }
                result += "_" + val->base_name();
            }
            return result;
        }


        static std::set<Weak<Node>> select_parents(Select::InnerType const& vals) {
            std::set<Weak<Node>> result;
            for(auto [value, conditions] : vals) {
                result.insert(value.weak());
                for(auto cond : conditions) {
                    result.insert(cond.weak());
                }
            }
            return result;
        }

        InnerType vals;

        Select(InnerType vals) : T{select_parents(vals)}, vals{vals} {}

        // Frac node assignment may need a `cast`, if so, it should be specified here
        virtual std::string optional_cast_method(std::string value, WordLength word_length, CppDataType t) const = 0;

        std::optional<Rc<Node>> select_else_last() override {
            this->else_method = select_util::ElseMethod::ElseLast;
            return std::nullopt;
        }
        std::optional<Rc<Node>> select_else_dont_care() override {
            // auto clone = this->clone();
            this->else_method = select_util::ElseMethod::ElseUnknown;
            return std::nullopt;
        }

        std::optional<Rc<Node>> remove_single_select() override {
            if(this->vals.size() == 1) {
                return this->vals[0].first;
            }
            return std::nullopt;
        }

        // Return the verilog code for getting the value of the true_register with the
        // correct word length and fractinoal point location
        virtual std::string value_register(T& value, WordLength wl) = 0;
    private:
        select_util::ElseMethod else_method{select_util::ElseMethod::Previous};
};

class SelectFrac : public Select<FracNode> {
    public:
        IMPL_DEFAULT_COMPARISON;
        IMPL_DEFAULT_CLONE(SelectFrac);
        static Rc<SelectFrac> create(InnerType vals) {
            return Rc(new SelectFrac(vals));
        };

        double _min() const override {
            std::vector<double> mins;
            std::transform(
                std::begin(this->vals),
                std::end(this->vals),
                std::back_inserter(mins),
                [](auto bundle){auto [val, cond] = bundle; return val->min();}
            );
            return *std::min_element(mins.begin(), mins.end());
        }
        double _max() const override {
            std::vector<double> maxes;
            std::transform(
                std::begin(this->vals),
                std::end(this->vals),
                std::back_inserter(maxes),
                [](auto bundle){auto [val, cond] = bundle; return val->max();}
            );
            return *std::max_element(maxes.begin(), maxes.end());
        }

        ExecutionType recalculate_value() override {
            for(auto [val, conds] : this->vals) {
                auto fullfilled = std::all_of(
                    conds.begin(),
                    conds.end(),
                    [](auto cond){return cond->calculate_value();}
                );
                if(fullfilled) {
                    return val->calculate_value();
                }
            }
            throw "Unreachable select variant";
        }

    private:
        using InnerType = Select<FracNode>::InnerType;
        SelectFrac(InnerType vals)
            : Select{vals}
        {}


        virtual std::string value_register(FracNode& value, WordLength wl) override {
            return value.as_parent_register(
                *this,
                wl,
                this->integer_bits(wl),
                this->fractional_bits(wl),
                this->has_forced_bound()
            );
        }


        std::string optional_cast_method(std::string value, WordLength wl, CppDataType t) const override {
            return cppcode::cast_value(value, this->integer_bits(wl), this->fractional_bits(wl), t);
        }
};

class SelectBool : public Select<BoolNode> {
    public:
        IMPL_DEFAULT_COMPARISON;
        IMPL_DEFAULT_CLONE(SelectBool);
        static Rc<SelectBool> create(InnerType vals) {
            return Rc(new SelectBool(vals));
        };

        bool recalculate_value() override {
            UNIMPLEMENTED
        }


        std::string value_register(BoolNode& value, WordLength wl) override {
            return value.as_parent_register(*this, wl);
        }

    private:
        using InnerType = Select<BoolNode>::InnerType;
        SelectBool(InnerType vals)
            : Select{vals}
        {}


        std::string optional_cast_method(std::string value, WordLength, CppDataType) const override {
            return value;
        }
};
