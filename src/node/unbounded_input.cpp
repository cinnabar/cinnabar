#include "unbounded_input.hpp"

// Incomplete input
UnboundedInput::UnboundedInput(std::string name)
    : FracNode({})
    , name(name)
{}
Rc<UnboundedInput> UnboundedInput::create(std::string name) {
    return Rc<UnboundedInput>(new UnboundedInput(name));
}

std::vector<std::string> UnboundedInput::get_code(WordLength) {
    return {};
}

std::vector<std::string> UnboundedInput::get_cpp(WordLength, CppDataType) {return {};}
double UnboundedInput::_min() const {return 0;}
double UnboundedInput::_max() const {return 0;}
bool UnboundedInput::is_input() const {return false;}
std::string UnboundedInput::graph_label_name() {
    return "";
}
void UnboundedInput::optimize_parents(OptPass) {};
std::optional<Rc<Node>> UnboundedInput::constant_fold() {return std::nullopt;}
ExecutionType UnboundedInput::recalculate_value() {return 0;}
std::string UnboundedInput::variable_name() const {return name;};
std::vector<Rc<Node>> UnboundedInput::get_parents() const {return {};}

