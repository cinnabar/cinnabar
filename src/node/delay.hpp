#pragma once

#include "base.hpp"


// Node that delays the value in this node `delay` clock cycles. Primarily used
// in tests
class Delay : public Node {
    public:
        static Rc<Delay> create(Rc<Node> parent, int delay);
        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<std::string> get_cpp(WordLength word_length, CppDataType) override;
        std::vector<Rc<Node>> get_parents() const override;
        void mark_dirty() override;
        std::string display_value() override;
        std::string graph_label_name() override;
        int register_size(WordLength word_length) const override;
        // Recalculate the value if it is dirty
        void update_value() override;
        std::string cpp_datatype(WordLength word_length, CppDataType) const override;
        std::optional<std::string> compare_local(Weak<Node> other) const override;
        int computation_time(WordLength) const override;
        IMPL_DEFAULT_CLONE(Delay);
        NO_EXTRA_FILES
        NO_EXTRA_OPT_CACHE(Node);
    protected:
        Delay(Rc<Node> parent, int delay);

        std::string variable_name() const override;

        void optimize_parents(OptPass pass) override;
        std::optional<Rc<Node>> constant_fold() override;
    private:
        int delay;
        Rc<Node> parent;
};

