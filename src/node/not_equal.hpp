#pragma once

#include "frac.hpp"
#include "boolean_binary_operator.hpp"

class NotEqual : public BooleanBinaryOperator<FracNode> {
    BOOLBINOP_COMMON(NotEqual, FracNode);
    HAS_REDUNDANT_COMPARISON_REMOVAL
};
