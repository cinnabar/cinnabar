#pragma once

#include "frac.hpp"
#include "boolean_binary_operator.hpp"


class GreaterThan : public BooleanBinaryOperator<FracNode> {
    BOOLBINOP_COMMON(GreaterThan, FracNode);
    HAS_REDUNDANT_COMPARISON_REMOVAL
};
