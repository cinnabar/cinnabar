#pragma once

#include "frac.hpp"

class BinaryOperator : public FracNode {
    public:
        std::vector<Rc<Node>> get_parents() const override;
        void optimize_parents(OptPass pass) override;

        std::string graph_label_name() override;
    protected:
        std::string variable_name() const override;

        virtual std::string operator_symbol() const = 0;
        virtual std::string operator_name() const = 0;

        Rc<FracNode> lhs;
        Rc<FracNode> rhs;

        BinaryOperator(Rc<FracNode> lhs, Rc<FracNode> rhs);
};



#define BINOP(Name, ...) class Name : public BinaryOperator { \
    public: \
        Name(Rc<FracNode> rhs, Rc<FracNode> lhs); \
        double _min() const override; \
        double _max() const override; \
        std::string operator_symbol() const override; \
        std::string operator_name() const override; \
        std::vector<std::string> get_code(WordLength word_length) override; \
        std::vector<std::string> get_cpp(WordLength word_length, CppDataType) override; \
        std::optional<Rc<Node>> constant_fold() override; \
        IMPL_DEFAULT_COMPARISON; \
        IMPL_DEFAULT_CLONE(Name); \
    protected: \
        ExecutionType recalculate_value() override; \
    __VA_ARGS__ \
}

BINOP(Add, NO_EXTRA_FILES);
BINOP(Sub, NO_EXTRA_FILES);
BINOP(
    Mul,
    NO_EXTRA_FILES
    public:
        int computation_time(WordLength) const override;
);
// TODO: Test error function
BINOP(Div,
    int computation_time(WordLength) const override;
    std::optional<Rc<Node>> replace_division_by_constant() override;
    std::vector<std::string> check_for_problems() const override;
    public:
        NO_EXTRA_FILES
);
