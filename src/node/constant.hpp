#pragma once


#include "frac.hpp"

class Constant : public FracNode {
    public:
        static Rc<Constant> create(std::string name, const double value);

        const std::string name;
        const double value;

        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<std::string> get_cpp(WordLength word_length, CppDataType) override;
        double _min() const override {return value;}
        double _max() const override {return value;}

        std::string pipeline_buffer_name(int n) override;
        std::string graph_label_name() override;
        std::vector<std::string> generate_pipeline_code(WordLength word_length) override;

        void optimize_parents(OptPass pass) override;
        std::optional<Rc<Node>> constant_fold() override;

        std::optional<std::string> compare_local(Weak<Node> other) const override;

        AaType uncached_compute_aa() override;

        std::string graph_node_color() const override {return "burlywood1";}

        bool is_constant() const override {return true;};

        IMPL_DEFAULT_CLONE(Constant);
        NO_EXTRA_FILES
    protected:
        std::string variable_name() const override {return name;};
        std::vector<Rc<Node>> get_parents() const override {return {};}
        ExecutionType recalculate_value() override;
    private:
        Constant(std::string name, const double value);
};


