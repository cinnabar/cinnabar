#include "non_referenced.hpp"

using NodeError::UnsupportedNodeOp;

Rc<NonReferencedNode> NonReferencedNode::create(
    std::string name,
    std::string description,
    std::vector<Rc<Node>> parents
) {
    return Rc(new NonReferencedNode(name, description, parents));
}
NonReferencedNode::NonReferencedNode(std::string name, std::string description, std::vector<Rc<Node>> parents)
    : Node{node::weak_pointers(parents)}
    , name{name}
    , description{description}
    , parents{parents}
{}

std::vector<std::string> NonReferencedNode::get_code(WordLength) {
    throw UnsupportedNodeOp(
        this->variable_name(),
        "Trying to generate code for NonReferencedNode with description: " + description
    );
}
std::vector<std::string> NonReferencedNode::get_cpp(WordLength, CppDataType) {
    throw UnsupportedNodeOp(
        this->variable_name(),
        "Trying to generate cpp for NonReferencedNode with description: " + description
    );
}
std::string NonReferencedNode::cpp_datatype(WordLength, CppDataType) const {
    throw UnsupportedNodeOp(
        this->variable_name(),
        "Trying to get cpp datatype for NonReferencedNode with description: " + description
    );
}
std::vector<Rc<Node>> NonReferencedNode::get_parents() const {
    return parents;
}
void NonReferencedNode::mark_dirty() {}
std::string NonReferencedNode::display_value() {
    throw UnsupportedNodeOp(
        this->variable_name(),
        "Trying to get display value of NonReferencedNode with description: " + description
    );
}
std::string NonReferencedNode::graph_label_name() {return name;}
void NonReferencedNode::update_value() {}
std::string NonReferencedNode::variable_name() const {
    return this->name;
}
int NonReferencedNode::register_size(WordLength) const {return 0;}
std::optional<std::string> NonReferencedNode::compare_local(Weak<Node>) const {
    return std::nullopt;
};
void NonReferencedNode::optimize_parents(OptPass pass) {
    for(auto& parent : parents) {
        ::optimize_parents(pass, &parent);
    }
}
std::optional<Rc<Node>> NonReferencedNode::constant_fold() {
    return std::nullopt;
}

