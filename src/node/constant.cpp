#include "constant.hpp"

Constant::Constant(std::string name, const double value)
    : FracNode({}), name(name), value(value)
{}

Rc<Constant> Constant::create(std::string name, const double value) {
    return Rc<Constant>(new Constant(name, value));
}

std::vector<std::string> Constant::get_code(WordLength word_length) {
    auto fractional_bits = this->fractional_bits(word_length);
    auto fixpoint_value = uint64_t(std::abs(value) * pow(2, fractional_bits));
    auto register_end = std::to_string(this->register_size(word_length) - 1);

    auto value_sign = std::signbit(value) ? "-" : "";
    return {
        "localparam signed [" + register_end + ":0] " + base_name()
            + " = $signed("
            + value_sign
            + std::to_string(this->register_size(word_length))
            + "'d" + std::to_string(fixpoint_value) + ");"
    };
}
std::vector<std::string> Constant::get_cpp(WordLength word_length, CppDataType t) {
    switch (t) {
    case CppDataType::Fp:
        return {
            "const " + this->cpp_datatype(word_length, t) + " "
                + this->base_name()
                + " = Fp<"
                + std::to_string(this->fractional_bits(word_length))
                + ">::from_raw("
                + std::to_string(
                    (int64_t)((this->value) * pow(2, this->fractional_bits(word_length)))
                )
                + "l);"
        };
    case CppDataType::Ac:
        return {
            "const " + this->cpp_datatype(word_length, t) + " "
                + this->base_name()
                + " = " + std::to_string(this->value) + ";"
        };
    case CppDataType::Ap:
        return {
            "const " + this->cpp_datatype(word_length, t) + " "
                + this->base_name()
                + " = " + std::to_string(this->value) + ";"
        };
    case CppDataType::Float:
        return {
            "const float " + this->base_name() + " = " + std::to_string(this->value) + ";"
        };
    }
}

std::string Constant::pipeline_buffer_name(int) {
    return base_name();
}

ExecutionType Constant::recalculate_value() {
    return value;
}

std::string Constant::graph_label_name() {
    return "const " + std::to_string(value);
}

std::vector<std::string> Constant::generate_pipeline_code(WordLength) {
    return {};
}


void Constant::optimize_parents(OptPass) {}
std::optional<Rc<Node>> Constant::constant_fold() {
    return std::nullopt;
}

std::optional<std::string> Constant::compare_local(Weak<Node> other) const {
    auto other_ = std::dynamic_pointer_cast<Constant>(other.lock());

    if(other_->value != this->value) {
        return "Incorrect value. Expected: " + std::to_string(this->value) +
            ", got: " + std::to_string(other_->value);
    }
    return std::nullopt;
}

AaType Constant::uncached_compute_aa() {
    return aa::constant(this->value);
}
