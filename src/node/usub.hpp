#pragma once

#include "frac.hpp"

class USub : public FracNode {
    public:
        static Rc<USub> create(Rc<FracNode> input);

        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<std::string> get_cpp(WordLength word_length, CppDataType) override;
        std::vector<Rc<Node>> get_parents() const override;
        double _min() const override;
        double _max() const override;

        std::string graph_label_name() override;

        void optimize_parents(OptPass pass) override;
        std::optional<Rc<Node>> constant_fold() override;

        IMPL_DEFAULT_COMPARISON;
        IMPL_DEFAULT_CLONE(USub);
        NO_EXTRA_FILES
    protected:
        ExecutionType recalculate_value() override;
    protected:
        std::string variable_name() const override;

        USub(Rc<FracNode> input);
        Rc<FracNode> input;
};

