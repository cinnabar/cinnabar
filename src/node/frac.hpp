#pragma once

#include "base.hpp"
#include "../exception_util.hpp"

struct AaCopy {
    static rust::Box<aa::Aa> copy(const rust::Box<aa::Aa>& other) {return clone_aa(other);}
};
using AaType = util::copyable<rust::Box<aa::Aa>, AaCopy>;

class FracNode : public Node {
    public:
        // Returns _min() unless the min/max value has been overridden
        double min() const;
        double max() const;
        // Return the minimum value of the node if it has not been overridden
        // by the user
        virtual double _min() const = 0;
        virtual double _max() const = 0;

        ExecutionType  calculate_value();
        void mark_dirty() override;
        std::string display_value() override;
        void update_value() override;
        int register_size(WordLength word_length) const override;
        int fractional_bits(WordLength word_length) const;
        int integer_bits(WordLength wl) const;
        std::string show_min_max_tightness(const int bar_length) const;
        std::string min_observed_str() const;
        std::string max_observed_str() const;
        bool observed_outside_bounds() const;
        std::optional<double> min_max_quality() const;

        void set_array_index(std::size_t array_position);
        std::size_t get_array_index() const;

        AaType compute_aa();

        // Total amount of bits needed to fully store this variable
        unsigned int total_bits(WordLength wl);

        std::string cpp_datatype(WordLength word_length, CppDataType kind) const override;

        // Change the min value from the automatically calculated value to this.
        // Useful if an if-statement clearly changes the bounds. But incorrect use can
        // lead to over or underflows
        void _override_min(double min) {forced_min = min;}
        // Change the max value from the automatically calculated value to this.
        // Useful if an if-statement clearly changes the bounds. But incorrect use can
        // lead to over or underflows
        void _override_max(double max) {forced_max = max;}
        bool has_forced_bound() const;

        // Returns the verilog code for the value of this node when it is used as a
        // parent of the provided node. The value is shifted to move the fixedpoint
        // to the specified position. If truncate is false, an exception will be thrown
        // if target_int_bits is too small to fit this value
        std::string as_parent_register(
            Node& child,
            WordLength wl,
            int target_int_bits,
            int target_fixedpoint,
            bool allow_fewer_int_bits
        );
        // Peforms the same function as `as_parent_register` but does not
        // perform sign extension or fractional bit alignment. Useful for example,
        // when comparing to 0
        std::string as_parent_register_unchanged(Node& child, WordLength wl);
    protected:
        FracNode(std::set<Weak<Node>> ancestors);

        // Compute the affine arithmetic bounds on this node. Called by compute_aa and uses
        // the aa_bounds member for cache
        virtual AaType uncached_compute_aa() {
            UNIMPLEMENTED_VIRTUAL
        };
        virtual ExecutionType recalculate_value() = 0;

        // Position of the size of this node in the array containing the
        // sizes of variables
        std::optional<std::size_t> array_position;

        double current_fractional_bits(double* fractional_bit_array) const;
        double default_truncation_error(
            std::vector<Weak<FracNode>> fractional_parents,
            double* fractional_bit_array
        ) const;

    private:
        std::optional<ExecutionType> previous_value;

        std::optional<ExecutionType> min_observed = std::nullopt;
        std::optional<ExecutionType> max_observed = std::nullopt;

        std::optional<double> forced_min;
        std::optional<double> forced_max;

        // Bounds on the value as computed using affine arithmetic
        std::optional<AaType> aa_bounds;

        void sanity_check_bounds(double value) const;

        // Cache
    private:
        // I feel dirty for writing this code. We really need to figure out a
        // better caching solution if we want to keep const-ness.
        //
        // Note added 2020/12/11
        mutable std::optional<double> min_cache;
        mutable std::optional<double> max_cache;
    protected:
        EXTRA_OPT_CACHE(Node, min_cache, max_cache)
};


class CastToFewerIntBits : public std::exception {
    public:
        CastToFewerIntBits(
            std::string node_name,
            int from_bits,
            int to_bits,
            std::string for_child
        )
            : from_bits(from_bits)
            , to_bits(to_bits)
            , node_name(node_name)
            , for_child(for_child)
        {}
        WITH_MESSAGE(
            "Casting ",
            node_name,
            " from ",
            std::to_string(from_bits),
            " integer bits to ",
            std::to_string(to_bits),
            " to fit ",
            for_child
        );
        int from_bits;
        int to_bits;
        std::string node_name;
        std::string for_child;
};
