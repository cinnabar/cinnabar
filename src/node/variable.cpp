#include "variable.hpp"

using util::register_code;

Variable::Variable(std::string name, const double min, const double max, ExecutionType value)
    : FracNode({}), name(name), min_val(min), max_val(max), value(value)
{}

Rc<Variable> Variable::create(
    std::string name,
    const double min,
    const double max,
    ExecutionType value
) {
    return Rc<Variable>(new Variable(name, min, max, value));
}

std::vector<std::string> Variable::get_code(WordLength word_length) {
    return {register_code(base_name(), register_size(word_length))};
}
std::vector<std::string> Variable::get_cpp(WordLength word_length, CppDataType t) {
    return {
        this->cpp_datatype(word_length, t)
            + " " + this->base_name() + " = " + std::to_string((float) this->value) + ";"
    };
}

ExecutionType Variable::recalculate_value() {
    return this->value;
}

void Variable::set_value(double value) {
    this->value = value;
}

std::string Variable::graph_label_name() {
    return "var " + base_name();
}

void Variable::optimize_parents(OptPass) {};
std::optional<Rc<Node>> Variable::constant_fold() {return std::nullopt;}
