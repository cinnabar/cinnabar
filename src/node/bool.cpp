#include "bool.hpp"
#include "merged_bool.hpp"

BoolNode::BoolNode(std::set<Weak<Node>> ancestors) : Node(ancestors) {}
bool BoolNode::calculate_value() {
    if (!previous_value.has_value()) {
        previous_value = recalculate_value();
    }
    // return recalculate_value();
    return previous_value.value();
}
void BoolNode::mark_dirty() {
    previous_value = std::nullopt;
}
void BoolNode::update_value() {
    this->calculate_value();
}

std::string BoolNode::display_value() {
    return std::to_string(calculate_value());
}

int BoolNode::register_size(WordLength) const {
    return 1;
}

std::string BoolNode::cpp_datatype(WordLength, CppDataType) const {
    return "bool";
}

std::optional<Rc<Node>> BoolNode::boolean_merge() {
    std::vector<Rc<Node>> non_bool_operands;
    std::vector<Rc<BoolNode>> inner_nodes;
    for (auto&& operand : this->get_unique_parents()) {
        if (auto b = operand.pointer_cast<BoolNode>()) {
            if (auto mb = operand.pointer_cast<MergedBool>()) {
                for(auto& inner : (*mb)->inner_nodes) {
                    inner_nodes.push_back(inner);
                }
                for(auto& operand : (*mb)->get_unique_parents()) {
                    non_bool_operands.push_back(operand);
                }
            }
            else {
                inner_nodes.push_back(*b);
            }
        }
        else {
            non_bool_operands.push_back(operand);
        }
    }
    inner_nodes.push_back(this->clone().pointer_cast<BoolNode>().value());
    return MergedBool::create(inner_nodes, non_bool_operands);
}

std::string BoolNode::as_parent_register(Node& child, WordLength wl) {
    return this->pipeline_buffer_name(
        child.start_depth(wl) - this->end_depth(wl) - 1
    );
}
