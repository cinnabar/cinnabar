#include "base.hpp"
#include "bool.hpp"

#include <sstream>


using util::register_code
    , util::merge_code
    , util::clocked_block
    , util::compare_types;

/*
  Computes and assigns the required delay slots for each node in the supplied vector
  given that their values are used at `depth`
*/
void node::assign_delay_slots(std::vector<Rc<Node>> nodes, int depth, WordLength wl) {
    std::set<Weak<Node>> unique_nodes;
    for(auto node : nodes) {
        unique_nodes.merge(node->unique_ancestors());
        unique_nodes.insert(node.weak());

        auto target = node->pipeline_proxy();
        target->request_delay_slots(depth - target->end_depth(wl) - 1);
    }

    for(auto node_ : unique_nodes) {
        auto node = node_.lock();
        auto depth = node->start_depth(wl);
        for(auto parent : node->get_parents()) {
            auto target = parent->pipeline_proxy();
            target->request_delay_slots(depth - target->end_depth(wl) - 1);
        }
    }
}

// Convenienve function for calling traverse depth first without having
// to write the node first
void node::traverse_depth_first(Weak<Node> root, std::function<void(Weak<Node>)> fn) {
    root.lock()->traverse_depth_first(root, fn);
}
void node::traverse_depth_first(Rc<Node> root, std::function<void(Weak<Node>)> fn) {
    traverse_depth_first(root.weak(), fn);
}


////////////////////////////////////////////////////////////////////////////////

// Nodes

/*
  Computes the start depth of a node with the specified parents
*/
int compute_start_depth(std::set<Weak<Node>> parents, WordLength wl) {
    int result = 0;
    for(auto ancestor_ : parents) {
        auto ancestor = ancestor_.lock();
        result = std::max(
            result,
            ancestor->end_depth(wl) + 1
        );
    }
    return result;
}

Node::Node(std::set<Weak<Node>> parents) {
    for(auto ancestor_ : parents) {
        auto ancestor = ancestor_.lock();
        this->ancestors.merge(ancestor->unique_ancestors());
        this->ancestors.insert(ancestor_);
    }
}


/*
  Sets the required delay slots of the node to at least `amount`
*/
void Node::request_delay_slots(int amount) {
    this->delay_slots = std::max(this->delay_slots.value_or(0), amount);
}

Node* Node::pipeline_proxy() {
    return this;
}

/*
  Returns the name of the nth pipeline register for this variable
*/
std::string Node::pipeline_buffer_name(int n) {
    if (n == 0) {
        return base_name();
    }
    return base_name() + "_pl" + std::to_string(n);
}

/*
  Generates the code for the pipeline registers required by this node.

  This function will throw an exception if `delay_slots` has not been set before calling.
*/
std::vector<std::string> Node::generate_pipeline_code(WordLength word_length) {
    // Ensure that we know how many delay slots will be used
    if (this->delay_slots == std::nullopt) {
        throw "Generate pipeline code was run without delay slots being assigned";
        exit(-1);
    }
    // Unwrap the value since we know it's not nullopt
    int delay_slots = this->delay_slots.value();
    // If no delay slots are required, we don't want to generate surrounding codej
    if(delay_slots == 0) {
        return {};
    }
    std::vector<std::string> result = {};
    // generate the registers
    auto last_var = base_name();
    for(int i = 0; i < delay_slots; ++i) {
        auto buffer_name = pipeline_buffer_name(i+1);
        result.push_back(register_code(buffer_name, register_size(word_length)));
        last_var = buffer_name;
    }
    // Generate the assignment code
    std::vector<std::string> assignment = {};
    last_var = base_name();
    for(int i = 0; i < delay_slots; ++i) {
        auto buffer_name = pipeline_buffer_name(i+1);
        assignment.push_back(buffer_name + " <= " + last_var + ";");
        last_var = buffer_name;
    }
    auto assignment_block = clocked_block(assignment);
    result.insert(result.end(), assignment_block.begin(), assignment_block.end());
    return result;
}

/*
  Calls fn on the nodes in this tree in a depth first order, i.e. child nodes
  are evaluated before parents. The order of evaluation for child nodes is the order
  they appear in get_parents
*/
void Node::traverse_depth_first(Weak<Node> self, std::function<void(Weak<Node>)> fn) {
    for(auto child : get_parents()) {
        child->traverse_depth_first(child.weak(), fn);
    }
    fn(self);
}


int Node::start_depth(WordLength wl) {
    if (adjusted_start_depth.has_value()) {
        return adjusted_start_depth.value();
    }
    // If we don't have a cached value, we must compute it
    else if(!cached_start_depth.has_value()) {
        auto p = this->get_parents();
        std::set<Weak<Node>> set;
        std::transform(
            p.begin(),
            p.end(),
            std::inserter(set, set.begin()),
            [](auto n){return n.weak();}
        );
        cached_start_depth = compute_start_depth(set, wl);
    }
    return cached_start_depth.value();
}
int Node::end_depth(WordLength wl) {
    int _start = start_depth(wl);
    return _start + computation_time(wl) - 1;
}
void Node::adjust_start_depth(int adjusted_start_depth) {
    this->adjusted_start_depth = adjusted_start_depth;
}


void Node::override_name(std::string forced_name) {
    this->forced_name = forced_name;
}
Rc<Node> Node::with_name(std::string name) {
    auto result = this->clone();
    result->override_name(name);
    return result;
}

std::string Node::base_name() {
    std::string name;
    if(forced_name.has_value()) {
        name = forced_name.value();
    }
    else if(cached_name.has_value()){
        name = cached_name.value();
    }
    else {
#ifdef RANDOM_NAME_SUFFIX
        if(!is_input()) {
            int name_end = rand();
            std::stringstream stream;
            stream << variable_name() << "_" << std::hex << name_end;
            name = stream.str();
        }
        else {
            name = variable_name();
        }
#else
        name = variable_name();
#endif
    }
    if(name.size() > 30) {
        std::size_t hash = std::hash<std::string>{}(name);
        // +name.size() because apparently hash collisions between simiarly lengthed
        // vars are very uncommmon
        cached_name = "h_" + std::to_string(hash) + std::to_string(name.size());
    }
    else {
        cached_name = name;
    }
    return cached_name.value();
}

std::vector<std::string> Node::generate_graph(
    std::function<std::string(Node*)> fn,
    std::function<std::optional<std::string>(Node*)> color_function,
    WordLength wl,
    bool include_delay
) {
    std::string parent_list = "";
    for(auto parent : get_parents()) {
        parent_list += parent->graph_end_name(wl, include_delay) + ",";
    }
    // Remove trailing ,
    if(!parent_list.empty()) {
        parent_list.pop_back();
    }
    auto node_type = graph_label_name();
    auto label = base_name();

    std::vector<std::string> extra_nodes;
    auto prev_extra = graph_identifier();
    for(auto extra : this->graph_extra_nodes(wl, include_delay)) {
        extra_nodes.push_back( extra
          + " [label=\"\", fillcolor="
          + "azure3"
          + ", style=filled"
          + "];"
        );
        extra_nodes.push_back(extra + " -> " + prev_extra + "");
        prev_extra = extra;
    }
    return merge_code({
        { graph_identifier()
          + " [label=\""
          + label
          + "\\n"
          + node_type
          + "\\n"
          + fn(this)
          + "\", fillcolor="
          + color_function(this).value_or(graph_node_color())
          + ", style=filled"
          + "];"
        },
        extra_nodes,
        {graph_identifier() + " -> {" + parent_list + "};"},
    });
}

std::string Node::graph_identifier() {
    return base_name();
}
std::vector<std::string> Node::graph_extra_nodes(WordLength wl, bool show_delay) {
    if (show_delay) {
        std::vector<std::string> result;
        for(int i = 1; i < computation_time(wl); i++) {
            result.push_back(this->graph_identifier() + "_d" + std::to_string(i));
        }
        return result;
    }
    else {
        return {};
    }
}
std::string Node::graph_end_name(WordLength wl, bool include_delay) {
    auto extra_nodes = graph_extra_nodes(wl, include_delay);
    if (!extra_nodes.empty()) {
        return extra_nodes.back();
    }
    return this->graph_identifier();
}

std::set<Weak<Node>> Node::unique_ancestors() {
    // std::set<Weak<Node>> result;

    // for(auto parent : this->get_parents()) {
    //     ::traverse_depth_first(parent, [&result](auto node){result.insert(node);});
    // }
    // return result;

    return ancestors;
}

std::optional<std::string> Node::compare(Weak<Node> other) const {
    // Check if the nodes are the same type
    auto other_ptr = other.lock().get();
    if(auto diff = compare_types(this, other_ptr)) {
        return diff;
    }
    // Check if the local node is the same. If there is a diff,
    // checking children is pointless
    auto local_diff = compare_local(other);
    if(local_diff.has_value()) {
        return local_diff.value();
    }

    std::vector<std::optional<std::string>> parent_diffs;
    bool is_same = true;
    auto self_parents = get_parents();
    auto other_parents = other.lock()->get_parents();
    if(self_parents.size() != other_parents.size()) {
        return "Different amount of parent nodes. self: " + std::to_string(self_parents.size()) + " other " + std::to_string(other_parents.size());
    }
    // for(auto parent : get_parents()) {
    for(std::size_t i = 0; i < self_parents.size(); ++i) {
        auto self_parent = self_parents[i];
        auto other_parent = other_parents[i];
        auto diff = self_parent->compare(other_parent.weak());
        if(diff.has_value()) {
            is_same = false;
        }
        parent_diffs.push_back(diff);
    }

    if(is_same) {
        return std::nullopt;
    }
    else {
        auto name = this->forced_name.value_or(this->variable_name());
        std::string result = name + ": " + typeid(*this).name();
        for(auto diff : parent_diffs) {
            auto diff_ = "\n" + diff.value_or("No diff");
            std::size_t pos = -1;
            while((pos = diff_.find("\n", pos+1)) != std::string::npos) {
                diff_.replace(pos, 1, "\n| ");
            }
            result += diff_;
        }
        return result;
    }
}


void Node::copy_properties(const Node& other) {
    this->forced_name = other.forced_name;
}

std::optional<Rc<Node>> Node::optimize(OptPass pass) {
    auto should_recalculate = !this->optimized.has_value()
        || std::get<0>(this->optimized.value()) != pass;
    if(should_recalculate) {
#ifdef PRINT_OPT_TREE
        std::cout << "Optimizing " << this->base_name() << std::endl;
        std::cout << ">>>" << std::endl;
#endif
        auto opt_result = uncached_optimize(pass);
        this->optimized = std::make_pair(pass, opt_result);
#ifdef PRINT_OPT_TREE
        std::cout << "Optimization changed node? " << this->optimized.has_value() << std::endl;
#endif

        // The parents of the node may have changed now, rebuild it
        this->ancestors.clear();
        for(auto parent : this->get_parents()) {
#ifdef PRINT_OPT_TREE
            std::cout << "Re-treeing " << parent->base_name() << std::endl;
#endif
            this->ancestors.merge(parent->unique_ancestors());
            this->ancestors.insert(parent.weak());
        }

        /*
        if(opt_result.has_value()) {
            debug(" | ", this->base_name(), " was changed by optimisation");
        }
        */
#ifdef PRINT_OPT_TREE
        std::cout << "<" << std::endl;
        std::cout << "<<<" << std::endl;
#endif
    }
    else {
#ifdef PRINT_OPT_TREE
        std::cout << "| Using cached " << this->base_name() << std::endl;
#endif
    }
    return std::get<1>(this->optimized.value());
}

#define OPT_PASS(PASS, IMPL) \
    case OptPass::PASS: { \
        auto result = this->IMPL(); \
        OptLogger::counter[OptPass::PASS] += result.has_value(); \
        return result; \
    }

std::optional<Rc<Node>> Node::uncached_optimize(OptPass pass) {
    this->clear_local_opt_cache();
    this->optimize_parents(pass);
    switch(pass) {
        // Tack Axel
        OPT_PASS(ConstantFold, constant_fold)
        OPT_PASS(RemoveRedundantComparisons, remove_redundant_comparisons)
        OPT_PASS(RemoveRedundantInterpValidity, remove_redundant_interp_validity)
        OPT_PASS(ConstantDivisionReplacement, replace_division_by_constant)
        OPT_PASS(RemoveRedundantBoolComputations, remove_redundant_bool_computations)
        OPT_PASS(MergeBools, boolean_merge)
        OPT_PASS(SelectElseLast, select_else_last)
        OPT_PASS(SelectElseX, select_else_dont_care)
        OPT_PASS(RemoveSingleSelect, remove_single_select)
    }
    // This can never be reached because the switch above covers all
    // cases. Sadly, g++ is too stupid to understand this
    throw "g++ is stupid exception";
}

int Node::computation_time(WordLength) const {
    return 1;
}

std::vector<Rc<Node>> Node::get_unique_parents() const {
    auto parents = get_parents();
    std::vector<Rc<Node>> result;
    std::set<std::string> included;
    for(auto parent : parents) {
        if(included.find(parent->base_name()) != included.end()) {
            continue;
        }
        included.insert(parent->base_name());
        result.push_back(parent);
    }
    return result;
}

void Node::clear_local_opt_cache() {
    this->cached_start_depth = std::nullopt;
}


std::set<Weak<Node>> node::weak_pointers(std::vector<Rc<Node>> in) {
    std::set<Weak<Node>> result;
    for(auto node : in) {
        result.insert(node.weak());
    }
    return result;
}

