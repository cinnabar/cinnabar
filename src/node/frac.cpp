#include "frac.hpp"
#include <string>

FracNode::FracNode(std::set<Weak<Node>> ancestors)
    : Node(ancestors) {}

ExecutionType FracNode::calculate_value() {
    ExecutionType value;
    if (!previous_value.has_value()) {
        value = recalculate_value();
        previous_value = value;
    }
    else {
        value = previous_value.value();
    }

    auto last_min = this->min_observed.value_or(value);
    this->min_observed = std::min(last_min, value);
    auto last_max = this->max_observed.value_or(value);
    this->max_observed = std::max(last_max, value);
    // return recalculate_value();
    return value;
}

void FracNode::sanity_check_bounds(double value) const {
    std::vector<std::string> messages;
    if(std::isinf(value)) {
        messages.push_back("bound is inf");
    }
    if(std::isnan(value)) {
        messages.push_back("bound is nan");
    }

    if(messages.size() != 0) {
        // Check if any of the operands here are "insane"
        bool are_parents_normal = true;
        for (auto operand : this->get_unique_parents()) {
            auto parent_frac = operand.pointer_cast<FracNode>();
            if(parent_frac.has_value()) {
                auto p = parent_frac.value();
                if(std::isnan(p->min()) || std::isinf(p->min()) || std::isnan(p->max()) || std::isinf(p->max())) {
                    are_parents_normal = false;
                    break;
                }
            }
        }

        // None of them are, so the inf/nan must be introduced here
        if(are_parents_normal) {
            warn("New nan or inf detected with non-extreme parents");
            warn("Node is ", const_cast<FracNode*>(this)->base_name());
            for (auto message : messages) {
                warn("\t", message);
            }
        }
    }
}
double FracNode::min() const {
    if(forced_min.has_value()) {
        return forced_min.value();
    }
    if(min_cache.has_value()) {
        return min_cache.value();
    }
    auto value = _min();
    sanity_check_bounds(value);
    min_cache = value;
    return value;
}
double FracNode::max() const {
    if(forced_max.has_value()) {
        return forced_max.value();
    }
    if(max_cache.has_value()) {
        return max_cache.value();
    }
    auto value = _max();
    sanity_check_bounds(value);
    max_cache = value;
    return value;
}
void FracNode::mark_dirty() {
    previous_value = std::nullopt;
}
void FracNode::update_value() {
    this->calculate_value();
}
std::string FracNode::display_value() {
    return std::to_string((double) calculate_value());
}
void FracNode::set_array_index(std::size_t array_position) {
    this->array_position = array_position;
}
std::size_t FracNode::get_array_index() const {
    return this->array_position.value();
}
double FracNode::current_fractional_bits(double* fractional_bit_array) const {
    return fractional_bit_array[array_position.value()];
}
double FracNode::default_truncation_error(
    std::vector<Weak<FracNode>> parents,
    double* bit_counts
) const {
    if(parents.size() == 0) {
        return pow(2, -this->current_fractional_bits(bit_counts));
    }
    else {
        // double result = 0;
        double highest_bit_count = -1000;
        for(auto node_ : parents) {
            auto node = node_.lock();
            if(node->current_fractional_bits(bit_counts) > highest_bit_count) {
                highest_bit_count = node->current_fractional_bits(bit_counts);
            }
        }
        return pow(2, -this->current_fractional_bits(bit_counts))
                - pow(2, -highest_bit_count);
    }
}
unsigned int FracNode::total_bits(WordLength wl) {
    // 1+ for sign bit
    return 1 + this->integer_bits(wl) + this->fractional_bits(wl);
}
template<class T> struct always_false : std::false_type {};
int FracNode::integer_bits(WordLength wl) const {
    return std::visit([this](auto&& arg) {
        using T = std::decay_t<decltype(arg)>;
        if constexpr (std::is_same_v<T, FixedTotalLength> || std::is_same_v<T, FixedFixpoint>)  {
            int result = ceil(log2(1+std::max(std::abs(min()), std::abs(max()))));
            if(min() == 0 && max() == 0) {
                result = 0;
            }
            return result;
        }
        else if constexpr (std::is_same_v<T, FixedType>) {
            return arg.int_bits;
        }
        else {
            static_assert(always_false<T>::value, "non-exhaustive visitor!");
        }
    }, wl);
}

int FracNode::register_size(WordLength wl) const {
    auto fractional_bits = this->fractional_bits(wl);
    auto sign_bit = 1;
    return fractional_bits + this->integer_bits(wl) + sign_bit;
}
int FracNode::fractional_bits(WordLength wl) const {
    // see https://en.cppreference.com/w/cpp/utility/variant/visit for explanation
    return std::visit([this, wl](auto&& arg) {
        using T = std::decay_t<decltype(arg)>;
        if constexpr (std::is_same_v<T, FixedFixpoint>) {
            return arg.position;
        }
        else if constexpr (std::is_same_v<T, FixedType>) {
            return arg.frac_bits;
        }
        else if constexpr (std::is_same_v<T, FixedTotalLength>) {
            return arg.length - this->integer_bits(wl);
        }
        else {
            static_assert(always_false<T>::value, "non-exhaustive visitor!");
        }
    }, wl);
}
std::string FracNode::cpp_datatype(WordLength wl, CppDataType t) const {
    return cppcode::fixed_point_type(this->integer_bits(wl), this->fractional_bits(wl), t);
}

std::string FracNode::show_min_max_tightness(int const bar_length) const {
    ExecutionType min_observed;
    ExecutionType max_observed;
    try {
        min_observed = this->min_observed.value();
    }
    catch(std::bad_optional_access& e) {
        return "Min value not present not present";
    }
    try {
        max_observed = this->max_observed.value();
    }
    catch(std::bad_optional_access& e) {
        return "Max value not present";
    }

    // If we overflowed, something is horribly wrong. Clearly mark that
    if(min_observed < this->min()) {
        return "[\033[0;91mERROR: min_observed < min ("
            + std::to_string(min_observed)
            + " < "
            + std::to_string(this->min())
            + ")\033[0m";
    }
    if(max_observed > this->max()) {
        return "[\033[0;91mERROR: max_observed < max ("
            + std::to_string(max_observed)
            + " < "
            + std::to_string(this->max())
            + ")\033[0m";
    }

    // We are in bounds, output bars to visualise
    auto range = this->max() - this->min();
    if (this->max() == this->min()) {
        return "No variation, constant?";
    }
    auto obs_start = (min_observed - this->min()) / range;
    auto obs_end = (max_observed - this->min()) / range;

    auto obs_length = obs_end - obs_start;

    return "["
        + std::string(obs_start * bar_length, '-')
        + std::string(obs_length * bar_length, '#')
        + std::string((1-obs_end) * bar_length, '-')
        + "]";
}
std::optional<double> FracNode::min_max_quality() const {
    auto expected_range = this->max() - this->min();
    if(!this->max_observed.has_value() || !this->min_observed.has_value()) {
        return std::nullopt;
    }
    auto actual_range = this->max_observed.value()
        - this->min_observed.value();
    if(expected_range != 0) {
        return actual_range / expected_range;
    }
    return 1.;
}
std::string FracNode::min_observed_str() const {
    if(auto value = this->min_observed) {
        return std::to_string(value.value());
    }
    return "--";
}
std::string FracNode::max_observed_str() const {
    if(auto value = this->max_observed) {
        return std::to_string(value.value());
    }
    return "--";
}
bool FracNode::observed_outside_bounds() const {
    return this->min_observed.value_or(min()) < min()
        || this->max_observed.value_or(max()) > max();
}
AaType FracNode::compute_aa() {
    if(!aa_bounds.has_value()) {
        aa_bounds = uncached_compute_aa();
    }
    return aa_bounds.value();
}

std::string FracNode::as_parent_register(
    Node& child,
    WordLength wl,
    int target_int_bits,
    int target_fixedpoint,
    bool allow_fewer_int_bits
) {
    auto buffer = this->pipeline_buffer_name(child.start_depth(wl) - this->end_depth(wl) - 1);

    int this_total_bits = this->total_bits(wl);
    int this_frac = this->fractional_bits(wl);

    if(!allow_fewer_int_bits && this->integer_bits(wl) > target_int_bits) {
        throw CastToFewerIntBits(
            this->base_name(),
            this->integer_bits(wl),
            target_int_bits,
            child.base_name()
        );
    }

    // Amount of bits to add on the right side through zero extention
    int zext_bits = std::max(0, target_fixedpoint - this_frac);
    // Amount of bits to add through sign extension
    int sext_bits = std::max(0, target_int_bits - this->integer_bits(wl));
    // Index of the leftmost bit to selext
    int left_index = this->total_bits(wl) - std::max(0, this->integer_bits(wl) - target_int_bits);
    int right_index = std::max(0, this_frac - target_fixedpoint);

    if(right_index > (int) this->total_bits(wl)) {
        warn("Trying to index ", child.base_name(), " with ", right_index, "  right_index but it only has ", this->total_bits(wl), " available");
        return std::to_string(1 + target_int_bits + target_fixedpoint) + "'b0";
    }

    if(left_index < 0) {
        warn("Trying to index ", child.base_name(), " with ", left_index, "  left_index  which is negative. Using with sign extension instead");
        return "{" + std::to_string(1 + target_int_bits + target_fixedpoint) + "{" + buffer + "[" + std::to_string(this_total_bits - 1) + "]}}";
    }

    auto sext_raw = "{" + std::to_string(sext_bits) + "{" + buffer + "[" + std::to_string(this_total_bits - 1) + "]}}, ";
    // Bit patterns
    auto zext = zext_bits ? ", " + std::to_string(zext_bits) + "'d0" : "";
    auto sext = sext_bits ? sext_raw : "";

    std::string indexed_buffer;
    if (right_index == 0 && left_index == this_total_bits) {
        indexed_buffer = buffer;
    }
    else {
        indexed_buffer = buffer + "[" + std::to_string(left_index-1) + ":" + std::to_string(right_index) + "]";
    }

    if (zext_bits == 0 && sext_bits == 0) {
        return indexed_buffer;
    }
    else {
        return "{" + sext + indexed_buffer + zext + "}";
    }
}

std::string FracNode::as_parent_register_unchanged(Node& child, WordLength wl) {
    return this->as_parent_register(child, wl, this->integer_bits(wl), this->fractional_bits(wl), false);
}


bool FracNode::has_forced_bound() const {
    return this->forced_max.has_value() || this->forced_min.has_value();
}
