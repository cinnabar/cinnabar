#pragma once

#include "frac.hpp"

class Sqrt : public FracNode {
    public:
        static Rc<Sqrt> create(Rc<FracNode> input);

        // TODO: Decide what to do when inputs can be < 0
        double _min() const override;
        double _max() const override;

        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<std::string> get_cpp(WordLength word_length, CppDataType) override;
        std::vector<Rc<Node>> get_parents() const override;

        int computation_time(WordLength) const override;

        std::string graph_label_name() override;

        void optimize_parents(OptPass pass) override;
        std::optional<Rc<Node>> constant_fold() override;

        std::vector<std::string> verilog_extra_commands(WordLength) const override;
        std::vector<std::string> extra_verilog_files(WordLength) override;

        IMPL_DEFAULT_COMPARISON;
        IMPL_DEFAULT_CLONE(Sqrt);
        NO_EXTRA_CPPS
    protected:
        ExecutionType recalculate_value() override;
    private:
        std::string variable_name() const override;

        std::string module_name(WordLength) const;

        Sqrt(Rc<FracNode> input);
        Rc<FracNode> input;
};

