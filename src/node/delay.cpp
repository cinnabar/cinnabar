#include "delay.hpp"

Rc<Delay> Delay::create(Rc<Node> parent, int delay) {
    return Rc(new Delay(parent, delay));
}
Delay::Delay(Rc<Node> parent, int delay)
    : Node{{parent.weak()}}
    , delay{delay}
    , parent{parent}
{}
std::vector<std::string> Delay::get_code(WordLength) {
    throw "Unsynthetisisable node";
}
std::vector<std::string> Delay::get_cpp(WordLength, CppDataType) {
    throw "Unsimulatable node";
}
std::vector<Rc<Node>> Delay::get_parents() const {
    return {this->parent};
}
void Delay::mark_dirty() {}
std::string Delay::display_value() {return "delay";}
std::string Delay::graph_label_name() {return "delay";}
int Delay::register_size(WordLength word_length) const {
    return parent->register_size(word_length);
}
void Delay::update_value() {}
std::string Delay::cpp_datatype(WordLength, CppDataType) const {
    throw "Unsimulatable node";
}
std::optional<std::string> Delay::compare_local(Weak<Node>) const {
    return std::nullopt;
}
int Delay::computation_time(WordLength) const {
    return delay;
}
std::string Delay::variable_name() const {
    return "delay_" + parent->base_name();
}

void Delay::optimize_parents(OptPass) {}
std::optional<Rc<Node>> Delay::constant_fold() {return std::nullopt;}

