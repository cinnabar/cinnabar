#pragma once

#include "frac.hpp"

class Variable : public FracNode {
    public:
        static Rc<Variable> create(
            std::string name,
            const double min,
            const double max,
            const ExecutionType value
        );

        void set_value(double value);

        const std::string name;
        const double min_val;
        const double max_val;

        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<std::string> get_cpp(WordLength word_length, CppDataType) override;
        double _min() const override {return min_val;}
        double _max() const override {return max_val;}

        std::string graph_label_name() override;

        void optimize_parents(OptPass pass) override;
        std::optional<Rc<Node>> constant_fold() override;

        IMPL_DEFAULT_COMPARISON;
        IMPL_DEFAULT_CLONE(Variable);
        NO_EXTRA_FILES
    protected:
        ExecutionType recalculate_value() override;
        std::string variable_name() const override {return name;};
        std::vector<Rc<Node>> get_parents() const override {return {};}
    private:
        Variable(std::string name, const double min, const double max, const ExecutionType value);

        ExecutionType value;
};

