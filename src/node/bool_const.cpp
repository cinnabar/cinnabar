#include "bool_const.hpp"

//////////////////////////////////////////////////////////////////////
// Boolean constants

BoolConst::BoolConst(bool value) : BoolNode({}), value(value) {
    this->forced_name = value ? "true" : "false";
}

Rc<BoolConst> BoolConst::create(bool value) {
    return Rc<BoolConst>(new BoolConst(value));
}
std::vector<std::string> BoolConst::get_code(WordLength) {
    return {
        "localparam " + base_name() + " = " + (value ? "1" : "0") + ";"
    };
}
std::vector<std::string> BoolConst::get_cpp(WordLength, CppDataType) {
    return {};
}
std::string BoolConst::variable_name() const {
    return value ? "true" : "false";
}
std::string BoolConst::graph_label_name() {
    return "boolconst " + base_name();
}

const Rc<BoolConst> BoolConst::True{new BoolConst{true}};
const Rc<BoolConst> BoolConst::False{new BoolConst{false}};

void BoolConst::optimize_parents(OptPass) {};
std::optional<Rc<Node>> BoolConst::constant_fold() {
    return std::nullopt;
}
std::optional<std::string> BoolConst::compare_local(Weak<Node> other_) const {
    if(auto other = std::dynamic_pointer_cast<BoolConst>(other_.lock())) {
        if (other->value != this->value) {
            return "value missmatch: " + std::to_string(this->value) + " != " +
                std::to_string(other->value);
        }
    }
    return std::nullopt;
}
