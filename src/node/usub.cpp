#include "usub.hpp"

using util::register_code
    , util::merge_code
    , util::clocked_block;
using node::do_constant_fold;

USub::USub(Rc<FracNode> input) : FracNode({input.weak()}), input(input) {};
Rc<USub> USub::create(Rc<FracNode> input) {
    return Rc<USub>(new USub(input));
}
std::vector<std::string> USub::get_code(WordLength wl) {
    auto input_val = input->as_parent_register(
        *this,
        wl,
        this->integer_bits(wl),
        this->fractional_bits(wl),
        this->has_forced_bound()
    );
    return merge_code({
        {register_code(this->base_name(), register_size(wl))},
        clocked_block({base_name() + " <= -" + input_val + ";"})
    });
}
std::vector<std::string> USub::get_cpp(WordLength wl, CppDataType t) {
    return {
        this->cpp_datatype(wl, t)
            + " " + base_name() + " = -" + input->base_name() + ";"
    };
}
double USub::_min() const {return -input->max();}
double USub::_max() const {return -input->min();}
ExecutionType USub::recalculate_value() {return -input->calculate_value();}
std::string USub::variable_name() const {
    return "usub_" + input->base_name();
}
std::string USub::graph_label_name() {return "-";}
std::optional<Rc<Node>> USub::constant_fold() {
    return do_constant_fold(*this, &input, [](auto x){return -x;});
}

Rc<USub> operator-(Rc<FracNode> input) {return USub::create(input);}

DERIVE_PARENT_FNS(USub, input);
