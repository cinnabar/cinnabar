#include "merged_bool.hpp"

using node::weak_pointers;

MergedBool::MergedBool(
    std::vector<Rc<BoolNode>> const& inner_nodes,
    std::vector<Rc<Node>> const& operands
) : BoolNode {weak_pointers(operands)}, inner_nodes{inner_nodes}, operands{operands}
    {}

Rc<MergedBool> MergedBool::create(
    std::vector<Rc<BoolNode>> const& inner_nodes,
    std::vector<Rc<Node>> const& operands
) {
    return Rc<MergedBool>(new MergedBool(inner_nodes, operands));
}

std::vector<std::string> MergedBool::get_code(WordLength) {
    throw NodeError::UnsupportedNodeOp("MergedBool", "get_code");
}
std::vector<std::string> MergedBool::get_cpp(WordLength, CppDataType) {
    throw NodeError::UnsupportedNodeOp("MergedBool", "get_cpp");
}
std::vector<Rc<Node>> MergedBool::get_parents() const {
    return this->operands;
}

std::string MergedBool::graph_label_name() {
    return std::to_string(this->inner_nodes.size()) + " merged bools";
}

std::string MergedBool::variable_name() const {
    std::string result = "merge_";
    for(auto&& operand : operands) {
        result += operand->base_name();
        result += "_";
    }
    for(auto&& inner : inner_nodes) {
        result += inner->base_name();
    }
    return result;
}


void MergedBool::optimize_parents(OptPass) {
    throw NodeError::UnsupportedNodeOp(
        "MergedBool",
        "Optimisation should not be run when merged bools are present"
    );
}

std::optional<Rc<Node>> MergedBool::constant_fold() {
    return std::nullopt;
}


bool MergedBool::recalculate_value() {
    UNIMPLEMENTED
}

