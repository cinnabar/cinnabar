#pragma once

#include "base.hpp"
#include "../exception_util.hpp"

namespace NonReferencedError {
    struct ParentsOfNonReferenced : std::exception {
        WITH_MESSAGE("Calling get_parents on NonReferenced node is disallowed");
    };
}

// A node which is never referenced downstream by other nodes. This is used for "marker nodes"
//
// If this node is synthesised, it will produce an exception which in practice means that
// it must not be reachable from any output node.
//
// For now, its use case is giving something for mercurous to generate for struct definitions
// which it needs to use internally for output.
class NonReferencedNode : public Node {
    public:
        static Rc<NonReferencedNode> create(std::string name, std::string description, std::vector<Rc<Node>> parents);

        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<std::string> get_cpp(WordLength word_length, CppDataType) override;
        std::vector<Rc<Node>> get_parents() const override;
        void mark_dirty() override;
        std::string display_value() override;
        std::string graph_label_name() override;
        int register_size(WordLength word_length) const override;
        // Recalculate the value if it is dirty
        void update_value() override;
        std::string cpp_datatype(WordLength word_length, CppDataType) const override;
        std::optional<std::string> compare_local(Weak<Node> other) const override;

        IMPL_DEFAULT_CLONE(NonReferencedNode);
        NO_EXTRA_FILES
        NO_EXTRA_OPT_CACHE(Node);
    protected:
        std::string variable_name() const override;

        void optimize_parents(OptPass pass) override;
        std::optional<Rc<Node>> constant_fold() override;

    private:
        NonReferencedNode(std::string name, std::string description, std::vector<Rc<Node>> parents);

        const std::string name;
        const std::string description;

        std::vector<Rc<Node>> parents;
};
