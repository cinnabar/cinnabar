#include "input.hpp"

using util::clocked_block
    , util::merge_code;


Input::Input(std::string name, float min, float max, ExecutionType value)
    : FracNode({})
    , name(name)
    , min_val(min)
    , max_val(max)
    , value(value)
{}

Rc<Input> Input::create(std::string name, float min, float max, ExecutionType value) {
    return Rc<Input>(new Input(name, min, max, value));
}


std::string Input::variable_definition(WordLength word_length) {
    auto register_end = std::to_string(register_size(word_length) - 1);
    return "input[" + register_end + ":0] " + base_name() + "_non_reg;";
}
std::vector<std::string> Input::get_code(WordLength word_length) {
    auto register_end = std::to_string(register_size(word_length) - 1);
    return merge_code({
        {"reg[" + register_end + ":0] " + base_name() + ";"},
        // {"input[" + register_end + ":0] " + base_name() + "_non_reg;"},
        clocked_block({base_name() + " <= " + base_name() + "_non_reg;"})
    });
}

std::vector<std::string> Input::get_cpp(WordLength, CppDataType) {return {};}
double Input::_min() const {return this->min_val;}
double Input::_max() const {return this->max_val;}
bool Input::is_input() const {return true;}
std::string Input::graph_label_name() {
    return "in " + base_name();
}
void Input::optimize_parents(OptPass) {};
std::optional<Rc<Node>> Input::constant_fold() {return std::nullopt;}
ExecutionType Input::recalculate_value() {return this->value;}
std::string Input::variable_name() const {return name;};
std::vector<Rc<Node>> Input::get_parents() const {return {};}
void Input::set_value(ExecutionType value) {this->value = value;}
