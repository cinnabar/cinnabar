#include "truncation.hpp"

using util::register_code
    , util::merge_code
    , util::clocked_block;
using node::do_constant_fold;

Truncate::Truncate(Rc<FracNode> input) : FracNode({input.weak()}), input(input) {}
Rc<Truncate> Truncate::create(Rc<FracNode> input) {
    return Rc<Truncate>(new Truncate(input));
}
double Truncate::_min() const {return (int)(input->min());}
double Truncate::_max() const {return (int)(input->max());}
std::vector<std::string> Truncate::get_code(WordLength word_length) {
    auto output_fixpoint = std::to_string(this->fractional_bits(word_length));
    auto input_fixpoint = std::to_string(input->fractional_bits(word_length));
    auto input_end = std::to_string(input->register_size(word_length) - 1);

    auto input_reg = input->as_parent_register_unchanged(*this, word_length);
    return merge_code({
        {register_code(base_name(), register_size(word_length))},
        clocked_block({base_name() + " <= " + "{" + input_reg
                + "[" + input_end + ":" + input_fixpoint + "], " + output_fixpoint + "'b0};"})
    });
}
std::vector<std::string> Truncate::get_cpp(WordLength word_length, CppDataType t) {
    switch (t) {
        case CppDataType::Fp: 
            return {
                this->cpp_datatype(word_length, t)
                    + " "
                    + this->base_name()
                    + " = "
                    + input->base_name()
                    + ".truncate();"
            };
        case CppDataType::Ap:
            return {
                this->cpp_datatype(word_length, t)
                    + " "
                    + this->base_name()
                    + " = hls::trunc("
                    + input->base_name()
                    + ");"
            };
        case CppDataType::Ac:
            UNIMPLEMENTED;
        case CppDataType::Float:
            return {
                "float " + this->base_name() + " = trunc(" + input->base_name() + ")"
            };
    }
}
std::string Truncate::graph_label_name() {return "truncate";}
ExecutionType Truncate::recalculate_value() {return (int)input->calculate_value();}
std::string Truncate::variable_name() const {
    return "truncate_" + input->base_name();
}
std::optional<Rc<Node>> Truncate::constant_fold() {
    return do_constant_fold(*this, &input, [](auto x){return (int) x;});
}

DERIVE_PARENT_FNS(Truncate, input);
