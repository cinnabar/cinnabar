#include "not.hpp"
#include "bool_const.hpp"


using util::merge_code;

std::atomic<bool> Not::compute_on_demand{false};
void Not::set_compute_on_demand(bool value) {
    Not::compute_on_demand = value;
}

Not::Not(Rc<BoolNode> input) : BoolNode({input.weak()}), input(input) {}
Rc<Not> Not::create(Rc<BoolNode> input) {
    return Rc<Not>(new Not(input));
}
std::vector<std::string> Not::get_code(WordLength wl) {
    if (Not::compute_on_demand) {
        return {};
    }
    else {
        return merge_code({
           {"wire " + base_name() + " = !" + input->as_parent_register(*this, wl) + ";"}
        });
    }
}
std::vector<std::string> Not::generate_pipeline_code(WordLength wl) {
    if(Not::compute_on_demand) {
        return {};
    }
    else {
        return this->BoolNode::generate_pipeline_code(wl);
    }
}
Node* Not::pipeline_proxy() {
    if(Not::compute_on_demand) {
        return &*this->input;
    }
    else {
        return this;
    }
}

std::string Not::as_parent_register(Node& child, WordLength wl) {
    if (Not::compute_on_demand) {
        return "!" + this->input->as_parent_register(child, wl);
    }
    else {
        return this->BoolNode::as_parent_register(child, wl);
    }
}

std::vector<std::string> Not::get_cpp(WordLength, CppDataType) {
    return {"bool " + base_name() + " = !" + input->base_name() + ";"};
}
bool Not::recalculate_value() {return !input->calculate_value();}
std::string Not::variable_name() const {
    return "not_" + input->base_name();
}
std::string Not::graph_label_name() {return "!";}

Rc<Not> operator!(Rc<BoolNode> input) {
    return Not::create(input);
}
std::optional<Rc<Node>> Not::constant_fold() {
    if(auto casted = input.pointer_cast<BoolConst>()) {
        return BoolConst::create(!(*casted)->value);
    }
    return std::nullopt;
}
DERIVE_PARENT_FNS(Not, input);
