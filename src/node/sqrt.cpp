#include "sqrt.hpp"
#include <cassert>

using  util::merge_code
    , util::verilog_variable;
using node::do_constant_fold;

Sqrt::Sqrt(Rc<FracNode> input) : FracNode({input.weak()}), input(input) {}
Rc<Sqrt> Sqrt::create(Rc<FracNode> input) {
    return Rc<Sqrt>(new Sqrt(input));
}
double Sqrt::_min() const {return sqrt(input->min());}
double Sqrt::_max() const {return sqrt(input->max());}
ExecutionType Sqrt::recalculate_value() {
#ifdef APPROXIMATE_SQRT
    #error "Approximate sqrt not implemented"
    return 0;
#else
    if(input->calculate_value() >= (ExecutionType) 0) {
        return sqrt((double)input->calculate_value());
    }
    else {
        std::cout << "Warning: Computing square root of -x, returning 0" << std::endl;
        return 0;
    }
#endif
};
std::vector<std::string> Sqrt::get_code(WordLength wl) {
    std::string unsigned_name = base_name() + "_unsigned";
    return merge_code({
        {verilog_variable(VarType::WIRE, base_name(), register_size(wl))},
        {verilog_variable(VarType::WIRE, unsigned_name, register_size(wl) - 1)},
        {module_name(wl)+ " " + base_name() + "_mod(clk, "
            + input->as_parent_register(*this, wl, input->integer_bits(wl), input->fractional_bits(wl), this->has_forced_bound()) + ", "
            + unsigned_name + ");"},
        {"assign " + base_name() + " = {1'b0, " + unsigned_name + "};"}
    });
}
std::vector<std::string> Sqrt::get_cpp(WordLength word_length, CppDataType t) {
    switch (t) {
        case CppDataType::Fp: {
            auto output_fp = std::to_string(this->fractional_bits(word_length));
            return {
                this->cpp_datatype(word_length, t) + " "
                    + this->base_name() + " = approx_sqrt<" + output_fp + ">(" + input->base_name() + ");"
            };
        }
        case CppDataType::Ap: {
            return {
                this->cpp_datatype(word_length, t) + " "
                    + this->base_name() + " = hls::sqrt(" + input->base_name() + ");"
            };
        }
        case CppDataType::Ac:
            UNIMPLEMENTED;
        case CppDataType::Float:
            return {
                "float " + this->base_name() + " = sqrt(" + input->base_name() + ")"
            };
    }
}
std::string Sqrt::variable_name() const {return "sqrt_" + input->base_name();}
std::string Sqrt::graph_label_name() {return "sqrt";}
std::optional<Rc<Node>> Sqrt::constant_fold() {
    return do_constant_fold(*this, &input, sqrt);
}
int Sqrt::computation_time(WordLength wl) const {
    return (this->input->register_size(wl) - this->input->fractional_bits(wl)) / 2 + this->fractional_bits(wl);
}

std::vector<std::string> Sqrt::verilog_extra_commands(WordLength wl) const {
    auto input_size = std::to_string(this->input->register_size(wl));
    auto input_fp = std::to_string(this->input->fractional_bits(wl));
    auto output_fp = std::to_string(this->fractional_bits(wl));

    auto outfile_name = "sqrt_" + input_size + "_" + input_fp + "_" + output_fp + ".v";

    return {"scripts/build_sqrt.hs " + outfile_name + " " + input_size + " " + input_fp + " " + output_fp};
}

std::vector<std::string> Sqrt::extra_verilog_files(WordLength wl) {
    auto input_size = std::to_string(this->input->register_size(wl));
    auto input_fp = std::to_string(this->input->fractional_bits(wl));
    auto output_fp = std::to_string(this->fractional_bits(wl));

    return {"sqrt_" + input_size + "_" + input_fp + "_" + output_fp + ".v"};
}

std::string Sqrt::module_name(WordLength wl) const {
    auto input_size = std::to_string(this->input->register_size(wl));
    auto input_fp = std::to_string(this->input->fractional_bits(wl));
    auto output_fp = std::to_string(this->fractional_bits(wl));

    return "sqrt_" + input_size + "_" + input_fp + "_" + output_fp;
}
DERIVE_PARENT_FNS(Sqrt, input);

