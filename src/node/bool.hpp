#pragma once

#include "base.hpp"

class BoolNode : public Node {
public:
    BoolNode(std::set<Weak<Node>> ancestors);

    bool calculate_value();
    void mark_dirty() override;
    void update_value() override;
    int register_size(WordLength word_length) const override;

    std::string display_value() override;
    std::string cpp_datatype(WordLength word_length, CppDataType kind) const override;

    std::string graph_node_color() const override {return "cadetblue";}

    // Returns the verilog code for the value of this node when it is used as a
    // parent of the provided node
    //
    // Virtual for `not` to override the behaviour
    virtual std::string as_parent_register(Node& child, WordLength wl);

    NO_EXTRA_OPT_CACHE(Node);
protected:
    virtual bool recalculate_value() = 0;

    std::optional<Rc<Node>> boolean_merge() override;

    // TODO: Is this used?
    std::optional<bool> previous_value;
};
