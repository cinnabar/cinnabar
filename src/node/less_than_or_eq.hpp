#pragma once

#include "frac.hpp"
#include "boolean_binary_operator.hpp"

class LessThanOrEq : public BooleanBinaryOperator<FracNode> {
    BOOLBINOP_COMMON(LessThanOrEq, FracNode);
    HAS_REDUNDANT_COMPARISON_REMOVAL
};
