#pragma once

#include "boolean_binary_operator.hpp"

class And : public BooleanBinaryOperator<BoolNode> {
    BOOLBINOP_COMMON(And, BoolNode);
    std::optional<Rc<Node>> remove_redundant_bool_computations() override;
    public:
        int computation_time(WordLength) const override;
};

