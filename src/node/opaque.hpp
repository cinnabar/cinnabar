#include "frac.hpp"

// An opaque node is one which behaves like an input but inherits its bounds
// from its parent
//
//      x   y
//       \ /
//        |
//        z
//        |
//        o
//
// If o is the opaque node, it will add a new input with unspecified name to
// the module which has the same bounds as z. This is mostly useful for cutting
// off part of the design without impacting the bit widths of subsequent
// operations

class OpaqueNode : public FracNode {
    public:
        static Rc<OpaqueNode> create(
            Rc<FracNode> parent
        );

        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<std::string> get_cpp(WordLength, CppDataType) override;
        double _min() const override;
        double _max() const override;
        std::string graph_label_name() override;
        void optimize_parents(OptPass pass) override;
        std::optional<Rc<Node>> constant_fold() override;
        std::vector<Rc<Node>> get_parents() const override;
        bool is_input() const override;

        IMPL_DEFAULT_COMPARISON;
        IMPL_DEFAULT_CLONE(OpaqueNode);
        NO_EXTRA_FILES
    protected:
        ExecutionType recalculate_value() override;

        std::string variable_name() const override;

    private:
        OpaqueNode( Rc<FracNode> parent );

        Rc<FracNode> parent;
};
