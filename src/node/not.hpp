#pragma once

#include "bool.hpp"

#include<atomic>

class Not : public BoolNode {
    public:
        static Rc<Not> create(Rc<BoolNode> input);

        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<std::string> get_cpp(WordLength word_length, CppDataType) override;
        std::vector<Rc<Node>> get_parents() const override;

        std::string graph_label_name() override;

        void optimize_parents(OptPass pass) override;
        std::optional<Rc<Node>> constant_fold() override;

        int computation_time(WordLength) const override {return 0;};

        std::string as_parent_register(Node& child, WordLength wl) override;
        std::vector<std::string> generate_pipeline_code(WordLength word_length) override;
        Node* pipeline_proxy() override;

        IMPL_DEFAULT_COMPARISON;
        IMPL_DEFAULT_CLONE(Not);
        NO_EXTRA_FILES
        static void set_compute_on_demand(bool value);

    protected:
        bool recalculate_value() override;

        std::string variable_name() const override;

        Not(Rc<BoolNode> input);

        Rc<BoolNode> input;

        // True if no pipeline registers should be generated for this operation and
        // values should be computed on demand.
        static std::atomic<bool> compute_on_demand;
};
