#pragma once

#include "base.hpp"
#include "frac.hpp"
#include "bool.hpp"


template<class T>
class If : public T {
    public:
        std::vector<std::string> get_code(WordLength word_length) override {
            // auto true_register = util::sign_extend(
            //     this->parent_register(on_true, word_length),
            //     on_true->register_size(word_length),
            //     this->register_size(word_length)
            // );
            // auto false_register = util::sign_extend(
            //     this->parent_register(on_false, word_length),
            //     on_false->register_size(word_length),
            //     this->register_size(word_length)
            // );
            // If we need to sign extend the true branch
            return util::merge_code({
                {util::register_code(this->base_name(), this->register_size(word_length))},
                util::clocked_block(util::if_statement(
                    condition->as_parent_register(*this, word_length),
                    {this->base_name() + " <= " + true_register(word_length) + ";"},
                    {this->base_name() + " <= " + false_register(word_length) + ";"}
                ))
            });
        }

        std::string graph_label_name() override {
            return "if";
        }

        std::optional<Rc<Node>> constant_fold() override {
            if(auto casted = condition.pointer_cast<BoolConst>()) {
                if((*casted)->value == true) {
                    return on_true;
                }
                return on_false;
            }
            return std::nullopt;
        }

        IMPL_DEFAULT_COMPARISON;
        DERIVE_PARENT_FNS_INLINE(condition, on_true, on_false);
        NO_EXTRA_FILES
    protected:
        std::string variable_name() const override {
            return "if_" + condition->base_name() + "_then_" +
                on_true->base_name() + "_else_" + on_false->base_name();
        }

        // Return the verilog code for getting the value of the true_register with the
        // correct word length and fractinoal point location
        virtual std::string true_register(WordLength wl) = 0;
        // Return the verilog code for getting the value of the false_register with the
        // correct word length and fractinoal point location
        virtual std::string false_register(WordLength wl) = 0;

        Rc<BoolNode> condition;
        Rc<T> on_true;
        Rc<T> on_false;

        If(Rc<BoolNode> condition, Rc<T> on_true, Rc<T> on_false)
            : T({condition.weak(), on_true.weak(), on_false.weak()})
            , condition(condition)
            , on_true(on_true)
            , on_false(on_false)
        {
            static_assert(std::is_base_of<Node, T>::value, "If-result must be a node");
        }
};

class IfFractional : public If<FracNode> {
    public:
        static Rc<IfFractional> create(
            Rc<BoolNode> condition,
            Rc<FracNode> on_true,
            Rc<FracNode> on_false
        ) {
            return Rc<IfFractional>(new IfFractional(condition, on_true, on_false));
        }

        double _min() const override {
            return std::min(on_true->min(), on_false->min());
        }
        double _max() const override {
            return std::max(on_true->max(), on_false->max());
        }

        // TODO: We may want to share this with the base class
        std::vector<std::string> get_cpp(WordLength wl, CppDataType t) override {
            return {
                this->cpp_datatype(wl, t) + " " + this->base_name() + ";",
                "if(" + this->condition->base_name() + ") {",
                util::indent(1, this->base_name() + " = " + cppcode::cast_value(
                    on_true->base_name(),
                    this->integer_bits(wl),
                    this->fractional_bits(wl),
                    t
                ) + ";"),
                "}",
                "else {",
                util::indent(1, this->base_name() + " = " + cppcode::cast_value(
                    on_false->base_name(),
                    this->integer_bits(wl),
                    this->fractional_bits(wl),
                    t
                ) + ";"),
                "}"
            };
        }

        IMPL_DEFAULT_CLONE(IfFractional);
    protected:
        ExecutionType recalculate_value() override {
            if(condition->calculate_value()) {
                return on_true->calculate_value();
            }
            return on_false->calculate_value();
        }

        std::string true_register(WordLength wl) override {
            return this->on_true->as_parent_register(
                *this,
                wl,
                this->integer_bits(wl),
                this->fractional_bits(wl),
                this->has_forced_bound()
            );
        }
        std::string false_register(WordLength wl) override {
            return this->on_false->as_parent_register(
                *this,
                wl,
                this->integer_bits(wl),
                this->fractional_bits(wl),
                this->has_forced_bound()
            );
        }
    private:
        IfFractional(
            Rc<BoolNode> condition,
            Rc<FracNode> on_true,
            Rc<FracNode> on_false
        )
            : If<FracNode>(condition, on_true, on_false)
        {}
};
class IfBoolean : public If<BoolNode> {
    public:
        static Rc<IfBoolean> create(
            Rc<BoolNode> condition,
            Rc<BoolNode> on_true,
            Rc<BoolNode> on_false
        ) {
            return Rc<IfBoolean>(new IfBoolean(condition, on_true, on_false));
        }

        // TODO: We may want to share this with the base class
        std::vector<std::string> get_cpp(WordLength word_length, CppDataType t) override {
            return {
                this->cpp_datatype(word_length, t) + " " + this->base_name() + ";",
                "if(" + this->condition->base_name() + ") {",
                util::indent(1, this->base_name() + " = " + on_true->base_name()) + ";",
                "}",
                "else {",
                util::indent(1, this->base_name() + " = " + on_false->base_name()) + ";",
                "}"
            };
        }

        std::string true_register(WordLength wl) override {
            return this->on_true->as_parent_register(*this, wl);
        }
        std::string false_register(WordLength wl) override {
            return this->on_false->as_parent_register(*this, wl);
        }

        IMPL_DEFAULT_CLONE(IfBoolean);
    protected:
        bool recalculate_value() override {
            if(condition->calculate_value()) {
                return on_true->calculate_value();
            }
            return on_false->calculate_value();
        }
    private:
        IfBoolean(
            Rc<BoolNode> condition,
            Rc<BoolNode> on_true,
            Rc<BoolNode> on_false
        )
            : If<BoolNode>(condition, on_true, on_false)
        {}
};

Rc<IfFractional> _if(Rc<BoolNode> condition, Rc<FracNode> on_true, Rc<FracNode> on_false);
Rc<IfBoolean> _if(Rc<BoolNode> condition, Rc<BoolNode> on_true, Rc<BoolNode> on_false);

Rc<IfFractional> min(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<IfFractional> max(Rc<FracNode> lhs, Rc<FracNode> rhs);
