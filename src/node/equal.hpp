#pragma once

#include "frac.hpp"
#include "boolean_binary_operator.hpp"

class Equal : public BooleanBinaryOperator<FracNode> {
    BOOLBINOP_COMMON(Equal, FracNode);
    HAS_REDUNDANT_COMPARISON_REMOVAL
};
