#pragma once

#include "frac.hpp"
#include "boolean_binary_operator.hpp"


class LessThan : public BooleanBinaryOperator<FracNode> {
    BOOLBINOP_COMMON(LessThan, FracNode);
    HAS_REDUNDANT_COMPARISON_REMOVAL
};
