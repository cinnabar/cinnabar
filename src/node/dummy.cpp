#include "dummy.hpp"
#include "base.hpp"

#include "../util.hpp"

using NodeError::UnsupportedNodeOp;
using node::weak_pointers;


Rc<DummyNode> DummyNode::create(std::vector<Rc<Node>> parents) {
    return Rc(new DummyNode(parents));
}
DummyNode::DummyNode(std::vector<Rc<Node>> parents) 
    : Node(weak_pointers(parents))
    , parents(parents)
{}
std::vector<std::string> DummyNode::get_code(WordLength) {
    throw UnsupportedNodeOp{this->base_name(), "Dummy nodes can't have code"};
}
std::vector<std::string> DummyNode::get_cpp(WordLength, CppDataType) {
    throw UnsupportedNodeOp{this->variable_name(), "Dummy nodes can't have code"};
}
std::string DummyNode::cpp_datatype(WordLength, CppDataType) const {
    throw UnsupportedNodeOp{this->variable_name(), "Dummy nodes can't have code"};
}
std::vector<Rc<Node>> DummyNode::get_parents() const {return parents;}
void DummyNode::mark_dirty() {};
std::string DummyNode::display_value() {return "dummy";}
std::string DummyNode::graph_label_name() {return "dummy";}
void DummyNode::update_value() {}
std::string DummyNode::variable_name() const {return "dummy";}
int DummyNode::register_size(WordLength) const {return 0;}
std::optional<std::string> DummyNode::compare_local(Weak<Node>) const {
    return std::nullopt;
};
void DummyNode::optimize_parents(OptPass pass) {
    for(auto& parent : parents) {
        ::optimize_parents(pass, &parent);
    }
}
std::optional<Rc<Node>> DummyNode::constant_fold() {
    return std::nullopt;
}
