#pragma once

#include "bool.hpp"

template<typename T>
class BooleanBinaryOperator : public BoolNode {
    public:
        std::string graph_label_name() override {
            return operator_symbol();
        }
        std::optional<Rc<Node>> constant_fold() override {
            return std::nullopt;
        }

        DERIVE_PARENT_FNS_INLINE(lhs, rhs);
        NO_EXTRA_FILES
    protected:
        std::string variable_name() const override {
            return operator_name() + "_" + lhs->base_name()
                + "_" + rhs->base_name();
        }
        virtual std::string operator_symbol() const = 0;
        virtual std::string operator_name() const = 0;

        Rc<T> lhs;
        Rc<T> rhs;

        BooleanBinaryOperator(Rc<T> lhs, Rc<T> rhs)
            : BoolNode({lhs.weak(), rhs.weak()}), lhs(lhs), rhs(rhs)
        {}
};

#define BOOLBINOP_COMMON(NAME, INPUT_TYPE) \
    public: \
        NAME(Rc<INPUT_TYPE> rhs, Rc<INPUT_TYPE> lhs); \
        std::vector<std::string> get_code(WordLength word_length) override;\
        std::vector<std::string> get_cpp(WordLength word_length, CppDataType) override; \
        std::string operator_symbol() const override; \
        std::string operator_name() const override; \
        std::optional<Rc<Node>> constant_fold() override; \
        IMPL_DEFAULT_COMPARISON; \
        IMPL_DEFAULT_CLONE(NAME); \
    protected: \
        bool recalculate_value() override;

#define HAS_REDUNDANT_COMPARISON_REMOVAL \
    public: std::optional<Rc<Node>> remove_redundant_comparisons() override;

