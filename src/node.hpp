#pragma once

#include "node/base.hpp"
#include "node/dummy.hpp"
#include "node/non_referenced.hpp"
#include "node/delay.hpp"

#include "node/frac.hpp"
#include "node/bool.hpp"
#include "node/opaque.hpp"

#include "node/constant.hpp"
#include "node/variable.hpp"
#include "node/input.hpp"
#include "node/unbounded_input.hpp"
#include "node/merged_bool.hpp"

#include "node/abs.hpp"
#include "node/sqrt.hpp"
#include "node/truncation.hpp"
#include "node/usub.hpp"
#include "node/binary_operator.hpp"

#include "node/bool_const.hpp"
#include "node/not.hpp"
#include "node/boolean_binary_operator.hpp"
#include "node/less_than.hpp"
#include "node/greater_than.hpp"
#include "node/less_than_or_eq.hpp"
#include "node/greater_than_or_eq.hpp"
#include "node/equal.hpp"
#include "node/not_equal.hpp"
#include "node/or.hpp"
#include "node/and.hpp"

#include "node/if.hpp"
#include "node/select.hpp"


Rc<Add> operator+(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<Sub> operator-(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<Mul> operator*(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<Div> operator/(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<USub> operator-(Rc<FracNode> input);

Rc<LessThan> operator<(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<GreaterThan> operator>(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<LessThanOrEq> operator<=(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<GreaterThanOrEq> operator>=(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<Equal> operator==(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<NotEqual> operator!=(Rc<FracNode> lhs, Rc<FracNode> rhs);

Rc<Or> operator||(Rc<BoolNode> lhs, Rc<BoolNode> rhs);
Rc<And> operator&&(Rc<BoolNode> lhs, Rc<BoolNode> rhs);

Rc<Not> operator!(Rc<BoolNode> input);
