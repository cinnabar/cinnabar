#ifndef H_INTERPOLATION
#define H_INTERPOLATION

#include "node.hpp"
#include "lookup_table.hpp"
#include "macro_util.hpp"
#include "../yafp/src/fixedpoint.hpp"

struct Interp1EquidParams {
    Interp1EquidParams(
        std::string name,
        const std::vector<double> x,
        const std::vector<double> y,
        Rc<FracNode> xin
    );

    std::string name;
    Rc<LookupTable> x;
    Rc<LookupTable> y;
    Rc<Constant> size_x;
    Rc<Constant> dx;
    Rc<Constant> dx_inv;
    Rc<Constant> xmin;
    Rc<Constant> xmax;
    double ymin;
    double ymax;
    Rc<FracNode> x_in;

    bool output_valid();

    std::vector<double> x_data;
    std::vector<double> y_data;

    bool operator==(Interp1EquidParams const& other) const;
    bool operator!=(Interp1EquidParams const& other) const;
};


struct Interp2EquidParams {
    Interp2EquidParams(
        std::string name,
        const std::vector<double> x,
        const std::vector<double> y,
        const std::vector<double> z,
        Rc<FracNode> x_in,
        Rc<FracNode> y_in
    );

    std::string name;
    Rc<LookupTable> x;
    Rc<LookupTable> y;
    Rc<LookupTable> z;
    Rc<Constant> nx;
    Rc<Constant> ny;
    Rc<Constant> xmin;
    Rc<Constant> ymin;
    Rc<Constant> xmax;
    Rc<Constant> ymax;
    Rc<Constant> dx;
    Rc<Constant> dy;
    Rc<Constant> dx_inv;
    Rc<Constant> dy_inv;
    Rc<FracNode> x_in;
    Rc<FracNode> y_in;

    const std::vector<double> x_data;
    const std::vector<double> y_data;
    const std::vector<double> z_data;

    bool input_in_bounds(float x, float y) const;

    // NOTE: If adding more members which are not derived from x, y, and z-data
    // remember to add them to this comparison
    bool operator==(Interp2EquidParams const& other) const;
};



/*
  Declares an interpolation node pair (value and valid nodes) with the
  specified parent nodes as members which get passed to the constructor.
*/
#define DECL_INTERP(NAME) \
    class NAME; \
    class NAME##Valid : public BoolNode { \
        public: \
            std::vector<std::string> get_code(WordLength) override; \
            std::vector<std::string> get_cpp(WordLength, CppDataType) override; \
            std::string graph_label_name() override; \
            std::optional<std::string> compare_local( \
                Weak<Node> other \
            ) const override; \
            std::string variable_name() const override; \
            void optimize_parents(OptPass pass) override; \
            std::optional<Rc<Node>> constant_fold() override; \
            std::vector<Rc<Node>> get_parents() const override; \
            bool recalculate_value() override; \
            \
            std::string graph_node_color() const override {return "coral4";} \
            std::optional<Rc<Node>> remove_redundant_interp_validity() override;\
            bool value_in_bounds(float) const; \
            NO_EXTRA_FILES \
            IMPL_DEFAULT_CLONE(NAME##Valid); \
        protected: \
            NAME##Valid(Rc<NAME> sibling); \
            \
            static Rc<NAME##Valid> create(Rc<NAME> sibling); \
        private: \
            Rc<NAME> sibling; \
            \
            friend class NAME; \
    }; \
    class NAME : public FracNode { \
        public: \
            static std::pair<Rc<NAME>, Rc<NAME##Valid>> create( \
                NAME##Params params \
            ); \
            \
            std::vector<std::string> get_code(WordLength) override; \
            std::vector<std::string> get_cpp(WordLength, CppDataType) override; \
            /* TODO: Get rid of this function once both use raw verilog code */ \
            std::vector<std::string> verilog_extra_commands(WordLength) const override; \
            std::string graph_label_name() override; \
            std::optional<std::string> compare_local( \
                Weak<Node> other \
            ) const override; \
            std::string variable_name() const override; \
            void optimize_parents(OptPass pass) override; \
            std::optional<Rc<Node>> constant_fold() override; \
            double _min() const override; \
            double _max() const override; \
            std::vector<Rc<Node>> get_parents() const override; \
            ExecutionType recalculate_value() override; \
            \
            std::string graph_node_color() const override {return "coral";} \
            int computation_time(WordLength wl) const override; \
            std::vector<std::string> extra_verilog_files(WordLength) override; \
            NO_EXTRA_CPPS \
            IMPL_DEFAULT_CLONE(NAME); \
        protected: \
            NAME(NAME##Params params); \
            \
        private: \
            NAME##Params params; \
            friend class NAME##Valid; \
    }; \

DECL_INTERP(Interp1Equid);
DECL_INTERP(Interp2Equid);

namespace interp {
    std::pair<Rc<BoolNode>, Rc<FracNode>> interp1equid(
        std::string name,
        Interp1EquidParams params,
        Config const& cfg
    );

    std::pair<Rc<BoolNode>, Rc<FracNode>> interp1equid(
            std::string name,
            const std::vector<double> x,
            const std::vector<double> y,
            Rc<FracNode> xin,
            Config const& config
        );

    std::pair<Rc<BoolNode>, Rc<FracNode>> interp2equid(
            std::string name,
            const std::vector<double> x,
            const std::vector<double> y,
            const std::vector<double> z,
            Rc<FracNode> xp,
            Rc<FracNode> yp,
            Config const& config
        );

    std::pair<Rc<BoolNode>, Rc<FracNode>> interp2equid(
        std::string name,
        Interp2EquidParams params,
        Config const& config
    );
}

#endif
