#pragma once

#include "types.hpp"

struct Config {
    WordLength word_length_strategy;
    bool single_node_1d_interp;
    bool single_node_2d_interp;
};
