#include "lookup_table.hpp"
#include "graph.hpp"
#include "log.hpp"

#include <sys/stat.h>

#include <cassert>
#include <fstream>

using util::indent
    , util::write_if_changed
    , util::verilog_variable
    , util::merge_code
    , util::join
    , util::indent_lines
    , util::clocked_block;

std::vector<Weak<LookupTable>> LookupTable::all_luts;

////////////////////////////////////////////////////////////
// LUT impls
////////////////////////////////////////////////////////////

LookupTable::LookupTable(
    std::string name,
    std::vector<double> values
)
    : name(name)
    , values{values}
{}

Rc<LookupTable> LookupTable::create(std::string name, std::vector<double> values) {
    auto result = Rc<LookupTable>(new LookupTable(name, values));
    all_luts.push_back(result.weak());
    return result;
}

std::string LookupTable::base_name() const {return name;}
std::string LookupTable::get_name(int fractional_bits) const {
    return name + std::to_string(fractional_bits);
}

double LookupTable::min() const {
    return *std::min_element(
        values.begin(),
        values.end()
    );
}
double LookupTable::max() const {
    return *std::max_element(
        values.begin(),
        values.end()
    );
}
int LookupTable::size() const {
    return values.size();
}

double LookupTable::lookup(std::size_t index) const {
    return values.at(index);
}

// Helper class for the WordLengthVisitor
template<class T> struct always_false : std::false_type {};
int word_length_fractional_bits(WordLength word_length) {
    // see https://en.cppreference.com/w/cpp/utility/variant/visit for explanation
    return std::visit([](auto&& arg) {
        using T = std::decay_t<decltype(arg)>;
        if constexpr (std::is_same_v<T, FixedFixpoint>) {
            return arg.position;
        }
        else if constexpr (std::is_same_v<T, FixedType>) {
            return arg.frac_bits;
        }
        else if constexpr(std::is_same_v<T, FixedTotalLength>) {
            std::cout
                << "Error: Fixed total length is unimplemented in"
                << "lookup_table.cpp::word_length_fractional_bits"
                << std::endl;
            exit(1);
            // Return to make the compiler happy
            return 1;
        }
        else {
            static_assert(always_false<T>::value, "non-exhaustive visitor!");
        }
    }, word_length);
}

std::vector<std::string> LookupTable::get_module(int fractional_bits) const {

    // Move the elements out of the map and into a vector to sort them
    // for more readable code
    std::vector<std::pair<double, double>> elems;
    double i = 0;
    for(auto elem : values) {
        elems.push_back({i, elem});
        i++;
    }
    std::sort(elems.begin(), elems.end(), [](auto a, auto b){return a.first < b.first;});

    auto input_bits_required = int(ceil(log2(elems.size())));

    // TODO: Rewrite this to only use integers
    std::vector<std::string> array_assignments;
    for(auto elem : elems) {
        auto index = elem.first;
        auto val = std::abs(int64_t(elem.second * pow(2, fractional_bits)));
        auto sign = elem.second < 0 ? "-" : "";

        // Make sure we don't overflow our huge literals
        assert(floor(log2(val))+1 < 64);

        array_assignments.push_back
            ( "data[" + std::to_string((int64_t)index) + "] = " + sign + "(output_size'(64'd" + std::to_string(val) + "));");
    }

    // case_branches.push_back("default: result_buffer <= 0;");

    return merge_code({
        {"module " + this->get_name(fractional_bits)},
        indent_lines(indent_lines({
            "#( parameter output_size = 64",
            ")"
        })),
        indent_lines(indent_lines({"( clk", ", addr", ", result", ");"})),
        indent_lines(
            merge_code({
                {
                verilog_variable(VarType::INPUT, "clk", 1),
                "localparam input_size = " + std::to_string(input_bits_required) + ";",
                "input[input_size-1:0] addr;",
                "output reg[output_size-1:0] result;",
                "logic[output_size-1:0] data["
                    + std::to_string(elems.size())
                    + ":0];",
                "initial begin"
                }
                , indent_lines(array_assignments)
                , {"end"}
            })
        ),
        indent_lines(clocked_block(merge_code({
            // The sign bit is always one, so we can remove it. If we don't do this,
            // the synthesis tool gets sad and confused
            {"result <= data[addr[input_size-1:0]];"},

        }))),
        {"endmodule"}
    });
}

void LookupTable::request_fractional_bits(int amount) {
    this->requested_fractional_bits.insert(amount);
}



std::string LookupTable::cpp_function_name(int fractional_bits) const {
    return get_name(fractional_bits) + "_lut";
}

std::pair<std::vector<std::string>, std::vector<std::string>> LookupTable::get_cpp(
    int fractional_bits,
    CppDataType t
) const {
    switch (t) {
        case CppDataType::Fp: {
            // TODO: Rewrite using integers
            auto type = cppcode::fixed_point_type(64, fractional_bits, t);
            auto function_head =  type + " " + cpp_function_name(fractional_bits) + "(int i)";
            std::vector<std::string> result = {
                "#include \"luts.hpp\"",
                type + "const data[] = {"
            };

            std::size_t i = 0;
            for(auto value : values) {
                auto val = std::abs(int64_t(value * pow(2, fractional_bits)));
                auto sign = value < 0 ? "-" : "";
                result.push_back(indent(
                    3,
                    "[" + std::to_string(i) + "] = " + sign + type + "::from_raw(" + std::to_string(val) + "ll),"
                ));
                i++;
            }

            result.push_back("};");

            // NOTE: Returning dummy data on OOB is correct as these lines will be executed
            // even if checks are performed. We should probably add a static analasys pass to
            // ensure that OOB accesses are not performed.
            std::vector<std::string> rest = {
                function_head + " {",
                indent(1, "if (i < 0) {"),
                indent(2, "return data[0];"),
                indent(1, "} else if (i >= " + std::to_string(this->values.size()) + "){"),
                indent(2, "return data[0];"),
                indent(1, "} else {"),
                indent(1, "return data[i];"),
                indent(1, "}"),
                "}"
            };

            result = merge_code({result, rest});


            return std::make_pair(std::vector<std::string>{function_head + ";"}, result);
        }
        case CppDataType::Ap: {
            // TODO: Rewrite using integers
            auto type = cppcode::fixed_point_type(64, fractional_bits, t);
            auto function_head =  type + " " + cpp_function_name(fractional_bits) + "(int i)";
            std::vector<std::string> result = {
                "#include \"luts.hpp\"",
                "#include <ap_fixed.h>",
                type + " const data[] = {"
            };

            std::size_t i = 0;
            for(auto value : values) {
                result.push_back(indent(
                    3,
                    "[" + std::to_string(i) + "] = " + std::to_string(value) + ","
                ));
                i++;
            }

            result.push_back("};");

            // NOTE: Returning dummy data on OOB is correct as these lines will be executed
            // even if checks are performed. We should probably add a static analasys pass to
            // ensure that OOB accesses are not performed.
            std::vector<std::string> rest = {
                function_head + " {",
                indent(1, "if (i < 0) {"),
                indent(2, "return data[0];"),
                indent(1, "} else if (i >= " + std::to_string(this->values.size()) + "){"),
                indent(2, "return data[0];"),
                indent(1, "} else {"),
                indent(1, "return data[i];"),
                indent(1, "}"),
                "}"
            };

            result = merge_code({result, rest});


            return std::make_pair(std::vector<std::string>{function_head + ";"}, result);
        }
        case CppDataType::Ac: UNIMPLEMENTED;
        case CppDataType::Float: UNIMPLEMENTED
    }

}

std::string LookupTable::base_filename(
    std::optional<std::string> storage_location,
    const std::string &extension
) {
    auto filename = base_name() + "." + extension;
    if (storage_location.has_value()) {
        return storage_location.value() + "/" + filename;
    }
    return filename;
}

void LookupTable::generate_code(std::string storage_location, CppDataType t) {
    mkdir(storage_location.c_str(), S_IRWXU | S_IROTH | S_IXOTH);

    if(requested_fractional_bits.size() > 1) {
        warn("More than one set of frac bits requested by LUT codegen");
        info("Requested ", requested_fractional_bits.size(), " different bits");
        info("For ", base_name());
    }
    for(auto fractional_bits : requested_fractional_bits) {
        debug("Lut with ", fractional_bits, " bits");
        write_if_changed(base_filename(storage_location, "sv"), join(get_module(fractional_bits)));

        auto [hpp_code, cpp_code] = get_cpp(fractional_bits, t);
        write_if_changed(base_filename(storage_location, "cpp"), join(cpp_code));
        write_if_changed(base_filename(storage_location, "hpp"), join(hpp_code));

        for(auto y : this->values) {
            auto stored = y * (1 << fractional_bits);
            if(log2(stored) > 64) {
                warn("LUT overflow ", y , " as ", stored, " overflowed " + this->get_name(fractional_bits));
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////
// Lookup table codegen
// Keeping track of lookup tables using a static non-mutexed global
// is ugly as hell and should probably be changed as soon as possible.
// (This was added on October 18 - 2019, let's see how long it lasts :D)
//////////////////////////////////////////////////////////////////////

void LookupTable::generate_code_for_lookup_tables(std::string storage_location, CppDataType t) {
    std::vector<std::string> lut_hpps;
    for(auto& lut : all_luts) {
        try {
            auto locked = lut.lock();
            locked->generate_code(storage_location, t);
            if(!locked->requested_fractional_bits.empty()) {
                lut_hpps.push_back(locked->base_name() + ".hpp");
            }
        }
        catch(ExpiredWeakPointer& e) {
            warn("Lut codegen attempted for an expired weak pointer. This might result in missing LUTS, or a LUT lookup was optimized away.");
        }
    }
    std::vector<std::string> combined_hpp;
    std::transform(
        lut_hpps.begin(),
        lut_hpps.end(),
        std::back_inserter(combined_hpp),
        [](auto file) {return "#include \"" + file + "\"";}
    );
    std::vector<std::string> header_header = {
        "#pragma once",
    };
    if(t == CppDataType::Fp) {
        header_header.push_back("#include <fixedpoint.hpp>");
    }
    else if (t == CppDataType::Ap) {
        header_header.push_back("#include <ap_fixed.h>");
    }

    auto with_include = merge_code({header_header, combined_hpp});
    write_if_changed(storage_location + "/luts.hpp", join(with_include));
}

std::vector<std::string> LookupTable::get_cpp_files() {
    std::vector<std::string> result;
    for(auto& lut : all_luts) {
        try {
            auto locked = lut.lock();
            if(!locked->requested_fractional_bits.empty()) {
                result.push_back(locked->base_filename(std::nullopt, "cpp"));
            }
        }
        catch(ExpiredWeakPointer& e) {
            warn("Lut codegen attempted for an expired weak pointer. This might result in missing LUTS, or a LUT lookup was optimized away.");
        }
    }
    return result;
}

// TODO: merge with get_cpp files if we start adding more similar functions
std::vector<std::string> LookupTable::get_verilog_files() {
    std::vector<std::string> result;
    for(auto& lut : all_luts) {
        try {
            auto locked = lut.lock();
            if(!locked->requested_fractional_bits.empty()) {
                result.push_back(locked->base_filename(std::nullopt, "sv"));
            }
        }
        catch(ExpiredWeakPointer& e) {
            warn("Lut codegen attempted for an expired weak pointer. This might result in missing LUTS, or a LUT lookup was optimized away.");
        }
    }
    return result;
}

std::string LookupTable::module_instance(
    std::string instance_name,
    int output_size,
    int fractional_bits,
    std::string addr,
    std::string result_name
) {
    auto module_head = this->get_name(fractional_bits)
        + "#(.output_size("
        + std::to_string(output_size)
        + "))";

    return module_head + " " + instance_name + "(.clk(clk), .addr("
        + addr + ")"", .result(" + result_name + "));";
}


//////////////////////////////////////////////////////////////////////
// Lookup result impls
//////////////////////////////////////////////////////////////////////

LutResult::LutResult(Rc<LookupTable> lut, Rc<FracNode> index)
    : FracNode({index.weak()}), index(index), lut(lut)
{}

Rc<LutResult> LutResult::create(
    Rc<LookupTable> lut,
    Rc<FracNode> index
) {
    return Rc<LutResult>(new LutResult(lut, index));
}

std::string LutResult::variable_name() const {
    return "lut_" + lut->base_name() + "_" + index->base_name();
}

std::vector<std::string> LutResult::get_code(WordLength wl) {
    auto frac_bits = fractional_bits(wl);
    lut->request_fractional_bits(frac_bits);

    auto index_reg = index->as_parent_register_unchanged(*this, wl);

    return {
        verilog_variable(VarType::WIRE, base_name(), register_size(wl)),
        lut->module_instance(
            base_name() + "_lut",
            register_size(wl),
            this->fractional_bits(wl),
            index_reg
                + "[" + std::to_string(this->index->integer_bits(wl) + this->index->fractional_bits(wl)) + ":"
                + std::to_string(this->index->fractional_bits(wl))
                + "]"
            , base_name()
        )
    };
}
std::vector<std::string> LutResult::get_cpp(WordLength wl, CppDataType t) {
    lut->request_fractional_bits(fractional_bits(wl));
    switch (t) {
        case CppDataType::Fp:
            return {
                cppcode::fixed_point_type(this->integer_bits(wl), this->fractional_bits(wl), t) + " "
                    + base_name()
                    + " = "
                    + lut->get_name(fractional_bits(wl))
                    + "_lut("
                    + index->base_name()
                    + ".as_int());"
            };
        case CppDataType::Ap:
            return {
                cppcode::fixed_point_type(this->integer_bits(wl), this->fractional_bits(wl), t) + " "
                    + base_name()
                    + " = "
                    + lut->get_name(fractional_bits(wl))
                    + "_lut((int)"
                    + index->base_name()
                    + ");"
            };
        case CppDataType::Ac:
            UNIMPLEMENTED;
        case CppDataType::Float:
            return {
                "float "
                    + base_name()
                    + " = "
                    + lut->get_name(fractional_bits(wl))
                    + "_lut((int)"
                    + index->base_name()
                    + ");"
            };
    }
}

double LutResult::_min() const {
    return lut->min();
}
double LutResult::_max() const {
    return lut->max();
}

ExecutionType LutResult::recalculate_value() {
    try {
        auto addr = (int) index->calculate_value();
        return lut->lookup(addr);
    }
    catch(std::out_of_range& e) {
        err("LUT went out of bounds");
        throw(std::move(e));
    }
}

std::vector<Rc<Node>> LutResult::get_parents() const {return {index};}

std::string LutResult::graph_label_name() {return "lut";}

void LutResult::optimize_parents(OptPass pass) {
    ::optimize_parents(pass, &index);
}
std::optional<Rc<Node>> LutResult::constant_fold() {
    return std::nullopt;
}


