#include "interpolation.hpp"

#include <math.h>

using node::traverse_depth_first;
using interp::interp2equid;
using interp::interp1equid;

using util::tuple_string
    , util::merge_code
    , util::verilog_variable
    , util::register_code
    , util::clocked_block
    , util::indent
    , util::verilog::parameter
    , util::verilog::int_parameter;

Interp1EquidParams::Interp1EquidParams(
        std::string name,
        const std::vector<double> x,
        const std::vector<double> y,
        Rc<FracNode> x_in
    )
    : name(name)
    , x(LookupTable::create(name + "_x", x))
    , y(LookupTable::create(name + "_y", y))
    , size_x(Constant::create(name + "_size_x", x.size()))
      // Works because x is equidistant
    , dx(Constant::create(name + "_dx", x[1]-x[0]))
    , dx_inv(Constant::create(name + "_dx_inv", 1/(x[1]-x[0])))
      // Works because x monotonically increasing
    , xmin(Constant::create(name + "_xmin", x[0]))
    , xmax(Constant::create(name + "_xmax", x[x.size()-1]))
    , ymin(*std::min_element(y.begin(), y.end()))
    , ymax(*std::max_element(y.begin(), y.end()))
    , x_in(x_in)
    , x_data(x)
    , y_data(y)
{}

bool Interp1EquidParams::operator==(Interp1EquidParams const& other) const {
    return this->x_data == other.x_data
        && this->y_data == other.y_data;
}
bool Interp1EquidParams::operator!=(Interp1EquidParams const& other) const {
    return !(*this == other);
}


std::pair<Rc<BoolNode>, Rc<FracNode>> interp::interp1equid(
        std::string name,
        Interp1EquidParams params,
        Config const& cfg
    )
{
    if (cfg.single_node_1d_interp) {
        (void) name;
        auto [value, valid] = Interp1Equid::create(params);
        return std::make_pair(valid, value);
    }
    else {
        auto zero = Constant::create(name + "_zero", 0);
        auto one = Constant::create(name + "_one", 1);
        auto valid = !( (params.x_in < params.xmin)
                     || (params.x_in >= params.xmax)
                     );

        auto xp = params.x_in;
        // auto xp = _if(valid, params.x_in, params.xmin);

        // TODO: Check if we can multiply by dx_inv here
        Rc<FracNode> ii_ = Truncate::create((xp - params.xmin) / params.dx ) + one;
        ii_->override_name(name + "_ii_");

        // This differes from the original implementation but seems required to avoid out
        // of bounds access
        ii_ = min(ii_, Constant::create(name + "_max", params.x->size()-1));
        ii_->override_name(name + "_ii_min");
        auto ii = max(ii_, zero);
        ii->override_name(name + "_ii");

        auto sub_ii_one = ii - one;

        auto x0 = LutResult::create(params.x, sub_ii_one);
        x0->override_name(name + "_x0");
        auto y0 = LutResult::create(params.y, sub_ii_one);
        y0->override_name(name + "_y0");
        auto y1 = LutResult::create(params.y, ii);
        y1->override_name(name + "_y1");

        auto result_ = y0 + (y1-y0) * (xp - x0) * params.dx_inv;
        result_->override_name(name + "_result_");

        auto result = _if(valid, result_, Constant::create(name + "_min", params.ymin));
        result->override_name(name + "_result");
        // This is needed because if-statements can't automatically find this relation
        result->_override_min(params.ymin);
        result->_override_max(params.ymax);

        result->override_name("interp1_" + name);
        valid->override_name("interp1valid_" + name);

        return std::make_pair(valid, result);
    }
}

std::pair<Rc<BoolNode>, Rc<FracNode>> interp::interp1equid(
        std::string name,
        const std::vector<double> x,
        const std::vector<double> y,
        Rc<FracNode> xin,
        Config const& cfg
    )
{
    Interp1EquidParams params(name, x, y, xin);
    return interp1equid(name, params, cfg);
}




//////////////////////////////////////////////////////////////////////
//  Interp2

Interp2EquidParams::Interp2EquidParams(
        std::string name,
        std::vector<double> const x,
        std::vector<double> const y,
        std::vector<double> const z,
        Rc<FracNode> x_in,
        Rc<FracNode> y_in
    )
    : name(name)
    , x(LookupTable::create(name + "_x", x))
    , y(LookupTable::create(name + "_y", y))
    , z(LookupTable::create(name + "_z", z))
    , nx(Constant::create(name + "_nx", x.size()))
    , ny(Constant::create(name + "_ny", y.size()))
    , xmin(Constant::create(name + "_xmin", *x.begin()))
    , ymin(Constant::create(name + "_ymin", *y.begin()))
    , xmax(Constant::create(name + "_xmax", *(x.end()-1)))
    , ymax(Constant::create(name + "_ymax", *(y.end()-1)))
    // // This works because the steps are equidistant
    , dx(Constant::create(name + "_dx", x[1] - x[0]))
    , dy(Constant::create(name + "_dy", y[1] - y[0]))
    , dx_inv(Constant::create(name + "_dx_inv", 1/(x[1] - x[0])))
    , dy_inv(Constant::create(name + "_dy_inv", 1/(y[1] - y[0])))
    , x_in(x_in)
    , y_in(y_in)
    , x_data{x}
    , y_data{y}
    , z_data{z}
{}

bool Interp2EquidParams::operator==(Interp2EquidParams const& other) const {
    return x_data == other.x_data
        && y_data == other.y_data
        && z_data == other.z_data
        && name == other.name;
}

bool Interp2EquidParams::input_in_bounds(float x, float y) const {
    auto [xmin, xmax] = std::minmax_element(x_data.begin(), x_data.end());
    auto [ymin, ymax] = std::minmax_element(y_data.begin(), y_data.end());
    return x >= *xmin && x < *xmax && y >= *ymin && y < *ymax;
}


std::pair<Rc<BoolNode>, Rc<FracNode>> interp::interp2equid(
    std::string name,
    Interp2EquidParams params,
    Config const& cfg
) {
    if (cfg.single_node_2d_interp) {
        (void) name;
        auto [value, valid] = Interp2Equid::create(params);
        return std::make_pair(valid, value);
    }
    else {
        auto zero = Constant::create(name + "interp_" + name + "_zero", 0);
        auto one = Constant::create(name + "interp_" + name + "_one", 1);

        auto valid =
                !( (params.x_in < params.xmin)
                || (params.x_in > params.xmax)
                || (params.y_in < params.ymin)
                || (params.y_in > params.ymax)
                );

        auto xp = _if(valid, params.x_in, params.xmin);
        xp->_override_min(params.xmin->value);
        xp->_override_max(params.xmax->value);
        auto yp = _if(valid, params.y_in, params.ymin);
        yp->_override_min(params.ymin->value);
        yp->_override_max(params.xmax->value);
        xp->override_name("interp_" + name + "_xp");
        yp->override_name("interp_" + name + "_yp");
        // auto xp = max(params.xmin, min(params.x_in, params.xmax));
        // auto yp = max(params.ymin, min(params.y_in, params.ymax));


        auto x_idx = Truncate::create((xp-params.xmin) / params.dx) + one;
        x_idx->override_name("interp_" + name + "_x_idx");
        auto y_idx = Truncate::create((yp-params.ymin) * params.dy_inv) + one;
        y_idx->override_name("interp_" + name + "_y_idx");

        auto sub_x_idx_one = (x_idx-one);
        auto sub_y_idx_one = (y_idx-one);
        auto mul_sub_x_idx_one_ny = sub_x_idx_one*params.ny;
        auto mul_x_idx_ny = x_idx*params.ny;

        auto q11 = LutResult::create(params.z, (mul_sub_x_idx_one_ny + sub_y_idx_one));
        auto q12 = LutResult::create(params.z, (mul_sub_x_idx_one_ny + y_idx));
        auto q21 = LutResult::create(params.z, (mul_x_idx_ny + sub_y_idx_one));
        auto q22 = LutResult::create(params.z, (mul_x_idx_ny + y_idx));
        q11->override_name("interp_" + name + "_q11");
        q12->override_name("interp_" + name + "_q12");
        q21->override_name("interp_" + name + "_q21");
        q22->override_name("interp_" + name + "_q22");

        auto x0 = LutResult::create(params.x, sub_x_idx_one);
        auto y0 = LutResult::create(params.y, sub_y_idx_one);
        auto x1 = LutResult::create(params.x, x_idx);
        auto y1 = LutResult::create(params.y, y_idx);
        x0->override_name("interp_" + name + "_x0");
        y0->override_name("interp_" + name + "_y0");
        x1->override_name("interp_" + name + "_x1");
        y1->override_name("interp_" + name + "_y1");


        auto result = (
                    q11*(x1 - xp)*(y1 - yp) +
                    q21*(xp-x0)*(y1 - yp) +
                    q12*(x1-xp)*(yp - y0) +
                    q22*(xp-x0)*(yp - y0)
                ) * params.dx_inv * params.dy_inv;

        result->_override_min(params.z->min());
        result->_override_max(params.z->max());

        result->override_name("interp2_" + name);
        valid->override_name("interp2valid_" + name);

        return std::make_pair(valid, result);
    }
}

std::pair<Rc<BoolNode>, Rc<FracNode>> interp::interp2equid(
        std::string name,
        const std::vector<double> x,
        const std::vector<double> y,
        const std::vector<double> z,
        Rc<FracNode> xp,
        Rc<FracNode> yp,
        Config const& config
    )
{
    auto params = Interp2EquidParams(name, x, y, z, xp, yp);
    return interp2equid(name, params, config);
}


//////////////////////////////////////////////////////////////////////
//  Class based Interp1

Interp1Equid::Interp1Equid(Interp1EquidParams params)
    : FracNode{{params.x_in.weak()}}
    , params{params}
{}
std::pair<Rc<Interp1Equid>, Rc<Interp1EquidValid>> Interp1Equid::create(
    Interp1EquidParams params
) {
    auto self = Rc{new Interp1Equid{params}};
    auto valid = Rc{new Interp1EquidValid{self}};
    return {self, valid};
}
// TODO: Test and implement
std::vector<std::string> Interp1Equid::get_code(WordLength wl) {
    auto&& y_data = this->params.y_data;
    std::vector<double> y_data_abs;
    std::transform(
        y_data.begin(),
        y_data.end(),
        std::back_inserter(y_data_abs),
        [](auto y) {return std::abs(y);}
    );

    // Integer parameters
    int64_t input_int_bits = this->params.x_in->integer_bits(wl);
    int64_t input_fp = this->params.x_in->fractional_bits(wl);
    int64_t output_size = this->register_size(wl);
    int64_t output_fp = this->fractional_bits(wl);
    int64_t lut_address_size = ceil(log2(y_data.size()));
    // NOTE: In order to avoid having to deal with edge cases in the verilog, we'll
    // force this value to be at least 1
    int64_t lut_int_bits = std::max(1., floor(log2(*std::max_element(y_data_abs.begin(), y_data_abs.end()))) + 1 + 1);

    // Floating point parameter computations
    auto dx_float = this->params.dx->value;
    int64_t log_dx = ceil(log2(dx_float));
    auto closest_larger_pow2 = powf(2, log_dx);
    auto alpha_float = closest_larger_pow2 / dx_float;

    // Fixed point parameters
    int64_t dx = dx_float * floor(powf(2, input_fp));
    int64_t alpha = alpha_float * floor(powf(2, input_fp));
    int64_t x_min = this->params.xmin->value * floor(powf(2, input_fp));

    auto lut1_addr = this->base_name() + "_lut1_addr";
    auto lut2_addr = this->base_name() + "_lut2_addr";
    auto lut1_output = this->base_name() + "_lut1_output";
    auto lut2_output = this->base_name() + "_lut2_output";

    this->params.y->request_fractional_bits(fractional_bits(wl));

    auto lut_output_size = lut_int_bits + input_fp + 1;

    return
        // LUT wires
        { "// INPUT_INT_BITS: " + std::to_string(input_int_bits)
        , "// INPUT_FP: " + std::to_string(input_fp)
        , "// OUTPUT_SIZE: " + std::to_string(output_size)
        , "// OUTPUT_FP: " + std::to_string(output_fp)
        , "// LUT_ADDRESS_SIZE: " + std::to_string(lut_address_size)
        , "// LUT_INT_BITS: " + std::to_string(lut_int_bits)
        , "// ALPHA: " + std::to_string(alpha_float)
        , "// DX: " + std::to_string(dx_float)
        , "// LOG_DX: " + std::to_string(log_dx)
        , "// X_MIN: " + std::to_string(this->params.xmin->value)
        , verilog_variable(VarType::WIRE, lut1_addr, lut_address_size)
        , verilog_variable(VarType::WIRE, lut2_addr, lut_address_size)
        , verilog_variable(VarType::WIRE, lut1_output, lut_output_size)
        , verilog_variable(VarType::WIRE, lut2_output, lut_output_size)
        // LUT instanciation
        , params.y->module_instance(
                base_name() + "_lut1",
                lut_output_size,
                this->fractional_bits(wl),
                lut1_addr,
                lut1_output
            )
        , params.y->module_instance(
                base_name() + "_lut2",
                lut_output_size,
                this->fractional_bits(wl),
                lut2_addr,
                lut2_output
            )
        // Result wire
        , verilog_variable(VarType::WIRE, base_name(), this->register_size(wl))
        , std::string("interp1d")
            + "#(" + int_parameter("INPUT_INT_BITS", input_int_bits)
            + ", " + int_parameter("INPUT_FP", input_fp)
            + ", " + int_parameter("OUTPUT_SIZE", output_size)
            + ", " + int_parameter("OUTPUT_FP", output_fp)
            + ", " + int_parameter("LUT_ADDRESS_SIZE", lut_address_size)
            + ", " + int_parameter("LUT_INT_BITS", lut_int_bits)
            + ", " + int_parameter("ALPHA", alpha)
            + ", " + int_parameter("DX", dx)
            + ", " + int_parameter("LOG_DX", log_dx)
            + ", " + int_parameter("X_MIN", x_min)
            + ")"
            + base_name() + "_instance "
            + "( " + parameter("clk", "clk")
            + ", " + parameter("x_s0", this->params.x_in->as_parent_register_unchanged(*this, wl))
            + ", " + parameter("lut1_addr", lut1_addr)
            + ", " + parameter("lut1_output", lut1_output)
            + ", " + parameter("lut2_addr", lut2_addr)
            + ", " + parameter("lut2_output", lut2_output)
            + ", " + parameter("out", this->base_name())
            + ");"
        };
}
std::vector<std::string> Interp1Equid::get_cpp(WordLength wl, CppDataType t) {
    auto fractional_bits = this->fractional_bits(wl);
    params.y->request_fractional_bits(fractional_bits);
    return {
        this->cpp_datatype(wl, t)
            + " "
            + this->base_name()
            + " = "
            + "interp::do_interpolation<" + std::to_string(fractional_bits) + ">("
            + this->params.x_in->base_name() + ", "
            + this->params.y->cpp_function_name(this->fractional_bits(wl)) + ", "
            + std::to_string(this->params.xmin->value) + ", "
            + std::to_string(this->params.xmax->value) + ", "
            + std::to_string(this->params.dx->value)
            + ");"
    };
}

std::vector<std::string> Interp1Equid::verilog_extra_commands(WordLength) const {
    return {};
}
std::vector<std::string> Interp1Equid::extra_verilog_files(WordLength) {
    return {};
}

// TODO: Test and implement
std::string Interp1Equid::graph_label_name() {
    return "Interp1Equid";
}
std::optional<std::string> Interp1Equid::compare_local(Weak<Node> other) const {
    auto comp = [](Interp1Equid const& lhs, Interp1Equid const& rhs) -> std::optional<std::string> {
        if(lhs.params != rhs.params) {
            return "Missmatched parameters";
        }
        return std::nullopt;
    };
    // I have no idea why this step is required, but for some reason, template
    // argument deduction fails if this is not here.
    //
    // Yay c++ :/
    std::function<std::optional<std::string>(
        Interp1Equid const&,
        Interp1Equid const&
    )> _comp = comp;
    return node::do_compare_local(*this, other, _comp);
}
std::string Interp1Equid::variable_name() const {
    return "interp1_" + params.name;
}
DERIVE_PARENT_FNS(Interp1Equid, params.x_in);
std::optional<Rc<Node>> Interp1Equid::constant_fold() {
    return node::do_constant_fold(*this, &params.x_in, [this](auto) {
        return this->calculate_value();
    });
}
double Interp1Equid::_min() const {
    return *std::min_element(params.y_data.begin(), params.y_data.end());
}
double Interp1Equid::_max() const {
    return *std::max_element(params.y_data.begin(), params.y_data.end());
}
ExecutionType Interp1Equid::recalculate_value() {
    auto xp = params.x_in->calculate_value();
    auto nx = params.x_data.size();
    auto dx = params.dx->calculate_value();
    auto xmin = params.xmin->calculate_value();
    auto& x = params.x_data;
    auto& y = params.y_data;
    if( (xp < x[0]) || (xp >= x[nx-1]) ) {
        return *std::min_element(params.y_data.begin(), params.y_data.end());
    }
    else {
        int ii = trunc( (xp-xmin)/dx ) + 1;
        return y[ii-1] + (y[ii]-y[ii-1])*(xp - x[ii-1])/(x[ii]-x[ii-1]);
    }
}
int Interp1Equid::computation_time(WordLength) const {
    return 4;
}


Interp1EquidValid::Interp1EquidValid(Rc<Interp1Equid> sibling)
    : BoolNode{{sibling->params.x_in.weak()}}
    , sibling{sibling}
{}
// TODO: Test and implement
std::vector<std::string> Interp1EquidValid::get_code(WordLength wl) {
    auto params = sibling->params;
    auto x_in = sibling->params.x_in;
    auto in_frac_bits = params.x_in->fractional_bits(wl);
    int64_t min_value = params.xmin->calculate_value()
                  * pow(2, in_frac_bits);
    int64_t max_value = params.xmax->calculate_value()
                      * pow(2, in_frac_bits);

    int in_bits = params.x_in->register_size(wl);
    return merge_code({
        {register_code(base_name(), 1)},
        clocked_block({
            base_name()
            + " <= !($signed("
            + x_in->as_parent_register(
                *this,
                wl,
                x_in->integer_bits(wl),
                x_in->fractional_bits(wl),
                false
            )
                + ") < "
                + std::to_string(in_bits)
                + "'d"
                + std::to_string(min_value)
            + " || $signed("
            + x_in->as_parent_register(
                *this,
                wl,
                x_in->integer_bits(wl),
                x_in->fractional_bits(wl),
                false
            )
                + ") > "
                + std::to_string(in_bits)
                + "'d"
                + std::to_string(max_value)
            + ");"
        })
    });
}
std::vector<std::string> Interp1EquidValid::get_cpp(WordLength wl, CppDataType t) {
    auto params = this->sibling->params;
    return {
        this->cpp_datatype(wl, t)
            + " "
            + this->base_name()
            + " = "
            + "interp::do_interpolation_valid("
            + params.x_in->base_name() + ", "
            + std::to_string(params.xmin->value) + ", "
            + std::to_string(params.xmax->value)
            + ");"
    };
}
std::string Interp1EquidValid::graph_label_name() {
    return "Interp1EquidValid";
}
std::optional<std::string> Interp1EquidValid::compare_local(Weak<Node> other) const {
    auto casted = std::dynamic_pointer_cast<Interp1EquidValid>(other.lock());
    return sibling->compare_local(casted->sibling.weak());
}
std::string Interp1EquidValid::variable_name() const {
    return "interp1valid_" + sibling->params.name;
}

std::optional<Rc<Node>> Interp1EquidValid::constant_fold() {
    return node::do_bool_constant_fold(*this, &sibling->params.x_in, [this](auto) {
        return this->calculate_value();
    });
}
std::optional<Rc<Node>> Interp1EquidValid::remove_redundant_interp_validity() {
    auto xp = sibling->params.x_in;
    if(value_in_bounds(xp->max())) {
        return BoolConst::True;
    }
    return std::nullopt;
}
bool Interp1EquidValid::value_in_bounds(float val) const {
    auto nx = sibling->params.x_data.size();
    auto& x = sibling->params.x_data;
    if( (val < x[0]) || (val >= x[nx-1]) ) {
        return false;
    }
    return true;
}
bool Interp1EquidValid::recalculate_value() {
    auto xp = sibling->params.x_in->calculate_value();
    return value_in_bounds(xp);
}

DERIVE_PARENT_FNS(Interp1EquidValid, sibling->params.x_in)

//////////////////////////////////////////////////////////////////////
//  Class based Interp2

Interp2Equid::Interp2Equid(
        Interp2EquidParams params
    ) : FracNode{{params.x_in.weak(), params.y_in.weak()}}
      , params{params}
{}

std::pair<Rc<Interp2Equid>, Rc<Interp2EquidValid>> Interp2Equid::create(
        Interp2EquidParams params
    ) {
    Rc<Interp2Equid> self = Rc{new Interp2Equid(params)};
    Rc<Interp2EquidValid> valid = Interp2EquidValid::create(self);
    return std::make_pair(self, valid);
}
// TODO: Implement and test
std::vector<std::string> Interp2Equid::get_code(WordLength wl) {
    return {
        verilog_variable(VarType::WIRE, base_name(), register_size(wl)),
        this->base_name() + "_mod " + base_name() + "_instance(.clk(clk), "
            + ".x(" + params.x_in->as_parent_register_unchanged(*this, wl) + "), "
            + ".y(" + params.y_in->as_parent_register_unchanged(*this, wl) + "), "
            + ".z(" + base_name() + "));"
    };
}
std::vector<std::string> Interp2Equid::get_cpp(WordLength wl, CppDataType t) {
    auto fractional_bits = this->fractional_bits(wl);
    params.z->request_fractional_bits(fractional_bits);
    return {
        this->cpp_datatype(wl, t)
            + " "
            + this->base_name()
            + " = "
            + "interp::do_2d_interpolation<" + std::to_string(fractional_bits) + ">("
            + this->params.z->cpp_function_name(fractional_bits) + ", "
            + std::to_string(this->params.y_data.size()) + ", "
            + std::to_string(this->params.xmin->value) + ", "
            + std::to_string(this->params.ymin->value) + ", "
            + std::to_string(this->params.xmax->value) + ", "
            + std::to_string(this->params.ymax->value) + ", "
            + std::to_string(this->params.dx->value) + ", "
            + std::to_string(this->params.dy->value) + ", "
            + this->params.x_in->base_name() + ", "
            + this->params.y_in->base_name()
            + ");"
    };
}
std::string Interp2Equid::graph_label_name() {return "Interp2Equid";}
// TODO: Implement and test
std::optional<std::string> Interp2Equid::compare_local(Weak<Node> other) const {
    auto casted = std::dynamic_pointer_cast<Interp2Equid>(other.lock());
    if (params == casted->params) {
        return std::nullopt;
    }
    else {
        return "Missmatched parameters";
    }
}
std::string Interp2Equid::variable_name() const {
    return "interp2_" + params.name;
}
void Interp2Equid::optimize_parents(OptPass pass) {
    ::optimize_parents(pass, &params.x_in, &params.y_in);
}
std::optional<Rc<Node>> Interp2Equid::constant_fold() {
    return node::do_constant_fold(*this, &params.x_in, &params.y_in, [this](auto, auto) {
        return this->calculate_value();
    });
}
// TODO: Implement and test
double Interp2Equid::_min() const {
    return *std::min_element(params.z_data.begin(), params.z_data.end());
}
double Interp2Equid::_max() const {
    return *std::max_element(params.z_data.begin(), params.z_data.end());
}
std::vector<Rc<Node>> Interp2Equid::get_parents() const {
    return {params.x_in, params.y_in};
}
ExecutionType Interp2Equid::recalculate_value() {
    int x_idx = 0;
    int y_idx = 0;
    
    double q11, q12, q21, q22;

    auto xp = params.x_in->calculate_value();
    auto yp = params.y_in->calculate_value();

    auto dx = params.dx->calculate_value();
    auto dy = params.dy->calculate_value();
    auto nx = params.nx->calculate_value();
    auto ny = params.ny->calculate_value();
    auto xmin = params.xmin->calculate_value();
    auto ymin = params.ymin->calculate_value();

    auto& x = params.x_data;
    auto& y = params.y_data;
    auto& z = params.z_data;

    if( (xp < x[0]) || (xp > x[nx-1]) || (yp < y[0]) || (yp > y[ny-1])){
        return params.z->min();
    }
    else {
        // estimate
        x_idx = trunc( (xp-xmin)/dx ) + 1;
        y_idx = trunc( (yp-ymin)/dy ) + 1;
        
        q11 = z[(x_idx-1)*ny + y_idx-1];
        q12 = z[(x_idx-1)*ny + y_idx];
        q21 = z[x_idx*ny + y_idx-1];
        q22 = z[x_idx*ny + y_idx];
        
        return ( q11*(x[x_idx] - xp)*(y[y_idx] - yp) + q21*(xp-x[x_idx-1])*(y[y_idx] - yp) + q12*(x[x_idx]-xp)*(yp - y[y_idx-1]) + q22*(xp-x[x_idx-1])*(yp - y[y_idx-1]) )/( ( x[x_idx] - x[x_idx-1])*(y[y_idx] - y[y_idx-1]) );
    }
}
std::vector<std::string> Interp2Equid::verilog_extra_commands(WordLength wl) const {
    auto module_name = this->clone()->base_name();

    return {
        "scripts/build_2d_interp.hs "
            + module_name + "_mod"
            + " "
            + params.z->get_name(this->fractional_bits(wl))
            + " "
            + tuple_string("xPreMult", 24)
            + " "
            + tuple_string("yPreMult", 33)
            + " "
            + tuple_string("xSize", params.x_in->register_size(wl))
            + " "
            + tuple_string("ySize", params.y_in->register_size(wl))
            + " "
            + tuple_string("xLutLsb", 10)
            + " "
            + tuple_string("yLutLsb", 10)
            + " "
            + tuple_string("outputSize", register_size(wl))
            + " "
            + tuple_string("ny", params.y_data.size())
            + " "
            + tuple_string("mul_dx_dy", 55)
            + " "
            + tuple_string("dx", 25)
            + " "
            + tuple_string("dy", 25)
    };
}
int Interp2Equid::computation_time(WordLength) const {
    return 11;
}
std::vector<std::string> Interp2Equid::extra_verilog_files(WordLength) {
    return {this->base_name() + "_mod.v"};
}

// Valid class
Interp2EquidValid::Interp2EquidValid(Rc<Interp2Equid> sibling)
    : BoolNode{{sibling->params.x_in.weak(), sibling->params.y_in.weak()}}
    , sibling{sibling}
{}
Rc<Interp2EquidValid> Interp2EquidValid::create(Rc<Interp2Equid> sibling) {
    return Rc{new Interp2EquidValid{sibling}};
}
// TODO: test
std::vector<std::string> Interp2EquidValid::get_code(WordLength wl) {
    auto params = sibling->params;
    auto x_in = sibling->params.x_in;
    auto y_in = sibling->params.y_in;
    int64_t x_min_value = params.xmin->calculate_value()
                  * pow(2, x_in->fractional_bits(wl));
    int64_t x_max_value = params.xmax->calculate_value()
                  * pow(2, x_in->fractional_bits(wl));
    int64_t y_min_value = params.ymin->calculate_value()
                  * pow(2, y_in->fractional_bits(wl));
    int64_t y_max_value = params.ymax->calculate_value()
                  * pow(2, y_in->fractional_bits(wl));

    auto int_bits = std::max({x_in->integer_bits(wl), y_in->integer_bits(wl)});
    auto frac_bits = std::max({x_in->fractional_bits(wl), y_in->fractional_bits(wl)});
    auto total_bits = int_bits + frac_bits + 1;
    return merge_code({
        {register_code(base_name(), 1)},
        clocked_block({
            base_name()
            + " <= !($signed("
            + x_in->as_parent_register(*this, wl, int_bits, frac_bits, false)
                + ") < $signed("
                + util::verilog::sized_constant(total_bits, x_min_value)
            + ") || $signed("
            + x_in->as_parent_register(*this, wl, int_bits, frac_bits, false)
                + ") >= $signed("
                + util::verilog::sized_constant(total_bits, x_max_value)
            + ") || $signed("
            + y_in->as_parent_register(*this, wl, int_bits, frac_bits, false)
                + ") < $signed("
                + util::verilog::sized_constant(total_bits, y_min_value)
                + std::to_string(y_min_value)
            + ") || $signed("
            + y_in->as_parent_register(*this, wl, int_bits, frac_bits, false)
                + ") >= $signed("
                + util::verilog::sized_constant(total_bits, y_max_value)
            + "));"
        })
    });
}
// TODO: test
std::vector<std::string> Interp2EquidValid::get_cpp(WordLength wl, CppDataType t) {
    auto params = this->sibling->params;
    return {
        this->cpp_datatype(wl, t)
            + " "
            + this->base_name()
            + " = "
            + "interp::do_2d_interpolation_valid("
            + std::to_string(params.xmin->value) + ", "
            + std::to_string(params.ymin->value) + ", "
            + std::to_string(params.xmax->value) + ", "
            + std::to_string(params.ymax->value) + ", "
            + params.x_in->base_name() + ", "
            + params.y_in->base_name()
            + ");"
    };
}
std::string Interp2EquidValid::graph_label_name() {return "Interp2EquidValid";}
std::optional<std::string> Interp2EquidValid::compare_local(Weak<Node> other) const {
    auto casted = std::dynamic_pointer_cast<Interp2EquidValid>(other.lock());
    return sibling->compare(casted->sibling.weak());
}
std::string Interp2EquidValid::variable_name() const {
    return "interp2valid_" + sibling->params.name;
}
void Interp2EquidValid::optimize_parents(OptPass pass) {
    sibling->optimize_parents(pass);
}
std::optional<Rc<Node>> Interp2EquidValid::constant_fold() {
    auto xp = sibling->params.x_in;
    auto yp = sibling->params.y_in;
    return node::do_bool_constant_fold(*this, &xp, &yp, [this](auto, auto) {
        return this->calculate_value();
    });
}
// TODO: Implement and test
std::optional<Rc<Node>> Interp2EquidValid::remove_redundant_interp_validity() {
    auto&& params = sibling->params;
    auto min_x = params.x_in->min();
    auto min_y = params.y_in->min();
    auto max_x = params.x_in->max();
    auto max_y = params.y_in->max();

    if(params.input_in_bounds(min_x, min_y) && params.input_in_bounds(max_x, max_y)) {
        return BoolConst::True;
    }
    return std::nullopt;
}
std::vector<Rc<Node>> Interp2EquidValid::get_parents() const {
    return sibling->get_parents();
}
bool Interp2EquidValid::recalculate_value() {
    auto xp = sibling->params.x_in->calculate_value();
    auto yp = sibling->params.y_in->calculate_value();
    return sibling->params.input_in_bounds(xp, yp);
}
