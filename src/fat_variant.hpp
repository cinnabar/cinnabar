#pragma once

#include <ostream>

// The problem here is that std::map needs a `operator<`

#define FAT_VARIANT_MEMBER1(NAME, INNER_TYPE) \
    struct NAME { \
        explicit NAME(INNER_TYPE val) : val{val} {}; \
        INNER_TYPE val; \
        bool operator==(NAME other) const { \
            return other.val == val; \
        } \
        \
        bool operator<(NAME const& other) const { \
            return other.val < val; \
        } \
    }; \
    /* Inline is required because defining functions in the header file */ \
    /* causes multiple definitions to appear. But I sure as hell don't want to */ \
    /* repeat this definition in cpp files because that makes this whole macro */ \
    /* useless */ \
    inline std::ostream& operator<< (std::ostream& stream, NAME const& val) { \
        return stream << val.val; \
    };
