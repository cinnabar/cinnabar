#include "ffi.hpp"
// #include "../../mercurous/target/cxxbridge/src/ffi.rs.h"
#include "cinnabar/src/ffi.rs.h"

#include "../depth_opt.hpp"
#include "../graph.hpp"
#include "../interpolation.hpp"
#include "../lookup_table.hpp"

#include <iterator>
#include <map>
#include <memory>
#include <vector>

using namespace cinnabar;

using util::join;

RcNodePtr cinnabar::wrap_rc(Rc<Node> const &to_wrap) {
    return std::make_unique<RcNode>(to_wrap);
}

std::unique_ptr<Config> cinnabar::new_config(WordLength const& wl,
                                             bool single_node_interp) {
    return std::make_unique<Config>(
        Config{wl, single_node_interp, single_node_interp});
}

// Construction functions
RcNodePtr cinnabar::new_input(rust::Str name, double min, double max,
                              double value) {
    return wrap(Input::create(copy_str(name), min, max, value));
}
RcNodePtr
cinnabar::new_non_referenced(rust::Str name, rust::Str description,
                             rust::Slice<const RcNodePtr> parents_rust) {
    std::vector<Rc<Node>> parents;
    for (auto &&parent : parents_rust) {
        parents.push_back(*parent);
    }

    return wrap(NonReferencedNode::create(copy_str(name), copy_str(description),
                                          parents));
}
RcNodePtr cinnabar::bool_const(bool value) {
    return wrap(BoolConst::create(value));
}

RcNodePtr cinnabar::frac_const(double value) {
    // TODO: Find a better way to name constants
    return wrap(Constant::create("__const__" + std::to_string(rand()), value));
}

#define INFIX_BINOP_CONSTRUCTOR(NAME, OP, ASSUMPTION)                          \
    RcNodePtr cinnabar::NAME(RcNode const &lhs, RcNode const &rhs) {           \
        return wrap(ASSUMPTION(lhs) OP ASSUMPTION(rhs));                       \
    }
INFIX_BINOP_CONSTRUCTOR(new_add, +, assume_frac);
INFIX_BINOP_CONSTRUCTOR(new_sub, -, assume_frac);
INFIX_BINOP_CONSTRUCTOR(new_mul, *, assume_frac);
INFIX_BINOP_CONSTRUCTOR(new_div, /, assume_frac);
INFIX_BINOP_CONSTRUCTOR(new_gt, >, assume_frac);
INFIX_BINOP_CONSTRUCTOR(new_gt_or_eq, >=, assume_frac);
INFIX_BINOP_CONSTRUCTOR(new_lt, <, assume_frac);
INFIX_BINOP_CONSTRUCTOR(new_lt_or_eq, <=, assume_frac);
INFIX_BINOP_CONSTRUCTOR(new_eq, ==, assume_frac);
INFIX_BINOP_CONSTRUCTOR(new_neq, !=, assume_frac);
INFIX_BINOP_CONSTRUCTOR(new_or, ||, assume_bool);
INFIX_BINOP_CONSTRUCTOR(new_and, &&, assume_bool);
#undef INFIX_BINOP_CONSTRUCTOR

#define FUNCTION_BINOP_CONSTRUCTOR(NAME, HANDLER)                              \
    RcNodePtr cinnabar::NAME(RcNode const &lhs, RcNode const &rhs) {           \
        return wrap(HANDLER(assume_frac(lhs), assume_frac(rhs)));              \
    }
FUNCTION_BINOP_CONSTRUCTOR(new_min, min)
FUNCTION_BINOP_CONSTRUCTOR(new_max, max)
#undef FUNCTION_BINOP_CONSTRUCTOR

#define UNOP_CONSTRUCTOR(NAME, CREATOR, TYPE_CONVERTER)                        \
    RcNodePtr cinnabar::NAME(RcNode const &op) {                               \
        return wrap(CREATOR(TYPE_CONVERTER(op)));                              \
    }
UNOP_CONSTRUCTOR(new_usub, USub::create, assume_frac);
UNOP_CONSTRUCTOR(new_truncate, Truncate::create, assume_frac);
UNOP_CONSTRUCTOR(new_sqrt, Sqrt::create, assume_frac);
UNOP_CONSTRUCTOR(new_abs, Abs::create, assume_frac);
UNOP_CONSTRUCTOR(new_not, Not::create, assume_bool);
UNOP_CONSTRUCTOR(new_opaque_barrier, OpaqueNode::create, assume_frac);
#undef UNOP_CONSTRUCTOR
RcNodePtr cinnabar::bypass_nop(RcNode const &inner) { return wrap(inner); }

RcNodePtr cinnabar::new_lut(RcNode const &idx, rust::Str name,
                            rust::Vec<double> const &values) {
    auto lut = LookupTable::create(copy_str(name), vec_to_vector(values));
    return wrap(LutResult::create(lut, assume_frac(idx)));
}

RcNodePtr cinnabar::new_interp1(rust::Str name, rust::Vec<double> const &x,
                                rust::Vec<double> const &y, RcNode const &index,
                                Config const &cfg) {
    auto [_valid, val] =
        interp::interp1equid(copy_str(name), vec_to_vector(x), vec_to_vector(y),
                             assume_frac(index), cfg);
    return wrap(val);
}
RcNodePtr cinnabar::new_interp1_valid(rust::Str name,
                                      rust::Vec<double> const &x,
                                      rust::Vec<double> const &y,
                                      RcNode const &index, Config const &cfg) {
    auto [valid, _val] =
        interp::interp1equid(copy_str(name), vec_to_vector(x), vec_to_vector(y),
                             assume_frac(index), cfg);
    return wrap(valid);
}
RcNodePtr cinnabar::new_interp2(rust::Str name, rust::Vec<double> const &x,
                                rust::Vec<double> const &y,
                                rust::Vec<double> const &z,
                                RcNode const &x_point, RcNode const &y_point,
                                Config const &cfg) {
    auto [_valid, val] = interp::interp2equid(
        copy_str(name), vec_to_vector(x), vec_to_vector(y), vec_to_vector(z),
        assume_frac(x_point), assume_frac(y_point), cfg);
    return wrap(val);
}
RcNodePtr
cinnabar::new_interp2_valid(rust::Str name, rust::Vec<double> const &x,
                            rust::Vec<double> const &y,
                            rust::Vec<double> const &z, RcNode const &x_point,
                            RcNode const &y_point, Config const &cfg) {
    auto [valid, _val] = interp::interp2equid(
        copy_str(name), vec_to_vector(x), vec_to_vector(y), vec_to_vector(z),
        assume_frac(x_point), assume_frac(y_point), cfg);
    return wrap(valid);
}

RcNodePtr cinnabar::new_if_fractional(RcNode const &cond, RcNode const &on_true,
                                      RcNode const &on_false) {
    return wrap(IfFractional::create(assume_bool(cond), assume_frac(on_true),
                                     assume_frac(on_false)));
}

RcNodePtr cinnabar::new_if_bool(RcNode const &cond, RcNode const &on_true,
                                RcNode const &on_false) {
    return wrap(IfBoolean::create(assume_bool(cond), assume_bool(on_true),
                                  assume_bool(on_false)));
}

// Select creation functions
std::unique_ptr<PartialSelect>
cinnabar::new_select_option(RcNode const &value) {
    return std::make_unique<PartialSelect>(
        PartialSelect{.value = value, .conditions = {}});
}
void cinnabar::push_select_condition(PartialSelect &target,
                                     RcNode const &condition) {
    target.conditions.push_back(assume_bool(condition));
}
RcNodePtr cinnabar::new_select_frac(rust::Vec<PartialSelectWrapper> wrapper) {
    std::vector<std::pair<Rc<FracNode>, std::vector<Rc<BoolNode>>>> inner;

    std::transform(wrapper.begin(), wrapper.end(), std::back_inserter(inner),
                   [](PartialSelectWrapper const &partial) {
                       return std::pair{assume_frac(partial.inner->value),
                                        partial.inner->conditions};
                   });
    return wrap(SelectFrac::create(inner));
}
RcNodePtr cinnabar::new_select_bool(rust::Vec<PartialSelectWrapper> wrapper) {
    std::vector<std::pair<Rc<BoolNode>, std::vector<Rc<BoolNode>>>> inner;

    std::transform(wrapper.begin(), wrapper.end(), std::back_inserter(inner),
                   [](PartialSelectWrapper const &partial) {
                       return std::pair{assume_bool(partial.inner->value),
                                        partial.inner->conditions};
                   });
    return wrap(SelectBool::create(inner));
}

std::unique_ptr<std::vector<WeakNode>>
cinnabar::unique_ancestors(const RcNode &node) {
    auto set = node->unique_ancestors();
    std::vector<WeakNode> result;
    std::copy(set.begin(), set.end(), std::back_inserter(result));
    return std::make_unique<std::vector<WeakNode>>(result);
}

void cinnabar::override_name(RcNode const &node, rust::Str name) {
    node->override_name(copy_str(name));
}
void cinnabar::print_name(RcNode const &node) {
    std::cout << node->base_name() << std::endl;
}
std::unique_ptr<std::string> cinnabar::get_weak_name(const WeakNode &node) {
    return std::make_unique<std::string>(node.lock()->base_name());
}
std::unique_ptr<std::string> cinnabar::get_node_name(RcNode const &node) {
    return std::make_unique<std::string>(node->base_name());
}
float cinnabar::calculate_value(RcNode const &node) {
    if (auto fnode = node.pointer_cast<FracNode>()) {
        return (*fnode)->calculate_value();
    }
    throw "Incorrect node type";
}

std::string cinnabar::get_name(RcNode const &node) { return node->base_name(); }
size_t cinnabar::integer_bits(RcNode const &node, const WordLength &wl) {
    return assume_frac(node)->integer_bits(wl);
}
size_t cinnabar::fractional_bits(RcNode const &node, const WordLength &wl) {
    return assume_frac(node)->fractional_bits(wl);
}
size_t cinnabar::register_size(RcNode const &node, const WordLength &wl) {
    return assume_frac(node)->register_size(wl);
}
int cinnabar::pipeline_end_depth(RcNode const &node, const WordLength &wl) {
    return node->end_depth(wl);
}

void cinnabar::dump_graph(rust::Str filename, RcNode const &node,
                          WordLength const &wl, bool include_delay) {
    info("Dumping graph to ", copy_str(filename));
    graph::dump_graph(
        "", copy_str(filename), node,
        [](Node *n) { return std::string("$") + n->base_name() + "$"; }, wl,
        include_delay);
}

void cinnabar::report_problems(RcNode const &root) {
    root->visit_with_cache<int>([](auto node) {
        auto problems = node->check_for_problems();
        if (problems.size() != 0) {
            err("Problem in node ", node->base_name());
            for (auto problem : problems) {
                std::cout << "\t" << problem << std::endl;
                ;
            }
        }
        // We need to return a value for consts to work
        return 0;
    });
}

// Codegen
std::unique_ptr<ReturnValues> cinnabar::new_return_values() {
    return std::make_unique<ReturnValues>(ReturnValues{});
}
void cinnabar::push_return_value(ReturnValues &target, const Rc<Node> &value) {
    target.inner.push_back(value);
}
std::unique_ptr<ReturnValues>
cinnabar::opt_return_values(ReturnValues const &target, OptPass const &pass) {
    auto result = new_return_values();
    for (auto &node : target.inner) {
        push_return_value(*result, node->optimize(pass).value_or(node));
    }
    return result;
}

RcNodePtr cinnabar::ret_values_as_dummy_node(ReturnValues const &ret) {
    return wrap(DummyNode::create(ret.inner));
}

std::unique_ptr<Codegen> cinnabar::start_codegen(ReturnValues const &outputs,
                                                 WordLength const &wl) {
    std::map<std::string, Rc<Node>> nodes = {};
    int index = 0;
    for (auto node : outputs.inner) {
        nodes.insert({"__" + std::to_string(index), node});
        index += 1;
    }
    // Something is causing the rust side to not receive the DuplicateNode
    // exception thrown by the codegen constructor, so we'll catch it here,
    // print the message and then re-throw it.
    try {
        return std::make_unique<Codegen>(Codegen{nodes, wl});
    } catch (codegen::DuplicateNodeError &e) {
        err(e.what());
        throw e;
    }
}
std::unique_ptr<std::string> cinnabar::codegen_get_cpp(rust::Str name,
                                                       Codegen &codegen,
                                                       WordLength const &wl,
                                                       CppDataType const &t) {
    return std::make_unique<std::string>(
        join(codegen.generate_cpp_function(copy_str(name), wl, t).cpp));
}
std::unique_ptr<std::string> cinnabar::codegen_get_hpp(rust::Str name,
                                                       Codegen &codegen,
                                                       WordLength const &wl,
                                                       CppDataType const &t) {
    return std::make_unique<std::string>(
        join(codegen.generate_cpp_function(copy_str(name), wl, t).hpp));
}
void cinnabar::build_lookup_tables(rust::Str storage_location,
                                   CppDataType const &t) {
    LookupTable::generate_code_for_lookup_tables(copy_str(storage_location), t);
}
void cinnabar::run_verilog_extra_commands(Codegen const &codegen,
                                          WordLength const &word_length,
                                          rust::Str cinnabar_root,
                                          rust::Str output_dir) {
    codegen.run_verilog_extra_commands(word_length, copy_str(cinnabar_root),
                                       copy_str(output_dir));
}

std::unique_ptr<std::vector<std::string>>
cinnabar::get_extra_cpps(Rc<Node> const &root, WordLength const &wl) {
    auto from_luts = LookupTable::get_cpp_files();

    auto from_nodes = root->visit_with_cache<std::vector<std::string>>(
        [wl](auto node) { return node->extra_cpp_files(wl); });

    std::vector<std::string> result;
    result.insert(result.end(), from_luts.begin(), from_luts.end());
    for (auto &&node : from_nodes) {
        result.insert(result.end(), node.begin(), node.end());
    }

    return std::make_unique<std::vector<std::string>>(result);
}
std::unique_ptr<std::vector<std::string>>
cinnabar::get_extra_verilogs(Rc<Node> const &root, WordLength const &wl) {
    auto from_luts = LookupTable::get_verilog_files();

    auto from_nodes =
        root->visit_with_cache<std::vector<std::string>>([wl](auto node) {
            std::string node_kind = +typeid(*node).name();
            return node->extra_verilog_files(wl);
        });

    std::vector<std::string> result;
    result.insert(result.end(), from_luts.begin(), from_luts.end());
    for (auto &&node : from_nodes) {
        result.insert(result.end(), node.begin(), node.end());
    }

    return std::make_unique<std::vector<std::string>>(result);
}

std::unique_ptr<WordLength> cinnabar::fixed_total_word_length(int amount) {
    return std::make_unique<WordLength>(FixedTotalLength{amount});
}
std::unique_ptr<WordLength> cinnabar::fixed_fractional_word_length(int amount) {
    return std::make_unique<WordLength>(FixedFixpoint{amount});
}
std::unique_ptr<WordLength> cinnabar::fully_specified_word_length(int integer, int fractional) {
    return std::make_unique<WordLength>(FixedType{integer, fractional});
}

std::unique_ptr<CppDataType> cinnabar::cpp_data_type_fp() {
    return std::make_unique<CppDataType>(CppDataType::Fp);
}
std::unique_ptr<CppDataType> cinnabar::cpp_data_type_ac() {
    return std::make_unique<CppDataType>(CppDataType::Ac);
}
std::unique_ptr<CppDataType> cinnabar::cpp_data_type_ap() {
    return std::make_unique<CppDataType>(CppDataType::Ap);
}

std::unique_ptr<std::string>
cinnabar::codegen_get_verilog(rust::Str name, Codegen &codegen,
                              WordLength const &wl, bool compress_inputs) {
    return std::make_unique<std::string>(
        join(codegen.generate_module(copy_str(name), wl, compress_inputs)));
}

std::unique_ptr<std::string> cinnabar::node_info(RcNode const &node) {
    {
        auto f = node.pointer_cast<FracNode>();
        if (f.has_value()) {
            auto n = f.value();
            return std::make_unique<std::string>(std::to_string(n->min()) +
                                                 " < n < " +
                                                 std::to_string(n->max()));
        }
    }

    {
        auto f = node.pointer_cast<BoolNode>();
        if (f.has_value()) {
            return std::make_unique<std::string>("bool node");
        }
    }
    return std::make_unique<std::string>("Unknown node type");
}

// Non-ffi helper functions
Rc<FracNode> cinnabar::assume_frac(Rc<Node> node) {
    auto n = node.pointer_cast<FracNode>();
    if (n.has_value()) {
        return n.value();
    }
    throw error::NonFracNode(node->base_name());
}
Rc<BoolNode> cinnabar::assume_bool(Rc<Node> node) {
    auto n = node.pointer_cast<BoolNode>();
    if (n.has_value()) {
        return n.value();
    }
    throw error::NonFracNode(node->base_name());
}

RcNodePtr cinnabar::wrap(RcNode node) { return std::make_unique<RcNode>(node); }

std::string cinnabar::copy_str(rust::Str val) {
    return std::string(val.data(), val.length());
}

void cinnabar::do_retiming(Rc<Node> &root, WordLength const &wl) {
    info("Generating depth model");
    auto gmpl = depth_opt::generate_gmpl(&*root, wl);
    info("Running optimisation");
    depth_opt::Glpk opt_result{util::join(gmpl)};

    std::vector<Weak<Node>> all_nodes;
    auto ancestors = root->unique_ancestors();
    std::copy(ancestors.begin(), ancestors.end(),
              std::back_inserter(all_nodes));
    depth_opt::use_new_depths(all_nodes, opt_result);

    // Do a sanity check to ensure that nodes still start after their parents
    root->visit_with_cache<bool>([wl](auto node) {
        for (auto parent : node->get_parents()) {
            if (!(parent->end_depth(wl) < node->start_depth(wl))) {
                err("Node ", parent->base_name(), " starts after",
                    node->base_name());
                return false;
            }
        }
        return true;
    });
}

void cinnabar::set_recompute_not(bool value) {
    Not::set_compute_on_demand(value);
}

////////////////////////////////////////////////////////////////////////////
//  Optimisation functions
RcNodePtr cinnabar::opt_optimize(RcNode const &root, OptPass const &pass) {
    return std::make_unique<RcNode>(
        root->optimize(pass).value_or(root->clone()));
}

// Optimisation pass constructors
#define OPT_PASS_CONSTRUCTOR(FUNCTION, VARIANT)                                \
    std::unique_ptr<OptPass> cinnabar::FUNCTION() {                            \
        return std::make_unique<OptPass>(OptPass::VARIANT);                    \
    }

OPT_PASS_CONSTRUCTOR(opt_pass_constant_fold, ConstantFold)
OPT_PASS_CONSTRUCTOR(opt_pass_remove_redundant_comparisons,
                     RemoveRedundantComparisons)
OPT_PASS_CONSTRUCTOR(opt_pass_remove_redundant_interp_validity,
                     RemoveRedundantInterpValidity)
OPT_PASS_CONSTRUCTOR(opt_pass_remove_redundant_bool_computations,
                     RemoveRedundantBoolComputations)
OPT_PASS_CONSTRUCTOR(opt_pass_constant_division_replacement,
                     ConstantDivisionReplacement)
OPT_PASS_CONSTRUCTOR(opt_pass_merge_bools, MergeBools)
OPT_PASS_CONSTRUCTOR(opt_pass_select_else_last, SelectElseLast)
OPT_PASS_CONSTRUCTOR(opt_pass_select_else_x, SelectElseX)
OPT_PASS_CONSTRUCTOR(opt_pass_remove_single_select, RemoveSingleSelect)

#undef OPT_PASS_CONSTRUCTOR
