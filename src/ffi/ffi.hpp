#pragma once

#include "../codegen.hpp"
#include "../node.hpp"
#include "../../mercurous/target/cxxbridge/rust/cxx.h"
#include "../exception_util.hpp"

#include <iostream>
#include <memory>
#include <vector>

namespace cinnabar {
    namespace error {
        struct NonFracNode : std::exception {
            NonFracNode(std::string node_name) : node_name{node_name} {};
            std::string node_name;
            WITH_MESSAGE("Expected ", node_name, " to be a FracNode");
        };
    }


    // Type aliases for cinnabar wrappers
    using RcNode = Rc<Node>;
    using WeakNode = Weak<Node>;
    using RcNodePtr = std::unique_ptr<Rc<Node>>;

    // Create a new UniquePtr containing the same value as to_wrap. This is done
    // to be able to pass RcNode in cases where RcNodePtr is required.
    // Since RC is reference counted, this is a fairly cheap operation
    RcNodePtr wrap_rc(Rc<Node> const& to_wrap);


    std::unique_ptr<Config> new_config(WordLength const& wl, bool single_node_interp);

    RcNodePtr frac_const(double val);
    RcNodePtr bool_const(bool value);
    RcNodePtr new_input(rust::Str name, double min, double max, double value);
    RcNodePtr new_non_referenced(rust::Str name, rust::Str description, rust::Slice<const RcNodePtr>);

    // Macro based constructor definitions
#define UNOP_CONSTRUCTOR(NAME) \
    RcNodePtr NAME(RcNode const&);
#define BINOP_CONSTRUCTOR(NAME) \
    RcNodePtr NAME(RcNode const& lhs, RcNode const& rhs);

    UNOP_CONSTRUCTOR(bypass_nop);
    UNOP_CONSTRUCTOR(new_usub);
    UNOP_CONSTRUCTOR(new_truncate);
    UNOP_CONSTRUCTOR(new_sqrt);
    UNOP_CONSTRUCTOR(new_abs);
    UNOP_CONSTRUCTOR(new_not);
    UNOP_CONSTRUCTOR(new_opaque_barrier);

    BINOP_CONSTRUCTOR(new_and);
    BINOP_CONSTRUCTOR(new_or);

    BINOP_CONSTRUCTOR(new_add);
    BINOP_CONSTRUCTOR(new_sub);
    BINOP_CONSTRUCTOR(new_mul);
    BINOP_CONSTRUCTOR(new_div);

    BINOP_CONSTRUCTOR(new_gt);
    BINOP_CONSTRUCTOR(new_lt);
    BINOP_CONSTRUCTOR(new_gt_or_eq);
    BINOP_CONSTRUCTOR(new_lt_or_eq);
    BINOP_CONSTRUCTOR(new_eq);
    BINOP_CONSTRUCTOR(new_neq);

    BINOP_CONSTRUCTOR(new_min);
    BINOP_CONSTRUCTOR(new_max);

#undef UNOP_CONSTRUCTOR
#undef BINOP_CONSTRUCTOR

    RcNodePtr new_interp1(
        rust::Str name,
        rust::Vec<double> const& x,
        rust::Vec<double> const& y,
        RcNode const& index,
        Config const& cfg
    );
    RcNodePtr new_interp1_valid(
        rust::Str name,
        rust::Vec<double> const& x,
        rust::Vec<double> const& y,
        RcNode const& index,
        Config const& cfg
    );
    RcNodePtr new_interp2(
        rust::Str name,
        rust::Vec<double> const& x,
        rust::Vec<double> const& y,
        rust::Vec<double> const& z,
        RcNode const& x_point,
        RcNode const& y_point,
        Config const& cfg
    );
    RcNodePtr new_interp2_valid(
        rust::Str name,
        rust::Vec<double> const& x,
        rust::Vec<double> const& y,
        rust::Vec<double> const& z,
        RcNode const& x_point,
        RcNode const& y_point,
        Config const& cfg
    );

    // Creates a new lookup operation into a lookup table with the specified name
    // and values
    RcNodePtr new_lut(
        RcNode const& idx,
        rust::Str lut_name,
        rust::Vec<double> const& values
    );

    RcNodePtr new_if_fractional(
        RcNode const& condition,
        RcNode const& on_true,
        RcNode const& on_false
    );
    RcNodePtr new_if_bool(
        RcNode const& condition,
        RcNode const& on_true,
        RcNode const& on_false
    );

    // Select node translation. More complex because cxx does not like vectors
    // of c++ references
    struct PartialSelectWrapper;
    // A specific select condition which can be extended using `push_select_condition`
    struct PartialSelect {
        Rc<Node> value;
        std::vector<Rc<BoolNode>> conditions;
    };

    std::unique_ptr<PartialSelect> new_select_option(RcNode const& value);
    void push_select_condition(PartialSelect& target, RcNode const& cond);
    RcNodePtr new_select_frac(rust::Vec<PartialSelectWrapper>);
    RcNodePtr new_select_bool(rust::Vec<PartialSelectWrapper>);

    std::unique_ptr<std::vector<WeakNode>> unique_ancestors(RcNode const& node);

    void print_name(RcNode const& node);
    std::unique_ptr<std::string> get_weak_name(WeakNode const& node);
    std::unique_ptr<std::string> get_node_name(RcNode const& node);
    void override_name(RcNode const& node, rust::Str name);
    float calculate_value(RcNode const& node);

    std::string get_name(RcNode const& node);
    size_t integer_bits(RcNode const& node, WordLength const& wl);
    size_t fractional_bits(RcNode const& node, WordLength const& wl);
    size_t register_size(RcNode const& node, WordLength const& wl);
    int pipeline_end_depth(RcNode const& node, WordLength const& wl);

    void dump_graph(
        rust::Str filename,
        RcNode const& root,
        WordLength const& wl,
        bool include_delay
    );

    void report_problems(RcNode const& graph_root);


    // Returns debug info about the node such as its type, and its bounds if applicable
    std::unique_ptr<std::string> node_info(RcNode const& node);


    // Helper functions that are not part of the FFI

    // Casts node to a Frac node, throws NonFracNode if pointer cast fails
    Rc<FracNode> assume_frac(Rc<Node>);
    Rc<BoolNode> assume_bool(Rc<Node>);
    // Wraps the specified node Rc in a unique pointer for moving across the FFI
    RcNodePtr wrap(RcNode);

    // Rust strings are not null terminated, so we need to explicitly copy
    // their data into a c++ string
    std::string copy_str(rust::Str);

    template<typename T>
    std::vector<T> vec_to_vector(rust::Vec<T> const& v) {
        std::vector<double> values;
        std::copy(v.begin(), v.end(), std::back_inserter(values));
        return values;
    }


    // Codegen
    struct ReturnValues {
        std::vector<Rc<Node>> inner;
    };
    std::unique_ptr<ReturnValues> new_return_values();
    void push_return_value(ReturnValues& target, const Rc<Node>& value);

    // Optimise the return values
    std::unique_ptr<ReturnValues> opt_return_values(ReturnValues const& target, OptPass const& pass);

    RcNodePtr ret_values_as_dummy_node(ReturnValues const& ret);

    std::unique_ptr<Codegen> start_codegen(
        const ReturnValues& return_values,
        WordLength const& wl
    );
    std::unique_ptr<std::string> codegen_get_cpp(
        rust::Str name,
        Codegen& codegen,
        WordLength const& wl,
        CppDataType const& t
    );
    std::unique_ptr<std::string> codegen_get_hpp(
        rust::Str name,
        Codegen& codegen,
        WordLength const& wl,
        CppDataType const& t
    );
    std::unique_ptr<std::string> codegen_get_verilog(
        rust::Str name,
        Codegen& codegen,
        WordLength const& wl,
        bool compress_inputs
    );
    void build_lookup_tables(rust::Str storage_location, CppDataType const& t);
    void run_verilog_extra_commands(
        Codegen const& codegen,
        WordLength const& word_length,
        rust::Str cinnabar_root,
        rust::Str output_dir
    );
    std::unique_ptr<std::vector<std::string>> get_extra_cpps(
        Rc<Node> const& root,
        WordLength const& wl
    );
    std::unique_ptr<std::vector<std::string>> get_extra_verilogs(
        Rc<Node> const& root,
        WordLength const& wl
    );

    std::unique_ptr<WordLength> fixed_total_word_length(int amount);
    std::unique_ptr<WordLength> fixed_fractional_word_length(int amount);
    std::unique_ptr<WordLength> fully_specified_word_length(int integer, int fractional);

    std::unique_ptr<CppDataType> cpp_data_type_fp();
    std::unique_ptr<CppDataType> cpp_data_type_ac();
    std::unique_ptr<CppDataType> cpp_data_type_ap();

    // Retime the graph to optimise register count
    void do_retiming(Rc<Node>& root, WordLength const& wl);

    void set_recompute_not(bool value);

    ////////////////////////////////////////////////////////////////////////////
    //  Optimisation functions
    RcNodePtr opt_optimize(RcNode const& root, OptPass const& pass);

    // Optimisation pass constructors
    std::unique_ptr<OptPass> opt_pass_constant_fold();
    std::unique_ptr<OptPass> opt_pass_remove_redundant_comparisons();
    std::unique_ptr<OptPass> opt_pass_remove_redundant_interp_validity();
    std::unique_ptr<OptPass> opt_pass_remove_redundant_bool_computations();
    std::unique_ptr<OptPass> opt_pass_constant_division_replacement();
    std::unique_ptr<OptPass> opt_pass_merge_bools();
    std::unique_ptr<OptPass> opt_pass_select_else_last();
    std::unique_ptr<OptPass> opt_pass_select_else_x();
    std::unique_ptr<OptPass> opt_pass_remove_single_select();
}
