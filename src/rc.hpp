#ifndef H_RC
#define H_RC

#include <memory>
#include <vector>
#include <exception>
#include <cstring>
#include <optional>

#include "log.hpp"


class InvalidPointerCast : public std::exception {};

class ExpiredWeakPointer : public std::exception {
    public:
        ExpiredWeakPointer() {
        }

        virtual const char* what() const throw() override {
            return old_content.c_str();
        }
        std::string old_content;
};
// Namespace is used to avoid getting a substitution failure when trying to call
// operator== or operator< on a class with no such operators defined. 
// Weak is re-exported in the outside namespace and the operators should be found
// using ADL
namespace weak {
    template<class T>
    class Weak {
        public:
            explicit Weak(std::shared_ptr<T> inner) {
                this->inner = inner;
            }

            template <typename U>
            Weak(const Weak<U>& other,
                 typename std::enable_if<std::is_convertible<U*, T*>::value, void>::type * = 0)
            : inner(other.get_inner())
            {}

            std::shared_ptr<T> lock() const {
                if(auto result = inner.lock()) {
                    return result;
                }
                else {
                    throw ExpiredWeakPointer();
                }
            }

            std::weak_ptr<T> get_inner() const {return inner;}

        private:
            std::weak_ptr<T> inner;
    };

    template<class T>
    bool operator==(const Weak<T>& lhs, const Weak<T>& rhs) {
        auto lhs_ = lhs.lock();
        auto rhs_ = rhs.lock();
        return lhs_ == rhs_;
    }
    template<class T>
    bool operator<(const Weak<T>& lhs, const Weak<T>& rhs) {
        if(auto lhs_ = lhs.lock()) {
            if(auto rhs_ = rhs.lock()) {
                return lhs_ < rhs_;
            }
            else {
                throw "Weak ptr deallocated";
            }
        }
        else {
            throw "Weak ptr deallocated";
        }
    }
}
using weak::Weak;

/*
  A wrapper around shared_ptr which does not contain overloads for comparison oprators
*/
template<class T>
class Rc {
    public:
        explicit Rc(T* inner) {
            this->inner =  std::shared_ptr<T>(inner);
        }
        explicit Rc(std::shared_ptr<T> inner) : inner(inner) {};

        // This abomination is spawned from the depths of the linked blog and
        // allows construction of an Rc<Base> from an Rc<Derived> where Derived
        // : public base
        // https://cpptruths.blogspot.com/2015/11/covariance-and-contravariance-in-c.html
        template <typename U>
        Rc(const Rc<U>& other,
             typename std::enable_if<std::is_convertible<U*, T*>::value, void>::type * = 0)
        : inner(other.get_inner())
        {}

        T& operator*() const noexcept {
            return *inner;
        }
        T* operator->() const noexcept {
            return inner.operator->();
        }

        std::shared_ptr<T> get_inner() const {
            return inner;
        }
        Weak<T> weak() const {
            return Weak(inner);
        }
        T* raw_ptr() const {
            return inner.get();
        }

        bool is_same(Rc<T> other) const {
            return this->inner == other.inner;
        }

        template <typename U>
        std::optional<Rc<U>> pointer_cast() const {
            if(auto casted = std::dynamic_pointer_cast<U>(inner)) {
                return Rc<U>(casted);
            }
            else {
                return std::nullopt;
            }
        }
    private:
        std::shared_ptr<T> inner;
};



template<class T>
bool compare_rcs(std::vector<Rc<T>> lhs, std::vector<Rc<T>> rhs) {
    if (lhs.size() != rhs.size()) {
        info("RC comparison: missmatched length");
        return false;
    }
    for(std::size_t i = 0; i < std::min(lhs.size(), lhs.size()); ++i) {
        if(!lhs[i].is_same(rhs[i])) {
            info("RC comparison: missmatched value ", i);
            return false;
        }
    }
    return true;
}

#endif

