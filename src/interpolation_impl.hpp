#pragma once

#include "../yafp/src/fixedpoint.hpp"

namespace interp {
    template<int O, int I, typename F>
    Fp<O> do_interpolation(
        Fp<I> input,
        F lut_y,
        float x_min,
        float x_max,
        float dx
    ) {
        auto xp = input.as_float();
        if( (xp < x_min) || (xp >= x_max) ) {
            return lut_y(0);
        }
        else {
            int ii = trunc( (xp-x_min)/dx ) + 1;
            float x_offset = ((xp-x_min) - (ii-1) * dx);
            return lut_y(ii-1).as_float() + (lut_y(ii)-lut_y(ii-1)).as_float()
                *x_offset/dx;
        }
    }
    // TODO: Add tests
    template<int I>
    bool do_interpolation_valid(Fp<I> input, float x_min, float x_max) {
        auto xp = input.as_float();
        if( (xp < x_min) || (xp >= x_max) ) {
            return false;
        }
        return true;
    }

    // TODO: Add tests
    template<int X, int Y>
    bool do_2d_interpolation_valid(
        double xmin,
        double ymin,
        double xmax,
        double ymax,
        Fp<X> xp_,
        Fp<Y> yp_
    ) {
        auto xp = xp_.as_float();
        auto yp = yp_.as_float();
        if( (xp < xmin) || (xp >= xmax) || (yp < ymin) || (yp >= ymax)){
            return false;
        }
        return true;
    }

    template<int O, int X, int Y, typename FZ>
    Fp<O> do_2d_interpolation(
        FZ fz,
        unsigned long ny,
        double xmin,
        double ymin,
        double xmax,
        double ymax,
        double dx,
        double dy,
        Fp<X> xp_,
        Fp<Y> yp_
    ) {
        auto xp = xp_.as_float();
        auto yp = yp_.as_float();
        int x_idx = 0;
        int y_idx = 0;

        if( !do_2d_interpolation_valid(xmin, ymin, xmax, ymax, xp_, yp_) ) {
            return fz(0);
        }
        else {
            x_idx = trunc( (xp-xmin)/dx ) + 1;
            y_idx = trunc( (yp-ymin)/dy ) + 1;

            float q11 = fz((x_idx-1)*ny + y_idx-1).as_float();
            float q12 = fz((x_idx-1)*ny + y_idx).as_float();
            float q21 = fz(x_idx*ny + y_idx-1).as_float();
            float q22 = fz(x_idx*ny + y_idx).as_float();

            // The location of the corners in the x-y plane
            float x_lower = floor((xp - xmin)/dx) * dx + xmin;
            float y_lower = floor((yp - ymin)/dy) * dy + ymin;
            float x_upper = x_lower+dx;
            float y_upper = y_lower+dy;

            float x_offset = xp - x_lower;
            float y_offset = yp - y_lower;
            float x_offset_upper = x_upper - xp;
            float y_offset_upper = y_upper - yp;
            return
                ( q11*x_offset_upper*y_offset_upper
                + q21*x_offset*y_offset_upper
                + q12*x_offset_upper*y_offset
                + q22*x_offset*y_offset
                )
                /( dx * dy);
        }
    }
}
