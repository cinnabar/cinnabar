`include "vatch/main.v"
`include "verilog/interp1d.v"

`define TRUNCATE_REAL(REAL, BITS) \
    (real'(longint'(REAL * (2.0**BITS))) / (2.0**BITS))
    // $itor($rtoi(REAL * (2**BITS))) / (2**BITS))

module interp1d_tb
    #(parameter X = 0
    , parameter EXPECTED = 0
    , parameter INPUT_INT_BITS = -1
    , parameter INPUT_FP = -1
    , parameter OUTPUT_SIZE = -1
    , parameter OUTPUT_FP = -1
    , parameter LUT_ADDRESS_SIZE = -1
    , parameter LUT_INT_BITS = -1
    , parameter ALPHA = -1
    , parameter DX = -1
    , parameter LOG_DX = -1
    , parameter X_MIN = -1
    ) ();
    `SETUP_TEST

    integer alpha = ALPHA;

    reg clk;
    wire signed [OUTPUT_SIZE:0] out;

    initial begin
        clk = 0;
        $dumpfile(`VCD_OUTPUT);
        $dumpvars(0, interp1d_tb);

        #1;

        forever begin
            clk <= ~clk;
            #1;
        end
    end


    localparam LUT_COUNT = 6;
    real lut_values[LUT_COUNT-1:0];
    assign lut_values[0] = 1;
    assign lut_values[1] = 2;
    assign lut_values[2] = -1;
    assign lut_values[3] = -0.5;
    assign lut_values[4] = 1.5;
    assign lut_values[5] = -2.5;


    localparam LUT_OUTPUT_SIZE = LUT_INT_BITS + INPUT_FP + 1;

    wire[LUT_ADDRESS_SIZE-1:0] lut1_addr;
    reg[LUT_OUTPUT_SIZE-1:0] lut1_out;
    wire[LUT_ADDRESS_SIZE-1:0] lut2_addr;
    reg[LUT_OUTPUT_SIZE-1:0] lut2_out;

    always @(posedge clk) begin
        if(lut1_addr < LUT_COUNT) begin
            lut1_out <= integer'(`TRUNCATE_REAL(lut_values[lut1_addr], INPUT_FP)) << INPUT_FP;
        end
        else if ($isunknown(lut2_addr)) begin
        end
        else begin
            $display("OOB access to LUT array 1 %0d >= %0d", lut2_addr, LUT_COUNT);
        end
        if(lut2_addr < LUT_COUNT) begin
            lut2_out <= integer'(`TRUNCATE_REAL(lut_values[lut2_addr], INPUT_FP)) << INPUT_FP;
        end
        else if ($isunknown(lut2_addr)) begin
        end
        else begin
            $display("OOB access to LUT array 2 %0d >= %0d", lut2_addr, LUT_COUNT);
        end
    end

    reg[INPUT_INT_BITS+INPUT_FP:0] x;

    wire signed [OUTPUT_SIZE-1:0] result;
    // Check output
    always begin
        x <= `TRUNCATE_REAL(X, INPUT_FP);

        repeat(5) @(negedge clk);


        `ASSERT_EQ(result, EXPECTED);

        #10

        `END_TEST
    end


    interp1d
        #(.INPUT_INT_BITS(INPUT_INT_BITS)
        , .INPUT_FP(INPUT_FP)
        , .OUTPUT_SIZE(OUTPUT_SIZE)
        , .OUTPUT_FP(OUTPUT_FP)
        , .LUT_ADDRESS_SIZE(LUT_ADDRESS_SIZE)
        , .LUT_INT_BITS(LUT_INT_BITS)
        , .ALPHA(ALPHA)
        , .DX(DX)
        , .LOG_DX(LOG_DX)
        , .X_MIN(X_MIN)
        )
        uut
        ( .clk(clk)
        , .x_s0(x)
        , .lut1_addr(lut1_addr)
        , .lut1_output(lut1_out)
        , .lut2_addr(lut2_addr)
        , .lut2_output(lut2_out)
        , .out(result)
        );
endmodule
