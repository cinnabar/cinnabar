#!/usr/bin/env rust-script
//! ```cargo
//! [dependencies]
//! anyhow = "1.0.34"
//! structopt = "0.3.21"
//! ```

use std::process::Command;
use std::path::PathBuf;

use anyhow::{Result, Context};
use structopt::StructOpt;

#[derive(StructOpt, Clone)]
struct Opt {
    #[structopt()]
    outdir: PathBuf,
    #[structopt(short="A")]
    verilog_args: Vec<String>
}

fn main() -> Result<()> {
    let opt = Opt::from_args();

    let test_cases = vec![
        // No fractional bits
        (0, 0, 0),
        // Only fractional bits in output
        (0, 0, 3),
        // Unform fractional bits
        (2, 2, 2),
        // More RHS bits than LHS bits
        (2, 3, 4),
        // More LHS bits than RHS bits
        (3, 2, 4),
        // 32 bit edge case?
        (0, 0, 30),
        (6, 31, 25),
        // Failing test
        (30, 35, 29),
    ];

    let testbench = "verilog/tb/divider_tb.sv";

    for (rhs, lhs, out) in test_cases {
        let out_dir = opt.outdir.join(&format!("{}_{}_{}", lhs, rhs, out));

        std::fs::create_dir_all(&out_dir).context("Failed to create output dir")?;

        println!("[\x1b[0;34miverilog\x1b[0m] Building {} ({} {} {})", testbench, rhs, lhs, out);

        let vvp_file = out_dir.join("out.vvp");
        let vcd_file = out_dir.join("out.vcd");
        let iverilog_output = Command::new("iverilog")
            .arg("-o")
            .arg(&vvp_file)
            .args(&opt.verilog_args)
            .arg(format!("-DVCD_OUTPUT=\"{}\"", vcd_file.to_string_lossy()))
            .arg(testbench)
            .arg(format!("-Pdivider_tb.RHS_FRAC_BITS={}", rhs))
            .arg(format!("-Pdivider_tb.LHS_FRAC_BITS={}", lhs))
            .arg(format!("-Pdivider_tb.OUT_FRAC_BITS={}", out))
            .status()
            .context("Failed to call iverilog")?;

        assert!(
            iverilog_output.success(),
            "Failed to build verilog for {} {} {}",
            lhs,
            rhs,
            out
        );

        let vvp_status = Command::new("vvp")
            .arg(vvp_file)
            .status()
            .context("Failed to call vvp")?;

        assert!(vvp_status.success(), "Tests failed for {} {} {}", lhs, rhs, out);
    }

    Ok(())
}
