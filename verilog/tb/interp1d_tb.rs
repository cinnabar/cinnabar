#!/usr/bin/env rust-script
//! ```cargo
//! [dependencies]
//! anyhow = "1.0.34"
//! structopt = "0.3.21"
//! owo-colors = "3.1"
//! ```

use std::process::Command;
use std::path::PathBuf;

use owo_colors::OwoColorize;
use anyhow::{Result, Context};
use structopt::StructOpt;

#[derive(StructOpt, Clone)]
struct Opt {
    #[structopt()]
    outdir: PathBuf,
    #[structopt(short="A")]
    verilog_args: Vec<String>
}

#[derive(Clone)]
struct Dx {
    pub alpha: f64,
    pub dx: f64,
    pub log_dx: i64,
}

impl Dx {
    pub fn new(dx: f64) -> Self {
        let log_dx = dx.log2().ceil();
        let closest_larger_pow2 = (2f64).powf(log_dx);
        let alpha = closest_larger_pow2 / dx;

        Self {
            log_dx: log_dx as i64,
            dx,
            alpha
        }
    }
}

#[derive(Clone)]
struct TestInput {
    pub x: f64,
    pub expected: f64,
    pub input_int_bits: i64,
    pub input_fp: i64,
    pub output_size: i64,
    pub output_fp: i64,
    pub lut_address_size: i64,
    pub lut_int_bits: i64,
    pub x_min: f64,
    pub dx: Dx,
}

macro_rules! rounded_fp_getter {
    ($name:ident, $($value:ident).*, $fp:ident) => {
        fn $name(&self) -> i64 {
            (self.$($value).* * (2f64).powf(self.$fp as f64).floor()) as i64
        }
    }
}

impl TestInput {
    rounded_fp_getter!(x,        x,        input_fp);
    rounded_fp_getter!(expected, expected, output_fp);
    rounded_fp_getter!(x_min,    x_min,    input_fp);
    rounded_fp_getter!(alpha,    dx.alpha, input_fp);
    rounded_fp_getter!(dx,       dx.dx,    input_fp);

    fn test_case(&self, x: f64, expected: f64) -> Self {
        let mut result = self.clone();
        result.x = x;
        result.expected = expected;
        result
    }
}

fn main() -> Result<()> {
    let opt = Opt::from_args();

    let integers_step_1 = TestInput {
        x: 0.,
        expected: 1.,
        input_int_bits: 8,
        input_fp: 0,
        output_size: 8,
        output_fp: 0,
        lut_address_size: 8,
        lut_int_bits: 8,
        x_min: 0.,
        dx: Dx::new(1.)
    };

    let integers_step_2 = TestInput {
        dx: Dx::new(2.),
        .. integers_step_1
    };

    let integers_step_2_with_xmin2 = TestInput {
        dx: Dx::new(2.),
        x_min: 4.,
        .. integers_step_1
    };

    let fp_step_1 = TestInput {
        x: 0.,
        expected: 1.,
        input_int_bits: 8,
        input_fp: 4,
        output_size: 8,
        output_fp: 4,
        lut_address_size: 8,
        lut_int_bits: 4,
        x_min: 0.,
        dx: Dx::new(1.)
    };

    let fp_step_05 = TestInput {
        x: 0.,
        expected: 1.,
        input_int_bits: 8,
        input_fp: 4,
        output_size: 8,
        output_fp: 4,
        lut_address_size: 8,
        lut_int_bits: 4,
        x_min: 0.,
        dx: Dx::new(0.5)
    };

    let mixed_bit_widths_step_05 = TestInput {
        x: 0.,
        expected: 1.,
        input_int_bits: 10,
        input_fp: 5,
        output_size: 7,
        output_fp: 4,
        lut_address_size: 8,
        lut_int_bits: 3,
        x_min: 0.,
        dx: Dx::new(0.5)
    };

    let test_cases = vec![
        // On even indices
        ("int step1 0", integers_step_1.test_case(0., 1.)),
        ("int step1 1", integers_step_1.test_case(1., 2.)),
        ("int step1 2", integers_step_1.test_case(2., -1.)),

        // On even indices
        ("int step2 0", integers_step_2.test_case(0., 1.)),
        ("int step2 2", integers_step_2.test_case(2., 2.)),
        ("int step2 4", integers_step_2.test_case(4., -1.)),

        ("int step2 with x_min 0", integers_step_2_with_xmin2.test_case(4. + 0., 1.)),
        ("int step2 with x_min 2", integers_step_2_with_xmin2.test_case(4. + 2., 2.)),
        ("int step2 with x_min 4", integers_step_2_with_xmin2.test_case(4. + 4., -1.)),

        ("fp step1 0", fp_step_1.test_case(0., 1.)),
        ("fp step1 0.5", fp_step_1.test_case(0.5, 1.5)),
        ("fp step1 3.5", fp_step_1.test_case(3.5, 0.5)),

        ("fp step05 0", fp_step_05.test_case(0., 1.)),
        ("fp step05 0.5", fp_step_05.test_case(0.5, 2.)),
        ("fp step05 0.25", fp_step_05.test_case(0.25, 1.5)),
        ("fp step05 0.25", fp_step_05.test_case(0.25, 1.5)),
        ("fp step05 0.25", fp_step_05.test_case(2.25, -0.5)),

        ("fp step05 0", mixed_bit_widths_step_05.test_case(0., 1.)),
        ("fp step05 0.5", mixed_bit_widths_step_05.test_case(0.5, 2.)),
        ("fp step05 0.25", mixed_bit_widths_step_05.test_case(0.25, 1.5)),
        ("fp step05 0.25", mixed_bit_widths_step_05.test_case(0.25, 1.5)),
        ("fp step05 0.25", mixed_bit_widths_step_05.test_case(2.25, -0.5)),
    ];

    let testbench = "verilog/tb/interp1d_tb.sv";

    for (i, (description, test_case)) in test_cases.iter().enumerate() {
        println!("==== Running test {} ====", description);
        let out_dir = opt.outdir.join(&format!("{}", i));

        std::fs::create_dir_all(&out_dir).context("Failed to create output dir")?;

        println!("[\x1b[0;34mrust\x1b[0m][\x1b[0;34miverilog\x1b[0m] Building {} (test {})", testbench, i);

        macro_rules! param_args(
            ($(($name:ident, $value:expr)),*) => {
                vec![
                    $(
                        format!("-Pinterp1d_tb.{}={}", stringify!($name), $value)
                    ),*
                ]
            }
        );

        let vvp_file = out_dir.join("out.vvp");
        let vcd_file = out_dir.join("out.vcd");
        let iverilog_output = Command::new("iverilog")
            .arg("-o")
            .arg(&vvp_file)
            .args(&opt.verilog_args)
            .args(&param_args![
                (X, test_case.x()),
                (EXPECTED, test_case.expected()),
                (INPUT_INT_BITS, test_case.input_int_bits),
                (INPUT_FP, test_case.input_fp),
                (OUTPUT_SIZE, test_case.output_size),
                (OUTPUT_FP, test_case.output_fp),
                (LUT_ADDRESS_SIZE, test_case.lut_address_size),
                (LUT_INT_BITS, test_case.lut_int_bits),
                (ALPHA, test_case.alpha()),
                (DX, test_case.dx()),
                (LOG_DX, test_case.dx.log_dx),
                (X_MIN, test_case.x_min())
            ])
            .arg(format!("-DVCD_OUTPUT=\"{}\"", vcd_file.to_string_lossy()))
            .arg(testbench)
            .status()
            .context("Failed to call iverilog")?;

        if !iverilog_output.success() {
            println!("{}", format!("Compilation failed for test {} {}", i, description).red());
            panic!("Building verilog failed")
        }

        let vvp_status = Command::new("vvp")
            .arg(vvp_file)
            .status()
            .context("Failed to call vvp")?;

        if !vvp_status.success() {
            println!("{}", format!("Tests failed for test {} {}", i, description).red());
            println!("\tvcd file: {}", vcd_file.to_string_lossy());
            panic!("Building verilog failed")
        }
    }

    Ok(())
}
