set nfacs [ gtkwave::getNumFacs ]

puts "Loading all signals"

set prev_stem ""
for {set i 0} {$i < $nfacs } {incr i} {
    set facs [list]
    set facname [ gtkwave::getFacName $i ]
    lappend facs "$facname"
    puts $facname
    regexp {(\w+(\.\w+(\[\d+\])?)*)+\.} $facname stem
    if {[info exists stem]} {
        if {$stem != $prev_stem} {
            puts "New stem: $stem"
            set prev_stem $stem
            gtkwave::addCommentTracesFromList $stem
        }
    }
    gtkwave::addSignalsFromList $facs
}
# set num_added [ gtkwave::addSignalsFromList $all_facs ]
puts "num signals added: $num_added"

# zoom full
gtkwave::/Time/Zoom/Zoom_Full
