`include "vatch/main.v"
`include "verilog/divider.sv"

`define TRUNCATE_REAL(REAL, BITS) \
    (real'(longint'(REAL * (2.0**BITS))) / (2.0**BITS))
    // $itor($rtoi(REAL * (2**BITS))) / (2**BITS))


`define TEST_CASE_LIMITED(IDX, LHS, RHS, LIMIT) \
    assign lhs_values[IDX] = LHS; \
    assign rhs_values[IDX] = RHS; \
    assign out_values[IDX] = `TRUNCATE_REAL(LHS, LHS_FRAC_BITS) / `TRUNCATE_REAL(RHS, RHS_FRAC_BITS); \
    assign min_rhs_fp_for_test[IDX] = LIMIT; \

`define TEST_CASE(IDX, LHS, RHS) \
    assign lhs_values[IDX] = LHS; \
    assign rhs_values[IDX] = RHS; \
    assign out_values[IDX] = `TRUNCATE_REAL(LHS, LHS_FRAC_BITS) / `TRUNCATE_REAL(RHS, RHS_FRAC_BITS); \
    assign min_rhs_fp_for_test[IDX] = 0;


module divider_tb
    #(parameter LHS_FRAC_BITS=0
    , parameter RHS_FRAC_BITS=0
    , parameter OUT_FRAC_BITS=0
    ) ();
    `SETUP_TEST
    localparam LHS_INT_BITS = 8;
    localparam RHS_INT_BITS = 8;
    localparam OUT_INT_BITS = 8;
    localparam LHS_TOTAL_BITS = LHS_FRAC_BITS + LHS_INT_BITS;
    localparam RHS_TOTAL_BITS = RHS_FRAC_BITS + RHS_INT_BITS;

    localparam SHIFT_AMOUNT = OUT_FRAC_BITS + RHS_FRAC_BITS - LHS_FRAC_BITS;
    localparam NUM_STAGES = LHS_INT_BITS + LHS_FRAC_BITS + SHIFT_AMOUNT + 2;

    localparam OUT_TOTAL_BITS = OUT_FRAC_BITS + OUT_INT_BITS;

    reg clk;
    wire signed [OUT_TOTAL_BITS:0] out;
    reg[LHS_TOTAL_BITS:0] lhs;
    reg[RHS_TOTAL_BITS:0] rhs;

    initial begin
        clk = 0;
        $dumpfile(`VCD_OUTPUT);
        $dumpvars(0, divider_tb);

        #1;

        forever begin
            clk <= ~clk;
            #1;
        end
    end

    localparam TEST_CASES = 12;
    real lhs_values[TEST_CASES-1:0];
    real rhs_values[TEST_CASES-1:0];
    real out_values[TEST_CASES-1:0];
    integer min_rhs_fp_for_test[TEST_CASES-1:0];

    `TEST_CASE(0, 1,   2)
    `TEST_CASE(1, 2,   4)
    `TEST_CASE(2, 1.5, 1.5)
    `TEST_CASE(3, 0,   1)
    `TEST_CASE(4, 1,   3)
    `TEST_CASE(5, 10,  2)
    `TEST_CASE(6, 25,  6)
    `TEST_CASE(7, 0.5, 2)

    `TEST_CASE(8,  2,  -2)
    `TEST_CASE(9,  -2, 2)
    `TEST_CASE(10, -2, -2)
    `TEST_CASE_LIMITED(11, 28.3560214266181, 0.3299999999871943, 20)


    // assign lhs_values = '{1,   4, 0.5, 0, 1,         10};
    // assign rhs_values = '{2,   2, 0.5, 1, 3,         2};
    // assign out_values = '{0.5, 2, 1,   0, 0.3333333, 5};

    initial begin
        integer i;
        for(i = 0; i < TEST_CASES; i = i + 1) begin
            lhs <= longint'(lhs_values[i] * longint'(2)**(LHS_FRAC_BITS));
            rhs <= longint'(rhs_values[i] * longint'(2)**(RHS_FRAC_BITS));

            @(negedge clk);
        end
    end

    integer test_case;

    // Check output
    initial begin
        integer chk_idx;
        longint expected;

        test_case = 0;
        repeat(NUM_STAGES) @(negedge clk);

        for(chk_idx = 0; chk_idx < TEST_CASES; chk_idx = chk_idx + 1) begin
            if (RHS_FRAC_BITS >= min_rhs_fp_for_test[chk_idx]) begin
                expected = longint'($floor(out_values[chk_idx] * real'(2)**(OUT_FRAC_BITS)));
                if (out !== expected) begin
                    $display("%f / %f = %f", lhs_values[chk_idx], rhs_values[chk_idx], out_values[chk_idx]);
                end
                `ASSERT_EQ(out, expected);
            end else begin
                $display("\033[0;36mSkipping \033[0m%0d", chk_idx);
            end
            @(negedge clk);
            test_case = chk_idx;
        end

        #10

        `END_TEST
    end

    divider
        #(.LHS_INT_BITS(LHS_INT_BITS)
        , .LHS_FRAC_BITS(LHS_FRAC_BITS)
        , .RHS_INT_BITS(RHS_INT_BITS)
        , .RHS_FRAC_BITS(RHS_FRAC_BITS)
        , .OUT_INT_BITS(OUT_INT_BITS)
        , .OUT_FRAC_BITS(OUT_FRAC_BITS)
        )
        uniform
        (.clk(clk), .lhs_signed(lhs), .rhs_signed(rhs), .out_signed(out));
endmodule
