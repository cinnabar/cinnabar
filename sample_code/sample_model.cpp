#include "../src/node.hpp"
#include "../src/codegen.hpp"

#include <iostream>



std::map<std::string, Rc<FracNode>> sample_fn(
    Rc<Input<-7, 7>> input_ax0,
    Rc<Input<-7, 7>> input_ax1,
    Rc<Input<0, 7>> input_ax2,
    Rc<Input<0, 7>> state_0,
    Rc<Input<0, 7>> state_1
) {
    auto out_state_1 = state_0 + state_1;
    return {
        {"out_state_0", state_0 + input_ax0 + input_ax2},
        {"out_state_1", state_1 + input_ax1},
        {"delayed_input_0", input_ax0},
        {"delayed_input_1", input_ax1},
        {
            "cost",
                Abs::create(input_ax0)
                    + Abs::create(input_ax1)
                    + Abs::create(input_ax2)
                    + Constant::create("one", 1)
        },
    };
}

void run_function(std::vector<std::pair<std::string, Rc<FracNode>>> outputs) {
    for(auto output : outputs) {
        std::cout << output.first << " = " << output.second->calculate_value() << std::endl;
    }
}

int main() {
    auto result = sample_fn(
        Input<-7, 7>::create("input_ax0", 1.5),
        Input<-7, 7>::create("input_ax1", -1),
        Input<0, 7>::create("input_ax2", 0),
        Input<0, 7>::create("state_0", 0),
        Input<0, 7>::create("state_1", 1)
    );

#ifndef RUN_CODE
    std::map<std::string, Rc<Node>> result_raw_nodes;
    for(auto node : result) {
        result_raw_nodes.insert(std::make_pair(node.first, node.second));
    }
    Codegen codegen(result_raw_nodes);
    std::cout << join(codegen.generate_module("sample_model", FixedFixpoint{12}));
#else
    run_function(result);
#endif
}

