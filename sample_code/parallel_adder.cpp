#include "../src/node.hpp"
#include "../src/codegen.hpp"

#include <iostream>



Rc<Node> sample_fn(Rc<Input<0,100>> a, Rc<Input<0,50>> b) {
    auto c = Constant::create("c", 5);
    auto lhs = a + c;
    auto rhs = b - c;
    return lhs * rhs;
}

int main() {
    auto result = sample_fn(Input<0, 100>::create("a", 0), Input<0, 50>::create("b", 0));

#ifndef RUN_CODE
    Codegen codegen({std::make_pair("result", result)});
    std::cout << join(codegen.generate_module("parallel_test", FixedFixpoint{12}));
#endif
}

