#include <catch2/catch.hpp>

#include "util.hpp"

#include "../src/node.hpp"
#include "../src/depth_opt.hpp"

using namespace depth_opt;

TEST_CASE("depth optimisation generates correct variables") {
    /*
      Generate this graph:
            d
           / \
          |   c
          | / |
          e | b (computation time = 5
           \|/
            a

      Where it is more efficient to compute `e` at the same time as `c` since
      a has to be stored for that long anyway.
    */
    auto a = Variable::create("a", 0, 10, 0);
    auto b = Delay::create(a, 5);
    b->override_name("b");
    auto c = DummyNode::create({a, b});
    c->override_name("c");
    auto e = -a;
    e->override_name("e");
    auto d = DummyNode({e, c});
    d.override_name("d");

    auto expected = std::vector<std::string> {
        // Latencies
        "var l_a integer = 1;",
        "var l_b integer = 5;",
        "var l_c integer = 1;",
        "var l_e integer = 1;",
        "var l_d integer = 1;",
        // Depths
        "var d_a integer;",
        "var d_b integer;",
        "var d_c integer;",
        "var d_d integer;",
        "var d_e integer;",

        // Edge delays
        "var y_a_b integer;",
        "var y_b_c integer;",
        "var y_a_c integer;",
        "var y_a_e integer;",
        "var y_e_d integer;",
        "var y_c_d integer;",

        // Amount of registers

       "var regs_a integer;",
       "var regs_b integer;",
       "var regs_c integer;",
       "var regs_d integer;",
       "var regs_e integer;",

        // Nodes can not start before input is available
        "s.t. depth_a_0: d_a = l_a;",
        "s.t. depth_b_0: d_b = d_a + y_a_b + l_b;",
        "s.t. depth_c_0: d_c = d_a + y_a_c + l_c;",
        "s.t. depth_c_1: d_c = d_b + y_b_c + l_c;",
        "s.t. depth_e_0: d_e = d_a + y_a_e + l_e;",
        "s.t. depth_d_0: d_d = d_e + y_e_d + l_d;",
        "s.t. depth_d_1: d_d = d_c + y_c_d + l_d;",
        // The amount of registers after a node is the max of the
        // delay to its parents
        "s.t. regs_a_max_b: regs_a >= y_a_b;",
        "s.t. regs_a_max_c: regs_a >= y_a_c;",
        "s.t. regs_a_max_e: regs_a >= y_a_e;",
        "s.t. regs_b_max_c: regs_b >= y_b_c;",
        "s.t. regs_c_max_d: regs_c >= y_c_d;",
        "s.t. regs_e_max_d: regs_e >= y_e_d;",
        // No negative total registers. These are tecnically only required
        // if there are no other constraits, but adding them for all nodes
        // is easier
        "s.t. regs_a_max_0: regs_a >= 0;",
        "s.t. regs_b_max_0: regs_b >= 0;",
        "s.t. regs_c_max_0: regs_c >= 0;",
        "s.t. regs_d_max_0: regs_d >= 0;",
        "s.t. regs_e_max_0: regs_e >= 0;",
        // Edges can not have negative delay
        "s.t. positive_a_b: y_a_b >= 0;",
        "s.t. positive_b_c: y_b_c >= 0;",
        "s.t. positive_a_c: y_a_c >= 0;",
        "s.t. positive_a_e: y_a_e >= 0;",
        "s.t. positive_e_d: y_e_d >= 0;",
        "s.t. positive_c_d: y_c_d >= 0;",
        // Both inputs to a node must be available at the same time
        "s.t. c_inputs_same_cycle_1: d_b + y_b_c = d_a + y_a_c;",
        "s.t. d_inputs_same_cycle_1: d_c + y_c_d = d_e + y_e_d;",

        "minimize obj:",
        // To avoid having to keep track of wether or not to add a +
        "0",
        "+ regs_a * 13",
        "+ regs_b * 13",
        // These two are dummy nodes and as such, have no size
        "+ regs_c * 0",
        "+ regs_d * 0",
        "+ regs_e * 13",
        ";",
        "solve;",
        "display",
        // To avoid having to keep track of wether or not to add a ,
        "\"result\"",
        ", \"start_a\", (d_a - l_a)",
        ", \"start_b\", (d_b - l_b)",
        ", \"start_c\", (d_c - l_c)",
        ", \"start_d\", (d_d - l_d)",
        ", \"start_e\", (d_e - l_e)",
        ";",
        "end;",
    };

    require_same_lines(expected, generate_gmpl(&d, FixedTotalLength{12}));
}

TEST_CASE("Depth opt does not generate duplicate constraint names") {
    auto a = Variable::create("a", 0, 10, 0);
    auto b = Variable::create("b", 0, 10, 0);
    auto c = Variable::create("c", 0, 10, 0);
    auto d = DummyNode::create({a, b, c});
    d->override_name("d");

    auto expected = std::vector<std::string> {
        // Nodes can not start before input is available
        "s.t. depth_a_0: d_a = l_a;",
        "s.t. depth_b_0: d_b = l_b;",
        "s.t. depth_c_0: d_c = l_c;",
        "s.t. depth_d_0: d_d = d_a + y_a_d + l_d;",
        "s.t. depth_d_1: d_d = d_b + y_b_d + l_d;",
        "s.t. depth_d_2: d_d = d_c + y_c_d + l_d;",
        // Edges can not have negative delay
        "s.t. positive_a_d: y_a_d >= 0;",
        "s.t. positive_b_d: y_b_d >= 0;",
        "s.t. positive_c_d: y_c_d >= 0;",
        // Both inputs to a node must be available at the same time
        "s.t. d_inputs_same_cycle_1: d_b + y_b_d = d_a + y_a_d;",
        "s.t. d_inputs_same_cycle_2: d_c + y_c_d = d_b + y_b_d;",
        // Total register count
        "s.t. regs_a_max_d: regs_a >= y_a_d;",
        "s.t. regs_b_max_d: regs_b >= y_b_d;",
        "s.t. regs_c_max_d: regs_c >= y_c_d;",
        "s.t. regs_a_max_0: regs_a >= 0;",
        "s.t. regs_b_max_0: regs_b >= 0;",
        "s.t. regs_c_max_0: regs_c >= 0;",
        "s.t. regs_d_max_0: regs_d >= 0;",
    };

    require_same_lines(expected, generate_constraints(&*d));
}


TEST_CASE("Duplicate constraints and variables are not generated with duplicated parents") {
    info("Depth opt test temporarily disabled");
    // auto a = Variable::create("a", 0, 10, 5);
    // auto b = a * a;
    // b->override_name("b");

    // auto expected = std::vector<std::string> {
    //     "var l_a integer = 1;",
    //     "var l_b integer = 1;",
    //     "var d_a integer;",
    //     "var d_b integer;",
    //     "var regs_a integer;",
    //     "var regs_b integer;",

    //     "var y_a_b integer;",

    //     "s.t. regs_a_max_b: regs_a >= y_a_b;",
    //     "s.t. regs_a_max_0: regs_a >= 0;",
    //     "s.t. regs_b_max_0: regs_b >= 0;",
    //     "s.t. depth_a_0: d_a = l_a;",
    //     "s.t. depth_b_0: d_b = d_a + y_a_b + l_b;",
    //     "s.t. positive_a_b: y_a_b >= 0;",

    //     "minimize obj:",
    //     "0",
    //     "+ regs_a * 13",
    //     "+ regs_b * 13",
    //     ";",
    //     "solve;",
    //     "display",
    //     "\"result\"",
    //     ", \"start_a\", (d_a - l_a)",
    //     ", \"start_b\", (d_b - l_b)",
    //     ";",
    //     "end;",
    // };

    // require_same_lines(expected, generate_gmpl(&*b, FixedTotalLength{12}));
}

TEST_CASE("Names longer than gmpl can handle do not occur") {
    auto a = Variable::create("a", 0, 10, 5);
    a->override_name("thisnameis31characterslong______");

    INFO(a->base_name());
    REQUIRE(a->base_name()[0] == 'h');
    REQUIRE(a->base_name()[1] == '_');
}


TEST_CASE("Constants are not included in depth optimisation cost") {
    info("Depth opt test temporarily disabled");
    // auto a = Variable::create("a", 0, 10, 5);
    // auto c = Constant::create("c", 10);
    // auto b = a * c;
    // b->override_name("b");

    // auto expected = std::vector<std::string> {
    //     "var l_a integer = 1;",
    //     "var l_b integer = 1;",
    //     "var d_a integer;",
    //     "var d_b integer;",
    //     "var regs_a integer;",
    //     "var regs_b integer;",

    //     "var y_a_b integer;",

    //     "s.t. regs_a_max_b: regs_a >= y_a_b;",
    //     "s.t. regs_a_max_0: regs_a >= 0;",
    //     "s.t. regs_b_max_0: regs_b >= 0;",
    //     "s.t. depth_a_0: d_a = l_a;",
    //     "s.t. depth_b_0: d_b = d_a + y_a_b + l_b;",
    //     "s.t. positive_a_b: y_a_b >= 0;",

    //     "minimize obj:",
    //     "0",
    //     "+ regs_a * 13",
    //     "+ regs_b * 13",
    //     ";",
    //     "solve;",
    //     "display",
    //     "\"result\"",
    //     ", \"start_a\", (d_a - l_a)",
    //     ", \"start_b\", (d_b - l_b)",
    //     ";",
    //     "end;",
    // };

    // require_same_lines(expected, generate_gmpl(&*b, FixedTotalLength{12}));
}
