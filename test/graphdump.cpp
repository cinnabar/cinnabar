#include <catch2/catch.hpp>

#include "../src/node.hpp"
#include "../src/graph.hpp"
#include "util.hpp"

TEST_CASE( "Graph generation works", "[graph]" ) {
    WordLength wl = FixedFixpoint{12};
    auto a = Input::create("a", 0, 100, 5);
    auto b = Variable::create("b", 0, 100, 10);
    auto c = Constant::create("c", 15);

    auto result = a + b + c;
    result->override_name("result");

    std::vector<std::string> expected = {
        "add_a_b [label=\"add_a_b\\n+\\n15.000000\", fillcolor=white, style=filled];",
        "add_a_b -> {a,b};",
        "b [label=\"b\\nvar b\\n10.000000\", fillcolor=white, style=filled];",
        "b -> {};",
        "a [label=\"a\\nin a\\n5.000000\", fillcolor=white, style=filled];",
        "a -> {};",
        "result [label=\"result\\n+\\n30.000000\", fillcolor=white, style=filled];",
        "result -> {add_a_b,c};",
        "c [label=\"c\\nconst 15.000000\\n15.000000\", fillcolor=burlywood1, style=filled];",
        "c -> {};",
    };

    auto graph = graph::generate_graph(result, [](auto node){return node->display_value();}, wl, false);
    REQUIRE(graph.size() == expected.size());

    require_same_lines(expected, graph);
}


TEST_CASE( "Graph label names work", "[graph]") {
    auto a = Constant::create("a", 5);
    auto b = Variable::create("b", 0, 100, 5);
    auto in = Input::create("c", 0, 100, 5);
    REQUIRE(a->graph_label_name() == "const 5.000000");
    REQUIRE(b->graph_label_name() == "var b");
    REQUIRE(in->graph_label_name() == "in c");
}

#define BINOP_GRAPH_LABEL_TEST(op, expected) TEST_CASE( "Graph labels work for " expected, "[graph]") { \
    auto a = Constant::create("a", 5); \
    auto b = Constant::create("b", 5); \
    REQUIRE((a op b)->graph_label_name() == expected); \
}

BINOP_GRAPH_LABEL_TEST(+, "+");
BINOP_GRAPH_LABEL_TEST(-, "-");
BINOP_GRAPH_LABEL_TEST(*, "*");
BINOP_GRAPH_LABEL_TEST(/, "/");

