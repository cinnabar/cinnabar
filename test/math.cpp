#include <catch2/catch.hpp>

// NOTE: Tese tests are disabled until this functionality is re-implemented
// using yafp.
/*


#include "../src/util.hpp"

#include <cmath>
#include "../MFixedPoint/include/MFixedPoint/FpF.hpp"

#include <iostream>

TEST_CASE( "Log approximation works for 1", "[log]") {
    ExecutionType input = 1;

    REQUIRE(approximate_log2(input) == 0);
}
TEST_CASE( "Log approximation works for 2", "[log]") {
    ExecutionType input = 2;
    REQUIRE(approximate_log2(input) == 1);
}
TEST_CASE( "Log approximation works for 3", "[log]") {
    ExecutionType input = 3;

    auto diff = approximate_log2(input).ToDouble() - log2(input.ToDouble());
    REQUIRE(fabs(diff) < (1./1000.));
}
TEST_CASE( "Log approximation works for 0.5", "[log]") {
    ExecutionType input = 0.5;

    auto diff = approximate_log2(input).ToDouble() - log2(input.ToDouble());
    REQUIRE(fabs(diff) < (1./1000.));
}

TEST_CASE( "Pow2 works for 0", "[pow2]") {
    ExecutionType input = 0;

    REQUIRE(approximate_pow2(input) == 1);
}
TEST_CASE( "Pow2 works for 1", "[pow2]") {
    ExecutionType input = 1;

    REQUIRE(approximate_pow2(input) == 2);
}
TEST_CASE( "Pow2 works for 1.5", "[pow2]") {
    ExecutionType input = 1.5;

    auto diff = approximate_pow2(input).ToDouble() - pow(2, input.ToDouble());
    REQUIRE(fabs(diff) < (1./1000.));
}
TEST_CASE( "Pow2 works for 0.5", "[pow2]") {
    ExecutionType input = 0.5;

    auto diff = approximate_pow2(input).ToDouble() - pow(2, input.ToDouble());
    REQUIRE(fabs(diff) < (1./1000.));
}
*/
