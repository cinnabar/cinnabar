#include <catch2/catch.hpp>

#include "../src/node.hpp"
#include "../src/lookup_table.hpp"
#include "../src/interpolation.hpp"
#include "util.hpp"

TEST_CASE("Node comparison works") {
    auto a = Constant::create("a", 10);
    auto b = Constant::create("b", 5);
    auto result = a + b;

    auto other_result = a - a;

    std::string expected = "Incorrect type, expected: Add, got: Sub" ;

    REQUIRE(result->compare(other_result.weak()).value() == expected);

    auto self_comparison = result->compare(result.weak());
    INFO(self_comparison.value_or(""));
    REQUIRE(self_comparison.has_value() == false);
}

TEST_CASE("Constants with varying values are detected") {
    auto a = Constant::create("a", 10);
    auto b = Constant::create("b", 5);

    auto comparison = a->compare(b.weak());

    REQUIRE(comparison.value() == "Incorrect value. Expected: 10.000000, got: 5.000000");
}

TEST_CASE("Bool comparisons work") {
    auto a = Constant::create("a", 5);
    auto b = Constant::create("b", 6);
    auto result = a < b;

    auto comparison = result->compare(BoolConst::False.weak());
    REQUIRE(comparison.value_or("") != "");
}


TEST_CASE("Division by x/c is optimized to multiplication if c is const") {
    auto x = Variable::create("x", 0, 10, 4);
    auto c = Constant::create("c", 4);

    Rc<Node> result = x/c;
    result->override_name("optimized");

    auto expected = x * Constant::create("c_replaced", 1/4.);

    result = result->optimize(OptPass::ConstantDivisionReplacement).value();
    REQUIRE(result->compare(expected.weak()).value_or("") == "");
    REQUIRE(result->base_name() == "optimized");
}

#define CONSTANT_FOLDING_TEST(NAME, EXPECTED, ...) \
    TEST_CASE("Constant folding works for " #NAME) { \
        auto result = __VA_ARGS__; \
        result->override_name("binop"); \
        auto expected = EXPECTED; \
        auto optimized = result->optimize(OptPass::ConstantFold).value(); \
        REQUIRE(expected->compare(optimized.weak()).value_or("") == ""); \
        REQUIRE(result->base_name() == "binop"); \
    }

#define _CONSTANT_FOLDING_TEST(NODE, EXPECTED, ...) \
    CONSTANT_FOLDING_TEST( \
        NODE, \
        (Constant::create("expected", EXPECTED)), \
        __VA_ARGS__ \
    )

_CONSTANT_FOLDING_TEST(Add, 15, Constant::create("x", 10) + Constant::create("y", 5));
_CONSTANT_FOLDING_TEST(Sub, 5, Constant::create("x", 10) - Constant::create("y", 5));
_CONSTANT_FOLDING_TEST(Mul, 50, Constant::create("x", 10) * Constant::create("y", 5));
_CONSTANT_FOLDING_TEST(Div, 2, Constant::create("x", 10) / Constant::create("y", 5));

_CONSTANT_FOLDING_TEST(Abs, 10, Abs::create(Constant::create("x", -10)));
_CONSTANT_FOLDING_TEST(Sqrt, 4, Sqrt::create(Constant::create("x", 16)));
_CONSTANT_FOLDING_TEST(Truncate, 4, Truncate::create(Constant::create("x", 4.5)));
_CONSTANT_FOLDING_TEST(USub, 4, -Constant::create("x", -4.0));
CONSTANT_FOLDING_TEST(Not, BoolConst::False, Not::create(BoolConst::True));

_CONSTANT_FOLDING_TEST(
    If,
    4,
    _if(BoolConst::False, -Constant::create("x", -4.0), Constant::create("x", 4))
);
CONSTANT_FOLDING_TEST(
    "If boolean",
    BoolConst::True,
    _if(BoolConst::True, BoolConst::True, BoolConst::False)
);


// Ensure that optimisations are applied recursively to binary operators
// LHS or RHS is set to a constant-foldable value while the other is set
// to a constant. Checks if the constant-folding occured
#define RECURSIVE_OPT_TEST_BINOP(NODE, OP) \
    TEST_CASE("Recursive opt works for LHS of " #NODE) { \
        auto foldable = -Constant::create("lhs", -1); \
        auto var = Variable::create("var", 1, 10, 1); \
        auto result = foldable OP var; \
        auto expected = Constant::create("l", 1) OP var; \
        result->optimize(OptPass::ConstantFold); \
        REQUIRE(expected->compare(result.weak()).value_or("") == ""); \
    } \
    TEST_CASE("Recursive opt works for RHS of " #NODE) { \
        auto foldable = -Constant::create("lhs", -1); \
        auto var = Variable::create("var", 1, 10, 1); \
        auto result = var OP foldable; \
        auto expected = var OP Constant::create("l", 1); \
        result->optimize(OptPass::ConstantFold); \
        REQUIRE(expected->compare(result.weak()).value_or("") == ""); \
    }


RECURSIVE_OPT_TEST_BINOP(Add, +);
RECURSIVE_OPT_TEST_BINOP(Sub, -);
RECURSIVE_OPT_TEST_BINOP(Mul, *);
RECURSIVE_OPT_TEST_BINOP(Div, /);

RECURSIVE_OPT_TEST_BINOP(LessThan, <);
RECURSIVE_OPT_TEST_BINOP(GreaterThan, >);
RECURSIVE_OPT_TEST_BINOP(LessThanOrEq, <=);
RECURSIVE_OPT_TEST_BINOP(GreaterThanOrEq, >);


#define RECURSIVE_UNARY_OP_TEST(NODE) \
    TEST_CASE("Recursive opt works for " #NODE) { \
        auto foldable = -Constant::create("lhs", -1); \
        auto result = NODE::create(foldable); \
        auto expected = NODE::create(Constant::create("l", 1)); \
        result->optimize(OptPass::ConstantFold); \
        REQUIRE(expected->compare(result.weak()).value_or("") == ""); \
    }

RECURSIVE_UNARY_OP_TEST(Abs);
RECURSIVE_UNARY_OP_TEST(Sqrt);
RECURSIVE_UNARY_OP_TEST(Truncate);
RECURSIVE_UNARY_OP_TEST(USub);

#define BINARY_BOOL_FOLD_TEST(ID, OP, LHS, RHS, EXPECTED) \
    TEST_CASE("Constant folding works for "#OP" "#ID) { \
        auto lhs = Constant::create("lhs", LHS); \
        auto rhs = Constant::create("rhs", RHS); \
        auto expected = BoolConst::EXPECTED; \
        auto result = (lhs OP rhs)->optimize(OptPass::ConstantFold).value(); \
        REQUIRE_SAME_NODE(expected, result); \
    }

BINARY_BOOL_FOLD_TEST("0", <, 5, 4, False);
BINARY_BOOL_FOLD_TEST("1", <, 5, 5, False);
BINARY_BOOL_FOLD_TEST("2", <, 4, 5, True);

BINARY_BOOL_FOLD_TEST("0", >, 5, 4, True);
BINARY_BOOL_FOLD_TEST("1", >, 5, 5, False);
BINARY_BOOL_FOLD_TEST("2", >, 4, 5, False);

BINARY_BOOL_FOLD_TEST("0", >=, 5, 4, True);
BINARY_BOOL_FOLD_TEST("1", >=, 5, 5, True);
BINARY_BOOL_FOLD_TEST("2", >=, 4, 5, False);

BINARY_BOOL_FOLD_TEST("0", <=, 5, 4, False);
BINARY_BOOL_FOLD_TEST("1", <=, 5, 5, True);
BINARY_BOOL_FOLD_TEST("2", <=, 4, 5, True);

#define BINARY_BOOL_OPT_TEST(NAME, ID, PASS, OP, LHS, RHS, EXPECTED) \
    TEST_CASE(#NAME " for "#OP" "#ID) { \
        auto expected = EXPECTED; \
        auto result = (LHS OP RHS)->optimize(PASS).value(); \
        REQUIRE_SAME_NODE(expected, result); \
    }\

#define BINARY_BOOL_FOLD_TEST_(ID, OP, LHS, RHS, EXPECTED) \
    BINARY_BOOL_OPT_TEST( \
        "Constant folding works", \
        ID, \
        OptPass::ConstantFold, \
        OP, \
        LHS, \
        RHS, \
        EXPECTED \
    )

BINARY_BOOL_FOLD_TEST_("0", ||, BoolConst::False, BoolConst::False, BoolConst::False);
BINARY_BOOL_FOLD_TEST_("1", ||, BoolConst::False, BoolConst::True,  BoolConst::True);
BINARY_BOOL_FOLD_TEST_("2", ||, BoolConst::True,  BoolConst::False, BoolConst::True);
BINARY_BOOL_FOLD_TEST_("3", ||, BoolConst::True,  BoolConst::True,  BoolConst::True);

#define REDUNDANT_BOOL_REMOVAL_TEST(ID, OP, LHS, RHS, EXPECTED) \
    BINARY_BOOL_OPT_TEST( \
        "Bool constant simplification works", \
        ID, \
        OptPass::RemoveRedundantBoolComputations, \
        OP, \
        LHS, \
        RHS, \
        EXPECTED \
    )

#define NON_CONST_BOOL() (Variable::create("a", -1, 1, 0) < Constant::create("a", 0))
REDUNDANT_BOOL_REMOVAL_TEST("0", ||, NON_CONST_BOOL(), BoolConst::True, BoolConst::True)
REDUNDANT_BOOL_REMOVAL_TEST("1", ||, BoolConst::True, NON_CONST_BOOL(), BoolConst::True)
REDUNDANT_BOOL_REMOVAL_TEST("2", ||, BoolConst::False, NON_CONST_BOOL(), NON_CONST_BOOL())
REDUNDANT_BOOL_REMOVAL_TEST("3", ||, NON_CONST_BOOL(), BoolConst::False, NON_CONST_BOOL())

REDUNDANT_BOOL_REMOVAL_TEST("5", &&, NON_CONST_BOOL(), BoolConst::True, NON_CONST_BOOL())
REDUNDANT_BOOL_REMOVAL_TEST("6", &&, BoolConst::True, NON_CONST_BOOL(), NON_CONST_BOOL())
REDUNDANT_BOOL_REMOVAL_TEST("7", &&, BoolConst::False, NON_CONST_BOOL(), BoolConst::False)
REDUNDANT_BOOL_REMOVAL_TEST("8", &&, NON_CONST_BOOL(), BoolConst::False, BoolConst::False)
#undef NON_CONST_BOOL

TEST_CASE("Optimisations are done recursively through or operator") {
    auto a = Constant::create("a", 1);
    auto b = Constant::create("b", 2);
    auto c = Variable::create("c", 0, 5, 4);

    auto result = a+b < c || BoolConst::False;
    auto expected = Constant::create("add_a_b", 3) < c || BoolConst::False;

    REQUIRE_FALSE(result->optimize(OptPass::ConstantFold).has_value());
    REQUIRE(expected->compare(result.weak()).value_or("") == "");
}
TEST_CASE("Optimisations are done recursively through or operator for Not nodes") {
    auto a = Variable::create("a", 1, 10, 5);
    auto b = Variable::create("b", 1, 10, 5);
    auto c = Not::create(BoolConst::True);

    auto result = c || a<b;
    auto expected = BoolConst::False || a < b ;

    REQUIRE_FALSE(result->optimize(OptPass::ConstantFold).has_value());
    REQUIRE(expected->compare(result.weak()).value_or("") == "");
}


TEST_CASE("Dummy node optimizes children") {
    auto x = Constant::create("x", 1);
    auto y = Constant::create("y", 2);

    auto dummy = DummyNode({x+y, x-y});
    REQUIRE(!dummy.optimize(OptPass::ConstantFold).has_value());

    Rc<DummyNode> expected(new DummyNode({
        Constant::create("add_x_y", 3),
        Constant::create("sub_x_y", -1)
    }));

    REQUIRE(dummy.compare(expected.weak()).value_or("") == "");
}

TEST_CASE("Select node optimizes children") {
    auto x = Constant::create("x", 1);
    auto y = Constant::create("y", 2);
    auto z = Variable::create("z", 0, 10, 5);

    auto dummy = SelectFrac::create({{x+y, {x < y}}, {x-y, {x < z}}});
    REQUIRE(!dummy->optimize(OptPass::ConstantFold).has_value());

    auto expected = SelectFrac::create({
        {Constant::create("add_x_y", 3), {BoolConst::True}},
        {Constant::create("sub_x_y", -1), {x < z}}
    });

    REQUIRE(dummy->compare(expected.weak()).value_or("") == "");
}


TEST_CASE("If statements optimize children") {
    auto a = Constant::create("a", 0);
    auto b = Constant::create("b", 1);
    auto c = Constant::create("c", 2);
    auto d = Constant::create("d", 3);
    auto x = Variable::create("x", 0, 5, 3);

    auto comparison = x < b+c;
    auto result = _if(comparison, c+d, a+c);

    auto apb = Constant::create("add_a_b", 1);
    auto bpc = Constant::create("add_b_c", 3);
    auto cpd = Constant::create("add_c_d", 5);
    auto apc = Constant::create("add_a_c", 2);

    auto expected = _if(x < bpc, cpd, apc);

    REQUIRE_FALSE(result->optimize(OptPass::ConstantFold).has_value());
    REQUIRE(expected->compare(result.weak()).value_or("") == "");
}
TEST_CASE("If statements constant fold") {
    auto a = Constant::create("a", 0);
    auto b = Constant::create("b", 1);
    auto c = Constant::create("c", 2);
    auto d = Constant::create("d", 3);

    auto comparison = a+b < b+c;
    auto result = _if(comparison, c+d, a+c);

    auto apb = Constant::create("add_a_b", 1);
    auto bpc = Constant::create("add_b_c", 3);
    auto cpd = Constant::create("add_c_d", 5);
    auto apc = Constant::create("add_a_c", 2);

    REQUIRE_SAME_NODE(result->optimize(OptPass::ConstantFold).value(), cpd);
}


TEST_CASE("Lookup tables perform optimizations") {
    auto a = Constant::create("a", 1);
    auto b = Constant::create("b", 1);
    auto c = Variable::create("b", 0, 1, 2);

    auto lut = LookupTable::create("test", {4,5,6});

    auto result = LutResult::create(lut, (a+b)+c);
    
    auto apb = Constant::create("add_a_b", 2);
    auto expected = LutResult::create(lut, apb+c);

    REQUIRE_FALSE(result->optimize(OptPass::ConstantFold).has_value());
    REQUIRE(expected->compare(result.weak()).value_or("") == "");
}

TEST_CASE("Not operator performs optimizations") {
    auto a = Constant::create("a", 1);
    auto b = Constant::create("b", 2);
    auto c = Variable::create("c", 0, 5, 4);

    auto result = !(a+b < c || BoolConst::False);
    auto expected = !(Constant::create("add_a_b", 3) < c || BoolConst::False);

    REQUIRE_FALSE(result->optimize(OptPass::ConstantFold).has_value());
    REQUIRE(expected->compare(result.weak()).value_or("") == "");
}


TEST_CASE("Redundant comparisons with constants are removed") {
    auto a = Variable::create("a", 0, 10, 5);
    auto b = Constant::create("b", -5);

    Rc<Node> result = a > b;

    result = result->optimize(OptPass::RemoveRedundantComparisons).value();

    auto expected = BoolConst::True;
    REQUIRE(expected->compare(result.weak()).value_or("") == "");
}
TEST_CASE("Redundant comparisons with variables are removed") {
    auto a = Variable::create("a", 0, 10, 5);
    auto b = Variable::create("b", -20, -10, -5);

    Rc<Node> result = a < b;

    result = result->optimize(OptPass::RemoveRedundantComparisons).value();

    auto expected = BoolConst::False;
    REQUIRE(expected->compare(result.weak()).value_or("") == "");
}
TEST_CASE("Redundant greater than opt works correctly") {
    auto small = Variable::create("small", -5, 2, 0);
    auto medium = Variable::create("medium", 0, 10, 5);
    auto large = Variable::create("large", 7, 15, 10);

    auto m_s = medium > small;
    auto l_s = large > small;
    auto l_m = large > medium;

    auto m_s_opt = m_s->optimize(OptPass::RemoveRedundantComparisons);
    auto l_s_opt = l_s->optimize(OptPass::RemoveRedundantComparisons).value();
    auto l_m_opt = l_m->optimize(OptPass::RemoveRedundantComparisons);

    REQUIRE_SAME_NODE(BoolConst::True, l_s_opt);
    REQUIRE_FALSE(m_s_opt.has_value());
    REQUIRE_FALSE(l_m_opt.has_value());

    auto m_l = medium > large;
    auto m_l_opt = m_l->optimize(OptPass::RemoveRedundantComparisons);
    REQUIRE_FALSE(m_l_opt.has_value());
}
TEST_CASE("Redundant comparisons with greater_than_or_eq are removed") {
    auto a = Variable::create("a", 0, 10, 5);
    auto b = Variable::create("b", -5, 0, -5);

    Rc<Node> result = a >= b;

    result = result->optimize(OptPass::RemoveRedundantComparisons).value();

    auto expected = BoolConst::True;
    REQUIRE(expected->compare(result.weak()).value_or("") == "");
}
TEST_CASE("Redundant comparisons with greater_than_or_eq are not removed incorrectly") {
    auto a = Variable::create("a", 0.3, 0.8, 0.3);
    auto b = Constant::create("b", 1);

    Rc<Node> result = a >= b;

    result = result->optimize(OptPass::RemoveRedundantComparisons).value();

    auto expected = BoolConst::False;
    REQUIRE(expected->compare(result.weak()).value_or("") == "");
}
TEST_CASE("Redundant comparisons with less_than_or_eq are removed") {
    auto a = Variable::create("a", 0, 10, 5);
    auto b = Variable::create("b", -5, 0, -5);

    Rc<Node> result = a <= b;

    result = result->optimize(OptPass::RemoveRedundantComparisons).value();

    auto expected = BoolConst::False;
    REQUIRE(expected->compare(result.weak()).value_or("") == "");
}
TEST_CASE("Redundant Interp1 validity checks are removed") {
    {
        auto input = Variable::create("input", 0, 1.9, 1);
        Interp1EquidParams params{"test", {0, 1, 2}, {5, 6, 7}, input};

        auto [_interp, valid] = Interp1Equid::create(params);

        auto result = valid->optimize(OptPass::RemoveRedundantInterpValidity).value();

        REQUIRE_SAME_NODE(BoolConst::True, result);
    }
}
TEST_CASE("Redundant Interp2 validity checks are removed") {
    {
        auto xp = Variable::create("input", 0, 1.9, 1);
        auto yp = Variable::create("input", 0, 0.9, 1);
        Interp2EquidParams params{"test", {0, 2}, {0, 1}, {5, 6, 7, 8}, xp, yp};

        auto [_interp, valid] = Interp2Equid::create(params);

        auto result = valid->optimize(OptPass::RemoveRedundantInterpValidity).value();

        REQUIRE_SAME_NODE(BoolConst::True, result);
    }
    // Ensure that optimisations aren't too eager when not both inputs are in bounds
    {
        auto xp = Variable::create("input", 0, 1.9, 1);
        auto yp = Variable::create("input", 0, 100, 1);
        Interp2EquidParams params{"test", {0, 2}, {0, 1}, {5, 6, 7, 8}, xp, yp};

        auto [_interp, valid] = Interp2Equid::create(params);

        auto result = valid->optimize(OptPass::RemoveRedundantInterpValidity);
        REQUIRE_FALSE(result.has_value());
    }
}


TEST_CASE("Multiple successive optimisation passes works") {
    auto a = Variable::create("a", 0, 10, 5);
    auto b = Constant::create("b", -5);
    auto c = Constant::create("c", 5);
    auto d = Constant::create("d", 4);

    // a < b = false, c+d = 9 => expected = b + c_d = -5 + 9 = 4
    auto e = _if(a < b, a, b);
    auto result = (c + d) + e;

    auto first_pass = result->optimize(OptPass::RemoveRedundantComparisons)
        .value_or(result);

    auto second_pass = first_pass->optimize(OptPass::ConstantFold)
        .value_or(first_pass);

    REQUIRE_SAME_NODE(Constant::create("result", 4), second_pass);
}

TEST_CASE("Merge bools works") {
    //         x(&&)
    //         /  \
    //        /     \
    //       y(<)   z(&&)
    //      / \     /   \
    //     a   b  u(<)   v(<)
    //            / \    / \
    //           c   d  e   f

    auto a = Variable::create("a", 0, 10, 5);
    auto b = Variable::create("b", 0, 10, 5);
    auto c = Variable::create("c", 0, 10, 5);
    auto d = Variable::create("d", 0, 10, 5);
    auto e = Variable::create("e", 0, 10, 5);
    auto f = Variable::create("f", 0, 10, 5);

    auto v = e < f;
    auto u = c < d;
    auto z = u && v;
    auto y = a < b;
    auto x = y && z;

    // Expected nodes
    auto expected = MergedBool::create({u, v, y}, {a, b, c, d, e, f});

    auto result = x->optimize(OptPass::MergeBools)
        .value_or(x);

    REQUIRE_SAME_NODE(expected, result);
}

TEST_CASE("Select else last works") {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 25, 100, 60);
    auto b = Variable::create("b", 50, 110, 75);
    auto c = Variable::create("c", 50, 110, 75);
    auto x = a < b;
    x->override_name("x");
    auto y = a > b;
    y->override_name("y");
    auto nx = !x;
    auto ny = !x;
    auto z = ny || nx;
    z->override_name("z");

    std::vector<std::string> expected_code = {
        "reg[19:0] select_x_a_x_y_b_z_c;",
        "always @(posedge clk) begin",
        "\tif(x) begin",
        "\t\tselect_x_a_x_y_b_z_c <= a_pl1;",
        "\tend",
        "\telse if(x && y) begin",
        "\t\tselect_x_a_x_y_b_z_c <= b_pl1;",
        "\tend",
        "\telse begin",
        "\t\tselect_x_a_x_y_b_z_c <= c_pl1;",
        "\tend",
        "end"
    };

    auto result = SelectFrac::create({{a, {x}}, {b, {x, y}}, {c, {z}}});
    result->optimize(OptPass::SelectElseLast);

    REQUIRE(util::join(result->get_code(wl)) == util::join(expected_code));
}

TEST_CASE("Select else undef works") {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 25, 100, 60);
    auto b = Variable::create("b", 50, 110, 75);
    auto c = Variable::create("c", 50, 110, 75);
    auto x = a < b;
    x->override_name("x");
    auto y = a > b;
    y->override_name("y");
    auto nx = !x;
    auto ny = !x;
    auto z = ny || nx;
    z->override_name("z");

    std::vector<std::string> expected_code = {
        "reg[19:0] select_x_a_x_y_b_z_c;",
        "always @(posedge clk) begin",
        "\tif(x) begin",
        "\t\tselect_x_a_x_y_b_z_c <= a_pl1;",
        "\tend",
        "\telse if(x && y) begin",
        "\t\tselect_x_a_x_y_b_z_c <= b_pl1;",
        "\tend",
        "\telse if(z) begin",
        "\t\tselect_x_a_x_y_b_z_c <= c_pl1;",
        "\tend",
        "\telse begin",
        "\t\tselect_x_a_x_y_b_z_c <= 128'bx;",
        "\tend",
        "end"
    };

    auto result = SelectFrac::create({{a, {x}}, {b, {x, y}}, {c, {z}}});
    // This optimisation pass modifies nodes in place
    result->optimize(OptPass::SelectElseX);

    REQUIRE(util::join(result->get_code(wl)) == util::join(expected_code));
}

TEST_CASE("Single Select removal works") {
    auto a = Variable::create("a", 25, 100, 60);
    auto b = Variable::create("b", 50, 110, 75);
    auto x = a < b;


    auto result = SelectFrac::create({{a, {x}}})
        ->optimize(OptPass::RemoveSingleSelect).value();

    REQUIRE_SAME_NODE(result, a);
}
