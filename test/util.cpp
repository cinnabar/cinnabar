#include "util.hpp"

#include "../src/util.hpp"

#include <catch2/catch.hpp>

#include <algorithm>

using util::join;

void require_same_lines(std::vector<std::string> lhs, std::vector<std::string> rhs) {
    std::sort(lhs.begin(), lhs.end());
    std::sort(rhs.begin(), rhs.end());

    REQUIRE(join(lhs) == join(rhs));
}
