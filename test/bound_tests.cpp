#include <catch2/catch.hpp>

#include "util.hpp"
#include "../src/node.hpp"

TEST_CASE( "Constants have correct bounds", "[nodes]") {
    auto a = Constant::create("a", 50);
    REQUIRE(a->min() == 50);
    REQUIRE(a->max() == 50);
}



TEST_CASE( "Variables have correct bounds", "[nodes]") {
    auto a = Variable::create("a", 0, 50, 0);
    REQUIRE(a->min() == 0);
    REQUIRE(a->max() == 50);
}

TEST_CASE( "Addition has correct bounds", "[nodes]") {
    auto a = Variable::create("a", 0, 50, 0);
    auto b = Variable::create("b", 25, 50, 25);

    auto result = a + b;

    REQUIRE(result->min() == 25);
    REQUIRE(result->max() == 100);
}
TEST_CASE( "Subtractions have correct bounds", "[nodes]") {
    auto a = Variable::create("a", 0, 50, 0);
    auto b = Variable::create("b", 25, 50, 25);

    auto result = a - b;

    REQUIRE(result->min() == -50);
    REQUIRE(result->max() == 25);
}
TEST_CASE( "Multiplications have correct bounds", "[nodes]") {
    auto a = Variable::create("a", 0, 50, 0);
    auto b = Variable::create("b", 25, 50, 25);

    auto result = a * b;

    REQUIRE(result->min() == 0);
    REQUIRE(result->max() == 50*50);
}
TEST_CASE("Multiplication with negative values has correct bounds") {
    auto a = Variable::create("a", -1885, 1593, 0);
    auto b = Variable::create("b", -20, 40, 0);
    auto result = a * b;

    REQUIRE(result->min() == -1885 * 40);
    REQUIRE(result->max() == 1593 * 40);
}

TEST_CASE( "Simple depth calculations work", "[nodes]") {
    auto a = Variable::create("a", 0, 50, 0);
    auto b = Variable::create("b", 25, 50, 25);
    auto result = a + b;

    WordLength wl = FixedFixpoint{10};

    REQUIRE(a->start_depth(wl) == 0);
    REQUIRE(b->start_depth(wl) == 0);
    REQUIRE(result->start_depth(wl) == 1);
    REQUIRE(a->end_depth(wl) == 0);
    REQUIRE(b->end_depth(wl) == 0);
    REQUIRE(result->end_depth(wl) == 1);
}

TEST_CASE( "Depth is correct for unbalanced trees", "[nodes]") {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 0, 50, 0);
    auto b = Variable::create("b", 25, 50, 25);

    auto result_ = a + b;
    auto result = result_ + b;

    REQUIRE(result->start_depth(wl) == 2);
    REQUIRE(result->end_depth(wl) == 2);
}

TEST_CASE( "Required pipeline stages is correct", "[nodes]" ) {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 0, 50, 0);
    auto b = Variable::create("b", 25, 50, 25);

    auto first_addition = a + b;
    auto second_addition = a + first_addition;

    node::assign_delay_slots({second_addition}, 2, wl);

    REQUIRE(a->get_delay_slots() == 1);
    REQUIRE(b->get_delay_slots() == 0);
    REQUIRE(first_addition->get_delay_slots() == 0);
    REQUIRE(second_addition->get_delay_slots() == 0);
}

TEST_CASE( "Input bounds are correct", "[nodes]" ) {
    auto a = Input::create("a", 5, 10, 5);

    REQUIRE(a->min() == 5);
    REQUIRE(a->max() == 10);
}

TEST_CASE( "Divisions have correct bounds", "[nodes]" ) {
    auto a = Variable::create("a", 5, 10, 5);
    auto b = Variable::create("b", 5, 10, 5);

    auto result = a / b;
    REQUIRE(result->min() == 0.5);
    REQUIRE(result->max() == 2);
}

TEST_CASE("limestone_min bit selection regression test") {
    WordLength wl = FixedTotalLength{36};

    auto zero = Constant::create("zero", 0);
    auto x = Variable::create("x", 0, 1497.5, 0);
    auto w_eng_ = max(x, zero);
    w_eng_->override_name("w_eng_");
    auto W_ENG_MAX = Constant::create("W_ENG_MAX", 612.8);
    auto w_eng = min(w_eng_, W_ENG_MAX);
    w_eng->override_name("w_eng");

    REQUIRE(w_eng_->fractional_bits(wl) == 25);
    REQUIRE(w_eng->fractional_bits(wl) == 26);

    std::vector<std::string> expexted_code = {
        "reg[36:0] w_eng;",
        "always @(posedge clk) begin",
        "\tif(lt_w_eng__W_ENG_MAX) begin",
        "\t\tw_eng <= {w_eng__pl1[35:0], 1'd0};",
        "\tend",
        "\telse begin",
        "\t\tw_eng <= W_ENG_MAX;",
        "\tend",
        "end"
    };

    require_same_lines(w_eng->get_code(wl), expexted_code);
}
