#include <catch2/catch.hpp>

#include "../src/node.hpp"
#include "../src/codegen.hpp"
#include "../src/lookup_table.hpp"
#include "../src/interpolation.hpp"
#include "util.hpp"

using util::join;

TEST_CASE( "Constants generate code", "[nodes]" ) {
    auto a = Constant::create("a", 50);
    std::vector<std::string> expected = {"localparam signed [18:0] a = $signed(19'd204800);"};
    auto code = a->get_code(FixedFixpoint{12});

    REQUIRE(code == expected);
}
TEST_CASE( "Negative constants generate correct code", "[nodes]" ) {
    auto a = Constant::create("a", -50);
    std::vector<std::string> expected = {"localparam signed [18:0] a = $signed(-19'd204800);"};
    auto code = a->get_code(FixedFixpoint{12});

    REQUIRE(code == expected);
}
TEST_CASE( "Constants generate cpp", "[nodes]" ) {
    auto a = Constant::create("a", 50);
    std::vector<std::string> expected = {"const Fp<12> a = Fp<12>::from_raw(204800l);"};
    auto code = a->get_cpp(FixedFixpoint{12}, CppDataType::Fp);

    REQUIRE(code == expected);
}

TEST_CASE( "Codegen for inputs works", "[nodes]" ) {
    WordLength wl = FixedFixpoint{12};
    auto a = Input::create("a", 5, 10, 5);

    std::vector<std::string> expected = {
        "reg[16:0] a;",
        "always @(posedge clk) begin",
        "\ta <= a_non_reg;",
        "end",
    };
    node::assign_delay_slots({a}, 0, wl);
    REQUIRE(a->get_code(FixedFixpoint{12}) == expected);
}

TEST_CASE( "Get code works for single layer additions", "[nodes]" ) {
    std::vector<std::string> expected = {
        "reg[19:0] add_a_b;",
        "always @(posedge clk) begin",
        "\tadd_a_b <= $signed(a)+$signed(b);",
        "end"
    };

    auto a = Variable::create("a", 25, 50, 25);
    auto b = Variable::create("b", 0, 50, 0);

    auto result = a + b;

    REQUIRE(result->get_code(FixedFixpoint{12}) == expected);
}
TEST_CASE( "Codegen works for single layer subtractions", "[nodes]" ) {
    WordLength wl = FixedFixpoint{12};
    std::vector<std::string> expected = {
        "reg[18:0] a;",
        "reg[18:0] b;",
        "reg[18:0] sub_a_b;",
        "always @(posedge clk) begin",
        "\tsub_a_b <= $signed(a)-$signed(b);",
        "end"
    };

    auto a = Variable::create("a", 25, 50, 25);
    auto b = Variable::create("b", 0, 50, 0);

    auto result = a - b;

    Codegen codegen({std::make_pair("out", result)}, wl);

    require_same_lines(codegen.generate_code(wl), expected);
}
TEST_CASE( "Codegen works for single layer multiplications", "[nodes]" ) {
    WordLength wl = FixedFixpoint{12};
    std::vector<std::string> expected = {
        "reg[18:0] a;",
        "reg[18:0] b;",
        "reg[18:0] mul_a_b_lhs_pre;",
        "reg[18:0] mul_a_b_rhs_pre;",
        "reg[37:0] mul_a_b_post1;",
        "reg[37:0] mul_a_b_post2;",
        "always @(posedge clk) begin",
        "\tmul_a_b_lhs_pre <= a;",
        "\tmul_a_b_rhs_pre <= b;",
        "end",
        "reg[24:0] mul_a_b;",
        "wire[37:0] mul_a_b_buf;",
        "assign mul_a_b_buf = $signed(mul_a_b_lhs_pre)*$signed(mul_a_b_rhs_pre);",
        "always @(posedge clk) begin",
        "\tmul_a_b_post1 <= mul_a_b_buf;",
        "\tmul_a_b_post2 <= mul_a_b_post1;",
        "\tmul_a_b <= mul_a_b_post2[36:12];",
        "end"
    };

    auto a = Variable::create("a", 25, 50, 25);
    auto b = Variable::create("b", 0, 50, 0);

    auto result = a * b;

    Codegen codegen({std::make_pair("out", result)}, wl);

    // REQUIRE(codegen.generate_code() == expected);
    require_same_lines(codegen.generate_code(wl), expected);
}

TEST_CASE( "Codegen works for single layer divisions", "[nodes]" ) {
    WordLength wl = FixedFixpoint{12};
    std::vector<std::string> expected = {
        "reg[18:0] a;",
        "reg[18:0] b;",
        "wire[19:0] div_a_b;",
        "divider#(.LHS_INT_BITS(6), .LHS_FRAC_BITS(12), .RHS_INT_BITS(6), .RHS_FRAC_BITS(12), .OUT_INT_BITS(7), .OUT_FRAC_BITS(12)) div_a_b_mod(.clk(clk), .lhs_signed(a), .rhs_signed(b), .out_signed(div_a_b));"
    };

    auto a = Variable::create("a", 25, 50, 25);
    auto b = Variable::create("b", 0.5, 50, 0);

    auto result = a / b;

    Codegen codegen({std::make_pair("out", result)}, wl);

    require_same_lines(codegen.generate_code(wl), expected);
}


TEST_CASE( "Codegen works for single layer additions", "[nodes]" ) {
    WordLength wl = FixedFixpoint{12};
    std::vector<std::string> expected = {
        "reg[18:0] a;",
        "reg[18:0] b;",
        "reg[19:0] add_a_b;",
        "always @(posedge clk) begin",
        "\tadd_a_b <= $signed(a)+$signed(b);",
        "end"
    };

    auto a = Variable::create("a", 25, 50, 25);
    auto b = Variable::create("b", 0, 50, 0);

    auto result = a + b;

    Codegen codegen({std::make_pair("out", result)}, wl);

    require_same_lines(codegen.generate_code(wl), expected);
}

TEST_CASE( "Pipeline registers are used", "[nodes]") {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 0, 50, 0);
    auto b = Variable::create("b", 0, 50, 0);

    auto result_ = a + b;
    auto result = result_ + a;

    std::vector<std::string> expected = {
        "reg[18:0] a;",
        "reg[18:0] a_pl1;",
        "always @(posedge clk) begin",
        "\ta_pl1 <= a;",
        "end",
        "reg[18:0] b;",
        "reg[19:0] add_a_b;",
        "always @(posedge clk) begin",
        "\tadd_a_b <= $signed(a)+$signed(b);",
        "end",
        "reg[20:0] add_add_a_b_a;",
        "always @(posedge clk) begin",
        "\tadd_add_a_b_a <= $signed(add_a_b)+$signed({{1{a_pl1[18]}}, a_pl1});",
        "end"
    };


    Codegen codegen({std::make_pair("out", result)}, wl);

    require_same_lines(codegen.generate_code(wl), expected);
}

TEST_CASE( "Codegen works when more than 2 vars are added in the same op", "[nodes]" ) {
    WordLength wl = FixedFixpoint{10};
    auto a = Variable::create("a", 0, 50, 0);
    auto b = Variable::create("b", 0, 50, 0);
    auto c = Variable::create("c", 0, 50, 0);

    auto result = a + b + c;

    std::vector<std::string> expected = {
        "reg[16:0] a;",
        "reg[16:0] b;",
        "reg[17:0] add_a_b;",
        "always @(posedge clk) begin",
        "\tadd_a_b <= $signed(a)+$signed(b);",
        "end",
        "reg[16:0] c;",
        "reg[16:0] c_pl1;",
        "always @(posedge clk) begin",
        "\tc_pl1 <= c;",
        "end",
        "reg[18:0] add_add_a_b_c;",
        "always @(posedge clk) begin",
        "\tadd_add_a_b_c <= $signed(add_a_b)+$signed({{1{c_pl1[16]}}, c_pl1});",
        "end"
    };

    Codegen codegen({std::make_pair("out", result)}, wl);

    require_same_lines(codegen.generate_code(wl), expected);
}

TEST_CASE( "Codegen works for module inputs", "[codegen]" ) {
    WordLength wl = FixedFixpoint{10};
    auto a = Input::create("a", 0, 50, 0);
    auto b = Input::create("b", 0, 50, 0);
    auto result = a + b;

    Codegen codegen({std::make_pair("result", result)}, wl);
    auto code = codegen.generate_module("test", wl, false);

    std::vector<std::string> expected = {
        "`include \"divider.sv\"",
        "`include \"interp1d.v\"",
        "module test",
        "\t\t( clk",
        "\t\t, rst",
        "\t\t, input_valid",
        "\t\t, output_valid",
        "\t\t, a_non_reg",
        "\t\t, b_non_reg",
        "\t\t, result",
        "\t\t);",
        "\toutput[17:0] result;",
        "\tassign result = add_a_b;",
        "\toutput output_valid;",
        "\tinput clk;",
        "\tinput rst;",
        "\tinput input_valid;",
        "\tinput[16:0] a_non_reg;",
        "\treg[16:0] a;",
        "\talways @(posedge clk) begin",
        "\t\ta <= a_non_reg;",
        "\tend",
        "\tinput[16:0] b_non_reg;",
        "\treg[16:0] b;",
        "\talways @(posedge clk) begin",
        "\t\tb <= b_non_reg;",
        "\tend",
        "\treg[17:0] add_a_b;",
        "\talways @(posedge clk) begin",
        "\t\tadd_a_b <= $signed(a)+$signed(b);",
        "\tend",
        "\treg pl_validity_buffer;",
        "\talways @(posedge clk) begin",
        "\t\tif(rst == 1) begin",
        "\t\t\tpl_validity_buffer <= 0;",
        "\t\tend",
        "\t\telse begin",
        "\t\t\tpl_validity_buffer <= input_valid;",
        "\t\tend",
        "\tend",
        "\tassign output_valid = pl_validity_buffer;",
        "endmodule",
        "`define test_DEPTH 1"
    };

    require_same_lines(code, expected);
}

TEST_CASE( "Pipeline validity check works", "[codegen]" ) {
    int depth = 10;
    std::vector<std::string> expected = {
        "reg[9:0] pl_validity_buffer;",
        "always @(posedge clk) begin",
        "\tif(rst == 1) begin",
        "\t\tpl_validity_buffer <= 0;",
        "\tend",
        "\telse begin",
        "\t\tpl_validity_buffer <= {pl_validity_buffer[8:0], input_valid};",
        "\tend",
        "end",
        "assign output_valid = pl_validity_buffer[9];"
    };

    auto result = validation_delay("input_valid", "output_valid", depth);

    REQUIRE(join(result) == join(expected));
}

TEST_CASE( "Pipeline validity at depth 1 works", "[codegen]" ) {
    std::vector<std::string> expected = {
        "reg pl_validity_buffer;",
        "always @(posedge clk) begin",
        "\tif(rst == 1) begin",
        "\t\tpl_validity_buffer <= 0;",
        "\tend",
        "\telse begin",
        "\t\tpl_validity_buffer <= input_valid;",
        "\tend",
        "end",
        "assign output_valid = pl_validity_buffer;"
    };

    auto result = validation_delay("input_valid", "output_valid", 1);

    REQUIRE(join(result) == join(expected));
}

TEST_CASE( "Pipeline registers are used in outputs", "[codegen]" ) {
    WordLength wl = FixedFixpoint{10};
    auto var1 = Input::create("a", 0, 100, 0);
    auto var2 = Input::create("b", 0, 100, 0) + Constant::create("c", 5);

    Codegen codegen({std::make_pair("var1", var1), std::make_pair("var2", var2)}, wl);
    auto code = codegen.generate_module("test", wl, false);

    REQUIRE(var1->get_delay_slots() == 1);

    bool found_assignment = false;
    for(auto line : code) {
        if(line.find("assign var1") != std::string::npos) {
            found_assignment = true;
            REQUIRE(line == std::string("\tassign var1 = a_pl1;"));
        }
    }

    REQUIRE(found_assignment == true);
}

TEST_CASE("Constant pipeline registers are not generated", "[codegen]") {
    WordLength wl = FixedFixpoint{10};
    auto var = Constant::create("a", 0);

    auto result = var + var + var;

    Codegen codegen({{"result", result}}, wl);

    std::vector<std::string> expected = {
        "localparam signed [10:0] a = $signed(11'd0);",
        "reg[10:0] add_a_a;",
        "always @(posedge clk) begin",
        "\tadd_a_a <= $signed(a)+$signed(a);",
        "end",
        "reg[10:0] add_add_a_a_a;",
        "always @(posedge clk) begin",
        "\tadd_add_a_a_a <= $signed(add_a_a)+$signed(a);",
        "end"
    };
    require_same_lines(codegen.generate_code(wl), expected);
}

TEST_CASE("Pipeline registers for bools are 1 bit", "[codegen]") {
    WordLength wl = FixedFixpoint{10};
    auto a = Constant::create("a", 2);
    auto b = Constant::create("b", 3);
    auto result = a < b;

    result->request_delay_slots(1);

    Codegen codegen({{"result", result}}, wl);

    std::vector<std::string> expected = {
        "localparam signed [12:0] a = $signed(13'd2048);",
        "localparam signed [12:0] b = $signed(13'd3072);",
        "reg lt_a_b;",
        "always @(posedge clk) begin",
        "\tlt_a_b <= $signed(a)<$signed(b);",
        "end",
        "reg lt_a_b_pl1;",
        "always @(posedge clk) begin",
        "\tlt_a_b_pl1 <= lt_a_b;",
        "end"
    };
    require_same_lines(codegen.generate_code(wl), expected);
}

TEST_CASE( "Min and max functions have reasonalbe names", "[codegen]") {
    auto var1 = Variable::create("a", 0, 100, 0);
    auto var2 = Variable::create("b", 0, 100, 0);

    REQUIRE(min(var1, var2)->base_name() == "min_a_b");
    REQUIRE(max(var1, var2)->base_name() == "max_a_b");
}

TEST_CASE( "Min function generate code", "[codegen]") {
    auto var1 = Variable::create("a", 0, 100, 0);
    auto var2 = Variable::create("b", 0, 100, 0);

    std::vector<std::string> expected = {
        "reg[17:0] min_a_b;",
        "always @(posedge clk) begin",
        "\tif(lt_a_b) begin",
        "\t\tmin_a_b <= a_pl1;",
        "\tend",
        "\telse begin",
        "\t\tmin_a_b <= b_pl1;",
        "\tend",
        "end"
    };

    REQUIRE(join(min(var1, var2)->get_code(FixedFixpoint{10})) == join(expected));
}


TEST_CASE("Long names get hashed") {
    auto a = Constant::create("aaaaaaaaaaaaaaaaaaaaaaaaa", 0);
    auto b = Constant::create("bbbbbbbbbbbbbbbbbbbbbbbbb", 0);

    std::string name = "add_aaaaaaaaaaaaaaaaaaaaaaaaa_bbbbbbbbbbbbbbbbbbbbbbbbb";

    std::size_t hash = std::hash<std::string>{}(name);
    // +name.size() because apparently hash collisions between simiarly lengthed
    // vars are very uncommmon
    auto expected = "h_" + std::to_string(hash) + std::to_string(name.size());

    auto result = a + b;
    REQUIRE(result->base_name() == expected);
}

TEST_CASE("Long bool binop names get hashed when generating code") {
    WordLength wl = FixedFixpoint{12};
    auto a = Constant::create("aaaaaaaaaaaaaaaaaaaaaa", 0);
    auto b = Constant::create("bbbbbbbbbbbbbbbbbbbbbb", 0);
    auto one = Constant::create("one", 1);

    std::string name = "or_lt_aaaaaaaaaaaaaaaaaaaaaa_one_lt_bbbbbbbbbbbbbbbbbbbbbb_one";

    auto result = (a<one) || (b<one);

    Codegen gen({{"", result}}, wl);

    auto code = gen.generate_code(wl);
    for(auto line : code) {
        if(line.find(name) != std::string::npos) {
            REQUIRE(line == "");
        }
    }
}


TEST_CASE("Lookup table module generation works") {
    auto lut = LookupTable::create("test", {1.5, 0.5});

    auto fractional_bits = 24;
    lut->request_fractional_bits(fractional_bits);

    std::vector<std::string> expected = {
        "module test24",
        "\t\t#( parameter output_size = 64",
        "\t\t)",
        "\t\t( clk",
        "\t\t, addr",
        "\t\t, result",
        "\t\t);",
        "\tinput clk;",
        "\tlocalparam input_size = 1;",
        "\tinput[input_size-1:0] addr;",
        "\toutput reg[output_size-1:0] result;",
        "\tlogic[output_size-1:0] data[2:0];",
        "\tinitial begin",
        "\t\tdata[0] = (output_size'(64'd25165824));",
        "\t\tdata[1] = (output_size'(64'd8388608));",
        "\tend",
        "\talways @(posedge clk) begin",
        "\t\tresult <= data[addr[input_size-1:0]];",
        "\tend",
        "endmodule"
    };

    REQUIRE(join(lut->get_module(fractional_bits)) == join(expected));
}


/* TODO: Tests disabled until proper division pipeline depth is implemented
TEST_CASE("Pipeline depth of division depends on config") {
    auto a = Constant::create("a", 3);
    auto b = Constant::create("b", 4);

    auto result = a / b;
    REQUIRE(result->end_depth() == 2);
    REQUIRE(result->start_depth() == 1);
}

TEST_CASE("Vars using division uses correct pipeline registers") {
    auto a = Variable::create("a", 1, 1, 1);
    auto b = Constant::create("b", 1);
    auto c = a / b;
    c->override_name("c");
    auto result = c + a;
    result->override_name("result");

    std::vector<std::string> expected_cpp = {
        "reg[14:0] result;",
        "always @(posedge clk) begin",
        "\tresult <= $signed(c)+$signed(a_2);",
        "end"
    };

    require_same_lines(result->get_code(FixedFixpoint{12}), expected_cpp);
}
*/
