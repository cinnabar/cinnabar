#include <catch2/catch.hpp>

#include "../src/node.hpp"
#include "../src/codegen.hpp"
#include "../src/lookup_table.hpp"
#include "../src/interpolation.hpp"


TEST_CASE("FTWL selects corect amount of fractional bits") {
    auto a = Variable::create("a", 0, 10, 5);
    auto b = Variable::create("b", 0, 10, 5);

    auto c = a + b;

    REQUIRE(a->fractional_bits(FixedTotalLength{5}) == 1);
    REQUIRE(b->fractional_bits(FixedTotalLength{5}) == 1);
    // Max(c) == 20 -> 5 integer bits -> 0 fractional bits
    REQUIRE(c->fractional_bits(FixedTotalLength{5}) == 0);
}


