#include <catch2/catch.hpp>

#include "../src/node.hpp"


/*
TEST_CASE( "Constants have correct error functions" ) {
    auto a = Constant::create("a", 0.05);

    a->set_array_index(0);

    double lengths[] = {1};
    // For now, the value of constants will be assumed to be the worst case despite
    // it being possible to know analytically
    REQUIRE(a->error_function()(lengths) == pow(2, -1));
}


TEST_CASE( "Variables have correct error functions" ) {
    auto a = Variable::create("a", 0, 1, 0.05);
    a->set_array_index(0);
    double lengths[] = {1};

    REQUIRE(a->error_function()(lengths) == 0.5);
}

TEST_CASE( "Additions have correct error function" ) {
    auto a = Variable::create("a", 0, 1, 0.05);
    auto b = Variable::create("b", 0, 2, 0.05);
    auto result = a + b;

    a->set_array_index(0);
    b->set_array_index(1);
    result->set_array_index(2);

    double lengths[] = {2,2,2};
    REQUIRE(result->error_function()(lengths) == 0.5);
}

TEST_CASE( "Subtractions have correct error function" ) {
    auto a = Variable::create("a", 0, 1, 0.05);
    auto b = Variable::create("b", 0, 2, 0.05);
    auto result = a - b;

    a->set_array_index(0);
    b->set_array_index(1);
    result->set_array_index(2);

    double lengths[] = {2,2,2};
    REQUIRE(result->error_function()(lengths) == 0.5);
}

TEST_CASE( "Multiplications have correct error function") {
    auto a = Variable::create("a", 0, 2, 0.05);
    auto b = Variable::create("b", 0, 3, 0.05);
    auto result = a * b;

    a->set_array_index(0);
    b->set_array_index(1);
    result->set_array_index(2);

    double lengths[] = {1,2,2};
    // 1.max()*2.error + 2.max()*1.error + truncation error
    REQUIRE(result->error_function()(lengths) == 3*0.5 + 2*0.25 + 0.25);
}
TEST_CASE( "Divisions have correct error function") {
    auto a = Variable::create("a", 1, 2, 0.05);
    auto b = Variable::create("b", 1, 3, 0.05);
    auto result = a / b;

    a->set_array_index(0);
    b->set_array_index(1);
    result->set_array_index(2);

    double lengths[] = {2,2,2};
    // error in a / min(b)
    REQUIRE(result->error_function()(lengths) == 0.25 / 1);
}

TEST_CASE( "Divisions have correct error function when truncated") {
    auto a = Variable::create("a", 1, 2, 0.05);
    auto b = Variable::create("b", 1, 3, 0.05);
    auto result = a / b;

    a->set_array_index(0);
    b->set_array_index(1);
    result->set_array_index(2);

    double lengths[] = {2,2,1};
    // error in a / min(b)
    REQUIRE(result->error_function()(lengths) == 0.25 / 1 + 0.25);
}

TEST_CASE("Inputs have correct error function") {
    auto input = Input<0, 100>::create("test", 5);
    input->set_array_index(0);

    double lengths[] = {2};
    REQUIRE(input->error_function()(lengths) == 0.25);
}
TEST_CASE("Abs has correct error function") {
    auto a = Variable::create("a", 0, 2, 0.05);
    auto result = Abs::create(a);
    a->set_array_index(0);
    result->set_array_index(1);

    double lengths[] = {2, 1};
    REQUIRE(result->error_function()(lengths) == 0.5);
}
TEST_CASE("Sqrt has correct error function") {
    auto a = Variable::create("a", 0, 2, 0.05);
    auto result = Sqrt::create(a);
    a->set_array_index(0);
    result->set_array_index(1);

    double lengths[] = {2, 1};
    REQUIRE(result->error_function()(lengths) == 0.5);
}
TEST_CASE("Truncate has correct error function") {
    auto a = Variable::create("a", 0, 2, 0.05);
    auto result = Truncate::create(a);
    a->set_array_index(0);
    result->set_array_index(1);

    double lengths[] = {2, 1};
    REQUIRE(result->error_function()(lengths) == 0);
}
TEST_CASE("USub has correct error function") {
    auto a = Variable::create("a", 0, 2, 0.05);
    auto result = -a;
    a->set_array_index(0);
    result->set_array_index(1);

    double lengths[] = {2, 1};
    REQUIRE(result->error_function()(lengths) == 0.5);
}

// This function tests the inputs provided by the paper
// Minimization of fractional wordlength on fixed-point conversion for high-level synthesis
// by Nobuhiro Doi et.al.
TEST_CASE("Error selection paper sample function gives correct error") {
    auto red = Variable::create("red", 0, 255, 0);
    auto green = Variable::create("red", 0, 255, 0);
    auto blue = Variable::create("red", 0, 255, 0);

    auto c1 = Constant::create("tmp0", 0.29900);
    auto c2 = Constant::create("tmp1", 0.58700);
    auto c3 = Constant::create("tmp2", 0.11400);
    auto tmp0 = c1 * red;
    auto tmp1 = c2 * green;
    auto tmp2 = c3 * blue;
    auto tmp3 = tmp0 + tmp1;
    auto y = tmp2 + tmp3;

    c1->set_array_index(0);
    c2->set_array_index(1);
    c3->set_array_index(2);

    tmp0->set_array_index(3);
    tmp1->set_array_index(4);
    tmp2->set_array_index(5);
    tmp3->set_array_index(6);
    y->set_array_index(7);

    red->set_array_index(8);
    green->set_array_index(9);
    blue->set_array_index(10);

    double lengths[] = {
        // Constants
        12, 12, 12,
        // tmp0..3, y
        4, 4, 4, 4, 4,
        // In the paper, these have no error, but since that is currently unsupported
        // we just give them a high value that won't affect the output
        64, 64, 64
    };

    double tmp0_expected = pow(2, -12) * 255 + pow(2, -4);
    double tmp1_expected = pow(2, -12) * 255 + pow(2, -4);
    double tmp2_expected = pow(2, -12) * 255 + pow(2, -4);
    REQUIRE(fabs(tmp0->error_function()(lengths) - tmp0_expected) < 0.000001);
    REQUIRE(fabs(tmp1->error_function()(lengths) - tmp1_expected) < 0.000001);
    REQUIRE(fabs(tmp2->error_function()(lengths) - tmp2_expected) < 0.000001);

    double tmp3_expected_error = tmp0_expected + tmp1_expected;
    REQUIRE(fabs(tmp3->error_function()(lengths) - tmp3_expected_error) < 0.0001);

    // 0.1257 is determined using a reference python impl
    REQUIRE(fabs(y->error_function()(lengths) - 0.5 + 0.125732421875) < 0.001);
}




//////////////////////////////////////////////////////////////////////
//                      Constraint tests
//////////////////////////////////////////////////////////////////////

TEST_CASE("Constant word length has no constraints on other variables") {
    auto a = Constant::create("a", 0);
    REQUIRE(a->smaller_word_constraints().size() == 0);
}
TEST_CASE("Input word length has no constraints on other variables") {
    auto a = Input<0,100>::create("a", 0);
    REQUIRE(a->smaller_word_constraints().size() == 0);
}

TEST_CASE("Variables have no word length constraints") {
    auto a = Variable::create("a", 0, 1, 0);
    REQUIRE(a->smaller_word_constraints().size() == 0);
}

TEST_CASE("Addition should not have larger word lengths than operands") {
    auto a = Variable::create("a", 0, 1, 0);
    auto b = Variable::create("b", 0, 1, 0);
    auto result = a + b;

    REQUIRE(compare_rcs(result->smaller_word_constraints(), {a, b}));
}

TEST_CASE("Multiplication should not have word length constraints") {
    auto a = Variable::create("a", 0, 1, 0);
    auto b = Variable::create("b", 0, 1, 0);
    auto result = a * b;

    REQUIRE(result->smaller_word_constraints().size() == 0);
}

TEST_CASE("Division has word length constraint") {
    auto a = Variable::create("a", 0, 1, 0);
    auto b = Variable::create("b", 0, 1, 0);
    auto result = a / b;

    REQUIRE(compare_rcs(result->smaller_word_constraints(), {a,b}));
}

TEST_CASE("Abs has smaller word length than input", "[word length]") {
    auto a = Constant::create("a", 0);
    auto result = Abs::create(a);
    REQUIRE(compare_rcs(result->smaller_word_constraints(), {a}));
}
TEST_CASE("Sqrt has smaller word length than input", "[word length]") {
    auto a = Constant::create("a", 0);
    auto result = Sqrt::create(a);
    REQUIRE(compare_rcs(result->smaller_word_constraints(), {a}));
}
TEST_CASE("USub has smaller word length than input", "[word length]") {
    auto a = Constant::create("a", 0);
    auto result = -a;
    REQUIRE(compare_rcs(result->smaller_word_constraints(), {a}));
}
*/
