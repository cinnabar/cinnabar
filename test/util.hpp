#ifndef H_TEST_UTIL
#define H_TEST_UTIL

#include <string>
#include <vector>

void require_same_lines(std::vector<std::string> lhs, std::vector<std::string> rhs);

#define REQUIRE_SAME_NODE(lhs, rhs) \
    REQUIRE((lhs)->compare((rhs).weak()).value_or("") == "")

#endif
