#include <catch2/catch.hpp>

#include "../src/node.hpp"
#include "../src/codegen.hpp"
#include "../src/lookup_table.hpp"
#include "../src/interpolation.hpp"
#include "../src/interpolation_impl.hpp"
#include "../src/graph.hpp"
#include "util.hpp"

using node::traverse_depth_first;
using interp::interp2equid;
using interp::interp1equid;

TEST_CASE( "Interp1Equid works", "[interpolation]" ) {
    auto cfg = Config{
        .word_length_strategy = FixedFixpoint{12},
        .single_node_1d_interp = false,
        .single_node_2d_interp = false
    };
    auto input = Variable::create("a", 0, 2, 1);

    auto result = interp1equid("i1", {0, 2}, {1, 3}, input, cfg);
    auto interpolator = result.second;
    auto validator = result.first;

    REQUIRE(interpolator->calculate_value() == 2);
    REQUIRE(validator->calculate_value() == true);
    // REQUIRE(interpolator->min() == 0);
    // REQUIRE(interpolator->max() == 2);
    // Test overflow
    input->set_value(4);
    traverse_depth_first(interpolator, [](auto node){node.lock()->mark_dirty();});
    traverse_depth_first(validator, [](auto node){node.lock()->mark_dirty();});
    REQUIRE(interpolator->calculate_value() == 1);
    REQUIRE(validator->calculate_value() == false);
    // Test underflow
    input->set_value(-4);
    traverse_depth_first(interpolator, [](auto node){node.lock()->mark_dirty();});
    traverse_depth_first(validator, [](auto node){node.lock()->mark_dirty();});
    REQUIRE(interpolator->calculate_value() == 1);
    REQUIRE(validator->calculate_value() == false);

    REQUIRE(interpolator->base_name() == "interp1_i1");
    REQUIRE(validator->base_name() == "interp1valid_i1");
}

TEST_CASE( "Interp1Equid class works", "[interpolation]" ) {
    auto input = Variable::create("a", 0, 2, 1);

    auto params = Interp1EquidParams("i1", {0, 2}, {1, 3}, input);
    auto [interpolator, validator] = Interp1Equid::create(params);

    REQUIRE(compare_rcs(interpolator->get_parents(), {input}));
    REQUIRE(compare_rcs(validator->get_parents(), {input}));

    REQUIRE(interpolator->calculate_value() == 2);
    REQUIRE(validator->calculate_value() == true);
    REQUIRE(interpolator->min() == 1);
    REQUIRE(interpolator->max() == 3);
    // Test overflow
    input->set_value(4);
    traverse_depth_first(interpolator, [](auto node){node.lock()->mark_dirty();});
    traverse_depth_first(validator, [](auto node){node.lock()->mark_dirty();});
    REQUIRE(interpolator->calculate_value() == 1);
    REQUIRE(validator->calculate_value() == false);
    // Test underflow
    input->set_value(-4);
    traverse_depth_first(interpolator, [](auto node){node.lock()->mark_dirty();});
    traverse_depth_first(validator, [](auto node){node.lock()->mark_dirty();});
    REQUIRE(interpolator->calculate_value() == 1);
    REQUIRE(validator->calculate_value() == false);

    REQUIRE(interpolator->base_name() == "interp1_i1");
    REQUIRE(validator->base_name() == "interp1valid_i1");

    std::vector<std::string> interp_expected_cpp =
            { "Fp<5> interp1_i1 = interp::do_interpolation<5>("
                    "a, i1_y5_lut, 0.000000, 2.000000, 2.000000"
                    ");"
            };

    require_same_lines(
        interp_expected_cpp,
        interpolator->get_cpp(FixedFixpoint{5}, CppDataType::Fp)
    );

    std::vector<std::string> validator_expected_cpp =
            { "bool interp1valid_i1 = interp::do_interpolation_valid("
                    "a, 0.000000, 2.000000"
                    ");"
            };

    require_same_lines(
        validator_expected_cpp,
        validator->get_cpp(FixedFixpoint{5}, CppDataType::Fp)
    );

    // Test node comparison 
    auto o_params = Interp1EquidParams("i1", {2, 4}, {1, 3}, input);
    auto [o_interpolator, o_validator] = Interp1Equid::create(o_params);
    REQUIRE(o_interpolator->compare_local(interpolator.weak()).value_or("") != "");
    REQUIRE(o_validator->compare_local(validator.weak()).value_or("") != "");
}


TEST_CASE("Interp1 parents are optimised") {
    auto a = Constant::create("a", 0);
    auto b = Constant::create("b", 0);
    auto c = Variable::create("a", 0, 2, 1);
    auto input = (a + b) + c;

    auto params = Interp1EquidParams("i1", {0, 2}, {1, 3}, input);
    auto [interpolator, validator] = Interp1Equid::create(params);

    auto ab = Constant::create("a", 0);
    auto e_input = ab + c;
    auto e_params = Interp1EquidParams("i1", {0, 2}, {1, 3}, e_input);
    auto [e_interpolator, e_validator] = Interp1Equid::create(e_params);
    auto optimised = interpolator->optimize(OptPass::ConstantFold);
    auto optimised_validator = validator->optimize(OptPass::ConstantFold);
    REQUIRE_FALSE(optimised.has_value());
    REQUIRE_FALSE(optimised_validator.has_value());
    REQUIRE_SAME_NODE(interpolator, e_interpolator);
    REQUIRE_SAME_NODE(validator, e_validator);
}

TEST_CASE("Interp1 constant folds in bounds") {
    {
        auto input = Constant::create("a", 1);
        auto params = Interp1EquidParams("i1", {0, 2}, {1, 3}, input);
        auto [interpolator, validator] = Interp1Equid::create(params);

        auto opt_interpolator = interpolator->optimize(OptPass::ConstantFold)
            .value();
        auto opt_validator = validator->optimize(OptPass::ConstantFold)
            .value();
        REQUIRE_SAME_NODE(Constant::create("a", 2), opt_interpolator);
        REQUIRE_SAME_NODE(BoolConst::True, opt_validator);
    }
}
TEST_CASE("Interp1 constant folds out of bounds") {
    {
        auto input = Constant::create("a", 4);
        auto params = Interp1EquidParams("i1", {0, 2}, {1, 3}, input);
        auto [interpolator, validator] = Interp1Equid::create(params);

        auto opt_interpolator = interpolator->optimize(OptPass::ConstantFold)
            .value();
        auto opt_validator = validator->optimize(OptPass::ConstantFold)
            .value();
        REQUIRE_SAME_NODE(Constant::create("a", 1), opt_interpolator);
        REQUIRE_SAME_NODE(BoolConst::False, opt_validator);
    }
}



double reference_interp2equid(
    const std::vector<double> &x,
    const std::vector<double> &y,
    const std::vector<double> &z,
    unsigned long nx,
    unsigned long ny,
    double xmin,
    double ymin,
    double dx,
    double dy,
    double xp,
    double yp
) {
    int x_idx = 0;
    int y_idx = 0;
    
    double q11, q12, q21, q22;
    
    if( (xp < x[0]) || (xp > x[nx-1]) || (yp < y[0]) || (yp > y[ny-1])){
        return *std::max_element(z.begin(), z.end());
    }
    else {
        
        // estimate
        x_idx = trunc( (xp-xmin)/dx ) + 1;
        y_idx = trunc( (yp-ymin)/dy ) + 1;
        
        q11 = z[(x_idx-1)*ny + y_idx-1];
        q12 = z[(x_idx-1)*ny + y_idx];
        q21 = z[x_idx*ny + y_idx-1];
        q22 = z[x_idx*ny + y_idx];
        
        return ( q11*(x[x_idx] - xp)*(y[y_idx] - yp) + q21*(xp-x[x_idx-1])*(y[y_idx] - yp) + q12*(x[x_idx]-xp)*(yp - y[y_idx-1]) + q22*(xp-x[x_idx-1])*(yp - y[y_idx-1]) )/( ( x[x_idx] - x[x_idx-1])*(y[y_idx] - y[y_idx-1]) );
    }
}

TEST_CASE( "Interp2Equid works", "[interpolation]" ) {
    auto cfg = Config{
        .word_length_strategy = FixedFixpoint{12},
        .single_node_1d_interp = false,
        .single_node_2d_interp = false
    };
    std::vector<double> x = {0,2};
    std::vector<double> y = {0,4};
    std::vector<double> z = {1, 2, 2, 3};
    auto nx = x.size();
    auto ny = y.size();
    auto dx = 2;
    auto dy = 4;
    auto xmin = 0;
    auto ymin = 0;
    auto ref_xp = 1;
    auto ref_yp = 1;
    auto reference_result = reference_interp2equid(
        x,
        y,
        z,
        nx,
        ny,
        xmin,
        ymin,
        dx,
        dy,
        ref_xp,
        ref_yp
    );

    auto xp = Variable::create("xp", -100, 2, ref_xp);
    auto yp = Variable::create("yp", -100, 4, ref_yp);

    auto result_ = interp2equid("result", x, y, z, xp, yp, cfg);
    auto result_valid = result_.first;
    auto result = result_.second;

    REQUIRE(int(result->calculate_value() * 1000) == int(reference_result * 1000));
    REQUIRE(result->min() == 1);
    REQUIRE(result->max() == 3);

    REQUIRE(result_valid->calculate_value() == true);

    // Out of bounds checks
    xp->set_value(-3);
    yp->set_value(-3);
    xp->mark_dirty();
    yp->mark_dirty();
    traverse_depth_first(
        result_valid.weak(),
        [](auto node) {node.lock()->mark_dirty();}
    );
    traverse_depth_first(
        result.weak(),
        [](auto node) {node.lock()->mark_dirty();}
    );
    REQUIRE(result_valid->calculate_value() == false);
    REQUIRE(result->calculate_value() == 1);

    xp->set_value(3);
    yp->set_value(3);
    xp->mark_dirty();
    yp->mark_dirty();
    traverse_depth_first(
        result_valid.weak(),
        [](auto node) {node.lock()->mark_dirty();}
    );
    traverse_depth_first(
        result.weak(),
        [](auto node) {node.lock()->mark_dirty();}
    );
    REQUIRE(result_valid->calculate_value() == false);
    REQUIRE(result->calculate_value() == 1);
    REQUIRE(result->base_name() == "interp2_result");
    REQUIRE(result_valid->base_name() == "interp2valid_result");
}


TEST_CASE( "Interp2Equid class works", "[interpolation]") {
    std::vector<double> x = {0,2};
    std::vector<double> y = {0,4};
    std::vector<double> z = {1, 2, 2, 3};
    auto nx = x.size();
    auto ny = y.size();
    auto dx = 2;
    auto dy = 4;
    auto xmin = 0;
    auto ymin = 0;
    auto ref_xp = 1;
    auto ref_yp = 1;
    auto reference_result = reference_interp2equid(
        x,
        y,
        z,
        nx,
        ny,
        xmin,
        ymin,
        dx,
        dy,
        ref_xp,
        ref_yp
    );

    auto xp = Variable::create("xp", -100, 2, ref_xp);
    auto yp = Variable::create("yp", -100, 4, ref_yp);

    auto params = Interp2EquidParams("result", x, y, z, xp, yp);
    auto [result, result_valid] = Interp2Equid::create(params);

    REQUIRE(int(result->calculate_value() * 1000) == int(reference_result * 1000));
    REQUIRE(result->min() == 1);
    REQUIRE(result->max() == 3);

    REQUIRE(result_valid->calculate_value() == true);

    std::vector<std::string> interp_expected_cpp =
            { "Fp<5> interp2_result = interp::do_2d_interpolation<5>("
                    "result_z5_lut, "
                    "2, "
                    "0.000000, 0.000000, "
                    "2.000000, 4.000000, "
                    "2.000000, 4.000000, "
                    "xp, yp"
                    ");"
            };
    require_same_lines(
        interp_expected_cpp,
        result->get_cpp(FixedFixpoint{5}, CppDataType::Fp)
    );

    std::vector<std::string> validator_expected_cpp =
            { "bool interp2valid_result = interp::do_2d_interpolation_valid("
                    "0.000000, 0.000000, "
                    "2.000000, 4.000000, "
                    "xp, yp"
                    ");"
            };
    require_same_lines(
        validator_expected_cpp,
        result_valid->get_cpp(FixedFixpoint{5}, CppDataType::Fp)
    );

    // Out of bounds checks
    xp->set_value(-3);
    yp->set_value(-3);
    xp->mark_dirty();
    yp->mark_dirty();
    traverse_depth_first(
        result_valid.weak(),
        [](auto node) {node.lock()->mark_dirty();}
    );
    traverse_depth_first(
        result.weak(),
        [](auto node) {node.lock()->mark_dirty();}
    );
    REQUIRE(result_valid->calculate_value() == false);
    REQUIRE(result->calculate_value() == 1);

    xp->set_value(3);
    yp->set_value(3);
    xp->mark_dirty();
    yp->mark_dirty();
    traverse_depth_first(
        result_valid.weak(),
        [](auto node) {node.lock()->mark_dirty();}
    );
    traverse_depth_first(
        result.weak(),
        [](auto node) {node.lock()->mark_dirty();}
    );
    REQUIRE(result_valid->calculate_value() == false);
    // REQUIRE(result->calculate_value() == 2);
    // REQUIRE(result->graph_label_name() == "interp2");
    // REQUIRE(result_valid->graph_label_name() == "interp2valid");
    REQUIRE(result->base_name() == "interp2_result");
    REQUIRE(result_valid->base_name() == "interp2valid_result");

    std::vector<Rc<Node>> expected_parents = {xp, yp};
    REQUIRE(compare_rcs(result->get_parents(), expected_parents));
    REQUIRE(compare_rcs(result_valid->get_parents(), expected_parents));

    // Test compare_local
    auto other_params = Interp2EquidParams("result_", x, {100, 101}, z, xp, yp);
    auto [other_result, other_result_valid] = Interp2Equid::create(
        other_params
    );

    auto valid_comparison = other_result_valid->compare(result_valid.weak());
    REQUIRE(valid_comparison.value_or("") != "");
    REQUIRE(other_result->compare(result.weak()).value_or("") != "");

}


TEST_CASE("interp2equid class constant folding works") {
    // In bounds
    {
        std::vector<double> x = {0,2};
        std::vector<double> y = {0,4};
        std::vector<double> z = {1, 2, 2, 3};
        auto ref_xp = 0;
        auto ref_yp = 0;

        auto xp = Constant::create("xp", ref_xp);
        auto yp = Constant::create("yp", ref_yp);

        auto params = Interp2EquidParams("result", x, y, z, xp, yp);
        auto [result, result_valid] = Interp2Equid::create(params);

        auto optimized = result->optimize(OptPass::ConstantFold).value();
        auto optimized_valid = result_valid->optimize(OptPass::ConstantFold).value();

        auto expected = Constant::create("interp2equid_result", 1);
        REQUIRE(optimized_valid->compare(BoolConst::True.weak()).value_or("") == "");
        REQUIRE(optimized->compare(expected.weak()).value_or("") == "");
    }
    // Out of bounds
    {
        std::vector<double> x = {0,2};
        std::vector<double> y = {0,4};
        std::vector<double> z = {1, 2, 2, 3};
        auto ref_xp = 4;
        auto ref_yp = 0;

        auto xp = Constant::create("xp", ref_xp);
        auto yp = Constant::create("yp", ref_yp);

        auto params = Interp2EquidParams("result", x, y, z, xp, yp);
        auto [result, result_valid] = Interp2Equid::create(params);

        auto optimized = result->optimize(OptPass::ConstantFold).value();
        auto optimized_valid = result_valid->optimize(OptPass::ConstantFold).value();

        auto expected = Constant::create("interp2equid_result", 1);
        REQUIRE(optimized_valid->compare(BoolConst::False.weak()).value_or("") == "");
        REQUIRE(optimized->compare(expected.weak()).value_or("") == "");
    }
}
TEST_CASE("interp2equid class optimizes parents") {
    // In bounds
    std::vector<double> x = {0,2};
    std::vector<double> y = {0,4};
    std::vector<double> z = {1, 2, 2, 3};
    auto ref_xp = 2;
    auto ref_yp = 0;

    auto xp = Constant::create("xp", ref_xp) + Constant::create("xp_", ref_xp);
    auto yp = Variable::create("yp", 0, 100, ref_yp);

    auto params = Interp2EquidParams("result", x, y, z, xp, yp);
    auto [result, result_valid] = Interp2Equid::create(params);

    auto optimized = result->optimize(OptPass::ConstantFold);
    REQUIRE_FALSE(optimized.has_value());
    auto optimized_valid = result_valid->optimize(OptPass::ConstantFold);
    REQUIRE_FALSE(optimized_valid.has_value());

    auto expected_xp = Constant::create("xp", ref_xp*2);
    auto expected_params = Interp2EquidParams("result", x, y, z, expected_xp, yp);
    auto [expected, expected_valid] = Interp2Equid::create(expected_params);

    REQUIRE(expected->compare(result.weak()).value_or("") == "");
    REQUIRE(expected_valid->compare(result_valid.weak()).value_or("") == "");
}




Fp<20> y_lut(int i) {
    Fp<20> points[] = {0.5, 1.5, 3};
    return points[i];
}

TEST_CASE("interp1 standalone function works correctly") {
    SECTION("default") {
        Fp<20> input{1.0};
        auto result = interp::do_interpolation<20>(input, y_lut, 0, 6, 2);
        REQUIRE(result == Fp<20>{1.0});
    }

    SECTION("With x offset") {
        Fp<20> input{1.0};
        auto result = interp::do_interpolation<20>(input, y_lut, 1, 7, 2);
        INFO(result.as_float());
        REQUIRE(result == Fp<20>{0.5});
    }
}

Fp<20> z_lut_flat_x(int i) {
    Fp<20> points[] = {0, 2, 0, 2};
    return points[i];
}

Fp<20> z_lut_flat_y(int i) {
    Fp<20> points[] = {0, 0, 2, 2};
    return points[i];
}

TEST_CASE("interp2 standalone function works correctly") {
    SECTION("flat_in_x_direction") {
        auto dx = 2;
        auto dy = 2;
        auto xmin = 0;
        auto ymin = 0;
        auto xmax = 2;
        auto ymax = 2;
        auto ny = 2;

        Fp<20> x{1.0};
        Fp<20> y{1.0};
        auto result = interp::do_2d_interpolation<20>(
            z_lut_flat_x,
            ny,
            xmin,
            ymin,
            xmax,
            ymax,
            dx,
            dy,
            x,
            y
        );
        INFO(result.as_float());
        REQUIRE(result == Fp<20>{1.0});
    }
    SECTION("flat_in_x_direction_with_offset") {
        auto dx = 2;
        auto dy = 2;
        auto xmin = 10;
        auto ymin = 0;
        auto xmax = 12;
        auto ymax = 2;
        auto ny = 2;

        Fp<20> x{11.0};
        Fp<20> y{1.0};
        auto result = interp::do_2d_interpolation<20>(
            z_lut_flat_x,
            ny,
            xmin,
            ymin,
            xmax,
            ymax,
            dx,
            dy,
            x,
            y
        );
        INFO(result.as_float());
        REQUIRE(result == Fp<20>{1.0});
    }
    SECTION("flat_in_x_direction_with_y_offset") {
        auto dx = 2;
        auto dy = 2;
        auto xmin = 0;
        auto ymin = 10;
        auto xmax = 2;
        auto ymax = 12;
        auto ny = 2;

        Fp<20> x{1.0};
        Fp<20> y{11.0};
        auto result = interp::do_2d_interpolation<20>(
            z_lut_flat_x,
            ny,
            xmin,
            ymin,
            xmax,
            ymax,
            dx,
            dy,
            x,
            y
        );
        INFO(result.as_float());
        REQUIRE(result == Fp<20>{1.0});
    }
    SECTION("flat_in_x_direction_with_both_offset") {
        auto dx = 2;
        auto dy = 2;
        auto xmin = 20;
        auto ymin = 10;
        auto xmax = 22;
        auto ymax = 12;
        auto ny = 2;

        Fp<20> x{21.0};
        Fp<20> y{11.0};
        auto result = interp::do_2d_interpolation<20>(
            z_lut_flat_x,
            ny,
            xmin,
            ymin,
            xmax,
            ymax,
            dx,
            dy,
            x,
            y
        );
        INFO(result.as_float());
        REQUIRE(result == Fp<20>{1.0});
    }
    SECTION("flat_in_x_direction_with_non_uniform_d") {
        auto dx = 2;
        auto dy = 4;
        auto xmin = 20;
        auto ymin = 10;
        auto xmax = 22;
        auto ymax = 14;
        auto ny = 2;

        Fp<20> x{21.0};
        Fp<20> y{12.0};
        auto result = interp::do_2d_interpolation<20>(
            z_lut_flat_x,
            ny,
            xmin,
            ymin,
            xmax,
            ymax,
            dx,
            dy,
            x,
            y
        );
        INFO(result.as_float());
        REQUIRE(result == Fp<20>{1.0});
    }
    SECTION("flat_in_y_direction_with_non_uniform_d") {
        auto dx = 2;
        auto dy = 4;
        auto xmin = 20;
        auto ymin = 10;
        auto xmax = 22;
        auto ymax = 14;
        auto ny = 2;

        Fp<20> x{21.0};
        Fp<20> y{12.0};
        auto result = interp::do_2d_interpolation<20>(
            z_lut_flat_y,
            ny,
            xmin,
            ymin,
            xmax,
            ymax,
            dx,
            dy,
            x,
            y
        );
        INFO(result.as_float());
        REQUIRE(result == Fp<20>{1.0});
    }
}
