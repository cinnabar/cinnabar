#include <catch2/catch.hpp>

#include "../src/node.hpp"
#include "../src/codegen.hpp"
#include "../src/lookup_table.hpp"
#include "../src/interpolation.hpp"
#include "../src/graph.hpp"

using util::write_if_changed;

////////////////////////////////////////////////////////////////////////////////
//              Node blanket impl tests

TEST_CASE( "parent_register works", "[node]" ) {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 0, 100, 0);
    auto b = Variable::create("b", 0, 100, 0);
    auto partial = a + a;
    auto sum = partial + b;

    REQUIRE(b->as_parent_register_unchanged(*sum, wl) == "b_pl1");
    REQUIRE(partial->as_parent_register_unchanged(*sum, wl) == "add_a_a");
}

TEST_CASE("As parent register works with sign extension") {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 0, 1, 0);
    auto b = Variable::create("b", 0, 100, 0);
    auto sum = a + b;

    std::string expected = "{{6{a[13]}}, a}";
    REQUIRE(a->as_parent_register(*sum, wl, 7, 12, false) == expected);
}
TEST_CASE("As parent register works with integer truncation") {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 0, 2, 0);
    auto b = Variable::create("b", 0, 100, 0);
    auto sum = a + b;

    std::string expected = "a[13:0]";
    REQUIRE(a->as_parent_register(*sum, wl, 1, 12, true) == expected);
}
TEST_CASE("As parent register works with frac extension") {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 0, 1, 0);
    auto b = Variable::create("b", 0, 100, 0);
    auto sum = a + b;

    std::string expected = "{a, 3'd0}";
    REQUIRE(a->as_parent_register(*sum, wl, a->integer_bits(wl), 15, false) == expected);
}
TEST_CASE("As parent register works with frac truncation") {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 0, 1, 0);
    auto b = Variable::create("b", 0, 100, 0);
    auto sum = a + b;

    std::string expected = "a[13:3]";
    REQUIRE(a->as_parent_register(*sum, wl, a->integer_bits(wl), 9, false) == expected);
}

TEST_CASE("As parent register uses all 0 if `this` is way smaller than `target`") {
    WordLength wl = FixedFixpoint{5};
    auto a = Variable::create("a", 0, 1, 0);
    auto b = Variable::create("b", 0, 100, 0);
    auto sum = a + b;

    std::string expected = "5'b0";
    REQUIRE(a->as_parent_register(*sum, wl, 7, -3, false) == expected);
}

TEST_CASE("As parent register uses sign extension if `this` is way bigger than `target`") {
    WordLength wl = FixedFixpoint{5};
    auto a = Variable::create("a", 0, 1, 0);
    auto b = Variable::create("b", 0, 100, 0);
    auto sum = a + b;

    std::string expected = "{6{a[6]}}";
    REQUIRE(a->as_parent_register(*sum, wl, -10, 15, true) == expected);
}

TEST_CASE("Removing integer bits is not allowed without flag") {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 0, 1, 0);
    auto b = Variable::create("b", 0, 100, 0);
    auto sum = a + b;

    REQUIRE_THROWS(a->as_parent_register(*sum, wl, a->integer_bits(wl)-1, 9, false));
}

TEST_CASE("Removing integer bits is allowed with flag") {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 0, 1, 0);
    auto b = Variable::create("b", 0, 100, 0);
    auto sum = a + b;

    std::string expected = "a[12:3]";
    REQUIRE(a->as_parent_register(*sum, wl, a->integer_bits(wl)-1, 9, true) == expected);
}

TEST_CASE( "Unique ancestors works", "[node]" ) {
    auto a = Constant::create("a", 0);
    auto b = Constant::create("b", 10);

    auto a_b = a + b;
    auto a_b_b = a_b + b;
    auto result = a_b_b + b;

    std::set<Weak<Node>> expected = {
        a.weak(),
        b.weak(),
        a_b.weak(),
        a_b_b.weak(),
    };
    REQUIRE(result->unique_ancestors() == expected);
}
////////////////////////////////////////////////////////////////////////////////
// Interpolation functions functions


////////////////////////////////////////////////////////////////////////////////
// Standalone function tests

TEST_CASE( "Min produces correct expression tree", "[codegen]") {
    auto a = Variable::create("a", 0, 100, 50);
    auto b = Variable::create("b", 25, 150, 40);
    auto result = min(a, b);
    REQUIRE(result->min() == 0);
    REQUIRE(result->max() == 100);
    REQUIRE(result->calculate_value() == 40);
}
TEST_CASE( "Max produces correct expression tree", "[codegen]") {
    auto a = Variable::create("a", 0, 100, 40);
    auto b = Variable::create("b", 25, 150, 50);
    auto result = max(a, b);
    REQUIRE(result->max() == 150);
    REQUIRE(result->min() == 25);
    REQUIRE(result->calculate_value() == 50);
}




////////////////////////////////////////////////////////////////////////////////
// Node execution functions

TEST_CASE( "Node exection works", "[codegen]" ) {
    auto a = Constant::create("a", 5);
    auto b = Variable::create("b", 5, 10, 7);
    auto c = Input::create("c", 0, 10, 7);

    auto sum = a + b;
    auto difference = a - b;
    auto product = a * b;
    auto fraction = b / c;
    auto abs = Abs::create(difference);

    REQUIRE(a->calculate_value() == 5);
    REQUIRE(b->calculate_value() == 7);
    REQUIRE(c->calculate_value() == 7);
    REQUIRE(sum->calculate_value() == 12);
    REQUIRE(difference->calculate_value() == -2);
    REQUIRE(product->calculate_value() == 35);
    REQUIRE(fraction->calculate_value() == 1);
    REQUIRE(abs->calculate_value() == 2);
}


////////////////////////////////////////////////////////////////////////////////
// Misc
TEST_CASE( "Unique ancestors for binary operators works" "[node]" ) {
    auto a = Constant::create("a", 0);
    auto b = Constant::create("b", 100);

    REQUIRE((a+b)->unique_ancestors() == std::set<Weak<Node>>{a.weak(), b.weak()});
    REQUIRE((a>b)->unique_ancestors() == std::set<Weak<Node>>{a.weak(), b.weak()});
}



TEST_CASE("File writing with change detection works") {
    std::string filename = "_test_dummy_file";
    std::string content1 = "content1";
    std::string content2 = "content2";
    std::string read_content;
    SECTION("a") {
        if(!write_if_changed(filename, content1)) {
            INFO("This test fails if " + filename + " exists. Remove it if it exists");
            REQUIRE(false);
        }
        REQUIRE_FALSE(write_if_changed(filename, content1));
        {
            std::ifstream{filename} >> read_content;
            REQUIRE(read_content == content1);
        }

        REQUIRE(write_if_changed(filename, content2));
        {
            std::ifstream{filename} >> read_content;
            REQUIRE(read_content == content2);
        }
    }
    std::filesystem::remove(filename);
}


////////////////////////////////////////////////////////////////////////////////
// Node traversal

TEST_CASE( "Traverse with cache works") {
    auto a = Variable::create("a", 0, 10, 5);
    auto b = Variable::create("b", 0, 10, 5);
    auto c = a+b;
    auto d = c+b;

    d->override_name("root");

    std::vector<std::string> expected = {
        "a",
        "b",
        "add_a_b",
        "root",
    };

    std::sort(expected.begin(), expected.end());

    std::vector<std::string> result = d->visit_with_cache<std::string>(
        [](Node* node) -> std::string {return node->base_name();}
    );

    std::sort(result.begin(), result.end());

    REQUIRE(expected == result);
}

