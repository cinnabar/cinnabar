#include <catch2/catch.hpp>
/*

#include "../src/word_length.hpp"

TEST_CASE("Paper example works", "[word_length]") {
    auto red = Variable::create("red", 0, 255, 0);
    auto green = Variable::create("green", 0, 255, 0);
    auto blue = Variable::create("blue", 0, 255, 0);

    auto c1 = Constant::create("c0", 0.29900);
    auto c2 = Constant::create("c1", 0.58700);
    auto c3 = Constant::create("c2", 0.11400);
    auto tmp0 = c1 * red;
    auto tmp1 = c2 * green;
    auto tmp2 = c3 * blue;
    auto tmp3 = tmp0 + tmp1;
    auto y = tmp2 + tmp3;

    tmp0->override_name("tmp0");
    tmp1->override_name("tmp1");
    tmp2->override_name("tmp2");
    tmp3->override_name("tmp3");
    y->override_name("y");

    WordLengthOptimizer::optimize({{0.5, y}}, {}, WordLengthOptions());

    REQUIRE(c1->optimal_fractional_bits.value() == 13);
    REQUIRE(c2->optimal_fractional_bits.value() == 13);
    REQUIRE(c3->optimal_fractional_bits.value() == 13);
    REQUIRE(tmp0->optimal_fractional_bits.value() == 4);
    REQUIRE(tmp1->optimal_fractional_bits.value() == 4);
    REQUIRE(tmp2->optimal_fractional_bits.value() == 4);
    REQUIRE(tmp3->optimal_fractional_bits.value() == 4);
    REQUIRE(y->optimal_fractional_bits.value() == 4);
}

TEST_CASE("Multiple output constraints are used", "[word_length]") {
    auto var1 = Variable::create("var1", 0, 255, 5);
    auto var2 = Variable::create("var2", 0, 255, 5);

    WordLengthOptimizer::optimize(
        {{0.125, var1}, {0.125, var2}},
        {},
        WordLengthOptions()
    );
    REQUIRE(var1->optimal_fractional_bits.value() == 3);
    REQUIRE(var2->optimal_fractional_bits.value() == 3);
}


TEST_CASE("If-statements have reasonable bit lengths", "[word_length]") {
    auto lhs = Variable::create("lhs", 0, 255, 5);
    auto rhs = Variable::create("rhs", 0, 255, 5);
    auto true_branch = Variable::create("true_branch", 0, 255, 5);
    auto false_branch = Variable::create("false_branch", 0, 255, 5);

    auto result = _if(lhs < rhs, true_branch, false_branch);

    double test[5] = {1, 1, 1, 1, 1};

    WordLengthOptimizer::optimize(
        {{0.5, result}, {0.5, lhs}, {0.5, rhs}},
        {},
        WordLengthOptions()
    );

    REQUIRE(result->error_function()(test) == 0.5);

    REQUIRE(compare_rcs({true_branch, false_branch}, result->smaller_word_constraints()));
    REQUIRE(true_branch->optimal_fractional_bits.value() == 1);
    REQUIRE(false_branch->optimal_fractional_bits.value() == 1);
    REQUIRE(result->optimal_fractional_bits.value() == 1);
}

TEST_CASE("Word length saving works", "[word_length]") {
    auto var = Variable::create("var1", 0, 255, 5);
    auto options = WordLengthOptions().with_filename("word_length_saving_works_test");
    WordLengthOptimizer::optimize({{0.5, var}}, {}, options);
    var->optimal_fractional_bits = std::nullopt;
    WordLengthOptimizer::optimize({{0.5, var}}, {}, options.from_file());

    REQUIRE(var->optimal_fractional_bits.value() == 1);
}
*/
