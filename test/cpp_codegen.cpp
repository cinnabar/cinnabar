#include <catch2/catch.hpp>

#include "../src/node.hpp"
#include "../src/codegen.hpp"

using util::join;


TEST_CASE("Cpp codegen generates correct body") {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 0, 10, 5);
    auto b = -a;

    std::vector<std::string> expected_code = {
        "Fp<12> a = 5.000000;",
        "Fp<12> usub_a = -a;"
    };

    REQUIRE(Codegen({{"b", b}}, wl).generate_cpp_body(wl, CppDataType::Fp) == expected_code);
}

TEST_CASE("Cpp codegen generates correct function") {
    WordLength wl = FixedFixpoint{12};
    auto a = Input::create("a", 10, 100, 20);
    auto b = -a;

    std::vector<std::string> expected_hpp = {
        "#pragma once",
        "#include \"luts.hpp\"",
        "#include <fixedpoint.hpp>",
        "#include <functions.hpp>",
        "#include <interpolation_impl.hpp>",
        "struct test_output {",
        "\tFp<12> b;",
        "};",
        "struct test_input {",
        "\tFp<12> a;",
        "};",
        "test_output test (test_input __input);",
    };

    std::vector<std::string> expected_cpp = {
        "#include \"test.hpp\"",
        "__attribute__((optnone)) void __gdb_trampoline() {}",
        "test_output test (test_input __input) {",
        "#pragma HLS pipeline II=1",
        "\tFp<12> a = __input.a;",
        "\tFp<12> usub_a = -a;",
        "\t__gdb_trampoline();",
        "\treturn test_output {",
        "\t\t.b = usub_a,",
        "\t};",
        "}"
    };

    auto result = Codegen({{"b", b}}, wl).generate_cpp_function("test", wl, CppDataType::Fp);
    REQUIRE(join(result.hpp) == join(expected_hpp));
    REQUIRE(join(result.cpp) == join(expected_cpp));
}
