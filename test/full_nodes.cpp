#include <catch2/catch.hpp>

#include "../src/node.hpp"
#include "../src/codegen.hpp"
#include "../src/lookup_table.hpp"
#include "../src/interpolation.hpp"

using node::assign_delay_slots;

using util::join;

TEST_CASE("New constant features work") {
    auto a = Constant::create("a", 5);

    auto bounds = to_bounds(**(a->compute_aa()));
    REQUIRE(bounds.min == 5);
    REQUIRE(bounds.max == 5);
}

TEST_CASE("Partial constant tests", "[node]") {
    REQUIRE(Constant::create("", 0)->unique_ancestors() == std::set<Weak<Node>>{});
}

TEST_CASE("Multiplication generates correct fixpoint op") {
    auto a = Variable::create("a", 0, 100, 50);
    auto b = Variable::create("b", 0, 100, 50);

    auto result = a * b;

    std::vector<std::string> expected = {
        "reg[19:0] mul_a_b_lhs_pre;",
        "reg[19:0] mul_a_b_rhs_pre;",
        "reg[39:0] mul_a_b_post1;",
        "reg[39:0] mul_a_b_post2;",
        "always @(posedge clk) begin",
        "\tmul_a_b_lhs_pre <= a;",
        "\tmul_a_b_rhs_pre <= b;",
        "end",
        "reg[26:0] mul_a_b;",
        "wire[39:0] mul_a_b_buf;",
        "assign mul_a_b_buf = $signed(mul_a_b_lhs_pre)*$signed(mul_a_b_rhs_pre);",
        "always @(posedge clk) begin",
        "\tmul_a_b_post1 <= mul_a_b_buf;",
        "\tmul_a_b_post2 <= mul_a_b_post1;",
        "\tmul_a_b <= mul_a_b_post2[38:12];",
        "end"
    };

    WordLength wl = FixedFixpoint{12};

    REQUIRE(join(result->get_code(wl)) == join(expected));
    REQUIRE(result->computation_time(wl) == 4);
}
TEST_CASE("Division generates correct fixpoint op") {
    auto a = Variable::create("a", 0, 100, 50);
    auto b = Variable::create("b", 0.25, 100, 50);

    auto result = a / b;

    std::vector<std::string> expected = {
        "wire[21:0] div_a_b;",
        "divider#(.LHS_INT_BITS(7), .LHS_FRAC_BITS(12), .RHS_INT_BITS(7), .RHS_FRAC_BITS(12), .OUT_INT_BITS(9), .OUT_FRAC_BITS(12)) div_a_b_mod(.clk(clk), .lhs_signed(a), .rhs_signed(b), .out_signed(div_a_b));"
    };

    REQUIRE(join(result->get_code(FixedFixpoint{12})) == join(expected));
}

TEST_CASE("C++ code gen works for add") {
    auto a = Variable::create("a", 0, 100, 50);
    auto b = Variable::create("b", 0, 100, 50);

    auto result = a + b;

    std::vector<std::string> expected_cpp = {
        "Fp<12> add_a_b = (a + b).cast<12>();"
    };
    REQUIRE(join(result->get_cpp(FixedFixpoint{12}, CppDataType::Fp)) == join(expected_cpp));
}
TEST_CASE("C++ code gen works for sub") {
    auto a = Variable::create("a", 0, 100, 50);
    auto b = Variable::create("b", 0, 100, 50);

    auto result = a - b;

    std::vector<std::string> expected_cpp = {
        "Fp<12> sub_a_b = (a - b).cast<12>();"
    };
    REQUIRE(join(result->get_cpp(FixedFixpoint{12}, CppDataType::Fp)) == join(expected_cpp));
}
TEST_CASE("C++ code gen works for mul") {
    auto a = Variable::create("a", 0, 100, 50);
    auto b = Variable::create("b", 0, 100, 50);

    auto result = a * b;

    std::vector<std::string> expected_cpp = {
        "Fp<12> mul_a_b = (a * b).cast<12>();"
    };
    REQUIRE(join(result->get_cpp(FixedFixpoint{12}, CppDataType::Fp)) == join(expected_cpp));
}
TEST_CASE("C++ code gen works for div") {
    auto a = Variable::create("a", 0, 100, 50);
    auto b = Variable::create("b", 0, 100, 50);

    auto result = a / b;

    std::vector<std::string> expected_cpp = {
        "Fp<12> div_a_b = (a / b).cast<12>();"
    };
    REQUIRE(join(result->get_cpp(FixedFixpoint{12}, CppDataType::Fp)) == join(expected_cpp));
}

TEST_CASE( "Input (partially) works", "[codegen]") {
    auto a = Input::create("a", 0, 100, 100);

    REQUIRE(a->calculate_value() == ExecutionType(100));
    REQUIRE(a->unique_ancestors() == std::set<Weak<Node>>{});

    // Inputs only have code in the function head in C++
    std::vector<std::string> expected_cpp = {};
    REQUIRE(join(a->get_cpp(FixedFixpoint{12}, CppDataType::Fp)) == join(expected_cpp));
}

TEST_CASE( "Abs works", "[codegen]" ) {
    WordLength wl = FixedFixpoint{12};
    std::vector<std::string> expected = {
        "reg[19:0] abs_var;",
        "always @(posedge clk) begin",
        "\tif($signed(var) < 0) begin",
        "\t\tabs_var <= -var;",
        "\tend",
        "\telse begin",
        "\t\tabs_var <= var;",
        "\tend",
        "end"
    };

    auto var = Variable::create("var", -100, 50, 0);
    auto result = Abs::create(var);

    assign_delay_slots({result}, 0, wl);
    REQUIRE(join(result->get_code(wl)) == join(expected));
    REQUIRE(result->min() == 0);
    REQUIRE(result->max() == 100);
    REQUIRE(result->graph_label_name() == "abs");
    REQUIRE(result->unique_ancestors() == std::set<Weak<Node>>{var.weak()});

    std::vector<std::string> expected_cpp = {
        "Fp<12> abs_var = var.abs();"
    };
    REQUIRE(join(result->get_cpp(wl, CppDataType::Fp)) == join(expected_cpp));
}
TEST_CASE( "Sqrt works", "[codegen]" ) {
    auto a = Variable::create("a", 0, 100, 4);

    auto result = Sqrt::create(a);

    REQUIRE(result->min() == 0);
    REQUIRE(result->max() == 10);
    REQUIRE(result->calculate_value() == 2);
    REQUIRE(compare_rcs(result->get_parents(), {a}));
    REQUIRE(result->base_name() == "sqrt_a");
    REQUIRE(result->graph_label_name() == "sqrt");
    REQUIRE(result->unique_ancestors() == std::set<Weak<Node>>{a.weak()});

    std::vector<std::string> expected_code = {
        "wire[16:0] sqrt_a;",
        "wire[15:0] sqrt_a_unsigned;",
        "sqrt_20_12_12 sqrt_a_mod(clk, a, sqrt_a_unsigned);",
        "assign sqrt_a = {1'b0, sqrt_a_unsigned};"
    };
    REQUIRE(join(result->get_code(FixedFixpoint{12})) == join(expected_code));

    std::vector<std::string> expected_cpp = {
        "Fp<14> sqrt_a = approx_sqrt<14>(a);"
    };
    REQUIRE(join(result->get_cpp(FixedFixpoint{14}, CppDataType::Fp)) == join(expected_cpp));

}
TEST_CASE( "Truncate works", "[codegen]" ) {
    auto a = Variable::create("a", -50.3, 100.7, 5.5);
    auto result = Truncate::create(a);

    REQUIRE(result->min() == -50);
    REQUIRE(result->max() == 100);
    REQUIRE(compare_rcs(result->get_parents(), {a}));
    REQUIRE(result->graph_label_name() == "truncate");
    REQUIRE(result->calculate_value() == 5);
    REQUIRE(result->base_name() == "truncate_a");
    REQUIRE(result->unique_ancestors() == std::set<Weak<Node>>{a.weak()});

    std::vector<std::string> expected = {
        "reg[17:0] truncate_a;",
        "always @(posedge clk) begin",
        "\ttruncate_a <= {a[17:10], 10'b0};",
        "end"
    };
    REQUIRE(join(result->get_code(FixedFixpoint{10})) == join(expected));

    std::vector<std::string> expected_cpp = {
        "Fp<12> truncate_a = a.truncate();"
    };
    REQUIRE(join(result->get_cpp(FixedFixpoint{12}, CppDataType::Fp)) == join(expected_cpp));
}



TEST_CASE( "Less than works", "[codegen]" ) {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 0, 100, 25);
    auto b = Variable::create("b", 0, 100, 50);

    auto result = a < b;

    // Value
    REQUIRE(result->calculate_value() == true);
    // TODO: Codegen
    std::vector<std::string> expected = {
        "reg lt_a_b;",
        "always @(posedge clk) begin",
        "\tlt_a_b <= $signed(a)<$signed(b);",
        "end"
    };

    assign_delay_slots({result}, 0, wl);
    REQUIRE(join(result->get_code(wl)) == join(expected));
    REQUIRE(result->graph_label_name() == "<");
    REQUIRE(result->unique_ancestors() == std::set<Weak<Node>>{a.weak(), b.weak()});

    std::vector<std::string> expected_cpp = {
        "bool lt_a_b = a < b;"
    };
    REQUIRE(join(result->get_cpp(wl, CppDataType::Fp)) == join(expected_cpp));
}
TEST_CASE( "Less than works for inverted order", "[codegen]" ) {
    auto a = Variable::create("a", 0, 100, 25);
    auto b = Variable::create("b", 0, 100, 50);

    auto result = b < a;

    // Value
    REQUIRE(result->calculate_value() == false);
}
TEST_CASE( "Greater than works", "[codegen]" ) {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 0, 100, 25);
    auto b = Variable::create("b", 0, 100, 50);

    auto result = a > b;

    // Value
    REQUIRE(result->calculate_value() == false);
    // TODO: Codegen
    std::vector<std::string> expected = {
        "reg gt_a_b;",
        "always @(posedge clk) begin",
        "\tgt_a_b <= $signed(a)>$signed(b);",
        "end"
    };

    assign_delay_slots({result}, 0, wl);
    REQUIRE(join(result->get_code(wl)) == join(expected));
    REQUIRE(result->graph_label_name() == ">");

    REQUIRE(result->unique_ancestors() == std::set<Weak<Node>>{a.weak(), b.weak()});

    std::vector<std::string> expected_cpp = {
        "bool gt_a_b = a > b;"
    };
    REQUIRE(join(result->get_cpp(wl, CppDataType::Fp)) == join(expected_cpp));
}
TEST_CASE( "LEq works", "[codegen]" ) {
    WordLength wl = FixedFixpoint{12};
    auto a = Constant::create("a", 4);
    auto b = Constant::create("b", 6);
    auto c = Constant::create("c", 7);

    REQUIRE((a <= b)->calculate_value());
    REQUIRE((b <= b)->calculate_value());
    REQUIRE_FALSE((c <= b)->calculate_value());

    auto result = a <= b;
    REQUIRE(result->base_name() == "leq_a_b");

    std::vector<std::string> expected_code = {
        "reg leq_a_b;",
        "always @(posedge clk) begin",
        "\tleq_a_b <= $signed(a)<=$signed(b);",
        "end"
    };
    REQUIRE(result->get_code(wl) == expected_code);
    REQUIRE(result->graph_label_name() == "<=");
    REQUIRE(result->unique_ancestors() == std::set<Weak<Node>>{a.weak(), b.weak()});

    std::vector<std::string> expected_cpp = {
        "bool leq_a_b = a <= b;"
    };
    REQUIRE(join(result->get_cpp(wl, CppDataType::Fp)) == join(expected_cpp));
}
TEST_CASE( "GEq works", "[codegen]" ) {
    WordLength wl = FixedFixpoint{12};
    auto a = Constant::create("a", 4);
    auto b = Constant::create("b", 5);
    auto c = Constant::create("c", 6);

    REQUIRE_FALSE((a >= b)->calculate_value());
    REQUIRE((b >= b)->calculate_value());
    REQUIRE((c >= b)->calculate_value());

    auto result = a >= b;
    REQUIRE(result->base_name() == "geq_a_b");

    std::vector<std::string> expected_code = {
        "reg geq_a_b;",
        "always @(posedge clk) begin",
        "\tgeq_a_b <= $signed(a)>=$signed(b);",
        "end"
    };
    REQUIRE(result->get_code(wl) == expected_code);
    REQUIRE(result->graph_label_name() == ">=");
    REQUIRE(result->unique_ancestors() == std::set<Weak<Node>>{a.weak(), b.weak()});

    std::vector<std::string> expected_cpp = {
        "bool geq_a_b = a >= b;"
    };
    REQUIRE(join(result->get_cpp(wl, CppDataType::Fp)) == join(expected_cpp));
}
TEST_CASE( "Eq works", "[codegen]" ) {
    WordLength wl = FixedFixpoint{12};
    auto a = Constant::create("a", 2);
    auto b = Constant::create("b", 3);

    REQUIRE((a == a)->calculate_value());
    REQUIRE_FALSE((a == b)->calculate_value());

    auto result = a == b;
    REQUIRE(result->base_name() == "eq_a_b");

    std::vector<std::string> expected_code = {
        "reg eq_a_b;",
        "always @(posedge clk) begin",
        "\teq_a_b <= $signed(a)==$signed(b);",
        "end"
    };
    REQUIRE(result->get_code(wl) == expected_code);
    REQUIRE(result->graph_label_name() == "==");
    REQUIRE(result->unique_ancestors() == std::set<Weak<Node>>{a.weak(), b.weak()});

    std::vector<std::string> expected_cpp = {
        "bool eq_a_b = a == b;"
    };
    REQUIRE(join(result->get_cpp(wl, CppDataType::Fp)) == join(expected_cpp));
}
TEST_CASE( "NEq works", "[codegen]" ) {
    WordLength wl = FixedFixpoint{12};
    auto a = Constant::create("a", 2);
    auto b = Constant::create("b", 3);

    REQUIRE_FALSE((a != a)->calculate_value());
    REQUIRE((a != b)->calculate_value());

    auto result = a != b;
    REQUIRE(result->base_name() == "neq_a_b");

    std::vector<std::string> expected_code = {
        "reg neq_a_b;",
        "always @(posedge clk) begin",
        "\tneq_a_b <= $signed(a)!=$signed(b);",
        "end"
    };
    REQUIRE(result->get_code(wl) == expected_code);
    REQUIRE(result->graph_label_name() == "!=");
    REQUIRE(result->unique_ancestors() == std::set<Weak<Node>>{a.weak(), b.weak()});

    std::vector<std::string> expected_cpp = {
        "bool neq_a_b = a != b;"
    };
    REQUIRE(join(result->get_cpp(wl, CppDataType::Fp)) == join(expected_cpp));
}

TEST_CASE( "Lookup tables work", "[luts]") {
    WordLength wl = FixedFixpoint{12};
    auto lut = LookupTable::create("test_lut", {1,2,3});

    auto index = Constant::create("index", 1);
    auto result = LutResult::create(lut, index);

    std::vector<std::string> expected_code = {
        "wire[14:0] lut_test_lut_index;",
        "test_lut12#(.output_size(15)) lut_test_lut_index_lut(.clk(clk), .addr(index[13:12]), .result(lut_test_lut_index));"
    };

    std::vector<Rc<Node>> expected_children = {index};

    REQUIRE(result->base_name() == "lut_test_lut_index");
    REQUIRE(result->min() == 1);
    REQUIRE(result->max() == 3);
    REQUIRE(result->calculate_value() == 2);
    REQUIRE(compare_rcs(result->get_parents(), expected_children));
    REQUIRE(join(result->get_code(wl)) == join(expected_code));
    REQUIRE(result->graph_label_name() == "lut");
    REQUIRE(result->unique_ancestors() == std::set<Weak<Node>>{index.weak()});

    std::vector<std::string> expected_cpp = {
        "Fp<12> lut_test_lut_index = test_lut12_lut(index.as_int());"
    };
    REQUIRE(join(result->get_cpp(wl, CppDataType::Fp)) == join(expected_cpp));
}


TEST_CASE( "If-statements work", "[codegen]" ) {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 25, 100, 60);
    auto b = Variable::create("b", 50, 110, 75);
    auto condition = a < b;
    auto result = _if(condition, a, b);

    std::vector<std::string> expected_code = {
        "reg[19:0] if_lt_a_b_then_a_else_b;",
        "always @(posedge clk) begin",
        "\tif(lt_a_b) begin",
        "\t\tif_lt_a_b_then_a_else_b <= a_pl1;",
        "\tend",
        "\telse begin",
        "\t\tif_lt_a_b_then_a_else_b <= b_pl1;",
        "\tend",
        "end"
    };

    REQUIRE(result->min() == 25);
    REQUIRE(result->max() == 110);


    // These assertions were added because something caused if-statements
    // to break when ExecutionType used more than 25 fractional bits
    REQUIRE(a->calculate_value() == ExecutionType(60));
    REQUIRE(b->calculate_value() == ExecutionType(75));
    REQUIRE(condition->calculate_value() == true);

    REQUIRE(result->calculate_value() == ExecutionType(60));

    REQUIRE(result->base_name() == "if_lt_a_b_then_a_else_b");
    REQUIRE(join(result->get_code(wl)) == join(expected_code));
    REQUIRE(compare_rcs(result->get_parents(), std::vector<Rc<Node>>{condition, a, b}));
    REQUIRE(result->graph_label_name() == "if");

    result->_override_min(123);
    result->_override_max(225);
    REQUIRE(result->min() == 123);
    REQUIRE(result->max() == 225);
    REQUIRE(result->unique_ancestors() == std::set<Weak<Node>>{a.weak(), b.weak(), condition.weak()});

    std::vector<std::string> expected_cpp = {
        "Fp<12> if_lt_a_b_then_a_else_b;",
        "if(lt_a_b) {",
        "\tif_lt_a_b_then_a_else_b = a.cast<12>();",
        "}",
        "else {",
        "\tif_lt_a_b_then_a_else_b = b.cast<12>();",
        "}"
    };
    REQUIRE(join(result->get_cpp(wl, CppDataType::Fp)) == join(expected_cpp));
}
TEST_CASE( "If-statements work for false branches", "[codegen]" ) {
    auto a = Variable::create("a", 25, 100, 75);
    auto b = Variable::create("b", 50, 150, 60);
    auto condition = a < b;
    auto result = _if(condition, a, b);

    REQUIRE(result->calculate_value() == 60);
}
TEST_CASE( "If-statements perform sign extension when output sizes are different") {
    WordLength wl = FixedFixpoint{12};
    auto a = Constant::create("a", 1);
    auto b = Constant::create("b", 100);
    auto result = _if(a < b, a, b);

    std::vector<std::string> expected = {
        "reg[19:0] if_lt_a_b_then_a_else_b;",
        "always @(posedge clk) begin",
        "\tif(lt_a_b) begin",
        "\t\tif_lt_a_b_then_a_else_b <= {{6{a[13]}}, a};",
        "\tend",
        "\telse begin",
        "\t\tif_lt_a_b_then_a_else_b <= b;",
        "\tend",
        "end"
    };

    REQUIRE(join(expected) == join(result->get_code(wl)));
}
TEST_CASE( "Boolean if statements generate correct C++") {
    WordLength wl = FixedFixpoint{12};
    auto a = BoolConst::True;
    auto b = BoolConst::False;
    auto c = BoolConst::False;
    auto result = _if(a, b, c);

    std::vector<std::string> expected_cpp = {
        "bool if_true_then_false_else_false;",
        "if(true) {",
        "\tif_true_then_false_else_false = false;",
        "}",
        "else {",
        "\tif_true_then_false_else_false = false;",
        "}"
    };
    REQUIRE(join(result->get_cpp(wl, CppDataType::Fp)) == join(expected_cpp));
}

TEST_CASE( "Fractional Select nodes work", "[codegen]" ) {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 25, 100, 60);
    auto b = Variable::create("b", 50, 110, 75);
    auto c = Variable::create("c", 50, 110, 75);
    auto x = a < b;
    x->override_name("x");
    auto y = a > b;
    y->override_name("y");
    auto nx = !x;
    auto ny = !x;
    auto z = ny || nx;
    z->override_name("z");

    std::vector<std::string> expected_code = {
        "reg[19:0] select_x_a_x_y_b_z_c;",
        "always @(posedge clk) begin",
        "\tif(x) begin",
        "\t\tselect_x_a_x_y_b_z_c <= a_pl1;",
        "\tend",
        "\telse if(x && y) begin",
        "\t\tselect_x_a_x_y_b_z_c <= b_pl1;",
        "\tend",
        "\telse if(z) begin",
        "\t\tselect_x_a_x_y_b_z_c <= c_pl1;",
        "\tend",
        "end"
    };

    auto result = SelectFrac::create({{a, {x}}, {b, {x, y}}, {c, {z}}});

    REQUIRE(result->min() == 25);
    REQUIRE(result->max() == 110);


    REQUIRE(result->calculate_value() == ExecutionType(60));

    REQUIRE(result->base_name() == "select_x_a_x_y_b_z_c");
    REQUIRE(join(result->get_code(wl)) == join(expected_code));
    REQUIRE(compare_rcs(
        result->get_parents(),
        std::vector<Rc<Node>>{a, x, b, x, y, c, z}
    ));
    REQUIRE(result->graph_label_name() == "select");

    result->_override_min(123);
    result->_override_max(225);
    REQUIRE(result->min() == 123);
    REQUIRE(result->max() == 225);
    auto ancestors = result->unique_ancestors();
    REQUIRE(ancestors == std::set<Weak<Node>>{
        a.weak(),
        b.weak(),
        c.weak(),
        x.weak(),
        y.weak(),
        z.weak(),
        nx.weak(),
        ny.weak(),
    });

    std::vector<std::string> expected_cpp = {
        "Fp<12> select_x_a_x_y_b_z_c;",
        "if(x) {",
        "\tselect_x_a_x_y_b_z_c = a.cast<12>();",
        "}",
        "else if(x && y) {",
        "\tselect_x_a_x_y_b_z_c = b.cast<12>();",
        "}",
        "else if(z) {",
        "\tselect_x_a_x_y_b_z_c = c.cast<12>();",
        "}",
    };
    REQUIRE(join(result->get_cpp(wl, CppDataType::Fp)) == join(expected_cpp));
}
TEST_CASE( "Bool Select nodes work", "[codegen]" ) {
    WordLength wl = FixedFixpoint{12};
    auto a = BoolConst::True;
    auto b = BoolConst::False;
    auto x = BoolConst::False;

    auto result = SelectBool::create({{a, {a}}, {b, {b}}});

    std::vector<std::string> expected_cpp = {
        "bool select_true_true_false_false;",
        "if(true) {",
        "\tselect_true_true_false_false = true;",
        "}",
        "else if(false) {",
        "\tselect_true_true_false_false = false;",
        "}",
    };
    REQUIRE(join(result->get_cpp(wl, CppDataType::Fp)) == join(expected_cpp));
}

TEST_CASE("Selects with an empty condition generates else") {
    WordLength wl = FixedFixpoint{12};
    auto a = Variable::create("a", 25, 100, 60);
    auto b = Variable::create("b", 50, 110, 75);
    auto c = Variable::create("c", 50, 110, 75);
    auto x = a < b;
    x->override_name("x");
    auto y = a > b;
    y->override_name("y");
    auto nx = !x;
    auto ny = !x;
    auto z = ny || nx;
    z->override_name("z");

    std::vector<std::string> expected_code = {
        "reg[19:0] result;",
        "always @(posedge clk) begin",
        "\tif(x) begin",
        "\t\tresult <= a_pl1;",
        "\tend",
        "\telse begin",
        "\t\tresult <= c_pl1;",
        "\tend",
        "end"
    };

    auto result = SelectFrac::create({{a, {x}}, {c, {}}});
    result->override_name("result");

    REQUIRE(join(result->get_code(wl)) == join(expected_code));

    std::vector<std::string> expected_cpp = {
        "Fp<12> result;",
        "if(x) {",
        "\tresult = a.cast<12>();",
        "}",
        "else {",
        "\tresult = c.cast<12>();",
        "}",
    };
    REQUIRE(join(result->get_cpp(wl, CppDataType::Fp)) == join(expected_cpp));
}

TEST_CASE( "Boolean constants work", "[codegen]" ) {
    WordLength wl = FixedFixpoint{12};
    auto var = BoolConst::True;

    std::vector<std::string> expected_code = {"localparam true = 1;"};

    REQUIRE(var->calculate_value() == true);
    REQUIRE(var->base_name() == "true");
    REQUIRE(var->pipeline_buffer_name(2) == "true");
    REQUIRE(var->generate_pipeline_code(wl) == std::vector<std::string>{});
    REQUIRE(var->get_code(wl) == expected_code);

    var = BoolConst::False;
    expected_code = {"localparam false = 0;"};

    REQUIRE(var->calculate_value() == false);
    REQUIRE(var->base_name() == "false");
    REQUIRE(var->pipeline_buffer_name(3) == "false");
    REQUIRE(var->get_code(wl) == expected_code);
    REQUIRE(var->graph_label_name() == "boolconst false");
    REQUIRE(var->unique_ancestors() == std::set<Weak<Node>>{});

    // Boolconsts names are keywords
    std::vector<std::string> expected_cpp = {};
    REQUIRE(join(var->get_cpp(wl, CppDataType::Fp)) == join(expected_cpp));
}

TEST_CASE( "Or works", "[codegen]" ) {
    WordLength wl = FixedFixpoint{12};
    auto result = BoolConst::True || BoolConst::False;
    auto false_result = BoolConst::False || BoolConst::False;

    // Value
    REQUIRE(result->calculate_value() == true);
    REQUIRE(false_result->calculate_value() == false);
    std::vector<std::string> expected = {
        "wire or_true_false = true | false;"
    };
    REQUIRE(join(result->get_code(wl)) == join(expected));
    REQUIRE(result->graph_label_name() == "|");
    REQUIRE(result->unique_ancestors() == std::set<Weak<Node>>{
        BoolConst::True.weak(),
        BoolConst::False.weak()
    });
    REQUIRE(result->computation_time(wl) == 0);


    std::vector<std::string> expected_cpp = {
        "bool or_true_false = true || false;"
    };
    REQUIRE(join(result->get_cpp(wl, CppDataType::Fp)) == join(expected_cpp));
}

TEST_CASE( "Not works", "[codegen]") {
    WordLength wl = FixedFixpoint{12};
    auto t = BoolConst::True;
    auto f = BoolConst::False;
    auto result = !t;

    std::vector<std::string> expected_true = {
        "wire not_true = !true;"
    };
    REQUIRE(result->base_name() == "not_true");
    REQUIRE(result->get_code(wl) == expected_true);
    REQUIRE(result->calculate_value() == false);
    REQUIRE(compare_rcs(result->get_parents(), std::vector<Rc<Node>>{t}));

    result = !f;
    std::vector<std::string> expected_false = {
        "wire not_false = !false;"
    };
    REQUIRE(result->get_code(wl) == expected_false);
    REQUIRE(result->calculate_value() == true);
    REQUIRE(compare_rcs(result->get_parents(), std::vector<Rc<Node>>{f}));
    REQUIRE(result->graph_label_name() == "!");
    REQUIRE(result->unique_ancestors() == std::set<Weak<Node>>{f.weak()});

    std::vector<std::string> expected_cpp = {
        "bool not_false = !false;"
    };
    REQUIRE(join(result->get_cpp(wl, CppDataType::Fp)) == join(expected_cpp));

    REQUIRE(result->computation_time(wl) == 0);
}

TEST_CASE( "USub works", "[codegen]" ) {
    auto a = Variable::create("a", -50, 100, 10);
    auto result = -a;

    std::vector<std::string> expected = {
        "reg[19:0] usub_a;",
        "always @(posedge clk) begin",
        "\tusub_a <= -a;",
        "end"
    };

    REQUIRE(compare_rcs(result->get_parents(), {a}));
    REQUIRE(result->get_code(FixedFixpoint{12}) == expected);
    REQUIRE(result->min() == -100);
    REQUIRE(result->max() == 50);
    REQUIRE(result->calculate_value() == -10);
    REQUIRE(result->graph_label_name() == "-");

    a->set_value(-10);
    a->mark_dirty();
    result->mark_dirty();
    REQUIRE(result->calculate_value() == 10);
    REQUIRE(result->unique_ancestors() == std::set<Weak<Node>>{a.weak()});

    std::vector<std::string> expected_cpp = {
        "Fp<12> usub_a = -a;"
    };
    REQUIRE(join(result->get_cpp(FixedFixpoint{12}, CppDataType::Fp)) == join(expected_cpp));
}

TEST_CASE( "Opaque nodes work as expected" ) {
    auto a = Variable::create("a", -50, 100, 10);
    auto result = OpaqueNode::create(a);

    REQUIRE(compare_rcs(result->get_parents(), {}));
    REQUIRE(result->min() == -50);
    REQUIRE(result->max() == 100);
    REQUIRE(result->graph_label_name() == "Opaque");
}

TEST_CASE( "Merged booleans work" ) {
    // Constructed from this graph
    //                 node
    //                  / \
    //                 a   b    +
    //                /   / \   | inner nodes
    //               /   c   d  +
    //               |   |   |
    //               x   y   z <- Frac nodes

    // Since we don't have a unary boolean from fractional node operation, we'll
    // just fake it with bool consts. For these tests, it makes no difference
    auto a = BoolConst::True;
    auto c = BoolConst::True;
    auto d = BoolConst::True;
    auto b = c && d;

    auto x = Variable::create("x", 0, 10, 5);
    auto y = Variable::create("y", 0, 10, 5);
    auto z = Variable::create("z", 0, 10, 5);

    auto node = MergedBool::create(
        {a, b, c, d},
        {x, y, z}
    );

    REQUIRE(compare_rcs(node->get_parents(), {x, y, z}));
    REQUIRE(node->graph_label_name() == "4 merged bools");
}
