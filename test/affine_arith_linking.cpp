#include <catch2/catch.hpp>

#include <affine_arith.hpp>

TEST_CASE("Affine arithmetic library works") {
    auto symbols = aa::new_symbols();
    auto x = aa::from_bounds(5, 15, *symbols);

    auto bounds = aa::to_bounds(*x);
    REQUIRE(bounds.min == 5);
    REQUIRE(bounds.max == 15);
}
